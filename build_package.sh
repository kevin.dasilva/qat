#!/bin/bash

mkdir -p client/qat/bin/plugins/
mkdir -p client/qat/templates/
cp build/libinjector.so client/qat/bin
cp build/libinjector.dylib client/qat/bin
cp build/injector.dll client/qat/bin
cp build/injector.exe client/qat/bin
cp build/libQatServer*.so client/qat/bin
cp build/libQatServer*.dylib client/qat/bin
cp build/QatServer*.dll client/qat/bin
cp -R build/plugins/* client/qat/bin/plugins
cp -R templates/* client/qat/templates
rm -rf ./pypackage
python3 -m build . -o pypackage
