# Change Log

## 1.1.2

### API

#### *Fixed*
- Focus issue when using *qat.type_in()* function ([issue #22](https://gitlab.com/testing-tool/qat/-/issues/22)).

## 1.1.1

### GUI

#### *Fixed:*
- User folder for Qat preferences is created if it does not exist.

## 1.1.0

### GUI
- GUI was completely rewritten and is now based on [CustomTkinter](https://customtkinter.tomschimansky.com/):
    - Themes are supported (system / light / dark),
    - More intuitive flow with Save/Cancel buttons,
    - Available methods are displayed in the property table ([issue #13](https://gitlab.com/testing-tool/qat/-/issues/13)),
    - Faster navigation in the Spy object tree and property table.

### Python API
#### *Changed:*
- All functions using an object _definition_ now accept a simple string that will be interpreted as an _objectName_.

#### *Added:*
- New method _list_methods()_ has been added to the QtObject class. Similar to the _list_properties()_ method, it returns a list of existing methods for an object.
- New classes to represent common Qt types, allowing to create instances of QPoint, QFont, QColor, QRect, QSize, etc. These instances can be used as properties and method arguments.
  ```python
  from qat.qt_types import *
  color = QColor("yellow", alpha=160)
  font = QFont("Times", pixelSize=16, bold=True, italic=False)
  rectangle = QRect(QPoint(10, 10), QSize(150, 50))
  ```
- New API function _qat.detach()_ allowing an application to keep running after a script terminates (see [issue #20](https://gitlab.com/testing-tool/qat/-/issues/20))

#### *Fixed:*
- Scripts hang if the application is not closed ([issue #20](https://gitlab.com/testing-tool/qat/-/issues/20)). Started applications are now automatically closed when a script terminates.