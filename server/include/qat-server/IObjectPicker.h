// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QObject>

namespace Qat
{
/// \brief Interface for the object picker
class IObjectPicker : public QObject
{
   Q_OBJECT

public:

   /// Constructor
   /// \param[in] parent The Qt parent
   explicit IObjectPicker(QObject* parent) : QObject(parent){}

   /// Default destructor
   ~IObjectPicker() override = default;

   /// Remove the current picked object
   virtual void Reset() = 0;

   /// Activate or deactivate the picker
   /// \param[in] activate True to activate the picker, False to deactivate it
   virtual void SetActivated(bool activate) = 0;

   /// Pause picking to let user click on widgets
   virtual void Pause() = 0;

   /// Restore picking. Has no effect if picker has not been activated.
   virtual void Restore() = 0;
};

} // namespace Qat