// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/BaseCommandExecutor.h>
#include <nlohmann/json.hpp>

namespace Qat
{
/// \brief Class responsible to execute gestures commands
class GestureCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit GestureCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~GestureCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;
};

} // namespace Qat