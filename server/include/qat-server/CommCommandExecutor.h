// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/BaseCommandExecutor.h>

#include <QPointer>

#include <nlohmann/json.hpp>

#include <string>

namespace Qat
{

/// Forward declarations
/// @{
class RequestHandler;
class SignalListener;
/// @}

/// \brief Class responsible to execute actions related to communication (initialization,
/// connection and disconnection)
class CommCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   /// \param[in] requestHandler The parent request handler for the current client
   CommCommandExecutor(
      const nlohmann::json& request,
      RequestHandler* requestHandler);

   /// Destructor
   ~CommCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;

private:
   /// Pointer to the parent request handler for the current client
   QPointer<RequestHandler> mRequestHandler;

   /// Cache of SignalListeners associated to a unique identifier
   static std::map<std::string, QPointer<SignalListener>> mListenerCache;
};

} // namespace Qat