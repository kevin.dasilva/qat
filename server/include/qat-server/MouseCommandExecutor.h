// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/BaseCommandExecutor.h>
#include <nlohmann/json.hpp>

namespace Qat
{
/// \brief Class responsible to execute mouse action commands.
/// It handles various mouse events :
///   - left/right/middle click
///   - double click
///   - mouse button press/release
///   - scrolling
///   - drag/move
class MouseCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit MouseCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~MouseCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;
};

} // namespace Qat