// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QObjectList>

#include <nlohmann/json.hpp>

#include <string>
#include <vector>

/// Forward declarations
/// @{
class QObject;
class QString;
/// @}

namespace Qat::ObjectLocator
{

/// Find objects in the given container.
/// Object must have the given property values.
/// \param[in] container The Qt parent containing the searched object
/// \param[in] definition Properties to be matched
/// \param[in] parentObject Optional parent object
/// \param[in] allMatches If True, returns all matched objects, otherwise stop 
///            searching when multiple objects are found. Default is True
/// \return A list of objects matching the given properties. May be empty if object
/// was not found and may contain multiple objects if given parameters do not provide 
/// unique identification.
std::vector<QObject*> FindObjects(
   const QObject* container,
   const nlohmann::json& definition,
   const QObject* parentObject,
   bool allMatches = true);

/// Determines whether the given object matches the definition or not
/// \param[in] object A Qt object
/// \param[in] definition Properties to be matched
/// \param[in] parentObject Optional parent object
/// \return True if object matches the definition, False otherwise
bool ObjectMatches(
   QObject* object,
   const nlohmann::json& definition,
   const QObject* parentObject);

/// Find children of any type of the given container matching the given name.
/// This includes QObjects, Items and Qt3D Nodes.
/// \param[in] container The container of the search object, must be a Scene3D instance
/// \param[in] objectName The name of the search object. If empty, all child nodes will be returned.
/// \param[in] recursive If True, collect all children recursively. 
///            Otherwise collect direct children only. Default is True.
/// \return The list of child items matching the given name if provided
QObjectList CollectAllChildren(
   const QObject* container,
   const std::string& objectName,
   bool recursive=true);

/// Format the Qt type into a more readable one by removing QML suffix and QtQuick prefix
/// \param[in] className The original class name
/// \return The formatted type name
[[nodiscard]] std::string FormatType(const std::string& className);

/// Return the type of the given object. The type is the associated class name
/// unless the given object provides a "type" property.
/// \param object Any Qt object
/// \return The formatted type of the given object
[[nodiscard]] std::string GetObjectType(const QObject* object);

/// Return whether the given object has children or not.
/// Intended to be used as an optimization when building object tree in GUI.
/// \param object Any Qt object
/// \return True if the given object has children, False otherwise.
[[nodiscard]] bool HasChildren(const QObject* object);

} // namespace Qat::ObjectLocator