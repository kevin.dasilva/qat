// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QObject>

#include <functional>

/// Forward declarations
class QTcpServer;

namespace Qat
{
/// \brief Class responsible for handling Qat server
class Server : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY_MOVE(Server)

public:
   /// Default constructor
   Server();

   /// Destructor
   ~Server() override;

   /// Factory to create a Server in Qt thread
   /// \param[in] onCreated callback to be called when Server has been created
   static void Create(std::function<void(const Server*)> onCreated);

   /// Return the port used by the server
   /// \return the port used by the server
   int GetPort() const noexcept;

signals:
   /// Emitted when the server is running
   void isRunning();

private slots:

   /// Start the server
   void Start();

   /// Start serving a client.
   /// Intended to be called upon new connections.
   void ServeClient();

private:
   /// Stop the server
   void Stop();

   /// Pointer to the TCP server
   QTcpServer* mServer{ nullptr };

   /// Port used by this server
   int mPort{-1};
};

} // namespace Qat