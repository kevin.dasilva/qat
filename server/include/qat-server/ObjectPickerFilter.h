// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QObject>

namespace Qat
{

/// @brief Filter the CTRL key events to pause and restore picking
class ObjectPickerFilter : public QObject
{
   Q_OBJECT

public:

   /// Constructor
   /// \param[in] parent the Qt parent of this filter
   explicit ObjectPickerFilter(QObject* parent) : QObject(parent){}

   /// Default destructor
   ~ObjectPickerFilter() override = default;

   /// \copydoc QObject::eventFilter
   bool eventFilter(QObject* object, QEvent* event) override;
};

} // namespace Qat