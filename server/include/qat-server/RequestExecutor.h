// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QPointer>

#include <nlohmann/json.hpp>

/// Forward declarations
class QObject;

namespace Qat
{
/// Forward declarations
class RequestHandler;

/// \brief Class responsible for executing JSON requests
/// It executes the command defined by the interface of the command executor
class RequestExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   /// \param[in] requestHandler The parent request handler for the current client
   RequestExecutor(
      const nlohmann::json& request,
      RequestHandler* requestHandler);

   /// Destructor
   virtual ~RequestExecutor() = default;

   /// Run the request and return the response
   /// \return The response in JSON format
   nlohmann::json Run() const;

private:
   /// The request in JSON format
   nlohmann::json mRequest;

   /// Pointer to the parent request handler for the current client
   QPointer<RequestHandler> mRequestHandler;
};

} // namespace Qat