// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/BaseCommandExecutor.h>
#include <nlohmann/json.hpp>

namespace Qat
{
/// \brief Class responsible to execute listing commands
///  It can list multiple types of elements:
///   - properties
///   - top-level windows
///   - objects matching a definition
///   - the Qt version used
class ListCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit ListCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~ListCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;
};

} // namespace Qat