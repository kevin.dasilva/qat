// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QObject>
#include <QPointer>
#include <QTcpSocket>

namespace Qat
{
/// \brief Class responsible for Sending/Reading requests on a TCP socket to a remote host
class RequestHandler : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY_MOVE(RequestHandler)

public:
   /// Constructor
   /// \param[in] parent The Qt parent (for memory management)
   /// \param[in] socket The underlying TCP socket
   RequestHandler(
      QObject* parent,
      QTcpSocket* socket);

   /// Destructor
   ~RequestHandler() override;

   /// Open a socket and connect it to the given host
   /// \param[in] hostName The host name to connect to
   /// \param[in] port The socket port to use
   void ConnectToHost(
      const std::string& hostName,
      int port
   );

   /// Disconnect the socket from the host.
   /// Intended to be used by the Python API to close the communication during shutdown.
   void DisconnectFromHost();

   /// Send the given message to the current host
   /// \note Will throw if ConnectToHost() was never called
   /// \param[in] message The message to send
   void SendMessage(const std::string& message) const;

private slots:
   /// Called when client socket is connected
   void onConnectedToClient();
   
   /// Called when the socket receives data
   void OnReadyRead();

private:
   /// The underlying TCP socket receiving client request
   QPointer<QTcpSocket> mSocket{ nullptr };

   /// The host TCP socket sending callbacks
   QTcpSocket* mClientSocket{ nullptr };

   /// Store the length of the message being received
   int mCurrentMessageLength{ 0 };
};

} // namespace Qat