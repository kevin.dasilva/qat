// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QObject>

namespace Qat
{
/// \brief Interface for native widgets.
class INativeWidget : public QObject
{
   Q_OBJECT

public:

   /// Constructor
   /// \param[in] parent The Qt parent
   explicit INativeWidget(QObject* parent) : QObject(parent) {};

   /// Default destructor
   ~INativeWidget() override = default;

};

} // namespace Qat
