// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QObject>
#include <QPointer>
#include <QVariant>

#include <string>

namespace Qat
{
/// Forward declarations
class RequestHandler;

/// \brief Class responsible for propagation signals from Qt/C++ to the Python client
class SignalListener : public QObject
{
   Q_OBJECT

public:
   /// Constructor
   /// \param[in] requestHandler The parent request handler for the current client
   explicit SignalListener(RequestHandler* requestHandler);

   /// Destructor
   ~SignalListener() override = default;

   /// Attach the connected object and property.
   /// Not used when connecting signals.
   /// \param[in] object Associated object when connecting properties
   /// \param[in] property Connected property
   void AttachTo(QObject* object, std::string property);

   /// Notify observer that the value changed
   Q_INVOKABLE void Notify();

   /// Return the unique ID of this listener
   /// \return the unique ID of this listener
   std::string GetId() const noexcept;

private:
   /// Pointer to the parent request handler for the current client
   QPointer<RequestHandler> mRequestHandler;

   /// ID identifying the remote callback
   std::string mId;

   /// Associated object when connecting properties (null when using signals)
   QPointer<QObject> mObject{ nullptr };

   /// Connected property (empty when using signals)
   std::string mPropertyName;
};

} // namespace Qat