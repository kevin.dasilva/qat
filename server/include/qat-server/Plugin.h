// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <memory>
#include <vector>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #include <windows.h>
   #define FOR_WINDOWS
#elif defined(__linux__)
   #define FOR_LINUX
#elif defined(__APPLE__)
   #define FOR_MACOS
#else
#   error "Unsupported platform"
#endif

/// Forward declarations
/// @{
class QImage;
class QObject;
class QWindow;
/// @}

namespace Qat
{
/// Forward declarations
/// @{
class IObjectPicker;
class IWidget;
/// @}

/// \brief Class responsible for creating a Qat plugin
class Plugin final
{
public:
#if defined(FOR_LINUX) || defined(FOR_MACOS)
   using LibHandle = void *const;
#elif defined (FOR_WINDOWS)
   using LibHandle = HMODULE;
   typedef IWidget* (CALLBACK* CastFunctionType)(const QObject*); 
   typedef bool (CALLBACK* GetTopWindowsFunctionType)(QObject**, unsigned int*); 
   typedef QImage* (CALLBACK* GrabFunctionType)(QObject*); 
   typedef IObjectPicker* (CALLBACK* CreatePickerFunctionType)(QObject*); 
#endif

   /// Constructor
   /// \param[in] handle Opaque pointer to the plugin library
   explicit Plugin(LibHandle handle);

   /// Destructor
   ~Plugin() = default;

   /// Create a wrapper for the given QObject
   /// \param[in] qobject A QObject instance
   /// \return The wrapped QObject as a IWidget instance or nullptr if object 
   ///         is not supported by the plugin
   std::unique_ptr<IWidget> CastObject(const QObject* qobject) const;

   /// Get all top-level windows/widgets
   /// \note windowList must be allocated (and freed) by the caller.
   /// When calling this function with a count of 0, the function writes
   /// the number of windows to the count argument.
   /// \param[in,out] windowList Pointer to array of windows
   /// \param[in,out] count Size of windowList
   /// \return True upon success, False upon failure
   bool GetTopWindows(QObject** windowList, unsigned int* count) const;

   /// Take a screenshot of the given window
   /// \param window Any valid window
   /// \return The screenshot as a QImage or nullptr if it failed.
   std::unique_ptr<QImage> GrabImage(QObject* window) const;

   /// Create an Object Picker for the given window
   /// \param window Any valid window
   /// \return The object picker or nullptr if it failed.
   IObjectPicker* CreatePicker(QObject* window) const;

private:
   /// Opaque pointer to the plugin library 
   LibHandle mHandle;
   
#if defined(FOR_LINUX) || defined(FOR_MACOS)
   /// Pointer to the Cast function
   IWidget* (*mCastFunction)(const QObject*);

   /// Pointer to the GetTopWindows function
   bool (*mGetTopWindowsFunction)(QObject**, unsigned int*);

   /// Pointer to the Grab function
   QImage* (*mGrabFunction)(QObject*);

   /// Pointer to the CreatePicker function
   IObjectPicker* (*mCreatePickerFunction)(QObject*);
#elif defined (FOR_WINDOWS)
   /// Pointer to the Cast function
   CastFunctionType mCastFunction;

   /// Pointer to the GetTopWindows function
   GetTopWindowsFunctionType mGetTopWindowsFunction;

   /// Pointer to the Grab function
   GrabFunctionType mGrabFunction;

   /// Pointer to the CreatePicker function
   CreatePickerFunctionType mCreatePickerFunction;
#endif
};

}