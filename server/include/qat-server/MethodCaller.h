// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QVariant>

#include <string>

/// Forward declarations
/// @{
class QMetaMethod;
class QObject;
class QJsonArray;
/// @}

namespace Qat
{
/// \brief Class responsible to call a given method
class MethodCaller final
{
public:
   /// Default constructor
   MethodCaller() = default;

   /// Default destructor
   ~MethodCaller() = default;

   /// Call the given method with the given arguments
   /// \param[in] object The object owning the function
   /// \param[in] functionName The name of the function to call
   /// \param[in] argJsonArray The arguments as a JSON array
   /// \return True if the call succeeded, False otherwise
   /// \warning If False is returned, the value returned by GetResult()  will be invalid
   bool Call(
      QObject* object,
      const std::string& functionName,
      const QJsonArray& argJsonArray);

   /// Return the result of the command or an invalid value if the call was not performed
   /// \return The result of the command
   const QVariant& GetResult() const noexcept;

private:
   /// Build Qt-compatible arguments for the given method,
   /// based on the received json values.
   /// \param[in] method The method to be called
   /// \param[out] argumentList The returned argument list
   /// \param[in] argJsonArray The received arguments in JSON format
   /// \return True if arguments can be converted, False otherwise
   bool BuildArguments(
      const QMetaMethod& method,
      QVariantList& argumentList,
      const QJsonArray& argJsonArray) const;

   /// Call the meta-method on the given object with the given arguments.
   /// Note that the implementation depends on the Qt version (split at 6.7).
   /// \param object The target object
   /// \param[in] method The method to be called
   /// \param[in] argumentList The list of arguments as QVariants
   /// \return True if the call succeeded, False otherwise
   bool CallMethod(
      QObject* object,
      const QMetaMethod& method,
      QVariantList& argumentList) const;

   /// Result of the command once it has run
   QVariant mResult;
};

} // namespace Qat::ObjectLocator