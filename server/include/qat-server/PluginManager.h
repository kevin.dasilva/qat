// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/Plugin.h>

#include <filesystem>
#include <map>
#include <memory>
#include <string>

namespace Qat
{
/// \brief Class responsible for managing Qat plugins
class PluginManager final
{
public:
   /// Global instance of PluginManager
   /// \return The global instance
   static const PluginManager& GetInstance();

   /// Constructor
   /// \param[in] root Folder containing the plugins. 
   ///                 Must be relative to the Qat server library location.
   explicit PluginManager(const std::filesystem::path& root);

   /// Default destructor
   ~PluginManager() = default;

   /// Accessor to the list of available plugins
   /// \return The list of available plugins
   const std::map<std::string, std::unique_ptr<Plugin>>&
      GetPlugins() const;
private:
   /// Get the path to the currently loaded library 
   /// \return the path to the currently loaded library
   std::filesystem::path GetLibraryPath() const;

   /// Load compatible plugin libraries
   /// \param[in] librarySuffix Suffix of compatible libraries, depends on Qt version.
   void LoadPlugins(const std::string& librarySuffix);

   /// Path to the root folder containing the plugin libraries
   std::filesystem::path mPluginRoot;

   /// List of available plugins
   std::map<std::string, std::unique_ptr<Plugin>> mPlugins;
};
}