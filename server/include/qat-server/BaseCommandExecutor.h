// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/ICommandExecutor.h>

#include <nlohmann/json.hpp>

#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <vector>

/// Forward declarations
class QObject;

namespace Qat
{
/// \brief Base class for command execution
class BaseCommandExecutor : public ICommandExecutor
{
public:
   using CacheUidType = std::string;

   /// Constructor
   /// \param[in] request The request in JSON format
   explicit BaseCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~BaseCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override = 0;

   /// Find the Qt object defined in the request
   /// \return The found object. Will throw an exception if object is not found
   QObject* FindObject() const;

   /// Register the given object to the cache
   /// \param[in] object Any Qt object
   /// \return The unique identifier for the registered object
   static CacheUidType RegisterObject(QObject* object);

   /// Return the registered object for the given registered object unique identifier
   /// \param[in] uid The unique identifier for the registered object
   /// \return The registered object or nullptr if not found
   static QObject* GetRegisteredObject(CacheUidType uid);

protected:

   /// Find all Qt objects defined in the request
   /// \param[in] parent The Qt parent containing the object
   /// \param[in] definition The properties to match in JSON format
   /// \param[in] allMatches If True, returns all matched objects, otherwise stop
   ///            searching when multiple objects are found. Default is True
   /// \return The found objects.
   std::set<QObject*> FindObjects(
      QObject* parent,
      const nlohmann::json& definition,
      bool allMatches = true) const;

   /// Find the Qt object matching the given properties
   /// \param[in] parent The Qt parent containing the object
   /// \param[in] definition The properties to match in JSON format
   /// \return The found object. Will throw an exception if object is not found
   QObject* FindObject(
      QObject* parent,
      const nlohmann::json& definition) const;

   /// Return the unique identifier for the given registered object
   /// \param[in] object Any Qt object
   /// \return The unique identifier for the registered object
   static CacheUidType GetObjectCacheUid(QObject* object);

   /// The request in JSON format
   nlohmann::json mRequest;

   /// Cache of QObjects associated to a unique identifier
   static std::map<CacheUidType, QObject*> mObjectCache;

   /// Counter tracking when QObject are re-using the same memory address.
   /// This allows to generate unique ID for the cache.
   static std::map<QObject*, int> mObjectCacheInstances;

   /// Mutex protecting mObjectCache access
   static std::mutex mObjectCacheMutex;

private:
   /// Container for temporary QObjects created during request execution (e.g. ModelIndex)
   mutable std::vector<std::unique_ptr<QObject>> mTemporaryObjects;
};

} // namespace Qat