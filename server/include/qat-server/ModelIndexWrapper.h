// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QColor>
#include <QModelIndex>
#include <QObject>
#include <QPointer>
#include <QString>
#include <QVariant>

/// Forward declarations
/// @{
class QAbstractItemModel;
class QItemSelectionModel;
/// @}

namespace Qat
{
/// \brief This class makes QModelIndex objects accessible through wait_for_object_xxx API calls.
/// \warning As they rely on underlying QModelIndex, instances of this class are temporary by nature and
/// must not be stored longer than the corresponding request.
class ModelIndexWrapper : public QObject
{
   Q_OBJECT

   Q_PROPERTY(
      int row
      READ GetRow
   )

   Q_PROPERTY(
      int column
      READ GetColumn
   )

   Q_PROPERTY(
      QModelIndex parent
      READ GetParent
   )

   Q_PROPERTY(
      QString text
      READ GetText
      WRITE SetText
   )

   Q_PROPERTY(
      QColor color
      READ GetColor
      WRITE SetColor
   )

public:
   /// Deleted default constructor
   ModelIndexWrapper() = delete;

   /// Constructor
   /// \param[in] model The model associated with the given index
   /// \param[in] selectionModel The selectionModel associated with the given model
   /// \param[in] index The wrapped index
   /// \param[in] parent The parent QAbstractItemView
   ModelIndexWrapper(
      QAbstractItemModel* model,
      QItemSelectionModel* selectionModel,
      const QModelIndex& index,
      QObject* parent);

   /// Default destructor
   ~ModelIndexWrapper() override = default;

   /// Return the underlying model
   /// \return the underlying model
   QAbstractItemModel* GetModel() const;

   /// Return the underlying selection model
   /// \return the underlying selection model
   QItemSelectionModel* GetSelectionModel() const;

   /// Return the underlying model index
   /// \return the underlying model index
   QModelIndex GetIndex() const;

   /// Return the associated parent QAbstractItemView
   /// \return the associated parent QAbstractItemView
   QObject* GetParentWidget() const;

   /// Return the row of the model index
   /// \return the row of the model index
   int GetRow() const;

   /// Return the column of the model index
   /// \return the column of the model index
   int GetColumn() const;

   /// Return the parent model index
   /// \return the parent model index
   QModelIndex GetParent() const;

   /// Get the value of this index for the given role
   /// \param[in] role The role associated to the requested value (default = Qt::DisplayRole)
   /// \return the value of this index
   Q_INVOKABLE QVariant data(int role = Qt::DisplayRole) const;

   /// Sets the given role data for this index to the given value.
   /// \param[in] value The value to set
   /// \param[in] role The role associated to the given value (default = Qt::DisplayRole)
   /// \return True if successful, False otherwise.
   Q_INVOKABLE bool setData(const QVariant& value, int role = Qt::DisplayRole) const;

   /// Get the text of this index (Display Role)
   /// \return the text of this index
   QString GetText() const;

   /// Set the text of this index (Display Role)
   /// \param[in] text the new text of this index
   void SetText(const QString& text);

   /// Get the color of this index (Foreground Role)
   /// \return the color of this index
   QColor GetColor() const;

   /// Set the color of this index (Foreground Role)
   /// \param[in] color the new color of this index
   void SetColor(const QColor& color);

   /// Select this index to make it visible
   Q_INVOKABLE void ScrollTo() const;

private:
   /// Model associated to the index
   QAbstractItemModel* mModel {nullptr};

   /// Selection Model associated to the model
   QItemSelectionModel* mSelectionModel {nullptr};
   
   /// Underlying model index
   QModelIndex mIndex;

   /// Parent QAbstractItemView
   QPointer<QObject> mParent;
};

} // namespace Qat

