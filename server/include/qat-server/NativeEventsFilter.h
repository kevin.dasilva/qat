// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QAbstractNativeEventFilter>

#include <QtGlobal>

namespace Qat
{

/// \brief Class responsible to filter out native events.
/// It ignores all external events received from the OS / platform.
/// This allows to "lock" the application GUI and prevents user activity from
/// affecting the tests.
class NativeEventsFilter : public QAbstractNativeEventFilter
{
   
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   using ResultType = long;
#else
   using ResultType = qintptr;
#endif

public:
   /// Default constructor
   NativeEventsFilter() = default;

   /// \copydoc QAbstractNativeEventFilter::nativeEventFilter
   bool nativeEventFilter(const QByteArray& eventType, void* message, ResultType* result) override;
};

} // namespace Qat