// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QObject>

namespace Qat
{

/// \brief Class responsible to filter out events.
/// It ignores all external events i.e. events received from the OS.
/// This allows to "lock" the application GUI and prevents user activity from
/// affecting the tests.
class ExternalEventsFilter : public QObject
{
   Q_OBJECT

public:
   /// Constructor
   /// \param[in] parent the Qt parent of this filter
   explicit ExternalEventsFilter(QObject* parent) : QObject(parent){}

   /// \copydoc QObject::eventFilter
   bool eventFilter(QObject* object, QEvent* event) override;
};

} // namespace Qat