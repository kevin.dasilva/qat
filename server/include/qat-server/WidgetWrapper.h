// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/IObjectPicker.h>

#include <QImage>

#include <memory>

/// Forward declarations
/// @{
class QObject;
class QWindow;
/// @}

namespace Qat
{
/// Forward declarations
class IWidget;

/// \brief Class providing access to various widget types (QWidget, QQuickItem, Native)
/// through a common interface (IWidget).
/// Widget implementations are provided by various plugins.
class WidgetWrapper
{
public:
   /// Wrap the given object to a widget. 
   /// Implementation is provided by plugins.
   /// \param qobject A widget as a QObject instance.
   /// \return The wrapped widget of nullptr if the object is not supported.
   static std::unique_ptr<IWidget> Cast(const QObject* qobject);

   /// Get all top-level windows/widgets
   /// \return all top-level windows/widgets
   static std::vector<QObject*> GetTopWindows();

   /// Take a screenshot of the given window
   /// \param window Any valid window
   /// \return The screenshot as a QImage or nullptr if it failed.
   static std::unique_ptr<QImage> GrabImage(QObject* window);

   /// Create an Object Picker for the given window
   /// \param window Any valid window
   /// \return The object picker or nullptr if it failed.
   static IObjectPicker* CreatePicker(QObject* window);
};

}