// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <qat-server/Exception.h>

#include <QBrush>
#include <QFont>

namespace Qat
{

/// @brief Custom definition of brush style to support multiple Qt versions.
/// Values must be synchronized with the Python API (see qt_types.QBrush.BrushStyle).
enum class BrushStyle
{
   NoBrush=0,
   SolidPattern,
   Dense1Pattern,
   Dense2Pattern,
   Dense3Pattern,
   Dense4Pattern,
   Dense5Pattern,
   Dense6Pattern,
   Dense7Pattern,
   HorPattern,
   VerPattern,
   CrossPattern,
   BDiagPattern,
   FDiagPattern,
   DiagCrossPattern,
   LinearGradientPattern,
   ConicalGradientPattern,
   RadialGradientPattern,
   TexturePattern,
};

/// Convert a BrushStyle from Qat enum to Qt enum. 
/// \param style A value from the Qat::BrushStyle enum.
/// \return The equivalent Qt::BrushStyle enum value
inline Qt::BrushStyle ToQt(BrushStyle style)
{
   switch(style)
   {
      case BrushStyle::NoBrush:
         return Qt::BrushStyle::NoBrush;
      case BrushStyle::SolidPattern:
         return Qt::BrushStyle::SolidPattern;
      case BrushStyle::Dense1Pattern:
         return Qt::BrushStyle::Dense1Pattern;
      case BrushStyle::Dense2Pattern:
         return Qt::BrushStyle::Dense2Pattern;
      case BrushStyle::Dense3Pattern:
         return Qt::BrushStyle::Dense3Pattern;
      case BrushStyle::Dense4Pattern:
         return Qt::BrushStyle::Dense4Pattern;
      case BrushStyle::Dense5Pattern:
         return Qt::BrushStyle::Dense5Pattern;
      case BrushStyle::Dense6Pattern:
         return Qt::BrushStyle::Dense6Pattern;
      case BrushStyle::Dense7Pattern:
         return Qt::BrushStyle::Dense7Pattern;
      case BrushStyle::HorPattern:
         return Qt::BrushStyle::HorPattern;
      case BrushStyle::VerPattern:
         return Qt::BrushStyle::VerPattern;
      case BrushStyle::CrossPattern:
         return Qt::BrushStyle::CrossPattern;
      case BrushStyle::BDiagPattern:
         return Qt::BrushStyle::BDiagPattern;
      case BrushStyle::FDiagPattern:
         return Qt::BrushStyle::FDiagPattern;
      case BrushStyle::DiagCrossPattern:
         return Qt::BrushStyle::DiagCrossPattern;
      case BrushStyle::LinearGradientPattern:
         return Qt::BrushStyle::LinearGradientPattern;
      case BrushStyle::ConicalGradientPattern:
         return Qt::BrushStyle::ConicalGradientPattern;
      case BrushStyle::RadialGradientPattern:
         return Qt::BrushStyle::RadialGradientPattern;
      case BrushStyle::TexturePattern:
         return Qt::BrushStyle::TexturePattern;

      default:
         throw Exception("Cannot convert to Qt::BrushStyle enum: invalid value");
   }
}

/// Convert a BrushStyle from Qt enum to Qat enum. 
/// \param style A value from the Qt::BrushStyle enum.
/// \return The equivalent Qat::BrushStyle enum value
inline BrushStyle FromQt(Qt::BrushStyle style)
{
   switch(style)
   {
      case Qt::BrushStyle::NoBrush:
         return BrushStyle::NoBrush;
      case Qt::BrushStyle::SolidPattern:
         return BrushStyle::SolidPattern;
      case Qt::BrushStyle::Dense1Pattern:
         return BrushStyle::Dense1Pattern;
      case Qt::BrushStyle::Dense2Pattern:
         return BrushStyle::Dense2Pattern;
      case Qt::BrushStyle::Dense3Pattern:
         return BrushStyle::Dense3Pattern;
      case Qt::BrushStyle::Dense4Pattern:
         return BrushStyle::Dense4Pattern;
      case Qt::BrushStyle::Dense5Pattern:
         return BrushStyle::Dense5Pattern;
      case Qt::BrushStyle::Dense6Pattern:
         return BrushStyle::Dense6Pattern;
      case Qt::BrushStyle::Dense7Pattern:
         return BrushStyle::Dense7Pattern;
      case Qt::BrushStyle::HorPattern:
         return BrushStyle::HorPattern;
      case Qt::BrushStyle::VerPattern:
         return BrushStyle::VerPattern;
      case Qt::BrushStyle::CrossPattern:
         return BrushStyle::CrossPattern;
      case Qt::BrushStyle::BDiagPattern:
         return BrushStyle::BDiagPattern;
      case Qt::BrushStyle::FDiagPattern:
         return BrushStyle::FDiagPattern;
      case Qt::BrushStyle::DiagCrossPattern:
         return BrushStyle::DiagCrossPattern;
      case Qt::BrushStyle::LinearGradientPattern:
         return BrushStyle::LinearGradientPattern;
      case Qt::BrushStyle::ConicalGradientPattern:
         return BrushStyle::ConicalGradientPattern;
      case Qt::BrushStyle::RadialGradientPattern:
         return BrushStyle::RadialGradientPattern;
      case Qt::BrushStyle::TexturePattern:
         return BrushStyle::TexturePattern;

      default:
         throw Exception("Cannot convert from Qt::BrushStyle enum: invalid value");
   }
}

/// @brief Custom definition of font weight to support multiple Qt versions.
/// Values must be synchronized with the Python API (see qt_types.QFont.Weight).
enum class FontWeight
{
   Thin=0,
   ExtraLight=1,
   Light=2,
   Normal=3,
   Medium=4,
   DemiBold=5,
   Bold=6,
   ExtraBold=7,
   Black=8
};

/// Convert a FontWeight from Qat enum to Qt enum. 
/// \param style A value from the Qat::FontWeight enum.
/// \return The equivalent QFont::Weight enum value
inline QFont::Weight ToQt(FontWeight weight)
{
   switch (weight)
   {
   case FontWeight::Thin:
      return QFont::Weight::Thin;
   case FontWeight::ExtraLight:
      return QFont::Weight::ExtraLight;
   case FontWeight::Light:
      return QFont::Weight::Light;
   case FontWeight::Normal:
      return QFont::Weight::Normal;
   case FontWeight::Medium:
      return QFont::Weight::Medium;
   case FontWeight::DemiBold:
      return QFont::Weight::DemiBold;
   case FontWeight::Bold:
      return QFont::Weight::Bold;
   case FontWeight::ExtraBold:
      return QFont::Weight::ExtraBold;
   case FontWeight::Black:
      return QFont::Weight::Black;
   default:
      throw Exception("Cannot convert to QFont::Weight enum: invalid value");
   }
}

/// Convert a FontWeight from Qt enum to Qat enum. 
/// \param style A value from the QFont::Weight enum.
/// \return The equivalent Qat::FontWeight enum value
inline FontWeight FromQt(QFont::Weight weight)
{
   switch (weight)
   {
   case QFont::Weight::Thin:
      return FontWeight::Thin;
   case QFont::Weight::ExtraLight:
      return FontWeight::ExtraLight;
   case QFont::Weight::Light:
      return FontWeight::Light;
   case QFont::Weight::Normal:
      return FontWeight::Normal;
   case QFont::Weight::Medium:
      return FontWeight::Medium;
   case QFont::Weight::DemiBold:
      return FontWeight::DemiBold;
   case QFont::Weight::Bold:
      return FontWeight::Bold;
   case QFont::Weight::ExtraBold:
      return FontWeight::ExtraBold;
   case QFont::Weight::Black:
      return FontWeight::Black;
   default:
      throw Exception("Cannot convert from QFont::Weight enum: invalid value");
   }
}

} // namespace Qat