// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <nlohmann/json.hpp>

#include <QMouseEvent>

namespace Qat::CommandHelper
{

/// Get the mouse button set in the arguments
/// \param[in] args The received arguments
/// \return The received mouse button
Qt::MouseButton GetButton(const nlohmann::json& args);

/// Get the key modifier set in the arguments
/// \param[in] args The received arguments
/// \return The received key modifier
Qt::KeyboardModifiers GetModifier(const nlohmann::json& args);

} // namespace Qat::CommandHelper