// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QObject>
#include <QPointer>
#include <QString>

namespace Qat
{
/// \brief This class makes QAction in QMenu objects accessible through wait_for_object_xxx API calls.
class MenuWrapper : public QObject
{
   Q_OBJECT

   Q_PROPERTY(
      QString text
      READ GetText
   )

   Q_PROPERTY(
      bool visible
      READ IsVisible
   )

   Q_PROPERTY(
      bool enabled
      READ IsEnabled
   )

   Q_PROPERTY(
      bool checked
      READ IsChecked
   )

   Q_PROPERTY(
      QObject* action
      READ GetAction
   )

public:
   /// Deleted default constructor
   MenuWrapper() = delete;

   /// Constructor
   /// \param[in] menu The menu containing this item
   /// \param[in] textOrName The text or objectName of this item
   MenuWrapper(
      QObject* menu,
      std::string textOrName);

   /// Default destructor
   ~MenuWrapper() override = default;

   /// Get the parent menu
   /// \return The parent menu
   QObject* GetMenu() const;

   /// Get the text or objectName of this item
   /// \return the text or name of this item
   const std::string& GetString() const;

   /// Get the displayed text of this item
   /// \return the displayed text of this item
   QString GetText() const;

   /// Return whether this item is visible or not
   /// \return True if this item is visible, False otherwise
   bool IsVisible() const;

   /// Return whether this item is enabled or not
   /// \return True if this item is enabled, False otherwise
   bool IsEnabled() const;

   /// Return whether this item is checked or not
   /// \return True if this item is checked, False otherwise
   bool IsChecked() const;

private:
   /// Return the underlying Action object.
   /// \return the underlying Action object.
   QObject* GetAction() const;

   /// Parent menu
   QPointer<QObject> mMenu;

   /// Identifier: either the objectName or the text of the item
   std::string mTextOrName;
};

} // namespace Qat

