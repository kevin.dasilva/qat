// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/BaseCommandExecutor.h>
#include <nlohmann/json.hpp>

namespace Qat
{
/// \brief Class responsible to execute touch commands.
/// It handles Touch, Release, Drag, Move actions
class TouchCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit TouchCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~TouchCommandExecutor() override = default;

   /// \brief ICommandExecutor::Run
   nlohmann::json Run() const override;
};

} // namespace Qat