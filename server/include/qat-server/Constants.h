// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <string>

namespace Qat::Constants
{
inline const std::string COMMAND_TYPE{ "command" };
inline const std::string OBJECT_DEFINITION{ "object" };
inline const std::string OBJECT_ATTRIBUTE{ "attribute" };
inline const std::string ARGUMENTS{ "args" };
inline const std::string OBJECT_NAME{ "objectName" };
inline const std::string OBJECT_TYPE{ "type" };
inline const std::string OBJECT_ID{ "id" };
inline const std::string OBJECT_PARENT{ "parent" };
inline const std::string QOBJECT_TYPE{ "QObject" };
inline const std::string CONTAINER{ "container" };
inline const std::string PROPERTIES{ "properties" };
inline const std::string METHODS{ "methods" };
inline const std::string TOP_WINDOWS{ "topWindows" };
inline const std::string VERSION_INFO{ "versionInfo" };
inline const std::string CURRENT_QT_VERSION{ "qtVersion" };
inline const std::string VALUES{ "values" };
inline const std::string CHILDREN{ "children" };
inline const std::string CLASS_NAME{ "className" };
inline const std::string CACHE_UID{ "cache_uid" };
inline const std::string GLOBAL_BOUNDS{ "globalBounds" };
inline const std::string PIXEL_RATIO{ "pixelRatio" };
inline const std::string HOST{ "host" };
inline const std::string PORT{ "port" };
inline const std::string PICKER_NAME{ "QatObjectPicker" };
inline const std::string PICKER_OVERLAY_NAME{ "QatObjectPickerOverlay" };
inline const std::string MODEL{ "model" };
inline const std::string SELECTION_MODEL{ "selectionModel" };
inline const std::string GLOBAL_APP_ID{ "GlobalApplication" };

namespace Command
{
inline const std::string FIND{ "find" };
inline const std::string LIST{ "list" };
inline const std::string GET{ "get" };
inline const std::string SET{ "set" };
inline const std::string CALL{ "call" };
inline const std::string ACTION{ "action" };
inline const std::string MOUSE{ "mouse" };
inline const std::string KEYBOARD{ "keyboard" };
inline const std::string COMMUNICATION{ "communication" };
inline const std::string GESTURE{ "gesture" };
inline const std::string TOUCH{ "touch" };
}

namespace Device
{
inline const std::string NAME_PREFIX{ "QatVirtual" };
}

namespace Mouse
{
inline const std::string DEVICE_NAME{ Device::NAME_PREFIX + "MouseDevice" };
inline const std::string CLICK{ "click" };
inline const std::string DOUBLE_CLICK{ "double-click" };
inline const std::string PRESS{ "press" };
inline const std::string RELEASE{ "release" };
inline const std::string MOVE{ "move" };
inline const std::string DRAG{ "drag" };
inline const std::string SCROLL{ "scroll" };
}

namespace Touch
{
inline const std::string DEVICE_NAME{ Device::NAME_PREFIX + "TouchDevice" };
inline const std::string TAP{ "tap" };
inline const std::string PRESS{ "press" };
inline const std::string RELEASE{ "release" };
inline const std::string MOVE{ "move" };
inline const std::string DRAG{ "drag" };
}

namespace Gesture
{
inline const std::string FLICK{ "flick" };
inline const std::string PINCH{ "pinch" };
}

namespace KeyBoard
{
inline const std::string DEVICE_NAME{ Device::NAME_PREFIX + "KeyboardDevice" };
inline const std::string SHORTCUT{ "shortcut" };
inline const std::string PRESS{ "press" };
inline const std::string RELEASE{ "release" };
inline const std::string TYPE{ "type" };
}

namespace Args
{
inline const std::string X{ "x" };
inline const std::string Y{ "y" };
inline const std::string DX{ "dx" };
inline const std::string DY{ "dy" };
inline const std::string BUTTON{ "button" };
inline const std::string MODIFIER{ "modifier" };
inline const std::string ANGLE{ "angle" };
inline const std::string SCALE{ "scale" };
}

namespace Button
{
inline const std::string NONE{ "none" };
inline const std::string LEFT{ "left" };
inline const std::string RIGHT{ "right" };
inline const std::string MIDDLE{ "middle" };
inline const std::string WHEEL{ "wheel" };
}

namespace Modifier
{
inline const std::string NONE{ "none" };
inline const std::string ALT{ "alt" };
inline const std::string CTL{ "ctrl" };
inline const std::string SHIFT{ "shift" };
}

namespace Action
{
inline const std::string SCREENSHOT{ "screenshot" };
inline const std::string GRAB{ "grab" };
inline const std::string PICKER{ "picker" };
inline const std::string LOCK_UI{ "lock" };
}

namespace Communication
{
inline const std::string INIT{ "init" };
inline const std::string CONNECT{ "connect" };
inline const std::string DISCONNECT{ "disconnect" };
inline const std::string CLOSE{ "close" };
}

} // namespace Qat