// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/BaseCommandExecutor.h>
#include <nlohmann/json.hpp>

namespace Qat
{
/// \brief Class responsible to execute keyboard commands.
/// \note Custom keyboard shortcuts are partially implemented
class KeyboardCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit KeyboardCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~KeyboardCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;
};

} // namespace Qat