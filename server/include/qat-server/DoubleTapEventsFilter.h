// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QObject>

namespace Qat
{

/// \brief Class responsible to filter out double-tap events.
/// Since Qt converts touch events to click events, when calling
/// qat.touch_tap() twice rapidly, it will generate a double-click event.
/// This filter replaces this double-click with 2 consecutive clicks.
class DoubleTapEventsFilter : public QObject
{
   Q_OBJECT

public:
   /// Constructor
   /// \param[in] parent the Qt parent of this filter
   explicit DoubleTapEventsFilter(QObject* parent) : QObject(parent){}

   /// \copydoc QObject::eventFilter
   bool eventFilter(QObject* object, QEvent* event) override;
};

} // namespace Qat