// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QColor>
#include <QImage>
#include <QObject>
#include <QRgb>
#include <QString>

#include <deque>
#include <map>
#include <mutex>

namespace Qat
{
/// \brief Class wrapper responsible to access the properties of an image
class ImageWrapper : public QObject
{
   Q_OBJECT

   Q_PROPERTY(
      int width
      READ GetWidth
   )

   Q_PROPERTY(
      int height
      READ GetHeight
   )

public:
   /// Default constructor
   ImageWrapper();

   /// Constructor
   /// \param[in] image The wrapped image
   explicit ImageWrapper(const QImage& image);

   /// Constructor
   /// \param[in] path The path to an image
   explicit ImageWrapper(const std::string& path);

   /// Default destructor
   ~ImageWrapper() override = default;

   /// Set the wrapped image
   /// \param[in] image The wrapped image
   void SetImage(const QImage& image);

   /// Get the width of the wrapped image
   /// \return the width of the wrapped image
   int GetWidth() const;

   /// Get the height of the wrapped image
   /// \return the height of the wrapped image
   int GetHeight() const;

   /// Get the pixel value at the given coordinates
   /// \param[in] x The X coordinate
   /// \param[in] y The Y coordinate
   /// \return the pixel value at the given coordinates
   Q_INVOKABLE unsigned int getPixel(int x, int y) const;

   /// Get the pixel color at the given coordinates
   /// \param[in] x The X coordinate
   /// \param[in] y The Y coordinate
   /// \return the pixel color at the given coordinates
   Q_INVOKABLE QColor getPixelRGBA(int x, int y) const;

   /// Save the wrapped image to the given file
   /// \param[in] fileName Full path to the file to be written
   Q_INVOKABLE void save(const QString& fileName);

   /// Compare the underlying images
   /// \param[in] other Another ImageWrapper
   /// \return True if the underlying images have the same content, False otherwise
   Q_INVOKABLE bool equals(const Qat::ImageWrapper* other) const;

private:
   /// The wrapped image
   QImage mImage;

   /// Flag indicating whether the wrapped image has been set or not
   bool mHasImage = { false };

   /// Use a cache to store all wrappers. This cache has a maximum size so older
   /// wrappers will be deleted when adding more wrappers.
   static std::deque<ImageWrapper*> mCache;

   /// Mutex protecting mCache access
   static std::mutex mMutex;
};

} // namespace Qat
