// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/BaseCommandExecutor.h>
#include <nlohmann/json.hpp>

namespace Qat
{
/// \brief Class responsible to execute action commands.
/// It handles various actions :
///   - locking the UI
///   - grabbing an image
///   - taking a screenshot
///   - picker de/activation
class ActionCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit ActionCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~ActionCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;
};

} // namespace Qat