// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/BaseCommandExecutor.h>
#include <nlohmann/json.hpp>

namespace Qat
{
/// \brief Class responsible to execute command to find a given object
class FindCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit FindCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~FindCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;
};

} // namespace Qat