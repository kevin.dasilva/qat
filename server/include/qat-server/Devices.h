// (c) Copyright 2024, Qat’s Authors
#pragma once
#include <QtGlobal>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   #include <QTouchEvent>
   #include <QTouchDevice>
   using TouchPointType = QTouchEvent::TouchPoint;
   using PointingDeviceType = QTouchDevice;
#else
   #include <QEventPoint>
   #include <QPointingDevice>
   using TouchPointType = QEventPoint;
   using PointingDeviceType = QPointingDevice;
   class QInputDevice;
#endif

namespace Qat::Devices
{

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   /// For Qt 5 compatibility, replace custom device (not supported) with synthesized event attribute
   Qt::MouseEventSource GetMouseDevice();
#else
   /// \brief Create a simulated mouse device
   /// \return A shared simulated device
   /// \warning This is based on Qt's private functions since there is no public API
   const PointingDeviceType* GetMouseDevice();

   /// \brief Create a simulated keyboard device
   /// \return A shared simulated device
   /// \warning This is based on Qt's private functions since there is no public API
   const QInputDevice* GetKeyboardDevice();
#endif

   /// \brief Create a simulated touch device
   /// \return A shared simulated device
   /// \warning This is based on Qt's private functions since there is no public API
   PointingDeviceType* GetTouchDevice();

} // namespace Qat::Devices