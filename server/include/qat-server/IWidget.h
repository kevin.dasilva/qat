// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QImage>
#include <QPointF>
#include <QRect>

#include <functional>
#include <string>
#include <vector>

/// Forward declarations
/// @{
class QAbstractItemModel;
class QItemSelectionModel;
class QObject;
class QWindow;
/// @}

namespace Qat
{

/// \brief Interface of widget (QWidget, QtQuick,...) wrapper
/// Wrapper implementation should be part of a plugin library.
/// This allows QatServer to not depend on UI libraries
/// so that it can be used with Qt applications built with Qml or QWidget only.
class IWidget
{
public:
   /// Destructor
   virtual ~IWidget() = default;

   /// Return the wrapped Qt object
   /// \return the wrapped Qt object
   virtual QObject* GetQtObject() const = 0;

   /// Return the "id" property of this object
   /// \return the "id" property of this object
   virtual std::string GetId() const = 0;

   /// Return the visual parent object
   /// \return the visual parent object
   virtual QObject* GetParent() const = 0;
   
   /// Return the child items of this widget
   /// \return the child items of this widget
   virtual std::vector<QObject*> GetChildWidgets() const = 0;

   /// Return the item model associated to this widget if it exists
   /// \return the item model or nullptr
   virtual QAbstractItemModel* GetModel() const = 0;

   /// Return the selection model associated to this widget if it exists
   /// \return the selection model or nullptr
   virtual QItemSelectionModel* GetSelectionModel() const = 0;

   /// Return the window containing this widget
   /// \return the window containing this widget
   virtual QWindow* GetWindow() const = 0;

   /// Map the given point in this widget's coordinate system to the equivalent point within
   /// global screen coordinate system, and return the mapped coordinates.
   /// \param[in] point A point defined by its local coordinates
   /// \return The same point defined by its screen coordinates
   virtual QPointF MapToGlobal(const QPointF &point) const = 0;

   /// Map the given point in the global screen coordinate system to the equivalent point within
   /// this widget's coordinate system, and return the mapped coordinate.
   /// \param[in] point A point defined by its screen coordinates
   /// \return The same point defined by its local coordinates
   virtual QPointF MapFromGlobal(const QPointF &point) const = 0;

   /// Map the given point in this widget's coordinate system to the equivalent point within
   /// the scene's coordinate system, and return the mapped coordinates.
   /// \param[in] point A point defined by its local coordinates
   /// \return The same point defined by its scene coordinates
   virtual QPointF MapToScene(const QPointF &point) const = 0;

   /// Map the given point in this widget's coordinate system to the equivalent point within
   /// widget's coordinate system, and return the mapped coordinate
   /// \param[in] widget Another widget
   /// \param[in] point A point defined by its local coordinates
   /// \return The same point defined by its coordinates in the other widget's coordinate system
   virtual QPointF MapToWidget(const IWidget* widget, const QPointF &point) const = 0;

   /// Return whether the widget contains the given point
   /// \param[in] point A point defined by its local coordinates
   virtual bool Contains(const QPointF &point) const = 0;

   /// Returns the size of this widget
   /// \return the size of this widget
   virtual QSizeF GetSize() const = 0;

   /// Returns the width of this widget
   /// \return the width of this widget
   virtual qreal GetWidth() const = 0;

   /// Returns the height of this widget
   /// \return the height of this widget
   virtual qreal GetHeight() const = 0;

   /// Returns the bounds of this widget in global coordinates
   /// \return the bounds of this widget
   virtual QRect GetBounds() const = 0;

   /// Return the pixel ratio for this widget,
   /// i.e the DPI scaling (current DPI / default DPI (=96))
   /// \return the pixel ratio for this widget
   virtual float GetPixelRatio() const = 0;

   /// Return the stacking order of sibling items. By default the stacking order is 0.
   /// \return The stacking order of this widget.
   virtual qreal GetZ() const = 0;

   /// Return whether this widget is visible or not.
   /// \return whether this widget is visible or not.
   virtual bool IsVisible() const = 0;

   /// Force active focus on this widget.
   /// \param[in] reason focus reason to enable better handling of the focus change.
   virtual void ForceActiveFocus(Qt::FocusReason reason) const = 0;

   /// Give active focus to this widget.
   /// \param[in] focus flag to enable/disable the focus
   /// \param[in] reason focus reason to enable better handling of the focus change.
   virtual void SetFocus(bool focus, Qt::FocusReason reason) const = 0;

   /// Take a snapshot of this widget.
   /// \param[in] callback A callback function that will receive the resulting image.
   virtual void GrabImage(std::function<void(const QImage&)> callback) const = 0;
};

}