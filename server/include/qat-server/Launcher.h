// (c) Copyright 2023, Qat’s Authors
#pragma once

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #define IS_WINDOWS
#endif

extern "C" void Start();
extern "C" void Stop();

#ifdef IS_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

extern "C" int __stdcall DllMain(HINSTANCE, DWORD fdwReason, LPVOID);

#else // Assuming linux

#include <unistd.h>

#endif
