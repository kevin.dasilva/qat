// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QRasterWindow>

#include <QRect>

class QPaintEvent;

namespace Qat::CocoaNativePlugin
{

/// \brief Class adding a frameless window above a native window
/// to display a colored rectangle over the picked object.
class HighlightOverlay: public QRasterWindow
{
public:
   /// Default constructor
   HighlightOverlay();

   /// Default destructor
   ~HighlightOverlay() override = default;

   /// Set the rectangle area to be highlighted.
   /// \param area The area rectangle.
   void SetHighlightArea(const QRect& area);
   
   protected:
   /// \copydoc QRasterWindow::paintEvent
   void paintEvent(QPaintEvent*) override;

private:
   /// The rectangle representing the area to highlight.
   /// If invalid or empty, nothing will be drawn.
   QRect mHighlightArea;
};

} // namespace Qat::CocoaNativePlugin