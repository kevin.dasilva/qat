// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <CocoaInterfaceConstants.h>

extern "C"
{
namespace CocoaInterface
{
   /// Allow threads to use autoreleasepool with Cocoa.
   /// see https://developer.apple.com/documentation/foundation/nsautoreleasepool#1651513
   void QatActivateCocoaMultiThreading();

   /// Return the list of current Cocoa windows for the current process.
   /// \param windowList List of window handles. Must be allocated by caller.
   /// \param viewList List of subview handles. Must be allocated by caller.
   /// \param classNames List of window types. Must be allocated by caller.
   /// \param len Size of all argument arrays. Will be set to the actual number of windows.
   void ListTopWindows(WINDOW_HANDLE windowList[], WIDGET_HANDLE viewList[], char* classNames[], int* len);

   /// Close the given window.
   /// \param handle Handle of the window.
   /// \return True upon success, False otherwise.
   bool CloseWindow(WINDOW_HANDLE handle);

   /// Return the list of all direct children (views) of the given widget.
   /// \param handle Handle of the parent widget.
   /// \param children List of child widget handles. Must be allocated by caller.
   /// \param len Size of children[]. Will be set to the actual number of children.
   void GetChildren(WIDGET_HANDLE handle, WIDGET_HANDLE children[], int* len);

   /// Return the type of the given widget.
   /// \param handle Handle of a widget.
   /// \param className Returned type of the widget. Must be allocated by caller.
   void GetClassName(WIDGET_HANDLE handle, char* className);

   /// Return whether this window is a panel (i.e inherits from NSPanel) or not.
   /// \param handle Handle of the window.
   /// \return whether this window is a panel or not.
   bool IsPanel(WINDOW_HANDLE handle);

   /// Return the hash of the given widget. Can be used as an ID.
   /// \param handle Handle of a widget.
   /// \return the hash of the given widget.
   unsigned long GetWidgetHash(WIDGET_HANDLE handle);

   /// Return whether the given widget is enabled or not.
   /// \param handle Handle of a widget.
   /// \return True if the widget is enabled, False otherwise.
   bool IsEnabled(long handle);

   /// Return the text or title of the given widget.
   /// \param handle Handle of a widget.
   /// \param text Returned text of the widget. Must be allocated by caller.
   void GetText(long handle, char* text);

   /// Return the bounds of the given widget in screen coordinates (with origin at top-left).
   /// \param handle Handle of a widget.
   /// \param x Returned horizontal position of the widget.
   /// \param y Returned vertical position of the widget.
   /// \param width Returned horizontal size of the widget.
   /// \param height Returned vertical size of the widget.
   void GetBounds(long handle, int* x, int* y, int* width, int* height);

   /// Return the bounds of the given window in screen coordinates (including title bar).
   /// \param windowHandle Handle of a window.
   /// \param x Returned horizontal position of the window.
   /// \param y Returned vertical position of the window.
   /// \param width Returned horizontal size of the window.
   /// \param height Returned vertical size of the window.
   bool GetWindowFullBounds(WINDOW_HANDLE windowHandle, int* x, int* y, int* width, int* height);

   /// Converts local widget coordinates to global (screen) coordinates.
   /// \param handle Handle of a widget.
   /// \param x Local horizontal coordinate.
   /// \param y Local vertical coordinate.
   /// \param globalX Returned global horizontal coordinate.
   /// \param globalY Returned global vertical coordinate.
   void MapToGlobal(long handle, long x, long y, long* globalX, long* globalY);

   /// Converts global (screen) coordinates to local widget coordinates.
   /// \param handle Handle of a widget.
   /// \param x Global horizontal coordinate.
   /// \param y Global vertical coordinate.
   /// \param localX Returned local horizontal coordinate.
   /// \param localY Returned local vertical coordinate.
   void MapFromGlobal(long handle, long x, long y, long* localX, long* localY);

   /// Give the focus to the given widget by making it the first responder of its window.
   /// If widget is a TextField, unselect its content and move the cursor to the end.
   /// \param handle Handle of a widget.
   /// \return True if the focus was given, False otherwise.
   bool GiveFocus(long handle);

   /// Grab a screenshot of the given widget and write it to the
   /// given file in transparent bitmap format.
   /// \param handle Handle of a widget.
   /// \param path Path to the image file to save. Must have a ".png" extension to support transparency.
   /// \return True upon success, False in case of error.
   bool SaveWidgetImage(long handle, const char* path);

   /// Send a generated mouse event to the given window.
   /// \param windowHandle Handle of the window that will receive the event.
   /// \param button Mouse button (left or right).
   /// \param type Event type (Button down or up, double-click).
   /// \param x Horizontal position of the event, in window coordinates.
   /// \param y Vertical position of the event, in window coordinates.
   /// \return True if the event was delivered, False otherwise.
   bool SendMouseEvent(
      long windowHandle,
      MouseButton button,
      MouseEventType type,
      int x,
      int y);

   /// Send a generated keyboard event to the given window.
   /// \param windowHandle Handle of the window that will receive the event.
   /// \param type Event type (Key up or down).
   /// \param text The character corresponding to the pressed key. May be 0 for special keys.
   /// \param keyCode The platform-dependent code for a virtual key. Can be 0 if 'text' is not 0.
   /// \return True if the event was delivered, False otherwise.
   bool SendKeyboardEvent(
      long windowHandle,
      KeyEventType type,
      const char text,
      unsigned short keyCode);
}
}
