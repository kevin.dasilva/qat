// (c) Copyright 2023, Qat’s Authors

#include <CocoaWidget.h>
#include <CocoaWidgetWrapper.h>
#include <CocoaInterface.h>

#include <qat-server/Exception.h>

#include <QObject>

#include <cstdlib>
#include <iostream>
#include <filesystem>
#include <sstream>

namespace
{
/// Return the path to the current temporary folder
/// \return The path to the current temporary folder
std::string GetTempPath()
{
   std::string value = std::getenv("TEMP");
   return value;
}
}

namespace Qat
{
CocoaWidget::CocoaWidget(Qat::CocoaWidgetWrapper* qtobject) :
   mQtobject{qtobject}
{
   if (!qtobject)
   {
      throw Exception("Cannot create CocoaWidget: wrapper is NULL");
   }
}

QObject* CocoaWidget::GetQtObject() const
{
   return mQtobject;
}

std::string CocoaWidget::GetId() const
{
   const auto id = CocoaInterface::GetWidgetHash(mQtobject->GetHandle());
   return std::to_string(id);
}

QObject* CocoaWidget::GetParent() const
{
   return mQtobject->parent();
}

std::vector<QObject*> CocoaWidget::GetChildWidgets() const
{
   std::vector<QObject*> children;
   const auto qChildren = mQtobject->GetChildren();
   children.insert(children.end(), qChildren.cbegin(), qChildren.cend());
   return children;
}

QAbstractItemModel* CocoaWidget::GetModel() const
{
   // Not applicable
   return nullptr;
}

QItemSelectionModel* CocoaWidget::GetSelectionModel() const
{
   // Not applicable
   return nullptr;
}

QWindow* CocoaWidget::GetWindow() const
{
   // Must return nullptr: events are handled by the underlying CocoaWidgetWrapper.
   return nullptr;
}

QPointF CocoaWidget::MapToGlobal(const QPointF &point) const
{
   const auto p = point.toPoint();
   long globalX = p.x();
   long globalY = p.y();
   CocoaInterface::MapToGlobal(mQtobject->GetHandle(), p.x(), p.y(), &globalX, &globalY);
   return QPointF(globalX, globalY);
}

QPointF CocoaWidget::MapFromGlobal(const QPointF &point) const
{
   const auto p = point.toPoint();
   long localX = p.x();
   long localY = p.y();
   CocoaInterface::MapFromGlobal(mQtobject->GetHandle(), p.x(), p.y(), &localX, &localY);
   return QPointF(localX, localY);
}

QPointF CocoaWidget::MapToScene(const QPointF &point) const
{
   // Not used
   return (point);
}

QPointF CocoaWidget::MapToWidget(const IWidget* widget, const QPointF& point) const
{
   if (!widget)
   {
      throw Exception("MapToWidget failed: target widget is NULL");
   }
   const auto* wrapper = qobject_cast<const CocoaWidgetWrapper*>(widget->GetQtObject());
   if (!wrapper)
   {
      throw Exception("MapToWidget failed: target widget is not a Cocoa widget");
   }
   const auto globalPoint = MapToGlobal(point);
   return widget->MapFromGlobal(globalPoint);
}

bool CocoaWidget::Contains(const QPointF& point) const
{
   return mQtobject->Contains(point);
}

QSizeF CocoaWidget::GetSize() const
{
   const auto bounds = GetBounds();
   return {(double)bounds.width(), (double)bounds.height()};
}

qreal CocoaWidget::GetWidth() const
{
   return GetBounds().width();
}

qreal CocoaWidget::GetHeight() const
{
   return GetBounds().height();
}

QRect CocoaWidget::GetBounds() const
{
   return mQtobject->GetBounds();
}

float CocoaWidget::GetPixelRatio() const
{  
   return mQtobject->GetPixelRatio();
}

qreal CocoaWidget::GetZ() const
{
   return 0;
}

bool CocoaWidget::IsVisible() const
{
   return true;
}

void CocoaWidget::ForceActiveFocus(Qt::FocusReason reason) const
{
   SetFocus(true, reason);
}

void CocoaWidget::SetFocus(bool focus, Qt::FocusReason) const
{
   if (focus)
   {
      CocoaInterface::GiveFocus(mQtobject->GetHandle());
   }
}

void CocoaWidget::GrabImage(std::function<void(const QImage&)> callback) const
{
   std::filesystem::path tempPath = GetTempPath();
   const auto filename = "qat_temp_screenshot_data_" + std::to_string(mQtobject->GetHandle()) + ".png";
   tempPath = tempPath / filename;
   CocoaInterface::SaveWidgetImage(mQtobject->GetHandle(), tempPath.string().c_str());

   if (!std::filesystem::exists(tempPath))
   {
      throw Exception("Cannot take screenshot: failed to create temporary file");
   }

   QImage image(QString::fromStdString(tempPath.string()));
   callback(image);

   std::error_code ec;
   if (!std::filesystem::remove(tempPath, ec))
   {
      std::cerr << "Could not delete temporary file" << std::endl;
   }
}

} // namespace Qat