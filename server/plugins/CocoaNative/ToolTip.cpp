// (c) Copyright 2024, Qat’s Authors

#include <ToolTip.h>

#include <QGuiApplication>
#include <QPainter>
#include <QPalette>

namespace
{
constexpr auto TEXT_FLAGS = Qt::AlignLeft | Qt::TextSingleLine | Qt::AlignVCenter;
}

namespace Qat::CocoaNativePlugin
{
ToolTip::ToolTip() :
   QRasterWindow(nullptr)
{
   // Configure flags to display an overlay window
   // without frame nor shadow and not appearing in the taskbar
   setFlag(Qt::FramelessWindowHint);
   setFlag(Qt::WindowStaysOnTopHint);
   setFlag(Qt::Tool);
   setFlag(Qt::NoDropShadowWindowHint);
}

void ToolTip::paintEvent(QPaintEvent*)
{
   QPainter painter(this);
   // -1 to account for border
   QRect allArea{0, 0, width() - 1, height() - 1};
   painter.eraseRect(allArea);

   const auto palette = QGuiApplication::palette();
   
   // Font
   painter.setPen(palette.toolTipText().color());
   painter.setFont(GetFont());

   // Colors
   painter.fillRect(allArea, palette.toolTipBase().color());
   painter.drawRect(allArea);
   painter.drawText(allArea, TEXT_FLAGS, mText);
}

void ToolTip::SetText(const std::string& text)
{
   const QString spacer{"  "};
   mText = spacer + QString::fromStdString(text) + spacer;
}

QRect ToolTip::AdjustSize(QPaintDevice* paintDevice)
{
   if (!paintDevice) return {};
   QPainter painter(paintDevice);
   painter.setFont(GetFont());
   QRect allArea{0, 0, paintDevice->width(), paintDevice->height()};
   auto bounds = painter.boundingRect(allArea, Qt::AlignLeft | Qt::TextSingleLine, mText);
   bounds.setHeight(1.5 * bounds.height());
   resize(bounds.width() + 2, bounds.height() + 2);

   return bounds;
}

QFont ToolTip::GetFont() const
{
   return QFont("Times");
}

} // namespace Qat::CocoaNativePlugin