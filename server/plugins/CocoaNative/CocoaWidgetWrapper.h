// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <CocoaInterface.h>

#include <qat-server/INativeWidget.h>

#include <QObject>
#include <QPointer>

#include <map>
#include <vector>

// Forward declarations
class QEvent;
class QKeyEvent;
class QMouseEvent;

namespace Qat
{

/// \brief Wraps a native Cocoa widget into a Qt object.
class CocoaWidgetWrapper : public INativeWidget
{
   Q_OBJECT
   Q_PROPERTY(
      bool visible
      READ IsVisible
   )
   Q_PROPERTY(
      bool enabled
      READ IsEnabled
   )
   Q_PROPERTY(
      QString type
      READ GetType
   )
   Q_PROPERTY(
      QString text
      READ GetText
   )
   Q_PROPERTY(
      int width
      READ GetWidth
   )
   Q_PROPERTY(
      int height
      READ GetHeight
   )
   Q_PROPERTY(
      long handle
      MEMBER mHandle
      CONSTANT
   )
   Q_PROPERTY(
      long windowHandle
      MEMBER mWindowHandle
      CONSTANT
   )
public:
   /// Constructor
   /// \param[in] parent Qt parent
   /// \param[in] handle Native handle to the underlying widget
   /// \param[in] window Parent window handle
   CocoaWidgetWrapper(CocoaWidgetWrapper* parent, WIDGET_HANDLE handle, WINDOW_HANDLE windowHandle);

   /// Default destructor
   ~CocoaWidgetWrapper() = default;

   /// Return the handle to the underlying object
   /// \return the handle to the underlying object
   WIDGET_HANDLE GetHandle() const;

   /// Return the handle to the associated window
   /// \return the handle to the associated window
   WINDOW_HANDLE GetWindowHandle() const;

   /// Return whether the underlying Cocoa view still exists or not.
   /// \return whether the underlying Cocoa view still exists or not.
   virtual bool Exists() const;

   /// Return the list of current children.
   /// \note Create one child object per child window handle
   /// \return the list of current children
   QObjectList GetChildren();

   /// Return the child widget at the given position.
   /// \param[in] position A position in local coordinates.
   /// \return the child widget or NULL if not found
   CocoaWidgetWrapper* GetChildAt(QPoint position);

   /// Return whether this widget is visible or not
   /// \return True if this widget is visible, false otherwise
   bool IsVisible() const;

   /// Return whether this widget is enabled or not
   /// \return True if this widget is enabled, false otherwise
   bool IsEnabled() const;

   /// Return the native type of this widget
   /// \return The native type of this widget
   virtual QString GetType() const;

   /// Return the text of this widget
   /// \return The text of this widget
   virtual QString GetText() const;

   /// Return the bounds of this widget in screen coordinates
   /// \return The bounds of this widget in screen coordinates
   QRect GetBounds() const;

   /// Return the width of this widget in pixels
   /// \return The width of this widget in pixels
   int GetWidth() const;

   /// Return the height of this widget in pixels
   /// \return The height of this widget in pixels
   int GetHeight() const;

   /// Return the scaled bounds of this widget in screen coordinates,
   /// i.e. the bounds with screen scaling applied.
   /// \return The scaled bounds of this widget
   QRectF GetScaledBounds() const;

   /// Return the pixel ratio for this widget,
   /// i.e the DPI scaling (current DPI / default DPI (=96))
   /// \return the pixel ratio for this widget
   float GetPixelRatio() const;

   /// Return whether this widget contains the given point.
   /// \param point A point in local coordinates
   /// \return True if the point is within this widget's bounds, False otherwise.
   bool Contains(const QPointF &point) const;

   /// \copydoc QObject::event()
   bool event(QEvent* event) override;

private:

   /// Handle the given mouse event
   /// \param mouseEvent A mouse event
   /// \return True if the event was properly handled, False otherwise
   bool HandleMouseEvent(const QMouseEvent* mouseEvent);

   /// Handle the given keyboard event
   /// \param keyboardEvent A keyboard event
   /// \return True if the event was properly handled, False otherwise
   bool HandleKeyboardEvent(const QKeyEvent* keyboardEvent);

   /// Native handle to the underlying widget
   WIDGET_HANDLE mHandle;

   /// Handle to the window wrapper containing this widget
   WINDOW_HANDLE mWindowHandle;

   /// Pointer to the wrapper of the parent widget
   QPointer<CocoaWidgetWrapper> mParent {nullptr};

   /// Children wrappers mapped to their native handle
   std::map<WIDGET_HANDLE, QPointer<CocoaWidgetWrapper>> mChildren;
};

} // namespace Qat