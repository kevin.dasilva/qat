// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <CocoaWidget.h>
#include <HighlightOverlay.h>

#include <qat-server/IObjectPicker.h>
#include <qat-server/IWidget.h>

#include <QObject>
#include <QPointer>

#include <atomic>
#include <memory>

class QMouseEvent;
namespace Qat
{
class CocoaWindowWrapper;
}

namespace Qat::CocoaNativePlugin
{

class ToolTip;

class ObjectPicker : public IObjectPicker
{
   Q_OBJECT
   Q_DISABLE_COPY_MOVE(ObjectPicker)
   Q_PROPERTY(
      QObject* pickedObject
      READ GetCurrentPickedObject
      NOTIFY objectPicked
   )
   Q_PROPERTY(
      bool hasNewObject
      MEMBER mHasNewObject
      NOTIFY hasNewObjectChanged
   )

public:
   /// Constructor
   explicit ObjectPicker(QObject* parent);

   /// Destructor
   ~ObjectPicker() override;

   /// \copydoc QObject::eventFilter
   bool eventFilter(QObject* object, QEvent* event) override;

   /// \copydoc IObjectPicker::Reset
   void Reset() override;

   /// \copydoc IObjectPicker::SetActivated
   void SetActivated(bool activate) override;

   /// \copydoc IObjectPicker::Pause
   void Pause() override;

   /// \copydoc IObjectPicker::Restore
   void Restore() override;
   
   /// Highlight the picked object by drawing a transparent rectangle above it
   Q_INVOKABLE void HighLightObject();

   /// Remove the transparent rectangle above the current object
   Q_INVOKABLE void CancelHighLighting();

signals:
   /// Emitted when an object is picked
   /// @{
   void objectPicked();
   void hasNewObjectChanged();
   /// @}

private:
   /// Get the current object under the mouse cursor
   /// \param[in] mousePosition The position of the mouse
   /// \param[in] deep If True, return the leaf-child under the mouse. Otherwise return 
   ///            any parent of the same size
   /// \return The object under the mouse cursor
   CocoaWidgetWrapper* GetPickedObject(QPoint mousePosition, bool deep);

   /// Forward a mouse event to the underlying widget.
   /// Intended to be used when the CTRL key is pressed.
   /// \param mouseEvent A mouse event
   /// \return True if the event was handled, false otherwise
   bool ForwardMouseEvent(const QMouseEvent* mouseEvent);

   /// Getter for the pickedObject property
   /// \return The picked widget as a QObject*
   QObject* GetCurrentPickedObject() const;

   /// Simulate a click on the overlay at the given global position.
   /// \note This method is intended to be used for testing purposes only.
   /// \param x Horizontal coordinate
   /// \param y Vertical coordinate
   /// \param holdCtrl Use the CTRL modifier (Default is false)
   /// \return True if the click event was sent, False otherwise.
   Q_INVOKABLE bool ClickAt(int x, int y, bool holdCtrl = false);

   /// Wrapper of the parent window
   QPointer<CocoaWindowWrapper> mParentWindow {nullptr};

   /// The last picked object
   QPointer<CocoaWidgetWrapper> mPickedObject{ nullptr };

   /// Wrapper for the picked widget
   std::unique_ptr<IWidget> mWidgetWrapper{nullptr};

   /// The current highlighted object
   QObject* mHighlightedObject{ nullptr };

   /// Flag indicating whether the picked object is currently highlighted or not
   bool mIsHighLighted{ false };

   /// Flag indicating whether a new picked object is available or not
   bool mHasNewObject{ false };
   
   /// The overlay displayed during highlighting
   QPointer<HighlightOverlay> mHighlightOverlay{ nullptr };

   /// Height in pixels of the associated windows' title bar
   int mTitleBarHeight{ 0 };

   /// Flag indicating whether this picker is active or not
   bool mIsActive{ false };
   
   /// Tooltip displaying highlighted object's type and handle
   ToolTip* mToolTip{ nullptr };

   /// Current pixel ratio of the parent window
   float mPixelRatio {1.0f};
};

} // namespace Qat::CocoaNativePlugin