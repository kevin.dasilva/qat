// (c) Copyright 2024, Qat’s Authors
#pragma once

#define WINDOW_HANDLE long
#define WIDGET_HANDLE long
#define MAX_CLASS_NAME_LENGTH 256
#define MAX_TEXT_LENGTH 1024

typedef enum
{
    ButtonLeft,
    ButtonRight
} MouseButton;

typedef enum
{
    MouseUp,
    MouseDown,
    MouseDblClick
} MouseEventType;

typedef enum
{
    KeyUp,
    KeyDown
} KeyEventType;
