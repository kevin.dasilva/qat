// (c) Copyright 2024, Qat’s Authors

#include <CocoaWindowWrapper.h>

#include <map>
#include <vector>

namespace
{
/// Container for all window wrappers
static std::map<WINDOW_HANDLE, QPointer<Qat::CocoaWindowWrapper>> windows;
}

namespace Qat
{

int CocoaWindowWrapper::GetNumOpenedWindows()
{
   int len = 0;
   CocoaInterface::ListTopWindows(nullptr, nullptr, nullptr, &len);
   return len;
}

std::vector<CocoaWindowWrapper*> CocoaWindowWrapper::GetOpenedWindows(int maxSize)
{
   std::vector<CocoaWindowWrapper*> wrappers;
   std::vector<WINDOW_HANDLE> windowsHandles(maxSize);
   std::vector<WIDGET_HANDLE> windowsViews(maxSize);
   std::vector<char*> windowsTypes(maxSize);
   std::generate(windowsTypes.begin(), windowsTypes.end(),
      []()
      {
         return new char[MAX_CLASS_NAME_LENGTH];
      });
   int len = maxSize;
   CocoaInterface::ListTopWindows(windowsHandles.data(), windowsViews.data(), windowsTypes.data(), &len);

   // Get wrappers for all opened windows
   for (int i = 0; i < maxSize; ++i)
   {
      const auto handle = windowsHandles[i];
      const auto view = windowsViews[i];
      const QString className { windowsTypes[i] };
      const auto it = windows.find(handle);
      if (it == windows.cend() || !it->second)
      {
         windows[handle] = new CocoaWindowWrapper(handle, view, className);
      }
      wrappers.push_back(windows[handle]);
      delete[] windowsTypes[i];
   }

   // Delete obsolete wrappers for closed windows
   std::vector<WINDOW_HANDLE> obsoleteHandles;
   for (const auto& window : windows)
   {
      const auto it = std::find(windowsHandles.cbegin(), windowsHandles.cend(), window.first);
      if (it == windowsHandles.cend())
      {
         obsoleteHandles.push_back(window.first);
         window.second->deleteLater();
      }
   }
   for (const auto& handle : obsoleteHandles)
   {
      windows.erase(handle);
   }

   return wrappers;
}

CocoaWindowWrapper::CocoaWindowWrapper(WINDOW_HANDLE handle, WIDGET_HANDLE viewHandle, const QString& type): 
   CocoaWidgetWrapper(nullptr, viewHandle, handle),
   mType{type}
{
}

bool CocoaWindowWrapper::Exists() const
{
   int len = 0;
   CocoaInterface::ListTopWindows(nullptr, nullptr, nullptr, &len);
   const auto windows = CocoaWindowWrapper::GetOpenedWindows(len);
   return std::find(windows.cbegin(), windows.cend(), this) != windows.cend();
}

bool CocoaWindowWrapper::IsPanel() const
{
   if (!Exists()) return false;
   return CocoaInterface::IsPanel(GetWindowHandle());
}

QString CocoaWindowWrapper::GetText() const
{
   if (!Exists()) return {};
   char buffer[MAX_TEXT_LENGTH] = {0};
   CocoaInterface::GetText(GetWindowHandle(), buffer);
   return QString(buffer);
}

QRect CocoaWindowWrapper::GetWindowBounds() const
{
   if (!Exists()) return {};
   int x,y,width,height;
   const auto result = CocoaInterface::GetWindowFullBounds(GetWindowHandle(), &x, &y, &width, &height);
   if (!result) return {};
   return {x, y, width, height};
}

bool CocoaWindowWrapper::close()
{
   if (!Exists()) return true;
   return CocoaInterface::CloseWindow(GetWindowHandle());
}

} // namespace Qat