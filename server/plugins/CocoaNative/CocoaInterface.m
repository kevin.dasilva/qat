#import <Cocoa/Cocoa.h>
#import <CocoaInterfaceConstants.h>

void QatActivateCocoaMultiThreading() 
{
   [[NSThread new] start];
}

void ListTopWindows(long windowList[], long viewList[], char* classNames[], int* len)
{
   @autoreleasepool {
      if (!len) {
         NSLog(@"Invalid call to listTopWindows (arg is null)");
      }
      int nbWindows = 0;
      NSArray *windows = [NSApp windows];
      for (NSWindow *window in windows) {
         if ([window isVisible] ) {
            NSString* title = [window title];
            if ([title length] == 0) {
               continue;
            }
            NSString* className = NSStringFromClass([window class]);
            NSView* contentView = [window contentView];
            if (nbWindows < *len) {
               windowList[nbWindows] = (long)[window windowNumber];
               viewList[nbWindows] = (long)contentView;
               [className getCString:classNames[nbWindows] maxLength:256 encoding:NSASCIIStringEncoding];
            }
            nbWindows++;
         }
      }
      *len = nbWindows;
   }
}

NSWindow* FindWindow(long handle)
{
   return [NSApp windowWithWindowNumber:handle];
}

void GetChildren(long handle, long children[], int* len)
{
   @autoreleasepool {
      if (!len) {
         NSLog(@"Invalid call to GetChildren (len is null)");
      }
      NSView* parent = (__bridge NSView*)handle;
      NSArray* subviews = [parent subviews];
      int nbChildren = 0;
      for (NSView* widget in subviews) {
         if (nbChildren < *len)
         {
            children[nbChildren] = (long)widget;
         }
         ++nbChildren;
      }
      *len = nbChildren;
   }
}

bool CloseWindow(long handle)
{
   NSWindow* window = FindWindow(handle);
   if (window)
   {
      [window close];
      return true;
   }
   return false;
}

void GetClassName(long handle, char* className)
{
   @autoreleasepool {
      NSView* widget = (__bridge NSView*)handle;
      if (!widget)
      {
         NSLog(@"GetClassName: widget is nil");
         return;
      }
      NSString* nsClassName = NSStringFromClass([widget class]);
      [nsClassName getCString:className maxLength:256 encoding:NSASCIIStringEncoding];
   }
}

bool IsEnabled(long handle)
{
   @autoreleasepool {
      NSView* widget = (__bridge NSView*)handle;
      if (!widget)
      {
         return false;
      }

      if ([widget isKindOfClass:[NSControl class]])
      {
         return [(NSControl*)widget isEnabled];
      }

      return true;
   }
}

bool IsPanel(WINDOW_HANDLE handle)
{
   @autoreleasepool {
      NSWindow* window = FindWindow(handle);
      if (window == nil)
      {
         return false;
      }
      return [window isKindOfClass:[NSPanel class]];
   }
}

unsigned long GetWidgetHash(WIDGET_HANDLE handle)
{
   @autoreleasepool {
      NSView* widget = (__bridge NSView*)handle;
      if (!widget)
      {
         return 0;
      }

      return [widget hash];
   }
}

void GetText(long handle, char* text)
{
   @autoreleasepool {
      NSWindow* window = FindWindow(handle);
      if (window != nil)
      {
         NSString* nsText = [window title];
         [nsText getCString:text maxLength:1024 encoding:NSASCIIStringEncoding];
         return;
      }
      NSView* widget = (__bridge NSView*)handle;
      if (!widget)
      {
         return;
      }
      if ([widget isKindOfClass:[NSButton class]])
      {
         NSButton* button = (__bridge NSButton*)handle;
         NSString* nsText = [button title];
         [nsText getCString:text maxLength:1024 encoding:NSASCIIStringEncoding];
         return;
      }
      if ([widget isKindOfClass:[NSControl class]])
      {
         NSControl* control = (__bridge NSControl*)handle;
         NSString* nsText = [control stringValue];
         [nsText getCString:text maxLength:1024 encoding:NSASCIIStringEncoding];
         return;
      }
      NSLog(@"GetText: widget is not supported");
   }
}

void MapToGlobal(long handle, long x, long y, long* globalX, long* globalY)
{
   @autoreleasepool {
      NSView* widget = (__bridge NSView*)handle;
      if (!widget)
      {
         return;
      }

      NSPoint coordinates = [widget convertPoint:NSMakePoint(x, widget.bounds.size.height - 1 -y) toView:nil];
      *globalX = coordinates.x;
      *globalY = coordinates.y;
   }
}

void MapFromGlobal(long handle, long x, long y, long* localX, long* localY)
{
   @autoreleasepool {
      NSView* widget = (__bridge NSView*)handle;
      if (!widget)
      {
         return;
      }

      NSPoint coordinates = [widget convertPoint:NSMakePoint(x, y) fromView:nil];
      *localX = coordinates.x;
      *localY = widget.bounds.size.height - 1 - coordinates.y;
   }
}

void GetBounds(long handle, int* x, int* y, int* width, int* height)
{
   @autoreleasepool {
      NSView* widget = (__bridge NSView*)handle;
      if (!widget)
      {
         return;
      }
      int screenHeight = widget.window.screen.frame.size.height;
      NSRect widgetBounds = [widget convertRect:[widget bounds] toView:nil];
      NSRect bounds = [widget.window convertRectToScreen:widgetBounds];
      *x = bounds.origin.x;
      *y = screenHeight - 1 - bounds.origin.y - bounds.size.height;
      *width = bounds.size.width;
      *height = bounds.size.height;
   }
}

bool GetWindowFullBounds(long windowHandle, int* x, int* y, int* width, int* height)
{
   @autoreleasepool {
      NSWindow* window = FindWindow(windowHandle);
      if (!window)
      {
         NSLog(@"Invalid call to GetWindowBounds (window not found)");
         return false;
      }
      int screenHeight = window.screen.frame.size.height;
      NSRect bounds = window.frame;
      *x = bounds.origin.x;
      *y = screenHeight - 1 - bounds.origin.y - bounds.size.height;
      *width = bounds.size.width;
      *height = bounds.size.height;
   }
   return true;
}

bool GiveFocus(long handle)
{
   @autoreleasepool {
      NSView* widget = (__bridge NSView*)handle;
      if (!widget)
      {
         NSLog(@"Invalid call to GiveFocus (widget not found)");
         return false;
      }

      if (![widget.window makeFirstResponder: widget])
      {
         NSLog(@"Failed to make widget first responder");
         return false;
      }
      if ([widget isKindOfClass:[NSTextView class]])
      {
         NSTextView* control = (__bridge NSTextView*)handle;
         [control setSelectedRange: NSMakeRange(0, 0)];
         return true;
      }
      if ([widget isKindOfClass:[NSTextField class]])
      {
         NSTextField* control = (__bridge NSTextField*)handle;
         NSString* currentText = control.stringValue;
         NSText* editor = [control currentEditor];
         if (editor)
         {
            [editor setSelectedRange: NSMakeRange(currentText.length, 0)];
         }
         
         return true;
      }
      return false;
   }
}

bool SaveWidgetImage(long handle, const char* path)
{
   @autoreleasepool {
      NSView* widget = (__bridge NSView*)handle;
      if (!widget)
      {
         NSLog(@"Invalid call to SaveWidgetImage (widget not found)");
         return false;
      }

      NSBitmapImageRep* rep = [widget bitmapImageRepForCachingDisplayInRect:[widget bounds]];
      if (rep == nil)
      {
         NSLog(@"Could not create image of widget");
         return false;
      }
      [widget cacheDisplayInRect:[widget bounds] toBitmapImageRep:rep];

      NSData* data = [rep representationUsingType:NSBitmapImageFileTypePNG properties:@{}];
      if (data == nil)
      {
         NSLog(@"Could not convert image to bitmap");
         return false;
      }

      [data writeToFile:[NSString stringWithUTF8String:path] atomically:YES];
      return true;
   }
}

bool SendMouseEvent(
   long windowHandle,
   MouseButton button,
   MouseEventType type,
   int x,
   int y)
{
   @autoreleasepool {
      // Find parent NSView
      NSWindow* window = FindWindow(windowHandle);
      if (!window)
      {
         NSLog(@"Invalid call to SendMouseEvent (window not found)");
         return false;
      }
      NSView* view = window.contentView;
      // Get event type
      NSEventType nsType = NSEventTypeLeftMouseDown;
      int nbClick = 1;
      switch (button)
      {
         case ButtonLeft:
            if (type == MouseDown)
            {
               nsType = NSEventTypeLeftMouseDown;
            }
            else if (type == MouseUp)
            {
               nsType = NSEventTypeLeftMouseUp;
            }
            else if (type == MouseDblClick)
            {
               nsType = NSEventTypeLeftMouseUp;
               nbClick = 2;
            }
            break;
         case ButtonRight:
            if (type == MouseDown)
            {
               nsType = NSEventTypeRightMouseDown;
            }
            else if (type == MouseUp)
            {
               nsType = NSEventTypeRightMouseUp;
            }
            else if (type == MouseDblClick)
            {
               nsType = NSEventTypeRightMouseUp;
               nbClick = 2;
            }
            break;
      }
      // Get position
      NSPoint coordinates = NSMakePoint(x, y);
      NSEvent* mouseEvent = [NSEvent
         mouseEventWithType: nsType
         location: coordinates
         modifierFlags: 0
         timestamp: 0
         windowNumber: view.window.windowNumber
         context: nil
         eventNumber: 0
         clickCount: nbClick
         pressure: 1.0
      ];

      [NSApp postEvent:mouseEvent atStart:NO];
      return true;
   }
}


bool SendKeyboardEvent(
   long windowHandle,
   KeyEventType type,
   const char text,
   unsigned short keyCode)
{
   @autoreleasepool {
      if (!text && !keyCode)
      {
         NSLog(@"Invalid call to SendKeyboardEvent (no key provided)");
         return false;
      }
      // Find parent NSView
      NSWindow* window = FindWindow(windowHandle);
      if (!window)
      {
         NSLog(@"Invalid call to SendKeyboardEvent (window not found)");
         return false;
      }
      NSView* view = window.contentView;
      // Get event type
      NSEventType nsType = NSEventTypeKeyDown;
      switch (type)
      {
         case KeyUp:
            nsType = NSEventTypeKeyUp;
            break;
         case KeyDown:
            nsType = NSEventTypeKeyDown;
            break;
      }
      
      NSPoint coordinates = NSMakePoint(0, 0);
      NSString* nsText = [NSString stringWithFormat:@"%c", text];
      NSEvent* keyEvent = [NSEvent
         keyEventWithType: nsType
         location: coordinates
         modifierFlags: 0
         timestamp: 0
         windowNumber: view.window.windowNumber
         context: nil
         characters: nsText
         charactersIgnoringModifiers: nsText
         isARepeat: NO
         keyCode: keyCode
      ];
      if (keyEvent == nil)
      {
         NSLog(@"Key event is NULL");
         return false;
      }

      [NSApp postEvent:keyEvent atStart:NO];
      return true;
   }
}
