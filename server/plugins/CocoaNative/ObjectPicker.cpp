// (c) Copyright 2024, Qat’s Authors

#include <ObjectPicker.h>

#include <HighlightOverlay.h>
#include <ToolTip.h>
#include <CocoaWidget.h>
#include <CocoaWindowWrapper.h>

#include <qat-server/Exception.h>
#include <qat-server/IWidget.h>
#include <qat-server/WidgetWrapper.h>

#include <QCoreApplication>
#include <QMouseEvent>

#include <iostream>

namespace Qat::CocoaNativePlugin
{

ObjectPicker::ObjectPicker(QObject* parent) :
   IObjectPicker(parent)
{
   std::cout << "ObjectPicker (native) created" << std::endl;

   mParentWindow = qobject_cast<CocoaWindowWrapper*>(parent);
   if (!mParentWindow)
   {
      throw Exception("Cannot create picker: parent is not a window");
   }
}

ObjectPicker::~ObjectPicker()
{
   SetActivated(false);
}

void ObjectPicker::Reset()
{
   mPickedObject = nullptr;
   mHasNewObject = false;

   emit hasNewObjectChanged();
}

void ObjectPicker::SetActivated(bool activate)
{
   if (activate)
   {
      if (!mParentWindow)
      {
         throw Exception("Cannot activate picker: parent is not a window");
      }
      if (!mHighlightOverlay)
      {
         mHighlightOverlay = new HighlightOverlay();
      }
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         mPixelRatio = 1.0f;
      #else
         mPixelRatio = mParentWindow->GetPixelRatio();
      #endif
      
      const auto windowBounds = mParentWindow->GetWindowBounds();
      mHighlightOverlay->setPosition(windowBounds.topLeft());
      mHighlightOverlay->resize(windowBounds.size());
      mHighlightOverlay->installEventFilter(this);
      mHighlightOverlay->setVisible(mParentWindow->IsEnabled());
      const auto contentHeight = mParentWindow->GetBounds().height();
      mTitleBarHeight = windowBounds.height() - contentHeight;

      if (!mToolTip)
      {
         mToolTip = new ToolTip();
      }
   }
   else
   {
      if (mToolTip)
      {
         mToolTip->setVisible(false);
         delete mToolTip;
         mToolTip = nullptr;
      }
      if (mHighlightOverlay)
      {
         mHighlightOverlay->hide();
         delete mHighlightOverlay.data();
         mHighlightOverlay = nullptr;
      }
   }

   mIsActive = activate;
}

void ObjectPicker::Pause() 
{
   if (mIsActive && mHighlightOverlay)
   {
      mHighlightOverlay->setVisible(false);
   }
}

void ObjectPicker::Restore()
{
   if (mIsActive && mHighlightOverlay)
   {
      mHighlightOverlay->setVisible(true);
   }
}

bool ObjectPicker::eventFilter(QObject*, QEvent* event)
{
   if (!mHighlightOverlay) return false;

   if (event->type() == QEvent::Leave)
   {
      CancelHighLighting();
   }
   else if (event->type() == QEvent::MouseButtonRelease)
   {
      auto* mouseEvent = static_cast<QMouseEvent*>(event);
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         auto coordinates = mouseEvent->localPos().toPoint();
      #else
         auto coordinates = mouseEvent->position().toPoint();
      #endif
      // Allow CTRL key to bypass picker.
      if (mouseEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier))
      {
         return ForwardMouseEvent(mouseEvent);
      }
      const auto deepPick = mouseEvent->modifiers().testFlag(
         Qt::KeyboardModifier::ShiftModifier);

      mPickedObject = GetPickedObject(coordinates, deepPick);
      if (mPickedObject)
      {
         emit objectPicked();
         HighLightObject();

         mHasNewObject = true;
         emit hasNewObjectChanged();
      }
      return true;
   }
   else if (event->type() == QEvent::MouseMove)
   {
      auto* mouseEvent = static_cast<QMouseEvent*>(event);
      // Allow CTRL key to bypass picker.
      if (mouseEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier))
      {
         CancelHighLighting();
         return ForwardMouseEvent(mouseEvent);
      }
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         auto coordinates = mouseEvent->localPos().toPoint();
      #else
         auto coordinates = mouseEvent->position().toPoint();
      #endif
      const auto deepPick = mouseEvent->modifiers().testFlag(
         Qt::KeyboardModifier::ShiftModifier);
      auto* pickedObject = GetPickedObject(coordinates, deepPick);

      mHighlightedObject = pickedObject;
      if (mWidgetWrapper)
      {
         HighLightObject();
      }
      else
      {
         CancelHighLighting();
      }
      return true;
   }
   else if(
      event->type() == QEvent::MouseButtonPress ||
      event->type() == QEvent::MouseButtonDblClick)
   {
      // Allow CTRL key to bypass picker.
      auto* mouseEvent = static_cast<QMouseEvent*>(event);
      if (mouseEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier))
      {
         return ForwardMouseEvent(mouseEvent);
      }
      return true;
   }
   // Forward keyboard events
   else if (
      event->type() == QEvent::KeyPress ||
      event->type() == QEvent::KeyRelease)
   {
      return parent()->event(event);
   }
   return false;
}

void ObjectPicker::HighLightObject()
{
   mIsHighLighted = true;

   if (!mWidgetWrapper)
   {
      if (mToolTip)
      {
         mToolTip->hide();
      }
      return;
   }
   const auto globalPos = mWidgetWrapper->GetBounds().topLeft() / mPixelRatio;
   const auto position = mHighlightOverlay->mapFromGlobal(globalPos);
   QRect widgetRect{position, mWidgetWrapper->GetBounds().size() / mPixelRatio};
   mHighlightOverlay->SetHighlightArea(widgetRect);
   mHighlightOverlay->update();

   // Tooltip text
   if (!mToolTip || !mHighlightedObject) return;
   auto* wrapper = qobject_cast<CocoaWidgetWrapper*>(mHighlightedObject);
   if (!wrapper) return;
   const auto tooltipText = wrapper->GetType().toStdString();
   mToolTip->SetText(tooltipText);
   
   // Tooltip size and position
   const auto bounds = mToolTip->AdjustSize(mHighlightOverlay);
   mToolTip->setPosition({globalPos.x(), globalPos.y() - bounds.height() - 5});
   mToolTip->show();
}

void ObjectPicker::CancelHighLighting()
{
   mIsHighLighted = false;
   mHighlightedObject = nullptr;
   mHighlightOverlay->SetHighlightArea({});
   mHighlightOverlay->update();
   if (mToolTip)
   {
      mToolTip->setVisible(false);
   }
}

CocoaWidgetWrapper* ObjectPicker::GetPickedObject(QPoint mousePosition, [[maybe_unused]] bool deep)
{
   if (!mParentWindow)
   {
      return nullptr;
   }

   // Move to Cocoa flipped coordinates
   QPoint position(mousePosition.x(), mParentWindow->GetHeight() - 1 - mousePosition.y() + mTitleBarHeight);

   // Find child at given position
   auto* child = mParentWindow->GetChildAt(position);
   if (!child) return mParentWindow;

   mWidgetWrapper = std::make_unique<CocoaWidget>(child);
   return QPointer(child);
}

QObject* ObjectPicker::GetCurrentPickedObject() const
{
   if (!mPickedObject)
   {
      return nullptr;
   }
   return mPickedObject;
}

bool ObjectPicker::ForwardMouseEvent(const QMouseEvent* mouseEvent)
{
   if (!mParentWindow) return false;
   CocoaWidget wWidget(mParentWindow);
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      auto coordinates = mouseEvent->localPos();
   #else
      auto coordinates = mouseEvent->position();
   #endif
   const auto localPos = coordinates;
   coordinates = wWidget.MapToGlobal(coordinates);
   QMouseEvent childEvent(
      mouseEvent->type(),
      localPos,
      localPos,
      coordinates,
      mouseEvent->button(),
      mouseEvent->buttons(),
      mouseEvent->modifiers()
   );
   return mParentWindow->event(&childEvent);
}

bool ObjectPicker::ClickAt(int x, int y, bool holdCtrl)
{
   if (!mHighlightOverlay) {
      std::cerr << "Cannot send click: overlay is not shown" << std::endl;
      return false;
   }
   const QPoint globalCoordinates {x, y};
   const auto localCoordinates = mHighlightOverlay->mapFromGlobal(globalCoordinates);
   const auto button = Qt::MouseButton::LeftButton;
   const auto modifiers = holdCtrl ?
      Qt::KeyboardModifiers::enum_type::ControlModifier :
      Qt::KeyboardModifiers::enum_type::NoModifier;
   QMouseEvent pressEvent(
      QEvent::Type::MouseButtonPress,
      localCoordinates,
      localCoordinates,
      globalCoordinates,
      button,
      Qt::MouseButtons().setFlag(button),
      modifiers
   );
   QMouseEvent releaseEvent(
      QEvent::Type::MouseButtonRelease,
      localCoordinates,
      localCoordinates,
      globalCoordinates,
      button,
      Qt::MouseButtons(),
      modifiers
   );
   auto result = qApp->sendEvent(mHighlightOverlay, &pressEvent);
   result &= qApp->sendEvent(mHighlightOverlay, &releaseEvent);
   return result;
}

} // namespace Qat::CocoaNativePlugin