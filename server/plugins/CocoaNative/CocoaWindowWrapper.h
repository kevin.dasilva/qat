// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <CocoaInterface.h>
#include <CocoaWidgetWrapper.h>

#include <QObject>
#include <QRect>

#include <vector>

namespace Qat
{
/// \brief Wraps a native dialog to manage child wrappers' lifecycle.
class CocoaWindowWrapper : public CocoaWidgetWrapper
{
   Q_OBJECT
public:
   /// Factory for window wrappers: return or create wrappers for each currently opened window
   /// \param[in] maxSize Maximum number of wrappers to return
   /// \return A list of CocoaWindowWrapper
   static std::vector<CocoaWindowWrapper*> GetOpenedWindows(int maxSize);

   /// Return the number of currently opened windows
   /// \return the number of currently opened windows
   static int GetNumOpenedWindows();

   /// Default destructor
   ~CocoaWindowWrapper() override = default;

   /// \copydoc CocoaWidgetWrapper::Exists()
   bool Exists() const override;

   /// Return the native type of this window
   /// \return The native type of this window
   QString GetType() const override {return mType;}

   /// Return whether this window is a panel (i.e inherits from NSPanel) or not
   /// \return whether this window is a panel or not
   bool IsPanel() const;

   /// Return the text of this window
   /// \return The text of this window
   QString GetText() const override;

   /// Return the bounds of this window in screen coordinates
   /// \return The bounds of this window in screen coordinates
   QRect GetWindowBounds() const;

   /// Close this window
   /// \return True if the function succeeds, False otherwise
   Q_INVOKABLE bool close();

private:
   /// Constructor
   /// Only called by the factory function GetOpenedWindows()
   /// \param handle Opaque handle of the native Window
   /// \param viewHandle Opaque handle of the contentView of the window
   /// \param type Type name of the window
   CocoaWindowWrapper(WINDOW_HANDLE handle, WIDGET_HANDLE viewHandle, const QString& type);

   /// Type (class name) of this window
   QString mType;
};

} // namespace Qat
