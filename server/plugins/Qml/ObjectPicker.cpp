// (c) Copyright 2023, Qat’s Authors

#include <ObjectPicker.h>
#include <qat-server/Constants.h>
#include <qat-server/ObjectLocator.h>
#include <qat-server/WidgetLocator.h>

#include <QEvent>
#include <QMouseEvent>
#include <QQuickItem>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQmlEngine>
#include <QTimer>
#include <QWindow>

#include <iostream>

namespace
{
constexpr int HIGHLIGHT_DELAY_MS{700};
} // anonymous namespace

namespace Qat::QmlPlugin
{

ObjectPicker::ObjectPicker(QObject* parent) :
   IObjectPicker(parent)
{
   std::cout << "ObjectPicker (QML) created" << std::endl;

   if (parent->isWindowType())
   {
      auto* window = qobject_cast<QWindow*>(parent);
      if (!window)
      {
         return;
      }
      QObject::connect(
         window,
         &QWindow::visibleChanged,
         [window, this]()
         {
            if (this->mIsActive && window->isVisible())
            {
               this->SetActivated(true);
               window->installEventFilter(this);
               std::cout << "ObjectPicker enabled" << std::endl;
            }
            else
            {
               this->SetActivated(false);
               window->removeEventFilter(this);
               std::cout << "ObjectPicker disabled" << std::endl;
            }
         });
   }
}

void ObjectPicker::Reset()
{
   mPickedObject = nullptr;
   mHasNewObject = false;

   emit hasNewObjectChanged();
}

void ObjectPicker::SetActivated(bool activate)
{
   mIsActive = activate;
   if (activate)
   {
      if (mHighlightOverlay)
      {
         mHighlightOverlay->deleteLater();
      }
      // Dynamically create a QML overlay
      QQmlEngine engine;
      auto* overlayComponent = new QQmlComponent(&engine);
      overlayComponent->setData(
         "import QtQuick 2.15;"
         "Rectangle {"
            "color: \"transparent\";"
            "z: 1e9;" // Overlay windows have a Z order superior to 1e6
            "anchors.fill: parent;"
         "}",
         {}
      );
      mHighlightOverlay = qobject_cast<QQuickItem*>(overlayComponent->create());
      mHighlightOverlay->installEventFilter(this);
      mHighlightOverlay->setAcceptHoverEvents(true);

      auto* window = qobject_cast<QWindow*>(parent());
      if (window)
      {
         mHighlightOverlay->setVisible(window->isVisible());
      }
      // Add the object to the root QML object of the window to avoid adding it
      // to any layout
      for (auto* child : parent()->children())
      {
         auto* quickChild = qobject_cast<QQuickItem*>(child);
         if (quickChild)
         {
            mHighlightOverlay->setParentItem(quickChild);
            break;
         }
      }
   }
   else if (mHighlightOverlay)
   {
      delete mHighlightOverlay.data();
      mHighlightOverlay = nullptr;
   }
}

void ObjectPicker::Pause() {}

void ObjectPicker::Restore() {}

bool ObjectPicker::eventFilter(QObject* object, QEvent* event)
{
   if (event->type() == QEvent::MouseButtonRelease)
   {
      try
      {
         auto* mouseEvent = static_cast<QMouseEvent*>(event);
         if (mouseEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier))
         {
            // Allow CTRL key to bypass picker. This is useful to open a combobox
            // and pick an item for example
            event->ignore();
            return false;
         }
         #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            const auto mousePosition = mouseEvent->localPos().toPoint();
         #else
            const auto mousePosition = mouseEvent->position().toPoint();
         #endif
         if (mHighlightOverlay)
         {
            mHighlightOverlay->setVisible(false);
         }
         const auto deepPick = mouseEvent->modifiers().testFlag(
            Qt::KeyboardModifier::ShiftModifier);
         mPickedObject = GetPickedObject(object, mousePosition, deepPick);
         if (mPickedObject)
         {
            if (mHighlightOverlay)
            {
               mHighlightOverlay->setVisible(true);
            }
            emit objectPicked();
            HighLightObject(mPickedObject);

            // Delete the rectangle after a short delay
            QTimer::singleShot(
               HIGHLIGHT_DELAY_MS,
               this,
               SLOT(CancelHighLighting())
            );

            mHasNewObject = true;
            emit hasNewObjectChanged();
         }
      }
      catch (...)
      {
         // Avoid throwing exceptions in Qt thread
         return false;
      }
      return true;
   }
   else if (event->type() == QEvent::HoverMove)
   {
      auto* hoverEvent = static_cast<QHoverEvent*>(event);
      bool newObject = false;
      bool objectFound = false;
      if (mHighlightOverlay)
      {
         mHighlightOverlay->setVisible(false);
      }
      try
      {
         #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            auto coordinates = hoverEvent->pos();
         #else
            auto coordinates = hoverEvent->position().toPoint();
         #endif
         if (object == mHighlightRectangle)
         {
            coordinates = mHighlightRectangle->mapToGlobal(coordinates).toPoint();
            coordinates = mHighlightOverlay->mapFromGlobal(coordinates).toPoint();
         }
         const auto deepPick = hoverEvent->modifiers().testFlag(
            Qt::KeyboardModifier::ShiftModifier);
         auto* pickedObject = GetPickedObject(parent(), coordinates, deepPick);
         objectFound = pickedObject != nullptr;
         if (pickedObject != mHighlightedObject)
         {
            mHighlightedObject = pickedObject;
            newObject = true;
         }
      }
      catch (...)
      {
         std::cerr << "Cannot find highlighted widget" << std::endl;
      }
      if (mHighlightOverlay)
      {
         mHighlightOverlay->setVisible(true);
      }
      if (newObject)
      {
         HighLightObject(mHighlightedObject);
      }
      if (objectFound)
      {
         hoverEvent->accept();
      }
      else
      {
         hoverEvent->ignore();
      }
      return objectFound;
   }
   else if (object == mHighlightRectangle && event->type() == QEvent::HoverLeave)
   {
      CancelHighLighting();
      event->accept();
      return true;
   }
   else if(
      event->type() == QEvent::MouseButtonPress ||
      event->type() == QEvent::MouseButtonDblClick)
   {
      // Allow CTRL key to bypass picker. This is useful to open a combobox
      // and pick an item for example
      auto* mouseEvent = static_cast<QMouseEvent*>(event);
      return !mouseEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier);
   }
   else if (
      event->type() == QEvent::HoverEnter ||
      event->type() == QEvent::HoverMove ||
      event->type() == QEvent::HoverLeave)
   {
      // Allow CTRL key to bypass picker. This is useful to open a combobox
      // and pick an item for example
      auto* hoverEvent = static_cast<QHoverEvent*>(event);
      return !hoverEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier);
   }
   return false;
}

void ObjectPicker::HighLightObject(QObject* object)
{
   auto* quickItem = qobject_cast<QQuickItem*>(object);
   if (quickItem)
   {
      auto* objectContext = qmlContext(quickItem);
      auto* parentItem = quickItem->parentItem();
      while (!objectContext && parentItem)
      {
         objectContext = qmlContext(parentItem);
         parentItem = parentItem->parentItem();
      }
      if (!objectContext)
      {
         std::cerr << "Cannot find a QML context" << std::endl;
      }
      if (objectContext)
      {
         if (mHighlightRectangle)
         {
            mHighlightRectangle->deleteLater();
         }
         mIsHighLighted = true;

         // Dynamically create a QML object
         auto* component = new QQmlComponent(objectContext->engine());
         component->setData(
            "import QtQuick 2.15;"
            "import QtQuick.Controls 2.15;"
            "Rectangle {"
               "id: highlightRectangle;"
               "property string tooltipText;"
               "color: \"yellow\";"
               "opacity: 0.5;"
               "ToolTip {"
                  "parent: highlightRectangle;"
                  "visible: highlightRectangle.visible;"
                  "text: highlightRectangle.tooltipText;"
               "}"
            "}",
            {}
         );

         mHighlightRectangle = qobject_cast<QQuickItem*>(component->create());
         mHighlightRectangle->setParentItem(mHighlightOverlay);
         mHighlightRectangle->setParent(mHighlightOverlay);
         mHighlightRectangle->installEventFilter(this);
         mHighlightRectangle->setAcceptHoverEvents(true);
         mHighlightRectangle->setObjectName(QString::fromStdString(Constants::PICKER_OVERLAY_NAME));

         // Make the rectangle the same size as the picked object
         mHighlightRectangle->setProperty("width", quickItem->width());
         mHighlightRectangle->setProperty("height", quickItem->height());
         // Superimpose the rectangle onto the picked object
         const auto globalPosition = quickItem->mapToItem(mHighlightOverlay, { 0, 0 });
         mHighlightRectangle->setProperty("x", globalPosition.x());
         mHighlightRectangle->setProperty("y", globalPosition.y());
         std::string type = quickItem->metaObject()->className();
         type = ObjectLocator::FormatType(type);
         const auto objectName = quickItem->objectName().toStdString();
         auto tooltipText = type;
         if (!objectName.empty())
         {
            tooltipText += ": " + objectName;
         }
         mHighlightRectangle->setProperty("tooltipText", tooltipText.c_str());
      }
   }
}

void ObjectPicker::CancelHighLighting()
{
   if (mHighlightRectangle)
   {
      mHighlightRectangle->deleteLater();
      mIsHighLighted = false;
   }
   mHighlightedObject = nullptr;
}


QObject* ObjectPicker::GetPickedObject(QObject* object, QPoint mousePosition, bool deep)
{
   object = Qat::WidgetLocator::FindWidget(object, mousePosition);
   if (!object)
   {
      return nullptr;
   }
   auto* quickItem = qobject_cast<QQuickItem*>(object);
   if (!quickItem)
   {
      return object;
   }

   // Make sure the object is in the current window
   auto* parentObject = object;
   while (parentObject && !parentObject->isWindowType())
   {
      parentObject = parentObject->parent();
   }
   if (parentObject && parentObject->isWindowType() && parentObject != parent())
   {
      return nullptr;
   }

   if (!deep && object)
   {
      const auto widgetSize = quickItem->size().toSize();
      auto* parent = quickItem->parentItem();
      while (parent && parent->size().toSize() == widgetSize)
      {
         object = parent;
         parent = parent->parentItem();
         if (!parent || parent->isWindowType())
         {
            break;
         }
      }
   }

   quickItem = qobject_cast<QQuickItem*>(object);
   if (!quickItem)
   {
      return object;
   }
   // Make sure the object is an item accessible with the API
   // i.e with a valid QML context.
   auto* objectContext = qmlContext(quickItem);
   while (!objectContext && quickItem)
   {
      quickItem = quickItem->parentItem();
      objectContext = qmlContext(quickItem);
   }

   return quickItem;
}

} // namespace Qat::QmlPlugin