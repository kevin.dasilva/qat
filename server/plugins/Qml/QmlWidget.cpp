// (c) Copyright 2023, Qat’s Authors

#include <QmlWidget.h>

#include <QQmlContext>
#include <QQuickItem>
#include <QQuickItemGrabResult>
#include <QQuickWindow>

#include <iostream>

namespace Qat
{
QmlWidget::QmlWidget(QQuickItem* quickItem) :
   mQuickItem(quickItem)
{
}

QObject* QmlWidget::GetQtObject() const
{
   return mQuickItem;
}

std::string QmlWidget::GetId() const
{
   const auto* objectContext = qmlContext(mQuickItem);
   if (objectContext)
   {
      return objectContext->nameForObject(mQuickItem).toStdString();
   }
   return "";
}

QObject* QmlWidget::GetParent() const
{
   return mQuickItem->parentItem();
}

std::vector<QObject*> QmlWidget::GetChildWidgets() const
{
   const auto childItems = mQuickItem->childItems();
   std::vector<QObject*> children(childItems.size());
   std::copy(childItems.cbegin(), childItems.cend(), children.begin());
   return children;
}

QAbstractItemModel* QmlWidget::GetModel() const
{
   return nullptr;
}

QItemSelectionModel* QmlWidget::GetSelectionModel() const
{
   return nullptr;
}

QWindow* QmlWidget::GetWindow() const
{
   return mQuickItem->window();
}

QPointF QmlWidget::MapToGlobal(const QPointF &point) const
{
   return mQuickItem->mapToGlobal(point);
}

QPointF QmlWidget::MapFromGlobal(const QPointF &point) const
{
   return mQuickItem->mapFromGlobal(point);
}

QPointF QmlWidget::MapToScene(const QPointF &point) const
{
   return mQuickItem->mapToScene(point);
}

QPointF QmlWidget::MapToWidget(const IWidget* widget, const QPointF &point) const
{
   if (!widget)
   {
      std::cerr << "Cannot map coordinates: widget is null" << std::endl;
      return point;
   }
   const auto item = qobject_cast<QQuickItem*>(widget->GetQtObject());
   if (!item)
   {
      std::cerr << "Cannot map coordinates: widget is not a QQuickItem" << std::endl;
      return point;
   }
   return mQuickItem->mapToItem(item, point);
}

bool QmlWidget::Contains(const QPointF &point) const
{
   return mQuickItem->contains(point);
}

QSizeF QmlWidget::GetSize() const
{
   return mQuickItem->size();
}

qreal QmlWidget::GetWidth() const
{
   return mQuickItem->width();
}

qreal QmlWidget::GetHeight() const
{
   return mQuickItem->height();
}

QRect QmlWidget::GetBounds() const
{
   auto globalPosition = mQuickItem->mapToGlobal(QPointF(0, 0)).toPoint();
   QRect bounds(globalPosition, mQuickItem->size().toSize());
   return bounds;
}

float QmlWidget::GetPixelRatio() const
{
   #if QT_VERSION < QT_VERSION_CHECK(6, 7, 0)
      return 1.0f;
   #else
      const auto* window = mQuickItem->window();
      if (!window) return 1.0f;
      return window->devicePixelRatio();
   #endif
}

qreal QmlWidget::GetZ() const
{
   return mQuickItem->z();
}

bool QmlWidget::IsVisible() const
{
   return mQuickItem->isVisible();
}

void QmlWidget::ForceActiveFocus(Qt::FocusReason reason) const
{
   mQuickItem->forceActiveFocus(reason);
}


void QmlWidget::SetFocus(bool focus, Qt::FocusReason reason) const
{
   mQuickItem->setFocus(focus, reason);
}

void QmlWidget::GrabImage(std::function<void(const QImage&)> callback) const
{
   const auto grabResult = mQuickItem->grabToImage();
   auto* connection = new QMetaObject::Connection;
   *connection = QObject::connect(
      grabResult.get(),
      &QQuickItemGrabResult::ready,
      [grabResult, connection, callback]()
      {
         callback(grabResult->image());
         QObject::disconnect(*connection);
      });
}
}