// (c) Copyright 2023, Qat’s Authors
#pragma once 

#include <qat-server/IWidget.h>

#include <QPointer>

class QQuickItem;

namespace Qat
{
class QmlWidget : public IWidget
{
public:
   /// Constructor
   /// \param[in] quickItem The QQuickItem instance to wrap
   explicit QmlWidget(QQuickItem* quickItem);

   /// Default destructor
   ~QmlWidget() override = default;

   /// \copydoc IWidget::GetQtObject
   QObject* GetQtObject() const override;

   /// \copydoc IWidget::GetId
   std::string GetId() const override;

   /// \copydoc IWidget::GetParent
   QObject* GetParent() const override;

   /// \copydoc IWidget::GetChildWidgets
   std::vector<QObject*> GetChildWidgets() const override;

   /// \copydoc IWidget::GetModel
   QAbstractItemModel* GetModel() const override;

   /// \copydoc IWidget::GetSelectionModel
   QItemSelectionModel* GetSelectionModel() const override;
   
   /// \copydoc IWidget::GetWindow
   QWindow* GetWindow() const override;

   /// \copydoc IWidget::MapToGlobal
   QPointF MapToGlobal(const QPointF &point) const override;

   /// \copydoc IWidget::MapFromGlobal
   QPointF MapFromGlobal(const QPointF &point) const override;

   /// \copydoc IWidget::MapToScene
   QPointF MapToScene(const QPointF &point) const override;

   /// \copydoc IWidget::MapToWidget
   QPointF MapToWidget(const IWidget* widget, const QPointF &point) const override;

   /// \copydoc IWidget::Contains
   bool Contains(const QPointF &point) const override;

   /// \copydoc IWidget::GetSize
   QSizeF GetSize() const override;

   /// \copydoc IWidget::GetWidth
   qreal GetWidth() const override;

   /// \copydoc IWidget::GetHeight
   qreal GetHeight() const override;
   
   /// \copydoc IWidget::GetBounds
   QRect GetBounds() const override;
   
   /// \copydoc IWidget::GetPixelRatio
   float GetPixelRatio() const override;

   /// \copydoc IWidget::GetZ
   qreal GetZ() const override;

   /// \copydoc IWidget::IsVisible
   bool IsVisible() const override;

   /// \copydoc IWidget::ForceActiveFocus
   void ForceActiveFocus(Qt::FocusReason reason) const override;

   /// \copydoc IWidget::SetFocus
   void SetFocus(bool focus, Qt::FocusReason reason) const override;

   /// \copydoc IWidget::GrabImage
   void GrabImage(std::function<void(const QImage&)> callback) const override;

private:
   /// Underlying QQuickItem
   QPointer<QQuickItem> mQuickItem;
};

}