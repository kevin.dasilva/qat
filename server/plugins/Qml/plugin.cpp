// (c) Copyright 2023, Qat’s Authors

#include <QmlWidget.h>
#include <ObjectPicker.h>

#include <QGuiApplication>
#include <QImage>
#include <QObject>
#include <QQuickItem>
#include <QQuickWindow>

#include <iostream>
#include <memory>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #define DLL_EXPORT _declspec(dllexport)
#elif defined(__linux__) || defined(__APPLE__)
   #define DLL_EXPORT
#else
#   error "Unsupported platform"
#endif

extern "C"
{
DLL_EXPORT Qat::IWidget* CastObject(QObject* qtobject)
{
   if (!qtobject)
   {
      return nullptr;
   }
   // Handle root windows
   const auto* window = qobject_cast<const QQuickWindow*>(qtobject);
   if (window)
   {
      qtobject = window->contentItem();
   }
   auto* item = qobject_cast<QQuickItem*>(qtobject);
   if (item)
   {
      return new Qat::QmlWidget(item);
   }
   return nullptr;
}

DLL_EXPORT bool GetTopWindows(QObject** windowsArray, unsigned int* size)
{
   const auto list = qApp->allWindows();
   if (!size)
   {
      std::cerr << "Invalid call to GetTopWindows()" << std::endl;
      return false;
   }
   if (*size == 0)
   {
      *size = 0;
      for (auto i = 0; i < list.size(); ++i)
      {
         auto* qwindow = qobject_cast<QQuickWindow*>(list[i]);
         if (qwindow)
         {
            ++(*size);
         }
      }
      return true;
   }
   if (!windowsArray)
   {
      std::cerr << "Invalid call to GetTopWindows()" << std::endl;
      return false;
   }
   for (auto i = 0; i < list.size(); ++i)
   {
      auto* qwindow = qobject_cast<QQuickWindow*>(list[i]);
      if (qwindow && i < (int)*size)
      {
         windowsArray[i] = list[i];
      }
   }
   return true;
}

DLL_EXPORT QImage* GrabImage(QObject* window)
{
   auto* qwindow = qobject_cast<QQuickWindow*>(window);
   if (!qwindow || !qwindow->isVisible())
   {
      return nullptr;
   }

   return new QImage(qwindow->grabWindow());
}

DLL_EXPORT Qat::IObjectPicker* CreatePicker(QObject* window)
{
   auto* qwindow = qobject_cast<QQuickWindow*>(window);
   if (!qwindow)
   {
      return nullptr;
   }

   return new Qat::QmlPlugin::ObjectPicker(window);
}

}