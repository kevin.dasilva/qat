// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <WindowsWidgetWrapper.h>

namespace Qat
{
/// \brief Wraps a native dialog to manage child wrappers' lifecycle.
class WindowsWindowWrapper : public WindowsWidgetWrapper
{
   Q_OBJECT
public:
   /// Factory for dialog wrappers: return or create wrappers for each currently opened dialog
   /// \return A list of WindowsWindowWrapper
   static std::vector<WindowsWindowWrapper*> GetOpenedWindows();

   /// Default destructor
   ~WindowsWindowWrapper() override = default;

   /// Close this dialog
   /// \return True if the function succeeds, False otherwise
   Q_INVOKABLE bool close();

private:
   /// Constructor
   /// Only called by the factory function GetOpenedWindows()
   /// \param handle Windows handle of the native Window
   explicit WindowsWindowWrapper(HWND handle);
};

} // namespace Qat
