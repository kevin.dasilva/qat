// (c) Copyright 2024, Qat’s Authors

#include <HighlightOverlay.h>

#include <QPainter>

namespace Qat::WindowsNativePlugin
{

HighlightOverlay::HighlightOverlay() :
   QRasterWindow(nullptr)
{
   // Configure flags to display an overlay window
   // without frame nor shadow and not appearing in the taskbar
   setFlag(Qt::FramelessWindowHint);
   setFlag(Qt::WindowStaysOnTopHint);
   setFlag(Qt::Tool);
   setFlag(Qt::NoDropShadowWindowHint);

   setModality(Qt::ApplicationModal);

   setOpacity(0.5);
}

void HighlightOverlay::SetHighlightArea(const QRect& area)
{
   mHighlightArea = area;
}

void HighlightOverlay::paintEvent(QPaintEvent*)
{
   QPainter painter(this);
   QRect allArea{0, 0, width(), height()};
   painter.eraseRect(allArea);
   
   if (!mHighlightArea.isValid() || mHighlightArea.isNull())
   {
      setOpacity(0.01);
      return;
   }
   setOpacity(0.5);
   painter.fillRect(mHighlightArea, Qt::yellow);
}

} // namespace Qat::WindowsNativePlugin