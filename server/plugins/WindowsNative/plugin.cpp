// (c) Copyright 2024, Qat’s Authors

#include <WindowsWidget.h>
#include <WindowsWidgetWrapper.h>
#include <WindowsWindowWrapper.h>
#include <ObjectPicker.h>

#include <qat-server/IObjectPicker.h>

#include <QGuiApplication>
#include <QImage>
#include <QObject>

#include <Windows.h>

#include <iostream>
#include <map>
#include <memory>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #define DLL_EXPORT _declspec(dllexport)
#else
#   error "Unsupported platform"
#endif


extern "C"
{
DLL_EXPORT Qat::IWidget* CastObject(QObject* qtobject)
{
   if (!qtobject)
   {
      return nullptr;
   }

   auto* nativeObject = qobject_cast<Qat::WindowsWidgetWrapper*>(qtobject);
   if (nativeObject)
   {
      return new Qat::WindowsWidget(nativeObject);
   }

   return nullptr;
}

DLL_EXPORT bool GetTopWindows(QObject** windowsArray, unsigned int* size)
{
   if (!size)
   {
      std::cerr << "Invalid call to GetTopWindows()" << std::endl;
      return false;
   }
   const auto list = Qat::WindowsWindowWrapper::GetOpenedWindows();
   if (*size == 0)
   {
      *size = static_cast<unsigned int>(list.size());
      return true;
   }
   if (!windowsArray)
   {
      std::cerr << "Invalid call to GetTopWindows()" << std::endl;
      return false;
   }
   memset(windowsArray, 0, *size * sizeof(windowsArray[0]));
   for (auto i = 0; i < list.size(); ++i)
   {
      if (i < (int)*size)
      {
         windowsArray[i] = list[i];
      }
   }
   return true;
}

DLL_EXPORT QImage* GrabImage(QObject*)
{
   return nullptr;
}

DLL_EXPORT Qat::IObjectPicker* CreatePicker(QObject* window)
{
   if (!window)
   {
      return nullptr;
   }

   auto* windowWrapper = qobject_cast<Qat::WindowsWindowWrapper*>(window);
   if (!windowWrapper)
   {
      return nullptr;
   }

   return new Qat::WindowsNativePlugin::ObjectPicker(windowWrapper);
}

}