// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QRasterWindow>

#include <QFont>
#include <QRect>

#include <string>

class QPaintDevice;
class QPaintEvent;

namespace Qat::WindowsNativePlugin
{

/// \brief Class implementing a custom tooltip as a QRasterWindow.
class ToolTip: public QRasterWindow
{
public:
   /// Default constructor
   ToolTip();

   /// Default destructor
   ~ToolTip() override = default;

   /// Set the text to be displayed on the tooltip
   /// \param[in] text Text to be displayed on the tooltip
   void SetText(const std::string& text);

   /// Resize this tooltip based on its current text and font
   /// \param paintDevice An active painting device, used to create a QPainter object
   /// \return The computed bounds of the tooltip
   QRect AdjustSize(QPaintDevice* paintDevice);

   /// Return the font used by this tooltip
   /// \return The font used by this tooltip
   QFont GetFont() const;

protected:
   /// \copydoc QRasterWindow::paintEvent
   void paintEvent(QPaintEvent*) override;

private:
   /// Text to be displayed on the tooltip
   QString mText;
};

} // namespace Qat::WindowsNativePlugin