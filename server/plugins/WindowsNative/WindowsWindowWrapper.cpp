// (c) Copyright 2024, Qat’s Authors

#include <WindowsWindowWrapper.h>

#include <QPointer>

#include <map>

namespace
{
/// Container for all window wrappers
static std::map<HWND, QPointer<Qat::WindowsWindowWrapper>> windows;

/// \brief Return the list of handles to the currently opened native windows.
/// \return The list of handles to the currently opened native windows.
std::vector<HWND> FindTopWindows()
{
   std::vector<HWND> windowsHandles;
   HWND handle = NULL;
   const auto currentPid = GetCurrentProcessId();
   for (;;)
   {
      handle = FindWindowExA(NULL, handle, NULL, NULL);
      if (!handle) break;

      // Ignore windows owned by other processes
      DWORD pid;
      GetWindowThreadProcessId(handle, &pid);
      if(currentPid != pid) continue;
      
      if (IsWindowVisible(handle))
      {
         windowsHandles.push_back(handle);
      }
   }
   return windowsHandles;
}
}

namespace Qat
{

std::vector<WindowsWindowWrapper*> WindowsWindowWrapper::GetOpenedWindows()
{
   std::vector<WindowsWindowWrapper*> wrappers;
   const auto windowsHandles = FindTopWindows();

   // Get wrappers for all opened windows
   for (const auto handle : windowsHandles)
   {
      const auto it = windows.find(handle);
      if (it == windows.cend() || !it->second)
      {
         windows[handle] = new WindowsWindowWrapper(handle);
      }
      wrappers.push_back(windows[handle]);
   }

   // Delete obsolete wrappers for closed windows
   std::vector<HWND> obsoleteHandles;
   for (const auto& window : windows)
   {
      const auto it = std::find(windowsHandles.cbegin(), windowsHandles.cend(), window.first);
      if (it == windowsHandles.cend())
      {
         obsoleteHandles.push_back(window.first);
         window.second->deleteLater();
      }
   }
   for (const auto& handle : obsoleteHandles)
   {
      windows.erase(handle);
   }

   return wrappers;
}

WindowsWindowWrapper::WindowsWindowWrapper(HWND handle) :
   WindowsWidgetWrapper(nullptr, handle)
{
}

bool WindowsWindowWrapper::close()
{
   return TRUE == PostMessage(mHandle, WM_SYSCOMMAND, SC_CLOSE, 0);
}

} // namespace Qat