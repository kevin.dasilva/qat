// (c) Copyright 2023, Qat’s Authors

#include <WindowsWidget.h>
#include <WindowsWidgetWrapper.h>

#include <qat-server/Exception.h>

#include <QObject>

#include <iostream>
#include <filesystem>
#include <sstream>

namespace
{

/// Return the path to the current temporary folder
/// \return The path to the current temporary folder
std::string GetTempPath()
{
   char* pValue;
   size_t len;
   const auto rc = _dupenv_s(&pValue, &len, "TEMP");
   if (rc || !pValue || !len)
   {
      return "";
   }

   std::string value = pValue;
   free(pValue);
   return value;
}

/// Save a memory image to the given file in BMP format.
/// \note This function comes from this example: https://learn.microsoft.com/en-us/windows/win32/gdi/capturing-an-image
/// \param hdcWidget Display device context of a widget
/// \param hBitmap Bitmap data
/// \param fileName Destination file name
void SaveBitmap(HDC hdcWidget, HBITMAP hBitmap, const std::string& fileName)
{
   // Create a BITMAP header from the given hBitmap.
   BITMAP bitmapHeader;
   GetObject(hBitmap, sizeof(BITMAP), &bitmapHeader);
   // Create bitmap info header
   BITMAPINFOHEADER infoHeader;
   infoHeader.biSize = sizeof(BITMAPINFOHEADER);
   infoHeader.biWidth = bitmapHeader.bmWidth;
   infoHeader.biHeight = bitmapHeader.bmHeight;
   infoHeader.biPlanes = 1;
   infoHeader.biBitCount = 32;
   infoHeader.biCompression = BI_RGB;
   infoHeader.biSizeImage = 0;
   infoHeader.biXPelsPerMeter = 0;
   infoHeader.biYPelsPerMeter = 0;
   infoHeader.biClrUsed = 0;
   infoHeader.biClrImportant = 0;

   // Allocate buffer for image data
   const auto bitmapDataSize = ((bitmapHeader.bmWidth * infoHeader.biBitCount + 31) / 32) * 4 * bitmapHeader.bmHeight;
   const auto imageDataBuffer = GlobalAlloc(GHND, bitmapDataSize);
   const auto rawData = (char*)GlobalLock(imageDataBuffer);
   // Copy image data to buffer
   GetDIBits(
      hdcWidget,
      hBitmap,
      0,
      static_cast<UINT>(bitmapHeader.bmHeight),
      rawData,
      reinterpret_cast<BITMAPINFO*>(&infoHeader),
      DIB_RGB_COLORS);

   // Create a file to store the bitmap image
   const auto fileHandle = CreateFileA(
      fileName.c_str(),
      GENERIC_WRITE,
      0,
      NULL,
      CREATE_ALWAYS,
      FILE_ATTRIBUTE_NORMAL,
      NULL);

   // Create the file header
   BITMAPFILEHEADER fileHeader;
   // Offset to where the actual bitmap data start
   fileHeader.bfOffBits = static_cast<DWORD>(sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER));
   // Total size of the file (data + header)
   const auto totalSize = bitmapDataSize + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
   fileHeader.bfSize = static_cast<DWORD>(totalSize);
   fileHeader.bfType = 0x4D42; // Value BM is mandatory for Bitmaps

   // Write the file
   DWORD nbBytesWritten = 0;
   WriteFile(fileHandle, (LPSTR)&fileHeader, sizeof(BITMAPFILEHEADER), &nbBytesWritten, NULL);
   WriteFile(fileHandle, (LPSTR)&infoHeader, sizeof(BITMAPINFOHEADER), &nbBytesWritten, NULL);
   WriteFile(fileHandle, (LPSTR)rawData, bitmapDataSize, &nbBytesWritten, NULL);

   // Free resources
   GlobalUnlock(imageDataBuffer);
   GlobalFree(imageDataBuffer);
   CloseHandle(fileHandle);
}

} // anonymous namespace

namespace Qat
{
WindowsWidget::WindowsWidget(Qat::WindowsWidgetWrapper* qtobject) :
   mQtobject{qtobject},
   mHandle{qtobject->GetHandle()}
{
   if (!qtobject)
   {
      throw Exception("Cannot create WindowsWidget: wrapper is NULL");
   }
}

QObject* WindowsWidget::GetQtObject() const
{
   return mQtobject;
}

std::string WindowsWidget::GetId() const
{
   std::stringstream builder;
   auto text = mQtobject->GetText().toStdString();
   if (text.empty())
   {
      text = "?";
   }
   builder << text << " " << mQtobject->GetType().toStdString();
   return builder.str();
}

QObject* WindowsWidget::GetParent() const
{
   return mQtobject->parent();
}

std::vector<QObject*> WindowsWidget::GetChildWidgets() const
{
   std::vector<QObject*> children;
   const auto qChildren = mQtobject->GetChildren();
   children.insert(children.end(), qChildren.cbegin(), qChildren.cend());
   return children;
}

QAbstractItemModel* WindowsWidget::GetModel() const
{
   // Not applicable
   return nullptr;
}

QItemSelectionModel* WindowsWidget::GetSelectionModel() const
{
   // Not applicable
   return nullptr;
}

QWindow* WindowsWidget::GetWindow() const
{
   // Must return nullptr: events are handled by the underlying WindowsWidgetWrapper.
   return nullptr;
}

QPointF WindowsWidget::MapToGlobal(const QPointF &point) const
{
   const auto intPoint = point.toPoint();
   POINT winPoint {intPoint.x(), intPoint.y()};
   if (!MapWindowPoints(mHandle, HWND_DESKTOP, &winPoint, 1))
   {
      std::cerr << "Unable to map point to global coordinates" << std::endl;
      return point;
   }

   return {
      static_cast<qreal>(winPoint.x),
      static_cast<qreal>(winPoint.y)
   };
}

QPointF WindowsWidget::MapFromGlobal(const QPointF &point) const
{
   const auto intPoint = point.toPoint();
   POINT winPoint {intPoint.x(), intPoint.y()};
   if (!MapWindowPoints(HWND_DESKTOP, mHandle, &winPoint, 1))
   {
      std::cerr << "Unable to map point from global coordinates" << std::endl;
      return point;
   }

   return {
      static_cast<qreal>(winPoint.x),
      static_cast<qreal>(winPoint.y)
   };
}

QPointF WindowsWidget::MapToScene(const QPointF &point) const
{
   // Not used
   return (point);
}

QPointF WindowsWidget::MapToWidget(const IWidget* widget, const QPointF& point) const
{
   if (!widget)
   {
      throw Exception("MapToWidget failed: target widget is NULL");
   }
   const auto* wrapper = qobject_cast<const WindowsWidgetWrapper*>(widget->GetQtObject());
   if (!wrapper)
   {
      throw Exception("MapToWidget failed: target widget is not a Windows widget");
   }
   const auto intPoint = point.toPoint();
   POINT winPoint {intPoint.x(), intPoint.y()};
   if (!MapWindowPoints(mHandle, wrapper->GetHandle(), &winPoint, 1))
   {
      std::cerr << "Unable to map point to global coordinates" << std::endl;
      return point;
   }

   return {
      static_cast<qreal>(winPoint.x),
      static_cast<qreal>(winPoint.y)
   };
}

bool WindowsWidget::Contains(const QPointF& point) const
{
   const auto x = point.x();
   const auto y = point.y();
   const auto bounds = mQtobject->GetBounds();
   return x >= 0 && x < bounds.width() && y >= 0 && y < bounds.height();
}

QSizeF WindowsWidget::GetSize() const
{
   const auto bounds = mQtobject->GetBounds();
   return {
      bounds.width(),
      bounds.height()
   };
}

qreal WindowsWidget::GetWidth() const
{
   return mQtobject->GetWidth();
}

qreal WindowsWidget::GetHeight() const
{
   return mQtobject->GetHeight();
}

QRect WindowsWidget::GetBounds() const
{
   return mQtobject->GetBounds().toRect();
}

float WindowsWidget::GetPixelRatio() const
{  
   return mQtobject->GetPixelRatio();
}

qreal WindowsWidget::GetZ() const
{
   return 0;
}

bool WindowsWidget::IsVisible() const
{
   return mQtobject->IsVisible();
}

void WindowsWidget::ForceActiveFocus(Qt::FocusReason) const
{
}

void WindowsWidget::SetFocus(bool, Qt::FocusReason) const
{
}

void WindowsWidget::GrabImage(std::function<void(const QImage&)> callback) const
{
   // Get display device context for the screen
   const auto hdcScreen = GetDC(NULL);
   // Get display device context for the widget
   const auto hdcWidget = GetDC(mHandle);

   if (!hdcScreen || !hdcWidget)
   {
      throw Exception("Cannot take screenshot: no display device context available");
   }
   
   // Do not use mQtobject->GetBounds() to avoid rounding issues
   RECT rect;
   if (!GetWindowRect(mHandle, &rect))
   {
      throw Exception("Cannot take screenshot: unable to get widget bounds");
   }
   const auto clientRect = QRect{
      rect.left,
      rect.top,
      std::abs(rect.right - rect.left),
      std::abs(rect.top - rect.bottom)
   };
   // Create a memory device context compatible with the display device of the widget
   const auto memoryDC = CreateCompatibleDC(hdcWidget);
   if (!memoryDC)
   {
      throw Exception("Cannot take screenshot: failed to create a memory device context");
   }

   const auto hBitmap = CreateCompatibleBitmap(hdcWidget, clientRect.width(), clientRect.height());
   if (!hBitmap)
   {
      throw Exception("Cannot take screenshot: failed to create a bitmap");
   }

   // Transfer image data from display context to memory context
   SelectObject(memoryDC, hBitmap);
   if (!BitBlt(
      memoryDC,
      0, 0, clientRect.width(), clientRect.height(),
      hdcScreen,
      clientRect.topLeft().x(), clientRect.topLeft().y(),
      SRCCOPY))
   {
      throw Exception("Cannot take screenshot: failed to copy image data");
   }

   // Generate a temporary bmp file
   std::filesystem::path tempPath = GetTempPath();
   const auto filename = "qat_temp_screenshot_data_" + std::to_string((unsigned long long)mQtobject->GetHandle()) + ".png";
   tempPath = tempPath / filename;
   SaveBitmap(hdcWidget, hBitmap, tempPath.string());
   if (!std::filesystem::exists(tempPath))
   {
      throw Exception("Cannot take screenshot: failed to create temporary file");
   }

   // Convert to QImage
   QImage screenshot{QString::fromStdString(tempPath.string())};
   callback(screenshot);
   
   // Free resources
   std::error_code ec;
   if (!std::filesystem::remove(tempPath, ec))
   {
      std::cerr << "Could not delete temporary file" << std::endl;
   }
   DeleteObject(hBitmap);
   DeleteObject(memoryDC);
   ReleaseDC(NULL, hdcScreen);
   ReleaseDC(mHandle, hdcWidget);
}

} // namespace Qat