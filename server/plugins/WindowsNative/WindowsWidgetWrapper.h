// (c) Copyright 2024, Qat’s Authors
#pragma once 

#include <qat-server/INativeWidget.h>

#include <QObject>
#include <QPointer>

#include <Windows.h>

#include <map>

// Forward declarations
class QKeyEvent;
class QMouseEvent;

namespace Qat
{
/// \brief Wraps windows native widget (Window) into a Qt object to expose its
/// properties in the Qt Meta Object Framework.
class WindowsWidgetWrapper : public INativeWidget
{
   Q_OBJECT
   Q_PROPERTY(
      bool visible
      READ IsVisible
   )
   Q_PROPERTY(
      bool enabled
      READ IsEnabled
   )
   Q_PROPERTY(
      QString type
      READ GetType
   )
   Q_PROPERTY(
      QString baseClass
      READ GetClass
   )
   Q_PROPERTY(
      QString text
      READ GetText
   )
   Q_PROPERTY(
      QString handle
      READ GetHandleHex
      CONSTANT
   )
   Q_PROPERTY(
      int width
      READ GetWidth
   )
   Q_PROPERTY(
      int height
      READ GetHeight
   )
public:
   /// Constructor
   /// \param[in] parent Qt parent (for lifecycle)
   /// \param[in] handle Native handle to the underlying widget
   WindowsWidgetWrapper(QObject* parent, HWND handle);

   /// Default destructor
   ~WindowsWidgetWrapper() override = default;

   /// Return the handle to the underlying widget
   /// \return the handle to the underlying widget
   HWND GetHandle() const;
   
   /// Return the list of current children.
   /// \note Create one child object per child window handle
   /// \return the list of current children
   QObjectList GetChildren();
   
   /// Return the child widget with the given handle.
   /// \param[in] handle The native handle of the child widget.
   /// \return the child widget with the given handle or NULL if not found
   WindowsWidgetWrapper* GetChild(HWND handle);

   /// Return whether this widget is visible or not
   /// \return True if this widget is visible, false otherwise
   bool IsVisible() const;

   /// Return whether this widget is enabled or not
   /// \return True if this widget is enabled, false otherwise
   bool IsEnabled() const;

   /// Return the native type (real class name) of this widget
   /// \return The native type (real class name) of this widget
   QString GetType() const;

   /// Return the native class name of this widget
   /// \return The native class name of this widget
   QString GetClass() const;

   /// Return the text of this widget
   /// \return The text of this widget
   QString GetText() const;

   /// Return the handle of this widget as a hexadecimal string
   /// \return The handle of this widget as a hexadecimal string
   QString GetHandleHex() const;

   /// Return the bounds of this widget in screen coordinates
   /// \return The bounds of this widget
   QRectF GetBounds() const;

   /// Return the scaled bounds of this widget in screen coordinates,
   /// i.e. the bounds with screen scaling applied.
   /// \return The scaled bounds of this widget
   QRectF GetScaledBounds() const;

   /// Return the width of this widget in pixels
   /// \return The width of this widget
   int GetWidth() const;

   /// Return the height of this widget in pixels
   /// \return The height of this widget
   int GetHeight() const;

   /// Return the pixel ratio for this widget,
   /// i.e the DPI scaling (current DPI / default DPI (=96))
   /// \return the pixel ratio for this widget
   float GetPixelRatio() const;

   /// \copydoc QObject::event() 
   bool event(QEvent* event) override;

   /// Handle the given mouse event
   /// \param mouseEvent Any mouse event
   /// \return True if event was properly handled, False otherwise
   bool HandleMouseEvent(const QMouseEvent* mouseEvent);

   /// Handle the given keyboard event
   /// \param keyEvent Any keyboard event
   /// \return True if event was properly handled, False otherwise
   bool HandleKeyboardEvent(const QKeyEvent* keyEvent);

protected:
   /// Handle to the underlying widget
   HWND mHandle;

private:
   /// Children wrappers mapped to their native handles
   std::map<HWND, QPointer<WindowsWidgetWrapper>> mChildren;
};
} // namespace Qat

