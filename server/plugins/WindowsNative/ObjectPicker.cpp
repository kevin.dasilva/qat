// (c) Copyright 2024, Qat’s Authors

#include <ObjectPicker.h>

#include <HighlightOverlay.h>
#include <ToolTip.h>
#include <WindowsWidget.h>
#include <WindowsWindowWrapper.h>

#include <qat-server/Exception.h>
#include <qat-server/IWidget.h>
#include <qat-server/WidgetWrapper.h>

#include <QMouseEvent>

#include <iostream>

namespace Qat::WindowsNativePlugin
{

ObjectPicker::ObjectPicker(QObject* parent) :
   IObjectPicker(parent)
{
   std::cout << "ObjectPicker (native) created" << std::endl;

   mParentWindow = qobject_cast<WindowsWindowWrapper*>(parent);
   if (!mParentWindow)
   {
      throw Exception("Cannot create picker: parent is not a window");
   }
}

ObjectPicker::~ObjectPicker()
{
   SetActivated(false);
}

void ObjectPicker::Reset()
{
   mPickedObject = nullptr;
   mHasNewObject = false;

   emit hasNewObjectChanged();
}

void ObjectPicker::SetActivated(bool activate)
{
   if (activate)
   {
      if (!mParentWindow)
      {
         throw Exception("Cannot activate picker: parent is not a window");
      }
      if (!mHighlightOverlay)
      {
         mHighlightOverlay = new HighlightOverlay();
      }
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         mPixelRatio = 1.0f;
      #else
         mPixelRatio = mParentWindow->GetPixelRatio();
      #endif
      
      mHighlightOverlay->setPosition(mParentWindow->GetScaledBounds().topLeft().toPoint());
      mHighlightOverlay->resize(mParentWindow->GetScaledBounds().size().toSize());
      mHighlightOverlay->installEventFilter(this);
      mHighlightOverlay->setVisible(mParentWindow->IsEnabled());

      if (!mToolTip)
      {
         mToolTip = new ToolTip();
      }
   }
   else
   {
      if (mToolTip)
      {
         mToolTip->setVisible(false);
         delete mToolTip;
         mToolTip = nullptr;
      }
      if (mHighlightOverlay)
      {
         mHighlightOverlay->hide();
         delete mHighlightOverlay.data();
         mHighlightOverlay = nullptr;
      }
   }

   mIsActive = activate;
}

void ObjectPicker::Pause() 
{
}

void ObjectPicker::Restore()
{
}

bool ObjectPicker::eventFilter(QObject*, QEvent* event)
{
   if (!mHighlightOverlay) return false;

   if (event->type() == QEvent::Leave)
   {
      CancelHighLighting();
   }
   else if (event->type() == QEvent::MouseButtonRelease)
   {
      auto* mouseEvent = static_cast<QMouseEvent*>(event);
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         auto coordinates = mouseEvent->localPos().toPoint();
      #else
         auto coordinates = mouseEvent->position().toPoint();
      #endif
      coordinates = mHighlightOverlay->mapToGlobal(coordinates);
      // Allow CTRL key to bypass picker.
      if (mouseEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier))
      {
         return ForwardMouseEvent(mouseEvent);
      }
      const auto deepPick = mouseEvent->modifiers().testFlag(
         Qt::KeyboardModifier::ShiftModifier);

      mPickedObject = GetPickedObject(parent(), coordinates, deepPick);
      if (mPickedObject)
      {
         emit objectPicked();
         HighLightObject();

         mHasNewObject = true;
         emit hasNewObjectChanged();
      }
      return true;
   }
   else if (event->type() == QEvent::MouseMove)
   {
      auto* mouseEvent = static_cast<QMouseEvent*>(event);
      // Allow CTRL key to bypass picker.
      if (mouseEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier))
      {
         CancelHighLighting();
         return ForwardMouseEvent(mouseEvent);
      }
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         auto coordinates = mouseEvent->pos();
      #else
         auto coordinates = mouseEvent->position().toPoint();
      #endif
      coordinates = mHighlightOverlay->mapToGlobal(coordinates);
      const auto deepPick = mouseEvent->modifiers().testFlag(
         Qt::KeyboardModifier::ShiftModifier);
      auto* pickedObject = GetPickedObject(parent(), coordinates, deepPick);

      mHighlightedObject = pickedObject;
      if (mWidgetWrapper)
      {
         HighLightObject();
      }
      else
      {
         CancelHighLighting();
      }
      return true;
   }
   else if(
      event->type() == QEvent::MouseButtonPress ||
      event->type() == QEvent::MouseButtonDblClick)
   {
      // Allow CTRL key to bypass picker.
      auto* mouseEvent = static_cast<QMouseEvent*>(event);
      if (mouseEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier))
      {
         return ForwardMouseEvent(mouseEvent);
      }
      return true;
   }
   // Forward keyboard events
   else if (
      event->type() == QEvent::KeyPress ||
      event->type() == QEvent::KeyRelease)
   {
      return parent()->event(event);
   }
   return false;
}

void ObjectPicker::HighLightObject()
{
   mIsHighLighted = true;

   if (!mWidgetWrapper)
   {
      if (mToolTip)
      {
         mToolTip->hide();
      }
      return;
   }
   const auto globalPos = mWidgetWrapper->GetBounds().topLeft() / mPixelRatio;
   const auto position = mHighlightOverlay->mapFromGlobal(globalPos);
   QRect widgetRect{position, mWidgetWrapper->GetBounds().size() / mPixelRatio};
   mHighlightOverlay->SetHighlightArea(widgetRect);
   mHighlightOverlay->update();

   // Tooltip text
   if (!mToolTip || !mHighlightedObject) return;
   auto* wrapper = qobject_cast<WindowsWidgetWrapper*>(mHighlightedObject);
   if (!wrapper) return;
   const auto tooltipText = wrapper->GetType().toStdString() + ": " + wrapper->GetHandleHex().toStdString();
   mToolTip->SetText(tooltipText);
   
   // Tooltip size and position
   const auto bounds = mToolTip->AdjustSize(mHighlightOverlay);
   mToolTip->setPosition({globalPos.x(), globalPos.y() - bounds.height() - 5});
   mToolTip->show();
}

void ObjectPicker::CancelHighLighting()
{
   mIsHighLighted = false;
   mHighlightedObject = nullptr;
   mHighlightOverlay->SetHighlightArea({});
   mHighlightOverlay->update();
   if (mToolTip)
   {
      mToolTip->setVisible(false);
   }
}

QObject* ObjectPicker::GetPickedObject(QObject* object, QPoint mousePosition, [[maybe_unused]] bool deep)
{
   if (!object)
   {
      return nullptr;
   }

   auto* wrapper = qobject_cast<WindowsWidgetWrapper*>(object);
   if (!wrapper)
   {
      return object;
   }

   // Find child at given position
   auto childHandle = wrapper->GetHandle();

   mousePosition *= mPixelRatio;
   // ChildWindowFromPoint is not recursive
   for (;;)
   {
      WindowsWidget parentWidget(wrapper);
      const auto localPos = parentWidget.MapFromGlobal(mousePosition).toPoint();
      POINT winPoint{localPos.x(), localPos.y()};
      const auto handle = ChildWindowFromPoint(
         childHandle,
         winPoint
      );
      // Mouse is outside widget boundaries
      if (!handle) break;
      // Last child found
      if (handle == childHandle) break;
      childHandle = handle;
      auto childWrapper = wrapper->GetChild(childHandle);
      // Invalid child
      if (!childWrapper) break;
      wrapper = childWrapper;
   }

   mWidgetWrapper = std::make_unique<WindowsWidget>(wrapper);
   return wrapper;
}

bool ObjectPicker::ForwardMouseEvent(const QMouseEvent* mouseEvent)
{
   auto* window = qobject_cast<WindowsWindowWrapper*>(parent());
   if (!window) return false;
   WindowsWidget wWidget(window);
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      auto coordinates = mouseEvent->localPos().toPoint();
   #else
      auto coordinates = mouseEvent->position().toPoint();
   #endif
   coordinates = mHighlightOverlay->mapToGlobal(coordinates);
   const auto localPos = wWidget.MapFromGlobal(coordinates);
   QMouseEvent childEvent(
      mouseEvent->type(),
      localPos,
      localPos,
      coordinates,
      mouseEvent->button(),
      mouseEvent->buttons(),
      mouseEvent->modifiers()
   );
   return parent()->event(&childEvent);
}

} // namespace Qat::WindowsNativePlugin