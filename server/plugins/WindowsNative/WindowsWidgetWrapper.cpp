// (c) Copyright 2024, Qat’s Authors

#include <WindowsWidgetWrapper.h>

#include <QEvent>
#include <QKeyEvent>
#include <QMouseEvent>

#include <CommCtrl.h>

#include <array>
#include <iostream>
#include <optional>

namespace
{
// According to Microsoft documentation, the max length of a class name is 256.
// see: https://learn.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-wndclassexa
constexpr int MAX_NAME_SIZE{256};

/// Return the local position of the given mouse event
/// \param[in] mouseEvent The mouse event
/// \return The local position
QPoint GetLocalPosition(const QMouseEvent* mouseEvent)
{   
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      return mouseEvent->localPos().toPoint();
   #else
      return mouseEvent->position().toPoint();
   #endif
}

/// Return the global position of the given mouse event
/// \param[in] mouseEvent The mouse event
/// \return The global position
QPoint GetGlobalPosition(const QMouseEvent* mouseEvent)
{   
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      return mouseEvent->globalPos();
   #else
      return mouseEvent->globalPosition().toPoint();
   #endif
}

/// Convert the given Qt key to a Windows Virtual key 
/// \param key A Qt key
/// \return The corresponding Virtual Key or nullopt is the key is not supported
std::optional<int> ConvertKeyToVKey(int key)
{
   switch (key)
   {
   case 0:
      return 0;
   case Qt::Key::Key_Escape:
      return VK_ESCAPE;
   case Qt::Key::Key_Return:
      return VK_RETURN;
   case Qt::Key::Key_Backspace:
      return VK_BACK;
   case Qt::Key::Key_Delete:
      return VK_DELETE;
   case Qt::Key::Key_Tab:
      return VK_TAB;
   case Qt::Key::Key_Control:
      return VK_CONTROL;
   case Qt::Key::Key_Shift:
      return VK_SHIFT;
   case Qt::Key::Key_Alt:
      return VK_MENU;
   case Qt::Key::Key_F1:
      return VK_F1;
   case Qt::Key::Key_F2:
      return VK_F2;
   case Qt::Key::Key_F3:
      return VK_F3;
   case Qt::Key::Key_F4:
      return VK_F4;
   case Qt::Key::Key_F5:
      return VK_F5;
   case Qt::Key::Key_F6:
      return VK_F6;
   case Qt::Key::Key_F7:
      return VK_F7;
   case Qt::Key::Key_F8:
      return VK_F8;
   case Qt::Key::Key_F9:
      return VK_F9;
   case Qt::Key::Key_F10:
      return VK_F10;
   case Qt::Key::Key_F11:
      return VK_F11;
   case Qt::Key::Key_F12:
      return VK_F12;
   default:
      return std::nullopt;
   }
}
}

namespace Qat
{

WindowsWidgetWrapper::WindowsWidgetWrapper(QObject* parent, HWND handle) : 
   INativeWidget(parent),
   mHandle{handle}
{
}

QObjectList WindowsWidgetWrapper::GetChildren()
{
   // Find current children
   std::vector<HWND> currentChildHandles;
   auto h = FindWindowExA(mHandle, NULL, NULL, NULL);
   while (h != 0)
   {
      currentChildHandles.push_back(h);
      const auto it = mChildren.find(h);
      if (it == mChildren.cend())
      {
         mChildren[h] = new WindowsWidgetWrapper(this, h);
      }
      h = FindWindowExA(mHandle, h, NULL, NULL);
   }

   // Delete obsolete wrappers
   std::vector<HWND> obsoleteChildHandles;
   for (const auto& child : mChildren)
   {
      const auto it = std::find(currentChildHandles.cbegin(), currentChildHandles.cend(), child.first);
      if (it == currentChildHandles.cend())
      {
         obsoleteChildHandles.push_back(child.first);
         child.second->deleteLater();
      }
   }
   for (const auto& handle : obsoleteChildHandles)
   {
      mChildren.erase(handle);
   }
   
   // Build result
   QObjectList children;
   children.reserve(static_cast<int>(mChildren.size()));
   for (const auto& child : mChildren)
   {
      children.push_back(child.second);
   }
   return children;
}

WindowsWidgetWrapper* WindowsWidgetWrapper::GetChild(HWND handle)
{
   // Refresh children
   GetChildren();
   if (mChildren.contains(handle))
   {
      return mChildren[handle];
   }
   return nullptr;
}

HWND WindowsWidgetWrapper::GetHandle() const
{
   return mHandle;
}

bool WindowsWidgetWrapper::IsVisible() const
{
   return IsWindowVisible(mHandle);
}

bool WindowsWidgetWrapper::IsEnabled() const
{
   return IsWindowEnabled(mHandle);
}

QString WindowsWidgetWrapper::GetType() const
{
   std::array<char, MAX_NAME_SIZE> buffer{};
   GetClassNameA(mHandle, buffer.data(), sizeof(buffer));
   QString typeName {buffer.data()};
   if (typeName == "#32770")
   {
      typeName = "Dialog";
   }
   return typeName;
}

QString WindowsWidgetWrapper::GetClass() const
{
   std::array<char, MAX_NAME_SIZE> buffer{};
   RealGetWindowClassA(mHandle, buffer.data(), sizeof(buffer));
   return buffer.data();
}

QString WindowsWidgetWrapper::GetText() const
{
   std::array<char, MAX_NAME_SIZE> buffer{};
   GetWindowTextA(mHandle, buffer.data(), sizeof(buffer));
   QString text {buffer.data()};
   text.replace('&', "");
   return text;
}

QString WindowsWidgetWrapper::GetHandleHex() const
{
   return QString("0x") + QString("%1").arg(
      reinterpret_cast<quintptr>(mHandle), 
      QT_POINTER_SIZE, 16, QChar('0') ).toUpper();
}

QRectF WindowsWidgetWrapper::GetBounds() const
{
   RECT rect;
   if (!GetWindowRect(mHandle, &rect))
   {
      std::cerr << "Failed to get bounds" << std::endl;
      return {0, 0, -1, -1};
   }
   
   return 
   {
      static_cast<qreal>(rect.left),
      static_cast<qreal>(rect.top),
      static_cast<qreal>(std::abs(rect.right - rect.left)),
      static_cast<qreal>(std::abs(rect.top - rect.bottom))
   };
}

QRectF WindowsWidgetWrapper::GetScaledBounds() const
{
   #if QT_VERSION < QT_VERSION_CHECK(6, 5, 0)
      const auto pixelRatio = 1.0f;
   #else
      const auto pixelRatio = GetPixelRatio();
   #endif

   const auto bounds = GetBounds();
   return 
   {
      bounds.topLeft() / pixelRatio,
      bounds.size() / pixelRatio
   };
}

int WindowsWidgetWrapper::GetWidth() const
{
   return GetBounds().toRect().width();
}

int WindowsWidgetWrapper::GetHeight() const
{
   return GetBounds().height();
}

float WindowsWidgetWrapper::GetPixelRatio() const
{
   // For historical reasons, default DPI is 96 pixels / inch
   constexpr const auto DEFAULT_DPI = 96.f;
   const float dpi = GetDpiForWindow(GetHandle());
   return dpi / DEFAULT_DPI;
}

bool WindowsWidgetWrapper::event(QEvent* event)
{
   switch (event->type())
   {
   case QEvent::MouseButtonPress:
   case QEvent::MouseButtonRelease:
   case QEvent::MouseButtonDblClick:
   {
      const auto* mouseEvent = static_cast<const QMouseEvent*>(event);
      return HandleMouseEvent(mouseEvent);
   }
   case:: QEvent::KeyPress:
   case:: QEvent::KeyRelease:
   {
      const auto* keyEvent = static_cast<const QKeyEvent*>(event);
      return HandleKeyboardEvent(keyEvent);
   }
   default:
      return QObject::event(event);
   }
}

bool WindowsWidgetWrapper::HandleMouseEvent(const QMouseEvent* mouseEvent)
{
   const auto position = GetLocalPosition(mouseEvent);

   // Forward event to child when possible
   POINT winPoint{position.x(), position.y()};
   const auto childWidget = ChildWindowFromPointEx(
      mHandle,
      winPoint,
      CWP_SKIPDISABLED | CWP_SKIPINVISIBLE | CWP_SKIPTRANSPARENT
   );

   if (childWidget != NULL && childWidget != mHandle && mChildren.count(childWidget))
   {
      QMouseEvent childEvent(
         mouseEvent->type(),
         mouseEvent->pos(),
         mouseEvent->pos(),
         GetGlobalPosition(mouseEvent),
         mouseEvent->button(),
         mouseEvent->buttons(),
         mouseEvent->modifiers()
      );
      return mChildren.at(childWidget)->HandleMouseEvent(&childEvent);
   }

   const auto positionParam = MAKELPARAM(position.x(), position.y());
   std::vector<int> eventTypeParam;
   auto buttonParam = MK_LBUTTON;
   switch(mouseEvent->button())
   {
   case Qt::MouseButton::LeftButton:
      switch (mouseEvent->type())
      {
      case QEvent::MouseButtonPress:
         eventTypeParam = {WM_LBUTTONDOWN};
         buttonParam = MK_LBUTTON;
         break;
      case QEvent::MouseButtonRelease:
         eventTypeParam = {WM_LBUTTONUP};
         buttonParam = MK_LBUTTON;
         break;
      case QEvent::MouseButtonDblClick:
         eventTypeParam = {WM_LBUTTONDOWN, WM_LBUTTONUP, WM_LBUTTONDBLCLK, WM_LBUTTONUP};
         buttonParam = MK_LBUTTON;
         break;
      default:
         std::cerr << "Mouse event not supported" << std::endl;
         return false;
      }
      break;
   case Qt::MouseButton::RightButton:
      switch (mouseEvent->type())
      {
      case QEvent::MouseButtonPress:
         eventTypeParam = {WM_RBUTTONDOWN};
         buttonParam = MK_RBUTTON;
         break;
      case QEvent::MouseButtonRelease:
         eventTypeParam = {WM_RBUTTONUP};
         buttonParam = MK_RBUTTON;
         break;
      case QEvent::MouseButtonDblClick:
         eventTypeParam = {WM_RBUTTONDOWN, WM_RBUTTONUP, WM_RBUTTONDBLCLK, WM_RBUTTONUP};
         buttonParam = MK_RBUTTON;
         break;
      default:
         std::cerr << "Mouse event not supported" << std::endl;
         return false;
      }
      break;
   case Qt::MouseButton::MiddleButton:
      switch (mouseEvent->type())
      {
      case QEvent::MouseButtonPress:
         eventTypeParam = {WM_MBUTTONDOWN};
         buttonParam = MK_MBUTTON;
         break;
      case QEvent::MouseButtonRelease:
         eventTypeParam = {WM_MBUTTONUP};
         buttonParam = MK_MBUTTON;
         break;
      case QEvent::MouseButtonDblClick:
         eventTypeParam = {WM_MBUTTONDOWN, WM_MBUTTONUP, WM_MBUTTONDBLCLK, WM_MBUTTONUP};
         buttonParam = MK_MBUTTON;
         break;
      default:
         std::cerr << "Mouse event not supported" << std::endl;
         return false;
      }
      break;
   default:
      std::cerr << "Mouse button not supported" << std::endl;
      return false;
   }

   /// \todo: Handle key modifiers
   /// buttonParam |= MK_CONTROL or MK_SHIFT

   bool result = true;
   for (const auto& eventType : eventTypeParam)
   {
      result = result && 0 != PostMessage(mHandle, eventType, buttonParam, positionParam);
   }
   return result;
}

bool WindowsWidgetWrapper::HandleKeyboardEvent(const QKeyEvent* keyEvent)
{
   auto eventTypeParam = WM_CHAR;
   LPARAM lParam = 1; // repeat = 1, meaning a single keystroke
   LRESULT expectedResult = 0;
   switch(keyEvent->type())
   {
   case QEvent::KeyPress:
      eventTypeParam = WM_KEYDOWN;
      if (keyEvent->modifiers().testFlag(Qt::KeyboardModifier::AltModifier))
      {
         if (keyEvent->key() == Qt::Key::Key_F4)
         {
            // For some reason, sending Alt+F4 with PostMessage does not trigger the SC_CLOSE shortcut
            return TRUE == PostMessage(mHandle, WM_SYSCOMMAND, SC_CLOSE, 0);
         }
         eventTypeParam = WM_SYSKEYDOWN;
         // see https://learn.microsoft.com/en-us/windows/win32/inputdev/wm-syskeydown
         // 29 	The context code. The value is 1 if the ALT key is down while the key is pressed; 
         // it is 0 if the WM_SYSKEYDOWN message is posted to the active window because no window has the keyboard focus.
         lParam |= 1 << 29;
         // 16-23 	The scan code. The value depends on the OEM.
         LPARAM altParam = MapVirtualKey(VK_MENU, MAPVK_VK_TO_VSC_EX) << 16;
         PostMessage(mHandle, eventTypeParam, VK_MENU, lParam | altParam);
      }
      expectedResult = 1;
      break;
   case QEvent::KeyRelease:
      eventTypeParam = WM_KEYUP;
      // See https://learn.microsoft.com/en-us/windows/win32/inputdev/wm-syskeyup
      lParam |= (1 << 30) | (1 << 31);
      if (keyEvent->modifiers().testFlag(Qt::KeyboardModifier::AltModifier))
      {
         eventTypeParam = WM_SYSKEYUP;
         lParam |= 1 << 29;
      }
      expectedResult = 0;
      break;
   default:
      return false;
   }
   const auto text = keyEvent->text();
   // Send each character (if any)
   for (auto c : text)
   {
      auto rc = SendMessage(mHandle, eventTypeParam, c.unicode(), MAKELPARAM(0, 0));
      if (expectedResult != rc)
      {
         std::cerr << "Failed to press key " << c.toLatin1() << "; rc = " << rc << std::endl;
      }
      if (eventTypeParam == WM_KEYDOWN)
      {
         rc = SendMessage(mHandle, WM_CHAR, c.unicode(), MAKELPARAM(0, 0));
         if (expectedResult != rc)
         {
            std::cerr << "Failed to send char " << c.toLatin1() << "; rc = " << rc << std::endl;
         }
      }
   }

   // Send special keys (if any)
   const auto key = keyEvent->key();
   auto vkey = VK_ESCAPE;
   if (key >= Qt::Key::Key_0 && key <= Qt::Key::Key_Z)
   {
      // ASCII key
      vkey = key;
   }
   else
   {
      const auto convertedKey = ConvertKeyToVKey(key);
      if (!convertedKey)
      {
         std::cerr << "Special key not supported: " << key << std::endl;
         return false;
      }
      if (convertedKey.value() == 0)
      {
         // If key is 0 it means that no special key was sent (only text, see above)
         return true;
      }
      vkey = *convertedKey;
   }

   // PostMessage is required for special keys (SendMessage does not work)
   lParam |= (MapVirtualKey(vkey, MAPVK_VK_TO_VSC_EX) << 16);
   const auto result = (TRUE == PostMessage(mHandle, eventTypeParam, vkey, lParam));

   // Release Alt key if necessary
   if (keyEvent->type() == QEvent::KeyRelease)
   {
      if (keyEvent->modifiers().testFlag(Qt::KeyboardModifier::AltModifier))
      {
         lParam = 1;
         lParam &= ~(1 << 29);
         PostMessage(mHandle, eventTypeParam, VK_MENU, lParam);
      }
   }

   return result;
}

} // namespace Qat
