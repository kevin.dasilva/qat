// (c) Copyright 2023, Qat’s Authors

#include <QtWidget.h>

#include <QAbstractItemView>
#include <QLayout>
#include <QWidget>
#include <QWindow>

#include <iostream>

namespace Qat
{
QtWidget::QtWidget(QWidget* widget) :
   mWidget(widget)
{
}

QtWidget::QtWidget(QLayout* layout) :
   mWidget(layout->parentWidget()),
   mLayout(layout)
{
}

QObject* QtWidget::GetQtObject() const
{
   return mWidget;
}

std::string QtWidget::GetId() const
{
   return "";
}

QObject* QtWidget::GetParent() const
{
   return mWidget->parentWidget();
}

std::vector<QObject*> QtWidget::GetChildWidgets() const
{
   std::vector<QObject*> children;
   if (mLayout)
   {
      for (int i = 0; i < mLayout->count(); ++i)
      {
         auto w = mLayout->itemAt(i)->widget();
         if (w)
         {
            children.push_back(w);
         }
      }
   }
   return children;
}

QAbstractItemModel* QtWidget::GetModel() const
{
   auto itemView = qobject_cast<QAbstractItemView*>(mWidget);
   if (!itemView)
   {
      return nullptr;
   }
   return itemView->model();
}

QItemSelectionModel* QtWidget::GetSelectionModel() const
{
   auto itemView = qobject_cast<QAbstractItemView*>(mWidget);
   if (!itemView)
   {
      return nullptr;
   }
   return itemView->selectionModel();
}

QWindow* QtWidget::GetWindow() const
{
   return mWidget->window()->windowHandle();
}

QPointF QtWidget::MapToGlobal(const QPointF &point) const
{
   return mWidget->mapToGlobal(point.toPoint());
}

QPointF QtWidget::MapFromGlobal(const QPointF &point) const
{
   return mWidget->mapFromGlobal(point.toPoint());
}

QPointF QtWidget::MapToScene(const QPointF &point) const
{
   const auto globalPoint = MapToGlobal(point);
   const auto windowOrigin = GetWindow()->geometry().topLeft();
   return globalPoint - windowOrigin;
}

QPointF QtWidget::MapToWidget(const IWidget* widget, const QPointF &point) const
{
   if (!widget)
   {
      std::cerr << "Cannot map coordinates: widget is null" << std::endl;
      return point;
   }
   const auto item = qobject_cast<QWidget*>(widget->GetQtObject());
   if (!item)
   {
      std::cerr << "Cannot map coordinates: widget is not a QWidget" << std::endl;
      return point;
   }
   const auto globalPoint = mWidget->mapToGlobal(point.toPoint());
   return item->mapFromGlobal(globalPoint);
}

bool QtWidget::Contains(const QPointF &point) const
{
   return mWidget->contentsRect().contains(point.toPoint());
}

QSizeF QtWidget::GetSize() const
{
   return mWidget->size();
}

qreal QtWidget::GetWidth() const
{
   return mWidget->width();
}

qreal QtWidget::GetHeight() const
{
   return mWidget->height();
}

QRect QtWidget::GetBounds() const
{
   auto globalPosition = mWidget->mapToGlobal(QPoint(0, 0));
   QRect bounds(globalPosition, mWidget->size());
   return bounds;
}

float QtWidget::GetPixelRatio() const
{  
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      return 1.0f;
   #else
      return mWidget->devicePixelRatioF();
   #endif
}

qreal QtWidget::GetZ() const
{
   /// TODO implement fake Z order based on child ordering
   return 0;
}

bool QtWidget::IsVisible() const
{
   return mWidget->isVisible();
}

void QtWidget::ForceActiveFocus(Qt::FocusReason reason) const
{
   SetFocus(true, reason);
   mWidget->grabKeyboard();
}

void QtWidget::SetFocus(bool focus, Qt::FocusReason reason) const
{
   if (focus)
   {
      mWidget->setFocus(reason);
   }
   else
   {
      mWidget->releaseKeyboard();
   }
}

void QtWidget::GrabImage(std::function<void(const QImage&)> callback) const
{
   const auto pixmap = mWidget->grab();
   callback(pixmap.toImage());
}
}