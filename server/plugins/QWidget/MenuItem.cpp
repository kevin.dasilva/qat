// (c) Copyright 2024, Qat’s Authors

#include <MenuItem.h>

#include <QMenu>
#include <QMenuBar>
#include <QRegularExpression>
#include <QWidget>
#include <QWindow>

#include <iostream>

namespace
{
/// Remove '&' characters from action text if any.
/// Double '&&' will be replaced with a single '&'
/// \param actionText The text of an action/menu
/// \return The formatted text with '&' removed
QString RemoveShortcut(QString actionText)
{
   return actionText.replace(QRegularExpression("&([^&])"), "\\1");
}
}

namespace Qat
{
template <class T>
IWidget* FindTypedMenuItem(T* menu, const QString& text)
{
   // Look for an Action
   for (auto* action : menu->actions())
   {
      if (action->objectName() == text ||
          action->text() == text ||
          RemoveShortcut(action->text()) == text)
      {
         return new MenuItem<T>(menu, action);
      }
   }
   // Look for a submenu
   for (auto subMenu : menu->template findChildren<QMenu*>())
   {
      auto* menuAction = subMenu->menuAction();
      if (subMenu->objectName() == text ||
          subMenu->title() == text ||
          RemoveShortcut(subMenu->title()) == text ||
          menuAction->objectName() == text ||
          menuAction->text() == text ||
          RemoveShortcut(menuAction->text()) == text)
      {
         return new MenuItem<T>(menu, menuAction);
      }
   }
   return nullptr;
}

IWidget* FindMenuItem(MenuWrapper* menuWrapper)
{
   const auto text = QString::fromStdString(menuWrapper->GetString());
   if (text.isEmpty()) return nullptr;
   auto* menu = qobject_cast<QMenu*>(menuWrapper->GetMenu());
   if (menu)
   {
      return FindTypedMenuItem(menu, text);
   }
   auto* menuBar = qobject_cast<QMenuBar*>(menuWrapper->GetMenu());
   if (menuBar)
   {
      return FindTypedMenuItem(menuBar, text);
   }
   
   return nullptr;
}

template <class T>
MenuItem<T>::MenuItem(T* menu, QAction* action) :
   mMenu{menu},
   mAction{action}
{
}

template <class T>
QObject* MenuItem<T>::GetQtObject() const
{
   return mAction;
}

template <class T>
std::string MenuItem<T>::GetId() const
{
   return "";
}

template <class T>
QObject* MenuItem<T>::GetParent() const
{
   return mMenu;
}

template <class T>
std::vector<QObject*> MenuItem<T>::GetChildWidgets() const
{
   return {};
}

template <class T>
QAbstractItemModel* MenuItem<T>::GetModel() const
{
   return nullptr;
}

template <class T>
QItemSelectionModel* MenuItem<T>::GetSelectionModel() const
{
   return nullptr;
}

template <class T>
QWindow* MenuItem<T>::GetWindow() const
{
   auto* window = mMenu->windowHandle();
   if (window) return window;
   auto* parent = mMenu->parent();
   auto* parentWidget = qobject_cast<QWidget*>(parent);
   if (parentWidget)
   {
      return parentWidget->window()->windowHandle();
   }
   return nullptr;
}

template <class T>
QPointF MenuItem<T>::MapToGlobal(const QPointF &point) const
{
   const auto origin = mMenu->actionGeometry(mAction).topLeft();
   return mMenu->mapToGlobal(origin + point.toPoint());
}

template <class T>
QPointF MenuItem<T>::MapFromGlobal(const QPointF &point) const
{
   const auto origin = mMenu->actionGeometry(mAction).topLeft();
   return mMenu->mapFromGlobal(point.toPoint()) - origin;
}

template <class T>
QPointF MenuItem<T>::MapToScene(const QPointF &point) const
{
   const auto globalPoint = MapToGlobal(point);
   const auto windowOrigin = GetWindow()->geometry().topLeft();
   return globalPoint - windowOrigin;
}

template <class T>
QPointF MenuItem<T>::MapToWidget(const IWidget* widget, const QPointF &point) const
{
   if (!widget)
   {
      std::cerr << "Cannot map coordinates: widget is null" << std::endl;
      return point;
   }
   const auto item = qobject_cast<QWidget*>(widget->GetQtObject());
   if (!item)
   {
      std::cerr << "Cannot map coordinates: widget is not a QWidget" << std::endl;
      return point;
   }
   const auto globalPoint = MapToGlobal(point.toPoint());
   return item->mapFromGlobal(globalPoint.toPoint());
}

template <class T>
bool MenuItem<T>::Contains(const QPointF &point) const
{
   const auto origin = mMenu->actionGeometry(mAction).topLeft();
   return mMenu->contentsRect().contains(origin + point.toPoint());
}

template <class T>
QSizeF MenuItem<T>::GetSize() const
{
   return mMenu->actionGeometry(mAction).size();
}

template <class T>
qreal MenuItem<T>::GetWidth() const
{
   return mMenu->actionGeometry(mAction).width();
}

template <class T>
qreal MenuItem<T>::GetHeight() const
{
   return mMenu->actionGeometry(mAction).height();
}

template <class T>
QRect MenuItem<T>::GetBounds() const
{
   const auto rect = mMenu->actionGeometry(mAction);
   auto globalPosition = mMenu->mapToGlobal(rect.topLeft());
   QRect bounds(globalPosition, rect.size());
   return bounds;
}

template <class T>
float MenuItem<T>::GetPixelRatio() const
{  
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      return 1.0f;
   #else
      return mMenu->devicePixelRatioF();
   #endif
}

template <class T>
qreal MenuItem<T>::GetZ() const
{
   /// TODO implement fake Z order based on child ordering
   return 0;
}

template <class T>
bool MenuItem<T>::IsVisible() const
{
   return mMenu->isVisible();
}

template <class T>
void MenuItem<T>::ForceActiveFocus(Qt::FocusReason) const
{
}

template <class T>
void MenuItem<T>::SetFocus(bool, Qt::FocusReason) const
{
}

template <class T>
void MenuItem<T>::GrabImage(std::function<void(const QImage&)> callback) const
{
   const auto rect = mMenu->actionGeometry(mAction);
   const auto pixmap = mMenu->grab(rect);
   callback(pixmap.toImage());
}
}