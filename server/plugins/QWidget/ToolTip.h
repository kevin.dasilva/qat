// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QPoint>
#include <QWidget>

class QLabel;

namespace Qat::QWidgetPlugin
{
/// \brief Class implementing a custom tooltip visually identical to native ones.
/// The reason for this custom implementation is that we need to display a tooltip
/// without hiding it upon some events such as MouseRelease.
class ToolTip : public QWidget
{
public:
   /// Constructor
   /// \param[in] parent The parent widget
   explicit ToolTip(QWidget* parent);

   /// Destructor
   ~ToolTip() override;

   /// Display the tooltip 
   /// \param widget The widget associated with the tooltip. Used for positioning.
   /// \param text The text to be displayed
   void Show(QWidget* widget, const std::string& text);

   /// Hide the tooltip
   void Hide();

private:
   /// Compute the best position for the tooltip based on given widget size and position.
   /// \param widget The widget associated with the tooltip.
   /// \return Computed position
   QPoint ComputePosition(QWidget* widget) const;

   /// Text container
   QLabel* mLabel;
};
} // namespace Qat::QWidgetPlugin