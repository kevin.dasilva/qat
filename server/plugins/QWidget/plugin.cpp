// (c) Copyright 2023, Qat’s Authors

#include <ItemWidget.h>
#include <MenuItem.h>
#include <QtWidget.h>
#include <ObjectPicker.h>

#include <qat-server/MenuWrapper.h>
#include <qat-server/ModelIndexWrapper.h>

#include <QApplication>
#include <QImage>
#include <QLayout>
#include <QMenu>
#include <QObject>
#include <QPixmap>
#include <QScreen>
#include <QWidget>
#include <QWindow>

#include <iostream>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #define DLL_EXPORT _declspec(dllexport)
#elif defined(__linux__) || defined(__APPLE__)
   #define DLL_EXPORT
#else
#   error "Unsupported platform"
#endif

extern "C"
{
DLL_EXPORT Qat::IWidget* CastObject(QObject* qtobject)
{
   if (!qtobject)
   {
      return nullptr;
   }
   auto* widget = qobject_cast<QWidget*>(qtobject);
   if (widget)
   {
      return new Qat::QtWidget(widget);
   }
   auto* layout = qobject_cast<QLayout*>(qtobject);
   if (layout)
   {
      return new Qat::QtWidget(layout);
   }
   // qobject_cast does not work between the Server and this plugin
   /// todo: investigate why qobject_cast cannot be used
   auto* item = dynamic_cast<Qat::ModelIndexWrapper*>(qtobject);
   if (item)
   {
      return new Qat::ItemWidget(item);
   }
   auto* menu = dynamic_cast<Qat::MenuWrapper*>(qtobject);
   if (menu)
   {
      return Qat::FindMenuItem(menu);
   }
   return nullptr;
}

DLL_EXPORT bool GetTopWindows(QObject** windowsArray, unsigned int* size)
{
   if (!size)
   {
      std::cerr << "Invalid call to GetTopWindows(): missing 'size' argument" << std::endl;
      return false;
   }

   auto topLevelWidgets = qApp->topLevelWidgets();
   std::vector<QWidget*> list;
   for (auto* widget : topLevelWidgets)
   {
      if (!widget->graphicsProxyWidget() && !qobject_cast<QMenu*>(widget))
      {
         list.push_back(widget);
      }
   }
   if (*size == 0)
   {
      *size = static_cast<unsigned int>(list.size());
      return true;
   }
   if (!windowsArray || *size < list.size())
   {
      std::cerr << "Invalid call to GetTopWindows(): invalid array size" << std::endl;
      return false;
   }
   for (auto i = 0u; i < list.size(); ++i)
   {
      windowsArray[i] = list[i];
   }
   return true;
}

DLL_EXPORT QImage* GrabImage(QObject* window)
{
   if (!window)
   {
      return nullptr;
   }
   for (auto* topWidget : qApp->topLevelWidgets())
   {
      if (topWidget->window() == window && topWidget->window()->isVisible())
      {
         const auto pixmap = topWidget->grab();
         return new QImage(pixmap.toImage());
      }
   }
   return nullptr;
}

DLL_EXPORT Qat::IObjectPicker* CreatePicker(QObject* window)
{
   if (!window)
   {
      return nullptr;
   }
   for (auto* topWidget : qApp->topLevelWidgets())
   {
      if (topWidget->window() == window)
      {
         return new Qat::QWidgetPlugin::ObjectPicker(window);
      }
   }
   return nullptr;
}

}