// (c) Copyright 2023, Qat’s Authors

#include <ToolTip.h>

#include <QLabel>

namespace Qat::QWidgetPlugin
{
ToolTip::ToolTip(QWidget* parent) : QWidget(parent)
{
   mLabel = new QLabel(this);

   const auto backgroundColor = palette().color(QPalette::ToolTipBase).name();
   const auto textColor = palette().color(QPalette::ToolTipText).name();
   QString style = "QLabel {background-color:";
   style += backgroundColor;
   style += "; border-style: solid; border-width: 1; border-color: ";
   style += textColor;
   style += "; color: ";
   style += textColor;
   style += ";}";
   
   mLabel->setStyleSheet(style);
   Hide();
}

ToolTip::~ToolTip()
{
   delete mLabel;
}

void ToolTip::Show(QWidget* widget, const std::string& text)
{
   if (!widget || !parentWidget())
   {
      Hide();
      return;
   }
   mLabel->setText(QString::fromStdString(text));
   mLabel->adjustSize();
   setFixedSize(mLabel->size());
   
   move(ComputePosition(widget));
   setVisible(true);
}

void ToolTip::Hide()
{
   setVisible(false);
}

QPoint ToolTip::ComputePosition(QWidget* widget) const
{
   auto parentPosition = parentWidget()->mapToGlobal(QPoint(0, 0));
   if (!widget)
   {
      return parentPosition;
   }
   auto position = widget->mapToGlobal(QPoint(0, 0)) - parentPosition;
   const auto horizontalOffset = 3;
   const auto verticalOffset = 3;

   // Select the best horizontal position (default: centered)
   auto posX = position.x() + widget->width() / 2 - mLabel->width() / 2;
   if (posX + mLabel->width() > parentWidget()->width())
   {
      // Align to the right border
      posX = parentWidget()->width() - mLabel->width() - horizontalOffset;
   }
   if (posX < 0)
   {
      // Align to the left border
      posX = horizontalOffset;
   }

   position.setX(posX);

   // Select the best vertical position (default: above)
   auto posY = position.y() - mLabel->height() - verticalOffset;
   if (posY < 0)
   {
      // Move below the widget
      posY = position.y() + widget->height() + verticalOffset;
   }
   if (posY + mLabel->height() > parentWidget()->height())
   {
      // Move inside the widget
      posY = verticalOffset;
   }
   position.setY(posY);

   return position;
}
} // namespace Qat::QWidgetPlugin