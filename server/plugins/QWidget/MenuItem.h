// (c) Copyright 2024, Qat’s Authors
#pragma once 

#include <qat-server/IWidget.h>
#include <qat-server/MenuWrapper.h>

#include <QAction>
#include <QMenu>
#include <QPointer>
#include <QWidget>

namespace Qat
{

/// Factory creating MenuItem instances when underlying item exists.
/// \param[in] menuWrapper The menu wrapper instance to wrap
/// \return A menu item wrapper as a IWidget or nullptr if no item was found
IWidget* FindMenuItem(MenuWrapper* menuWrapper);

/// \brief Class providing widget-like interface to menu elements.
/// \tparam T Type of the menu container: QMenu or QMenuBar
template <class T>
class MenuItem : public IWidget
{
public:

   /// Constructor to wrap a QMenu item as a widget
   /// \param[in] menu The parent menu or menubar
   /// \param[in] action The action associated to the menu item
   MenuItem(T* menu, QAction* action);

   /// Default destructor
   ~MenuItem() override = default;

   /// \copydoc IWidget::GetQtObject
   QObject* GetQtObject() const override;

   /// \copydoc IWidget::GetId
   std::string GetId() const override;

   /// \copydoc IWidget::GetParent
   QObject* GetParent() const override;

   /// \copydoc IWidget::GetChildWidgets
   std::vector<QObject*> GetChildWidgets() const override;

   /// \copydoc IWidget::GetModel
   QAbstractItemModel* GetModel() const override;

   /// \copydoc IWidget::GetSelectionModel
   QItemSelectionModel* GetSelectionModel() const override;

   /// \copydoc IWidget::GetWindow
   QWindow* GetWindow() const override;

   /// \copydoc IWidget::MapToGlobal
   QPointF MapToGlobal(const QPointF &point) const override;

   /// \copydoc IWidget::MapFromGlobal
   QPointF MapFromGlobal(const QPointF &point) const override;

   /// \copydoc IWidget::MapToScene
   QPointF MapToScene(const QPointF &point) const override;

   /// \copydoc IWidget::MapToWidget
   QPointF MapToWidget(const IWidget* widget, const QPointF &point) const override;

   /// \copydoc IWidget::Contains
   bool Contains(const QPointF &point) const override;

   /// \copydoc IWidget::GetSize
   QSizeF GetSize() const override;

   /// \copydoc IWidget::GetWidth
   qreal GetWidth() const override;

   /// \copydoc IWidget::GetHeight
   qreal GetHeight() const override;
   
   /// \copydoc IWidget::GetBounds
   QRect GetBounds() const override;
   
   /// \copydoc IWidget::GetPixelRatio
   float GetPixelRatio() const override;

   /// \copydoc IWidget::GetZ
   qreal GetZ() const override;

   /// \copydoc IWidget::IsVisible
   bool IsVisible() const override;

   /// \copydoc IWidget::ForceActiveFocus
   void ForceActiveFocus(Qt::FocusReason reason) const override;

   /// \copydoc IWidget::SetFocus
   void SetFocus(bool focus, Qt::FocusReason reason) const override;

   /// \copydoc IWidget::GrabImage
   void GrabImage(std::function<void(const QImage&)> callback) const override;

private:
   /// Menu containing this item
   QPointer<T> mMenu;

   /// Action associated to this item
   QPointer<QAction> mAction;
};

}