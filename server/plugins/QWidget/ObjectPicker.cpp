// (c) Copyright 2023, Qat’s Authors

#include <ObjectPicker.h>
#include <ItemWidget.h>
#include <QtWidget.h>
#include <ToolTip.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/IWidget.h>
#include <qat-server/ModelIndexWrapper.h>
#include <qat-server/ObjectLocator.h>
#include <qat-server/WidgetLocator.h>
#include <qat-server/WidgetWrapper.h>

#include <QAbstractItemView>
#include <QApplication>
#include <QEvent>
#include <QLabel>
#include <QMouseEvent>
#include <QTimer>
#include <QWindow>
#include <QWidget>

#include <iostream>

namespace Qat::QWidgetPlugin
{

ObjectPicker::ObjectPicker(QObject* parent) :
   IObjectPicker(parent)
{
   std::cout << "ObjectPicker (QWidget) created" << std::endl;

   auto* parentWidget = qobject_cast<QWidget*>(parent);
   if (!parentWidget)
   {
      return;
   }
   parentWidget->setAttribute(Qt::WA_Hover);

   // Calling winId() will make sure windowHandle() returns a valid handle
   if (!parentWidget->winId())
   {
      std::cerr << "Cannot find window ID" << std::endl;
   }

   auto* window = parentWidget->window()->windowHandle();
   if (!window)
   {
      std::cerr << "Cannot find window" << std::endl;
      return;
   }
   QObject::connect(
      window,
      &QWindow::visibleChanged,
      [window, this]()
      {
         if (window->isVisible())
         {
            this->Restore();
            if (this->mIsActive)
            {
               this->parent()->installEventFilter(this);
               std::cout << "ObjectPicker enabled (window opened)" << std::endl;
            }
         }
         else
         {
            this->Pause();
            if (this->mIsActive)
            {
               this->parent()->removeEventFilter(this);
               std::cout << "ObjectPicker disabled (window closed)" << std::endl;
            }
         }
      });
}

void ObjectPicker::Reset()
{
   mPickedObject = nullptr;
   mHasNewObject = false;

   emit hasNewObjectChanged();
}

void ObjectPicker::SetActivated(bool activate)
{
   mIsActive = activate;
   if (activate)
   {
      auto* window = qobject_cast<QWidget*>(parent());
      if (!window)
      {
         return;
      }
      if (mHighlightOverlay)
      {
         mHighlightOverlay->deleteLater();
      }
      mHighlightOverlay = new QWidget(window);
      mHighlightOverlay->setAttribute(Qt::WA_Hover);
      mHighlightOverlay->setFixedSize(window->size());
      mHighlightOverlay->setFocusPolicy(Qt::StrongFocus);
      mHighlightOverlay->installEventFilter(this);
      mHighlightOverlay->show();

      mToolTip = new ToolTip(mHighlightOverlay);
   }
   else if (mHighlightOverlay)
   {
      if (mToolTip)
      {
         mToolTip->Hide();
         delete mToolTip;
         mToolTip = nullptr;
      }
      if (mHighlightRectangle)
      {
         delete mHighlightRectangle.data();
         mHighlightRectangle = nullptr;
      }
      delete mHighlightOverlay.data();
      mHighlightOverlay = nullptr;
   }
}

void ObjectPicker::Pause()
{
   if (mIsActive && mHighlightOverlay)
   {
      mHighlightOverlay->setVisible(false);
   }
}

void ObjectPicker::Restore()
{
   if (mIsActive && mHighlightOverlay)
   {
      mHighlightOverlay->setVisible(true);
   }
}

/// \todo Factorize this code and share it between plugins
bool ObjectPicker::eventFilter(QObject* object, QEvent* event)
{
   if (mHighlightOverlay)
   {
      if (event->type() == QEvent::Resize)
      {
         auto* parentWidget = qobject_cast<QWidget*>(parent());
         if (parentWidget)
         {
            mHighlightOverlay->setFixedSize(parentWidget->size());
         }
         return false;
      }
      else if(mToolTip && event->type() == QEvent::Move)
      {
         mToolTip->Hide();
      }
      if (!mHighlightOverlay->isVisible())
      {
         event->setAccepted(false);
         return false;
      }
   }
   if (event->type() == QEvent::MouseButtonRelease)
   {
      try
      {
         auto* mouseEvent = static_cast<QMouseEvent*>(event);
         #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            auto mousePosition = mouseEvent->localPos().toPoint();
         #else
            auto mousePosition = mouseEvent->position().toPoint();
         #endif
         if (mHighlightOverlay)
         {
            mHighlightOverlay->setVisible(false);
         }
         const auto deepPick = mouseEvent->modifiers().testFlag(
            Qt::KeyboardModifier::ShiftModifier);
         if (object == mHighlightRectangle)
         {
            mousePosition = mHighlightRectangle->mapToGlobal(mousePosition);
            mousePosition = mHighlightOverlay->mapFromGlobal(mousePosition);
         }
         mPickedObject = GetPickedObject(parent(), mousePosition, deepPick);
         if (mHighlightOverlay)
         {
            mHighlightOverlay->setVisible(true);
         }
         if (mPickedObject)
         {
            emit objectPicked();
            HighLightObject(mPickedObject);

            mHasNewObject = true;
            emit hasNewObjectChanged();
         }
      }
      catch (...)
      {
         // Avoid throwing exceptions in Qt thread
         return false;
      }
      return true;
   }
   else if (event->type() == QEvent::HoverMove)
   {
      auto* hoverEvent = static_cast<QHoverEvent*>(event);
      bool newObject = false;
      bool objectFound = false;
      if (mHighlightOverlay)
      {
         mHighlightOverlay->setVisible(false);
      }
      try
      {
         #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            auto coordinates = hoverEvent->pos();
         #else
            auto coordinates = hoverEvent->position().toPoint();
         #endif
         if (object == mHighlightRectangle)
         {
            auto* parentWidget = qobject_cast<QWidget*>(parent());
            coordinates = mHighlightRectangle->mapToGlobal(coordinates);
            coordinates = parentWidget->mapFromGlobal(coordinates);
         }
         const auto deepPick = hoverEvent->modifiers().testFlag(
            Qt::KeyboardModifier::ShiftModifier);
         auto* pickedObject = GetPickedObject(parent(), coordinates, deepPick);
         objectFound = pickedObject != nullptr;
         if (pickedObject != mHighlightedObject)
         {
            mHighlightedObject = pickedObject;
            newObject = true;
         }
      }
      catch (...)
      {
         std::cerr << "Cannot find highlighted widget" << std::endl;
      }
      if (mHighlightOverlay)
      {
         mHighlightOverlay->setVisible(true);
      }
      if (newObject)
      {
         HighLightObject(mHighlightedObject);
      }
      if (objectFound)
      {
         hoverEvent->accept();
      }
      else
      {
         hoverEvent->ignore();
      }
      return objectFound;
   }
   else if (object == mHighlightRectangle && event->type() == QEvent::HoverLeave)
   {
      CancelHighLighting();
      event->accept();
      return true;
   }
   else if(
      event->type() == QEvent::MouseButtonPress ||
      event->type() == QEvent::MouseButtonDblClick)
   {
      // Allow CTRL key to bypass picker. This is useful to open a combobox
      // and pick an item for example
      auto* mouseEvent = static_cast<QMouseEvent*>(event);
      return !mouseEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier);
   }
   else if (
      event->type() == QEvent::HoverEnter ||
      event->type() == QEvent::HoverMove ||
      event->type() == QEvent::HoverLeave)
   {
      // Allow CTRL key to bypass picker. This is useful to open a combobox
      // and pick an item for example
      auto* hoverEvent = static_cast<QHoverEvent*>(event);
      return !hoverEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier);
   }
   return false;
}


void ObjectPicker::HighLightObject(QObject* object)
{
   mIsHighLighted = true;

   if (!mWidgetWrapper)
   {
      return;
   }

   if (!mHighlightRectangle)
   {
      mHighlightRectangle = new QLabel(mHighlightOverlay);
      mHighlightRectangle->installEventFilter(this);

      mHighlightRectangle->setStyleSheet(
         "background-color: rgba(255, 255, 0, 128)");
      mHighlightRectangle->setObjectName(QString::fromStdString(Constants::PICKER_OVERLAY_NAME));
   }

   try
   {
      // Make the rectangle the same size as the picked object
      mHighlightRectangle->setFixedSize(mWidgetWrapper->GetSize().toSize());
      // Superimpose the rectangle onto the picked object
      auto globalPosition = mWidgetWrapper->MapToGlobal(QPoint(0, 0)).toPoint();
      globalPosition = mHighlightOverlay->mapFromGlobal(globalPosition);
      mHighlightRectangle->move(globalPosition);
      std::string type = object->metaObject()->className();
      type = ObjectLocator::FormatType(type);
      const auto objectName = object->objectName().toStdString();
      auto tooltipText = type;
      if (!objectName.empty())
      {
         tooltipText += ": " + objectName;
      }
      mHighlightRectangle->show();
      if(mToolTip)
      {
         mToolTip->Show(mHighlightRectangle, tooltipText);
      }
   }
   catch(const Exception& e)
   {
      std::cerr << "Could not highlight widget: " << e.what() << std::endl;
   }
}

void ObjectPicker::CancelHighLighting()
{
   if (mHighlightRectangle)
   {
      mIsHighLighted = false;
   }
   mHighlightedObject = nullptr;
}


QObject* ObjectPicker::GetPickedObject(QObject* object, QPoint mousePosition, bool deep)
{
   object = Qat::WidgetLocator::FindWidget(object, mousePosition);
   if (!object)
   {
      return nullptr;
   }
   auto* widget = qobject_cast<QWidget*>(object);
   if (!widget)
   {
      return object;
   }

   auto* itemView = qobject_cast<QAbstractItemView*>(widget);
   if (!itemView)
   {
      itemView = qobject_cast<QAbstractItemView*>(widget->parentWidget());
   }
   if (itemView)
   {
      auto localPosition = mHighlightOverlay->mapToGlobal(mousePosition);
      localPosition = itemView->viewport()->mapFromGlobal(localPosition);
      const auto modelIndex = itemView->indexAt(localPosition);
      if (modelIndex.isValid())
      {
         const auto indexWrapper = new ModelIndexWrapper(
            itemView->model(), itemView->selectionModel(), modelIndex, itemView);

         mWidgetWrapper = std::make_unique<ItemWidget>(indexWrapper);
         return indexWrapper;
      }
      else
      {
         mWidgetWrapper = std::make_unique<QtWidget>(widget);
      }
   }
   else
   {
      mWidgetWrapper = std::make_unique<QtWidget>(widget);
   }

   auto* parentObject = object;
   while (parentObject && !parentObject->isWindowType())
   {
      parentObject = parentObject->parent();
   }
   if (parentObject && parentObject->isWindowType() && parentObject != parent())
   {
      return nullptr;
   }

   if (!deep && object)
   {
      const auto widgetSize = widget->size();
      auto* parent = widget->parentWidget();
      while (parent && parent->size() == widgetSize)
      {
         object = parent;
         parent = parent->parentWidget();
         if (!parent || parent->isWindowType())
         {
            break;
         }
      }
   }

   return object;
}

} // namespace Qat::QWidgetPlugin