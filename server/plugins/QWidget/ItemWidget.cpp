// (c) Copyright 2023, Qat’s Authors

#include <ItemWidget.h>
#include <qat-server/ModelIndexWrapper.h>
#include <qat-server/Exception.h>

#include <QAbstractItemView>
#include <QLayout>
#include <QWidget>
#include <QWindow>

#include <iostream>

namespace Qat
{
ItemWidget::ItemWidget(ModelIndexWrapper* item) :
   mItem(item)
{
   if (mItem)
   {
      mParent = qobject_cast<QAbstractItemView*>(mItem->GetParentWidget());
   }
   if (!mItem || !mParent)
   {
      throw Exception("Cannot create ItemWidget (null item or parent)");
   }
}

QObject* ItemWidget::GetQtObject() const
{
   return mItem;
}

std::string ItemWidget::GetId() const
{
   return "";
}

QObject* ItemWidget::GetParent() const
{
   return mItem->GetParentWidget();
}

std::vector<QObject*> ItemWidget::GetChildWidgets() const
{
   return {};
}

QAbstractItemModel* ItemWidget::GetModel() const
{
   return mItem->GetModel();
}

QItemSelectionModel* ItemWidget::GetSelectionModel() const
{
   return mParent->selectionModel();
}

QWindow* ItemWidget::GetWindow() const
{
   return mParent->window()->windowHandle();
}

QPointF ItemWidget::MapToGlobal(const QPointF &point) const
{
   const auto itemBounds = mParent->visualRect(mItem->GetIndex());
   if (itemBounds.isNull())
   {
      throw Exception("Item is not visible");
   }
   const auto mappedPoint = mParent->viewport()->pos() + itemBounds.topLeft() + point.toPoint();
   if (!mParent->contentsRect().contains(mappedPoint))
   {
      throw Exception("Item is not visible (out-of-bounds)");
   }
   return mParent->mapToGlobal(mappedPoint);
}

QPointF ItemWidget::MapFromGlobal(const QPointF &point) const
{
   const auto itemBounds = mParent->visualRect(mItem->GetIndex());
   return mParent->mapFromGlobal(point.toPoint() - itemBounds.topLeft());
}

QPointF ItemWidget::MapToScene(const QPointF &point) const
{
   const auto globalPoint = MapToGlobal(point);
   const auto windowOrigin = GetWindow()->geometry().topLeft();
   return globalPoint - windowOrigin;
}

QPointF ItemWidget::MapToWidget(const IWidget* widget, const QPointF &point) const
{
   if (!widget)
   {
      std::cerr << "Cannot map coordinates: widget is null" << std::endl;
      return point;
   }
   const auto item = qobject_cast<QWidget*>(widget->GetQtObject());
   if (!item)
   {
      std::cerr << "Cannot map coordinates: widget is not a QWidget" << std::endl;
      return point;
   }
   const auto globalPoint = MapToGlobal(point);
   return MapFromGlobal(globalPoint);
}

bool ItemWidget::Contains(const QPointF &point) const
{
   auto itemBounds = mParent->visualRect(mItem->GetIndex());
   if (itemBounds.isNull())
   {
      throw Exception("Item is not visible");
   }
   itemBounds.setTopLeft({0,0});
   return itemBounds.contains(point.toPoint());
}

QSizeF ItemWidget::GetSize() const
{
   return GetBounds().size();
}

qreal ItemWidget::GetWidth() const
{
   return GetBounds().width();
}

qreal ItemWidget::GetHeight() const
{
   return GetBounds().height();
}

QRect ItemWidget::GetBounds() const
{
   const auto itemBounds = mParent->visualRect(mItem->GetIndex());
   const auto viewPortPos = mParent->viewport()->pos();
   auto globalPosition = mParent->mapToGlobal(viewPortPos);
   QRect bounds(globalPosition, itemBounds.size());
   return bounds;
}

float ItemWidget::GetPixelRatio() const
{  
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      return 1.0f;
   #else
      return mParent->devicePixelRatioF();
   #endif
}

qreal ItemWidget::GetZ() const
{
   return 0;
}

bool ItemWidget::IsVisible() const
{
   auto itemBounds = mParent->visualRect(mItem->GetIndex());
   return mParent->isVisible() && !itemBounds.isNull();
}

void ItemWidget::ForceActiveFocus(Qt::FocusReason) const
{
   // Not supported
}

void ItemWidget::SetFocus(bool, Qt::FocusReason) const
{
   // Not supported
}

void ItemWidget::GrabImage(std::function<void(const QImage&)> callback) const
{
   const auto bounds = mParent->visualRect(mItem->GetIndex());
   if (bounds.isNull())
   {
      throw Exception("Cannot grab screenshot of hidden item");
   }
   const auto viewPortPos = mParent->viewport()->pos();
   const auto topLeft = viewPortPos + bounds.topLeft();
   const auto size = bounds.size();
   const auto pixmap = mParent->grab({topLeft, size});
   callback(pixmap.toImage());
}
}