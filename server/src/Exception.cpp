// (c) Copyright 2023, Qat’s Authors

#include <qat-server/Exception.h>

namespace Qat
{

Exception::Exception(const std::string& message):
   std::runtime_error(message)
{
}

} // namespace Qat