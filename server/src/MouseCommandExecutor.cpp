// (c) Copyright 2023, Qat’s Authors

#include <qat-server/MouseCommandExecutor.h>
#include <qat-server/CommandHelper.h>
#include <qat-server/Constants.h>
#include <qat-server/Devices.h>
#include <qat-server/Exception.h>
#include <qat-server/IWidget.h>
#include <qat-server/ObjectLocator.h>
#include <qat-server/WidgetLocator.h>
#include <qat-server/WidgetWrapper.h>

#include <QCursor>
#include <QGuiApplication>
#include <QEvent>
#include <QJsonArray>
#include <QMouseEvent>
#include <QObject>
#include <QObjectData>
#include <QRegularExpression>
#include <QWheelEvent>
#include <QWindow>

#include <string>
#include <tuple>

namespace
{

   /// Send the given mouse event to the given receiver.
   /// For Qt widgets, the event will be sent to the parent window using
   /// Qt event system and QGuiApplication::sendEvent() function.
   /// For native widgets, the event will be sent directly to the underlying
   /// QObject by calling its event() function.
   /// \param widget The receiver widget
   /// \param event A mouse event
   /// \return True if the event was handled, False otherwise
   bool sendMouseEvent(Qat::IWidget& widget, QEvent* event)
   {
      if (widget.GetWindow())
      {
         return qApp->sendEvent(widget.GetWindow(), event);
      }
      if (widget.GetQtObject())
      {
         return widget.GetQtObject()->event(event);
      }
      return false;
   }

} // anonymous namespace

namespace Qat
{

using namespace Constants;

MouseCommandExecutor::MouseCommandExecutor(const nlohmann::json& request):
   BaseCommandExecutor(request)
{
   for (const auto& field : { OBJECT_DEFINITION, OBJECT_ATTRIBUTE, ARGUMENTS })
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " + field
         );
      }
   }
}

nlohmann::json MouseCommandExecutor::Run() const
{
   nlohmann::json result;
   result["status"] = true;
   auto* object = FindObject();
   const auto action = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   const auto args = mRequest.at(ARGUMENTS);

   const auto button = CommandHelper::GetButton(args);
   const auto modifier = CommandHelper::GetModifier(args);

   auto widget = WidgetWrapper::Cast(object);
   if (!widget)
   {
      throw Exception(
         "Cannot execute mouse operation: "
         "Object is not a supported Widget"
      );
   }

   if (button == Qt::MouseButton::NoButton && action != Mouse::MOVE)
   {
      throw Qat::Exception(
         "Cannot run mouse command: "
         "Button cannot be None for this event"
      );
   }

   // Optional position arguments (X, Y)
   QPoint coordinates, globalCoordinates;
   if (args.contains(Args::X) && args.contains(Args::Y))
   {
      const auto x = args.at(Args::X).get<int>();
      const auto y = args.at(Args::Y).get<int>();
      coordinates = QPoint(x, y);
      if (action != Mouse::MOVE && !widget->Contains(coordinates))
      {
         throw Exception(
            "Cannot execute mouse operation: "
            "Given coordinates are outside widget's boundaries"
         );
      }
      globalCoordinates = widget->MapToGlobal(coordinates).toPoint();
   }
   else
   {
      std::tie(coordinates, globalCoordinates) = WidgetLocator::GetWidgetCenter(object);
   }
   coordinates = widget->MapToScene(coordinates).toPoint();

   // Optional move arguments (DX, DY)
   QPoint moveCoordinates{ 0,0 };
   if (args.contains(Args::DX) && args.contains(Args::DY))
   {
      const auto x = args.at(Args::DX).get<int>();
      const auto y = args.at(Args::DY).get<int>();
      moveCoordinates = QPoint(x, y);
   }

   // Generate mouse events
   // Note: the order is important to make click and drag events work
   bool actionFound = false;
   bool eventAccepted = false;
   if (action == Mouse::PRESS || action == Mouse::CLICK || action == Mouse::DRAG || action == Mouse::DOUBLE_CLICK)
   {
      QMouseEvent pressEvent(
         QEvent::Type::MouseButtonPress,
         coordinates,
         coordinates,
         globalCoordinates,
         button,
         Qt::MouseButtons().setFlag(button),
         modifier,
         Devices::GetMouseDevice()
      );

      if (!sendMouseEvent(*widget, &pressEvent))
      {
         throw Exception(
            "Cannot send press event"
         );
      }

      eventAccepted = eventAccepted || pressEvent.isAccepted();
      actionFound = true;
   }
   if (action == Mouse::DOUBLE_CLICK)
   {
      QMouseEvent doubleClickEvent(
         QEvent::Type::MouseButtonDblClick,
         coordinates,
         coordinates,
         globalCoordinates,
         button,
         Qt::MouseButtons().setFlag(button),
         modifier,
         Devices::GetMouseDevice()
      );

      if (!sendMouseEvent(*widget, &doubleClickEvent))
      {
         throw Exception(
            "Cannot send double-click event"
         );
      }

      eventAccepted = eventAccepted || doubleClickEvent.isAccepted();
      actionFound = true;
   }

   if (action == Mouse::MOVE)
   {
      QMouseEvent moveEvent(
         QEvent::Type::MouseMove,
         coordinates,
         coordinates,
         globalCoordinates,
         Qt::NoButton,
         Qt::MouseButtons().setFlag(button),
         modifier,
         Devices::GetMouseDevice()
      );

      if (!sendMouseEvent(*widget, &moveEvent))
      {
         // Make sure the mouse is not stuck in a pressed state
         QMouseEvent releaseEvent(
            QEvent::Type::MouseButtonRelease,
            coordinates,
            coordinates,
            globalCoordinates,
            button,
            Qt::NoButton,
            modifier,
            Devices::GetMouseDevice()
         );
         sendMouseEvent(*widget, &releaseEvent);
         
         throw Exception(
            "Cannot send move event"
         );
      }
      eventAccepted = eventAccepted || moveEvent.isAccepted();
      actionFound = true;
   }
   else if (action == Mouse::DRAG)
   {
      auto length = static_cast<int>(
         std::sqrt(std::pow(moveCoordinates.x(), 2) + std::pow(moveCoordinates.y(), 2)));

      // Avoid generating too many events
      length = std::min(length, 20);
      QEventLoop::ProcessEventsFlags flags;
      flags.setFlag(QEventLoop::ExcludeUserInputEvents);
      // This freezes the execution when using QWidgets
      //flags.setFlag(QEventLoop::WaitForMoreEvents);
      for (int i = 0; i <= length; ++i)
      {
         /// \todo Find another way to run async events during mouse drag since this causes issues
         /// when running tests in parallel.
         qApp->processEvents(flags, 50);
         const auto stepCoordinates = moveCoordinates * i / length;

         QMouseEvent moveEvent(
            QEvent::Type::MouseMove,
            coordinates + stepCoordinates,
            coordinates + stepCoordinates,
            globalCoordinates + stepCoordinates,
            Qt::NoButton,
            Qt::MouseButtons().setFlag(button),
            modifier,
            Devices::GetMouseDevice()
         );

         if (!sendMouseEvent(*widget, &moveEvent))
         {
            // Make sure the mouse is not stuck in a pressed state
            QMouseEvent releaseEvent(
               QEvent::Type::MouseButtonRelease,
               coordinates,
               coordinates,
               globalCoordinates,
               button,
               Qt::NoButton,
               modifier,
               Devices::GetMouseDevice()
            );
            sendMouseEvent(*widget, &releaseEvent);
            
            throw Exception(
               "Cannot send move event"
            );
         }
         //eventAccepted = eventAccepted || moveEvent.isAccepted();
      }
      actionFound = true;
   }
   else if (action == Mouse::SCROLL)
   {
      QWheelEvent wheelEvent(
         coordinates,
         globalCoordinates,
         {0, 0}, // pixelDelta
         moveCoordinates,
         Qt::MouseButtons(),
         modifier,
         Qt::ScrollPhase::NoScrollPhase,
         false,
         Qt::MouseEventSynthesizedByApplication
      #if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
         , Devices::GetMouseDevice()
      #endif
      );

      if (!sendMouseEvent(*widget, &wheelEvent))
      {
         throw Exception(
            "Cannot send wheel event"
         );
      }

      // Scene3D does not always accept wheel events
      eventAccepted = eventAccepted || wheelEvent.isAccepted() || object->inherits("Qt3DRender::Scene3DItem");
      actionFound = true;

      QMouseEvent releaseEvent(
         QEvent::Type::MouseButtonRelease,
         coordinates,
         coordinates,
         globalCoordinates,
         button,
         Qt::MouseButtons(),
         modifier,
         Devices::GetMouseDevice()
      );

      if (!sendMouseEvent(*widget, &releaseEvent))
      {
         throw Exception(
            "Cannot send release event"
         );
      }
   }

   // Most events need a Release event at the end
   if(action == Mouse::RELEASE || action == Mouse::CLICK || action == Mouse::DRAG || 
      action == Mouse::DOUBLE_CLICK)
   {
      QMouseEvent releaseEvent(
         QEvent::Type::MouseButtonRelease,
         coordinates + moveCoordinates,
         coordinates + moveCoordinates,
         globalCoordinates + moveCoordinates,
         button,
         Qt::MouseButtons(),
         modifier,
         Devices::GetMouseDevice()
      );

      if (!sendMouseEvent(*widget, &releaseEvent))
      {
         throw Exception(
            "Cannot send release event"
         );
      }
      if (action == Mouse::RELEASE || action == Mouse::CLICK)
      {
         eventAccepted = eventAccepted || releaseEvent.isAccepted();
      }
      actionFound = true;
   }
   
   if (!actionFound)
   {
      throw Exception(
         "Cannot send unknown event '" + action + "': "
         "Event type is not supported"
      );
   }
   if (!eventAccepted)
   {
      result["warning"] = "No widget accepted this event";
   }
   return result;
}

} // namespace Qat