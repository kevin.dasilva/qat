// (c) Copyright 2023, Qat’s Authors

#include <qat-server/CommCommandExecutor.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/RequestHandler.h>
#include <qat-server/SignalListener.h>

#include <QMetaMethod>
#include <QMetaProperty>
#include <QObject>
#include <QPointer>
#include <QString>

#include <iostream>
#include <string>

namespace Qat
{

using namespace Constants;

std::map<std::string, QPointer<SignalListener>> CommCommandExecutor::mListenerCache;

CommCommandExecutor::CommCommandExecutor(
   const nlohmann::json& request,
   RequestHandler* requestHandler):
   BaseCommandExecutor(request),
   mRequestHandler(requestHandler)
{
   for (const auto& field : { OBJECT_ATTRIBUTE })
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " + field
         );
      }
   }
}

nlohmann::json CommCommandExecutor::Run() const
{
   const auto cmdName = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   if (cmdName == Communication::INIT)
   {
      const auto args = mRequest.at(ARGUMENTS);
      if (!args.contains(HOST) || !args.contains(PORT))
      {
         throw Exception(
            "Host and port arguments are mandatory"
         );
      }

      const auto hostName = args.at(HOST).get<std::string>();
      const auto port = args.at(PORT).get<int>();
      mRequestHandler->ConnectToHost(hostName, port);
   }
   else if (cmdName == Communication::CLOSE)
   {
      mRequestHandler->DisconnectFromHost();
   }
   else if (cmdName == Communication::CONNECT)
   {
      auto* object = FindObject();
      if (!object)
      {
         throw Exception(
            "Cannot connect property: object not found"
         );
      }
      auto propertyName = mRequest.at(ARGUMENTS).get<std::string>();
      // Using () at the end of the property name forces the connection to use this signal
      // even if a property with the same name exists
      const auto propertyNameLen = propertyName.size();
      bool isSignal = false;
      if (propertyNameLen > 2 && propertyName.substr(propertyNameLen - 2) == "()")
      {
         propertyName = propertyName.substr(0, propertyNameLen - 2);
         isSignal = true;
      }
      auto* listener = new SignalListener(mRequestHandler);
      QMetaMethod metaSignal;

      auto index = isSignal ? -1 : object->metaObject()->indexOfProperty(propertyName.c_str());
      if (!isSignal && index >= 0)
      {
         auto metaProp = object->metaObject()->property(index);
         if (!metaProp.hasNotifySignal())
         {
            listener->deleteLater();
            throw Exception(
               "Cannot connect property: it has no associated notification signal"
            );
         }
         metaSignal = metaProp.notifySignal();
         // Send current value when binding properties
         listener->AttachTo(object, propertyName);
      }
      else
      {
         const auto signalName = propertyName + "()";
         index = object->metaObject()->indexOfSignal(signalName.c_str());
         if (index >= 0)
         {
            metaSignal = object->metaObject()->method(index);
         }
         else
         {
            listener->deleteLater();
            throw Exception(
               "Cannot connect property or signal: property was not found"
            );
         }
      }

      mListenerCache[listener->GetId()] = listener;
      const auto slotIndex = listener->metaObject()->indexOfMethod("Notify()");

      QObject::connect(
         object,
         metaSignal,
         listener,
         listener->metaObject()->method(slotIndex)
      );

      nlohmann::json result;
      result["found"] = true;
      result[OBJECT_ID] = listener->GetId();
      return result;
   }
   else if (cmdName == Communication::DISCONNECT)
   {
      bool found = false;
      const auto callbackId = mRequest.at(ARGUMENTS).get<std::string>();
      if (mListenerCache.count(callbackId))
      {
         found = true;
         auto listener = mListenerCache[callbackId];
         mListenerCache.erase(callbackId);
         if (listener)
         {
            listener->deleteLater();
         }
      }

      nlohmann::json result;
      result["found"] = found;
      return result;
   }
   else
   {
      throw Exception(
         "Command '" + cmdName + "' is not supported"
      );
   }

   nlohmann::json result;
   result["found"] = true;
   return result;
}

} // namespace Qat