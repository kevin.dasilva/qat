// (c) Copyright 2023, Qat’s Authors

#include <qat-server/MethodCaller.h>
#include <qat-server/Exception.h>
#include <qat-server/QVariantToJson.h>

#include <QGenericArgument>
#include <QJsonArray>
#include <QMetaMethod>
#include <QVariant>
#include <QVariantList>

#include <array>
#include <iostream>

#define LIST_ONE_ARG(array,i) array[i]
#define LIST_1_ARG(array) LIST_ONE_ARG(array, 0)
#define LIST_2_ARGS(array) LIST_1_ARG(array), LIST_ONE_ARG(array, 1)
#define LIST_3_ARGS(array) LIST_2_ARGS(array), LIST_ONE_ARG(array, 2)
#define LIST_4_ARGS(array) LIST_3_ARGS(array), LIST_ONE_ARG(array, 3)
#define LIST_5_ARGS(array) LIST_4_ARGS(array), LIST_ONE_ARG(array, 4)
#define LIST_6_ARGS(array) LIST_5_ARGS(array), LIST_ONE_ARG(array, 5)
#define LIST_7_ARGS(array) LIST_6_ARGS(array), LIST_ONE_ARG(array, 6)
#define LIST_8_ARGS(array) LIST_7_ARGS(array), LIST_ONE_ARG(array, 7)
#define LIST_9_ARGS(array) LIST_8_ARGS(array), LIST_ONE_ARG(array, 8)
#define LIST_10_ARGS(array) LIST_9_ARGS(array), LIST_ONE_ARG(array, 9)

namespace
{
constexpr int MAX_NUM_ARGS{10};
} // anonymous namespace

namespace Qat
{

bool MethodCaller::Call(
   QObject* object,
   const std::string& functionName,
   const QJsonArray& argJsonArray)
{
   // Invalidate the result
   mResult = QVariant();

   // Cannot invoke method with more than 10 parameters (Qt limitation)
   if (argJsonArray.count() > MAX_NUM_ARGS)
   {
      throw Exception(
         "Unsupported method call: "
         "Function '" + functionName + "' takes more than " + 
            std::to_string(MAX_NUM_ARGS) + " parameters"
      );
   }

   // Find methods with the given name
   const auto nbMethods = object->metaObject()->methodCount();
   for (int i = 0; i < nbMethods; ++i)
   {
      const auto method = object->metaObject()->method(i);
      const auto methodName = method.name().toStdString();
      if (functionName == methodName)
      {
         /// \note This code is from https://gist.github.com/andref/2838534
         QVariantList argumentList;
         std::array<QGenericArgument, MAX_NUM_ARGS> genericArgumentList;
         // Build arguments for each overload and use the first that is compatible
         if (BuildArguments(method, argumentList, argJsonArray))
         {
            // Initialize result variant
            #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
               mResult = QVariant(
                  QMetaType::type(method.typeName()),
                  static_cast<void*>(nullptr));
            #else
               mResult = QVariant(
                  method.returnMetaType(),
                  static_cast<void*>(nullptr));
            #endif

            QList<QGenericArgument> arguments;
            for (int j = 0; j < argumentList.size(); ++j)
            {
               // Notice that we have to take a reference to the argument, else 
               // we'd be pointing to a copy that will be destroyed when this
               // loop exits. 
               QVariant& argument = argumentList[j];

               #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
                  const auto typeName = QMetaType::typeName(argument.userType());
               #elif QT_VERSION < QT_VERSION_CHECK(6, 5, 0)
                  const auto typeName = QMetaType(argument.userType()).name();
               #else
                  const auto typeName = method.parameterTypeName(j);
               #endif

               // A const_cast is needed because calling data() would detach
               // the QVariant.
               QGenericArgument genericArgument(
                  typeName,
                  const_cast<void*>(argument.constData())
               );

               arguments << genericArgument;
            }

            const auto success = CallMethod(object, method, argumentList);

            if (!success)
            {
               throw Exception(
                  "Method call failed: "
                  "Function '" + functionName + "' failed to be invoked"
               );
            }

            return true;
         }
      }
   }

   return false;
}

const QVariant& MethodCaller::GetResult() const noexcept
{
   return mResult;
}

bool MethodCaller::BuildArguments(
   const QMetaMethod& method,
   QVariantList& argumentList,
   const QJsonArray& argJsonArray) const
{
   const auto nbParameters = method.parameterCount();
   if (nbParameters != argJsonArray.count())
   {
      return false;
   }
   for (auto i = 0; i < nbParameters; ++i)
   {
      
      const auto arg = FromJson(argJsonArray.at(i));
      QVariant copy = QVariant(arg);

      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         const auto methodArguments = method.parameterTypes();
         const auto& paramTypeName = methodArguments.at(i);
         const auto methodType = QVariant::nameToType(paramTypeName);
         const bool isValidType = methodType != QVariant::Type::Invalid;
         const auto variantType = copy.type();
         const auto qVariantType = static_cast<QVariant::Type>(QMetaType::QVariant);
      #elif QT_VERSION < QT_VERSION_CHECK(6, 2, 0)
         const auto methodArguments = method.parameterTypes();
         const auto& paramTypeName = methodArguments.at(i);
         const auto methodType = QMetaType::fromName(paramTypeName);
         const bool isValidType = methodType.id() != QMetaType::UnknownType;
         const auto variantType = copy.metaType();
         const auto qVariantType = QMetaType(QMetaType::Type::QVariant);
      #else
         const auto paramTypeName = method.parameterTypeName(i);
         const auto methodType = method.parameterMetaType(i);
         const bool isValidType = methodType.isValid();
         const auto variantType = copy.metaType();
         const auto qVariantType = QMetaType(QMetaType::Type::QVariant);
      #endif

      // If required, try to convert the argument value to the expected type
      if (isValidType && variantType != methodType) 
      {
         // convert() does not support QVariant as a destination
         if (methodType == qVariantType)
         {
            QVariant temp(qVariantType, copy.data());
            std::swap(temp, copy);
         }
         else if (!copy.convert(methodType)) 
         {
            std::cerr << "Converting argument #" << i << " to type '" << paramTypeName.toStdString() << "' failed" << std::endl;
            return false;
         }
      }

      argumentList << copy;
   }

   return true;
}


#if QT_VERSION < QT_VERSION_CHECK(6, 5, 0)
bool MethodCaller::CallMethod(
      QObject* object,
      const QMetaMethod& method,
      QVariantList& argumentList) const
{
   QList<QGenericArgument> arguments;
   for (int j = 0; j < argumentList.size(); ++j)
   {
      // Notice that we have to take a reference to the argument, else 
      // we'd be pointing to a copy that will be destroyed when this
      // loop exits. 
      QVariant& argument = argumentList[j];      
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         const auto typeName = QMetaType::typeName(argument.userType());
      #elif QT_VERSION < QT_VERSION_CHECK(6, 5, 0)
         const auto typeName = QMetaType(argument.userType()).name();
      #else
         const auto typeName = method.parameterTypeName(j);
      #endif      
      
      // A const_cast is needed because calling data() would detach
      // the QVariant.
      QGenericArgument genericArgument(
         typeName,
         const_cast<void*>(argument.constData())
      );      
      arguments << genericArgument;
   }
   
   QGenericReturnArgument returnArgument(
      method.typeName(),
      const_cast<void*>(mResult.constData())
   );

   return method.invoke(
      object,
      Qt::AutoConnection,
      returnArgument,
      arguments.value(0),
      arguments.value(1),
      arguments.value(2),
      arguments.value(3),
      arguments.value(4),
      arguments.value(5),
      arguments.value(6),
      arguments.value(7),
      arguments.value(8),
      arguments.value(9)
   );
}

#else // Qt >= 6.5.0
bool MethodCaller::CallMethod(
      QObject* object,
      const QMetaMethod& method,
      QVariantList& argumentList) const
{
   std::vector<QMetaMethodArgument> metaArguments;

   for (int k = 0; k < method.parameterCount(); ++k)
   {
      auto iface = method.parameterMetaType(k).iface();
      QMetaMethodArgument arg {
         iface,
         method.parameterNames()[k],
         argumentList[k].data()
      };
      metaArguments.push_back(arg);
   }

   #if QT_VERSION < QT_VERSION_CHECK(6, 7, 0)
      QMetaMethodReturnArgument returnValue {
         method.returnMetaType().iface(),
         method.returnMetaType().name(),
         const_cast<void*>(mResult.data())
      };
   #else
      QTemplatedMetaMethodReturnArgument<void> returnValue {
         method.returnMetaType().iface(),
         method.returnMetaType().name(),
         const_cast<void*>(mResult.data())
      };
   #endif
   if (metaArguments.size() == 0)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue
      );
   }
   else if (metaArguments.size() == 1)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_1_ARG(metaArguments)
      );
   }
   else if (metaArguments.size() == 2)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_2_ARGS(metaArguments)
      );
   }
   else if (metaArguments.size() == 3)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_3_ARGS(metaArguments)
      );
   }
   else if (metaArguments.size() == 4)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_4_ARGS(metaArguments)
      );
   }
   else if (metaArguments.size() == 5)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_5_ARGS(metaArguments)
      );
   }
   else if (metaArguments.size() == 6)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_6_ARGS(metaArguments)
      );
   }
   else if (metaArguments.size() == 7)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_7_ARGS(metaArguments)
      );
   }
   else if (metaArguments.size() == 8)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_8_ARGS(metaArguments)
      );
   }
   else if (metaArguments.size() == 9)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_9_ARGS(metaArguments)
      );
   }
   else if (metaArguments.size() == 10)
   {
      return method.invoke(
         object,
         Qt::AutoConnection,
         returnValue,
         LIST_10_ARGS(metaArguments)
      );
   }

   return false;
}
#endif

} // namespace Qat