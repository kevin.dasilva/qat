// (c) Copyright 2023, Qat’s Authors

#include <qat-server/QVariantToJson.h>
#include <qat-server/Constants.h>
#include <qat-server/EnumConversion.h>
#include <qat-server/FindCommandExecutor.h>

#include <QAbstractItemModel>
#include <QBrush>
#include <QColor>
#include <QFont>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QLine>
#include <QModelIndex>
#include <QObject>
#include <QQuaternion>
#include <QRect>
#include <QVariant>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

#include <iostream>

namespace
{
/// Return the type name of the given type ID
/// \param[in] type A type ID
/// \return the corresponding type name
const auto* GetTypeName(int type)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   return QMetaType::typeName(type);
#else
   return QMetaType(type).name();
#endif
}

/// Return the type ID of the given type name
/// \param[in] name A type name
/// \return the corresponding type ID
int GetTypeId(const QByteArray& name)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   return QMetaType::type(name);
#else
   return QMetaType::fromName(name).id();
#endif
}
}

namespace Qat
{

int GetVariantType(const QVariant& variant)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   return variant.type();
#else
   return variant.metaType().id();
#endif
}

nlohmann::json ToJson(const QVariant& variant)
{
   nlohmann::json result;
   const auto variantType = GetVariantType(variant);
   result["QVariantType"] = variantType;
   const auto* typeName = GetTypeName(variantType);
   if (typeName)
   {
      result["QVariantTypeName"] = typeName;
   }
   switch (variantType)
   {
   case QMetaType::Type::QColor:
   {
      const auto color = variant.value<QColor>();
      result["name"] = color.name(QColor::HexArgb).toStdString();
      result["red"] = color.red();
      result["green"] = color.green();
      result["blue"] = color.blue();
      result["alpha"] = color.alpha();
      break;
   }
   case QMetaType::Type::QBrush:
   {
      const auto brush = variant.value<QBrush>();
      result["style"] = static_cast<std::underlying_type_t<BrushStyle>>(FromQt(brush.style()));
      result["color"] = ToJson(brush.color());
      result["gradient"] = brush.gradient() ? brush.gradient()->type() : QGradient::Type::NoGradient;
      break;
   }
   case QMetaType::Type::QFont:
   {
      const auto font = variant.value<QFont>();
      
      result["bold"] = font.bold();
      result["family"] = font.family().toStdString();
      result["fixedPitch"] = font.fixedPitch();
      result["italic"] = font.italic();
      result["pixelSize"] = font.pixelSize();
      result["pointSize"] = font.pointSize();
      result["strikeOut"] = font.strikeOut();
      result["underline"] = font.underline();
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      const auto weight = static_cast<QFont::Weight>(font.weight());
      #else
      const auto weight = font.weight();
      #endif
      result["weight"] = static_cast<std::underlying_type_t<FontWeight>>(FromQt(weight));
      break;
   }
   case QMetaType::Type::QPoint:
   {
      const auto point = variant.value<QPoint>();
      result["x"] = point.x();
      result["y"] = point.y();
      break;
   }
   case QMetaType::Type::QPointF:
   {
      const auto point = variant.value<QPointF>();
      result["x"] = point.x();
      result["y"] = point.y();
      break;
   }
   case QMetaType::Type::QLine:
   {
      const auto line = variant.value<QLine>();
      result["p1"] = ToJson(line.p1());
      result["p2"] = ToJson(line.p2());
      result["center"] = ToJson(line.center());
      break;
   }
   case QMetaType::Type::QLineF:
   {
      const auto line = variant.value<QLineF>();
      result["p1"] = ToJson(line.p1());
      result["p2"] = ToJson(line.p2());
      result["center"] = ToJson(line.center());
      break;
   }
   case QMetaType::Type::QSize:
   {
      const auto size = variant.value<QSize>();
      result["width"] = size.width();
      result["height"] = size.height();
      break;
   }
   case QMetaType::Type::QSizeF:
   {
      const auto size = variant.value<QSizeF>();
      result["width"] = size.width();
      result["height"] = size.height();
      break;
   }
   case QMetaType::Type::QRect:
   {
      const auto rect = variant.value<QRect>();
      result["x"] = rect.x();
      result["y"] = rect.y();
      result["width"] = rect.width();
      result["height"] = rect.height();
      break;
   }
   case QMetaType::Type::QRectF:
   {
      const auto rect = variant.value<QRectF>();
      result["x"] = rect.x();
      result["y"] = rect.y();
      result["width"] = rect.width();
      result["height"] = rect.height();
      break;
   }
   case QMetaType::Type::QQuaternion:
   {
      const auto quaternion = variant.value<QQuaternion>();
      result["x"] = quaternion.x();
      result["y"] = quaternion.y();
      result["z"] = quaternion.z();
      result["scalar"] = quaternion.scalar();
      break;
   }
   case QMetaType::Type::QVector2D:
   {
      const auto vector = variant.value<QVector2D>();
      result["x"] = vector.x();
      result["y"] = vector.y();
      break;
   }
   case QMetaType::Type::QVector3D:
   {
      const auto vector = variant.value<QVector3D>();
      result["x"] = vector.x();
      result["y"] = vector.y();
      result["z"] = vector.z();
      break;
   }
   case QMetaType::Type::QVector4D:
   {
      const auto vector = variant.value<QVector4D>();
      result["x"] = vector.x();
      result["y"] = vector.y();
      result["z"] = vector.z();
      result["w"] = vector.w();
      break;
   }
   case QMetaType::Type::QByteArray:
   {
      const auto byteArray = variant.value<QByteArray>();
      std::vector<char> array(byteArray.cbegin(), byteArray.cend());
      result["bytes"] = array;
      break;
   }
   case QMetaType::Type::QModelIndex:
   {
      const auto index = variant.value<QModelIndex>();
      result["row"] = index.row();
      result["column"] = index.column();
      result["model"] = std::to_string(reinterpret_cast<std::uintptr_t>(index.model()));
      const auto parentIndex = index.parent();
      if(parentIndex.isValid())
      {
         result["parentIndex"] = ToJson(parentIndex);
      }
      break;
   }
   case QMetaType::Type::QVariant:
   {
      return ToJson(variant.value<QVariant>());
   }
   default:
   {
      const auto jsonValue = QJsonValue::fromVariant(variant);
      QJsonObject jsonObject;
      jsonObject.insert("temp", jsonValue);
      QJsonDocument doc(jsonObject);
      const auto serialized = doc.toJson().toStdString();
      const auto value = nlohmann::json::parse(serialized);
      result = value["temp"];
      break;
   }
   }

   return result;
}


QVariant FromJson(const QJsonValue& json)
{
   if (json.isObject())
   {
      try
      {
         QJsonObject jsonObject;
         jsonObject.insert(Constants::OBJECT_DEFINITION.c_str(), json);
         QJsonDocument doc(jsonObject);
         nlohmann::json definition = nlohmann::json::parse(doc.toJson().toStdString());
         FindCommandExecutor executor(definition);
         auto* obj = executor.FindObject();
         return QVariant::fromValue(obj);
      }
      catch (...)
      {
         // May be a custom object
      }
   }
   const auto jsonObject = json.toObject();
   int variantType = QMetaType::Type::UnknownType;
   if (jsonObject.contains("QVariantType"))
   {
      variantType = jsonObject.value("QVariantType").toInt();
   }
   else if (jsonObject.contains("QVariantTypeName"))
   {
      variantType = GetTypeId(jsonObject.value("QVariantTypeName").toString().toUtf8());
   }
   else
   {
      return json.toVariant();
   }
   
   switch (variantType)
   {
   case QMetaType::Type::QColor:
   {
      if (jsonObject.contains("name"))
      {
         return QColor(jsonObject["name"].toString());
      }
      else if (jsonObject.contains("red") &&
         jsonObject.contains("green") &&
         jsonObject.contains("blue"))
      {
         int alpha = 255;
         if (jsonObject.contains("alpha"))
         {
            alpha = jsonObject["alpha"].toInt();
         }
         return QColor(
            jsonObject["red"].toInt(),
            jsonObject["green"].toInt(),
            jsonObject["blue"].toInt(),
            alpha);
      }
      break;
   }
   case QMetaType::Type::QBrush:
   {
      Qt::BrushStyle style = Qt::SolidPattern;
      QColor color;
      if (jsonObject.contains("style"))
      {
         const auto brushStyle = static_cast<BrushStyle>(jsonObject["style"].toInt());
         style = ToQt(brushStyle);
      }
      if (jsonObject.contains("color"))
      {
         color = FromJson(jsonObject["color"]).value<QColor>();
      }
      if (jsonObject.contains("gradient"))
      {
         const auto gradientType = jsonObject["gradient"].toInt();
         if (static_cast<QGradient::Type>(gradientType) != QGradient::Type::NoGradient)
         {
            std::cout << "Warning: gradient will be ignored when using this QBrush" << std::endl;
         }
      }
      return QBrush(color, style);
   }
   case QMetaType::Type::QFont:
   {
      QFont font;
      if (jsonObject.contains("family"))
      {
         font.setFamily(jsonObject["family"].toString());
      }
      if (jsonObject.contains("bold"))
      {
         font.setBold(jsonObject["bold"].toBool());
      }
      if (jsonObject.contains("italic"))
      {
         font.setItalic(jsonObject["italic"].toBool());
      }
      if (jsonObject.contains("strikeOut"))
      {
         font.setStrikeOut(jsonObject["strikeOut"].toBool());
      }
      if (jsonObject.contains("underline"))
      {
         font.setUnderline(jsonObject["underline"].toBool());
      }
      if (jsonObject.contains("fixedPitch"))
      {
         font.setFixedPitch(jsonObject["fixedPitch"].toBool());
      }
      if (jsonObject.contains("pixelSize"))
      {
         font.setPixelSize(jsonObject["pixelSize"].toInt());
      }
      if (jsonObject.contains("pointSize"))
      {
         font.setPointSize(jsonObject["pointSize"].toInt());
      }
      if (jsonObject.contains("weight"))
      {
         const auto weight = static_cast<FontWeight>(jsonObject["weight"].toInt());
         font.setWeight(ToQt(weight));
      }
      return font;
   }
   case QMetaType::Type::QPoint:
   {
      QPoint point;
      if (jsonObject.contains("x"))
      {
         point.setX(jsonObject["x"].toInt());
      }
      if (jsonObject.contains("y"))
      {
         point.setY(jsonObject["y"].toInt());
      }
      return point;
   }
   case QMetaType::Type::QPointF:
   {
      QPointF point;
      if (jsonObject.contains("x"))
      {
         point.setX(jsonObject["x"].toDouble());
      }
      if (jsonObject.contains("y"))
      {
         point.setY(jsonObject["y"].toDouble());
      }
      return point;
   }
   case QMetaType::QLine:
   {
      QPoint point1, point2;
      if (jsonObject.contains("p1"))
      {
         point1 = FromJson(jsonObject["p1"]).value<QPoint>();
      }
      if (jsonObject.contains("p2"))
      {
         point2 = FromJson(jsonObject["p2"]).value<QPoint>();
      }
      return QLine(point1, point2);
   }
   case QMetaType::QLineF:
   {
      QPointF point1, point2;
      if (jsonObject.contains("p1"))
      {
         point1 = FromJson(jsonObject["p1"]).value<QPointF>();
      }
      if (jsonObject.contains("p2"))
      {
         point2 = FromJson(jsonObject["p2"]).value<QPointF>();
      }
      return QLineF(point1, point2);
   }
   case QMetaType::Type::QSize:
   {
      QSize size;
      if (jsonObject.contains("width"))
      {
         size.setWidth(jsonObject["width"].toInt());
      }
      if (jsonObject.contains("height"))
      {
         size.setHeight(jsonObject["height"].toInt());
      }
      return size;
   }
   case QMetaType::Type::QSizeF:
   {
      QSizeF size;
      if (jsonObject.contains("width"))
      {
         size.setWidth(jsonObject["width"].toDouble());
      }
      if (jsonObject.contains("height"))
      {
         size.setHeight(jsonObject["height"].toDouble());
      }
      return size;
   }
   case QMetaType::Type::QRect:
   {
      QRect rect;
      if (jsonObject.contains("x"))
      {
         rect.setLeft(jsonObject["x"].toInt());
      }
      if (jsonObject.contains("y"))
      {
         rect.setTop(jsonObject["y"].toInt());
      }
      QSize size;
      if (jsonObject.contains("width"))
      {
         size.setWidth(jsonObject["width"].toInt());
      }
      if (jsonObject.contains("height"))
      {
         size.setHeight(jsonObject["height"].toInt());
      }
      rect.setSize(size);
      return rect;
   }
   case QMetaType::Type::QRectF:
   {
      QRectF rect;
      if (jsonObject.contains("x"))
      {
         rect.setLeft(jsonObject["x"].toDouble());
      }
      if (jsonObject.contains("y"))
      {
         rect.setTop(jsonObject["y"].toDouble());
      }
      QSizeF size;
      if (jsonObject.contains("width"))
      {
         size.setWidth(jsonObject["width"].toDouble());
      }
      if (jsonObject.contains("height"))
      {
         size.setHeight(jsonObject["height"].toDouble());
      }
      rect.setSize(size);
      return rect;
   }
   case QMetaType::Type::QQuaternion:
   {
      QQuaternion quaternion;
      if (jsonObject.contains("x"))
      {
         quaternion.setX(static_cast<float>(jsonObject["x"].toDouble()));
      }
      if (jsonObject.contains("y"))
      {
         quaternion.setY(static_cast<float>(jsonObject["y"].toDouble()));
      }
      if (jsonObject.contains("z"))
      {
         quaternion.setZ(static_cast<float>(jsonObject["z"].toDouble()));
      }
      if (jsonObject.contains("scalar"))
      {
         quaternion.setScalar(static_cast<float>(jsonObject["scalar"].toDouble()));
      }
      return quaternion;
   }
   case QMetaType::Type::QVector2D:
   {
      QVector2D vector;
      if (jsonObject.contains("x"))
      {
         vector.setX(static_cast<float>(jsonObject["x"].toDouble()));
      }
      if (jsonObject.contains("y"))
      {
         vector.setY(static_cast<float>(jsonObject["y"].toDouble()));
      }
      return vector;
   }
   case QMetaType::Type::QVector3D:
   {
      QVector3D vector;
      if (jsonObject.contains("x"))
      {
         vector.setX(static_cast<float>(jsonObject["x"].toDouble()));
      }
      if (jsonObject.contains("y"))
      {
         vector.setY(static_cast<float>(jsonObject["y"].toDouble()));
      }
      if (jsonObject.contains("z"))
      {
         vector.setZ(static_cast<float>(jsonObject["z"].toDouble()));
      }
      return vector;
   }
   case QMetaType::Type::QVector4D:
   {
      QVector4D vector;
      if (jsonObject.contains("x"))
      {
         vector.setX(static_cast<float>(jsonObject["x"].toDouble()));
      }
      if (jsonObject.contains("y"))
      {
         vector.setY(static_cast<float>(jsonObject["y"].toDouble()));
      }
      if (jsonObject.contains("z"))
      {
         vector.setZ(static_cast<float>(jsonObject["z"].toDouble()));
      }
      if (jsonObject.contains("w"))
      {
         vector.setW(static_cast<float>(jsonObject["w"].toDouble()));
      }
      return vector;
   }
   case QMetaType::Type::QByteArray:
   {
      QByteArray byteArray;
      if (jsonObject.contains("bytes"))
      {
         const auto jsonArray = jsonObject["bytes"].toArray();
         for (const auto& value : jsonArray)
         {
            byteArray.append(static_cast<char>(value.toInt()));
         }
      }
      return byteArray;
   }
   case QMetaType::Type::QModelIndex:
   {
      if (jsonObject.contains("row") &&
         jsonObject.contains("column") &&
         jsonObject.contains("model"))
      {
         const auto row = jsonObject["row"].toInt();
         const auto column = jsonObject["column"].toInt();
         QModelIndex parentIndex;
         if (jsonObject.contains("parentIndex"))
         {
            parentIndex = FromJson(jsonObject["parentIndex"]).value<QModelIndex>();
         }
         const auto pointer = std::stoull(jsonObject["model"].toString().toStdString());
         auto* model = reinterpret_cast<QAbstractItemModel*>(pointer);
         if (model)
         {
            return model->index(row, column, parentIndex);
         }
      }
      break;
   }

   }

   return {};
}

} // namespace Qat