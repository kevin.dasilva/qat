// (c) Copyright 2023, Qat’s Authors

#include <qat-server/PluginManager.h>
#include <qat-server/Exception.h>

#include <QtGlobal>

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #define FOR_WINDOWS
   #include <windows.h>
   const std::string LIB_EXTENSION{".dll"};
#elif defined(__linux__)
   #define FOR_LINUX
   #include <dlfcn.h>
   #include <link.h>
   const std::string LIB_EXTENSION{".so"};
#elif defined (__APPLE__)
   #define FOR_MACOS
   #include <dlfcn.h>
   const std::string LIB_EXTENSION{".dylib"};
#else
#   error "Unsupported platform"
#endif

/// This function is defined only to provide a symbolic name in the current library.
/// Its address is used with dladdr1 to retrieve the absolute path of this library.
void resolve_path(){}

namespace Qat
{

const PluginManager& PluginManager::GetInstance()
{
   static auto globalManager = PluginManager("plugins");
   return globalManager;
}

PluginManager::PluginManager(const std::filesystem::path& root)
{
   const auto currentPath = GetLibraryPath();
   mPluginRoot = currentPath.parent_path() / root;

   // Get current Qt version
   const std::string qtVersion = QT_VERSION_STR;

   // Get plugin library suffix based on Qt version
   std::stringstream versionStream(qtVersion);
   std::string token;
   std::vector<std::string> elements;
   while (std::getline(versionStream, token, '.'))
   {
      elements.push_back(token);
   }
   if (elements.size() < 2)
   {
      std::cerr << "Could not get Qt version elements" << std::endl;
      return;
   }
   const auto librarySuffix = elements[0] + "." + elements[1] + LIB_EXTENSION;
   LoadPlugins(librarySuffix);
}

const std::map<std::string, std::unique_ptr<Plugin>>&
      PluginManager::GetPlugins() const
{
   return mPlugins;
}

std::filesystem::path PluginManager::GetLibraryPath() const
{
#ifdef FOR_LINUX
   Dl_info dlinfo;
   struct link_map* result = nullptr;
   if (!dladdr1((void*)&resolve_path, &dlinfo, (void**)&result, RTLD_DI_LINKMAP))
   {
      throw Exception("dladdr1 failed: Unable to load plugins");
   }
   if (!result)
   {
      throw Exception("dladdr1 returned NULL pointer: Unable to load plugins");
   }

   return result->l_name;
#elif defined(FOR_WINDOWS)
   constexpr int MAX_PATH_LEN{4096};
   char libPathData[MAX_PATH_LEN];
   const auto libHandle = GetModuleHandleA("injector.dll");
   const auto rc = GetModuleFileNameA(libHandle, libPathData, MAX_PATH_LEN);
   if (!rc)
   {
      std::stringstream message;
      message << "Could not retrieve library path (error #" << 
         GetLastError() << ")";
         throw Exception(message.str());
   }
   return libPathData;
#elif defined(FOR_MACOS)
   Dl_info dlinfo;
   if (!dladdr((void*)&resolve_path, &dlinfo))
   {
      throw Exception("dladdr1 failed: Unable to load plugins");
   }
   return dlinfo.dli_fname;
#endif
}

void PluginManager::LoadPlugins(const std::string& librarySuffix)
{
#if defined(FOR_LINUX) || defined (FOR_MACOS)
   for (const auto& file: std::filesystem::directory_iterator(mPluginRoot))
   {
      const auto& filePath = file.path();
      if(filePath.filename().string().ends_with(librarySuffix))
      {
         const auto handle = dlopen(filePath.string().c_str(), RTLD_LAZY);
         if (!handle)
         {
            std::cerr << "Failed to load plugin: " << filePath.string() << std::endl;
            std::cerr << dlerror() << std::endl;
            continue;
         }
         else
         {
            std::cout << "Successfully loaded plugin: " << filePath.string() << std::endl;
         }

         // Strip "lib" prefix
         const auto pluginName = filePath.stem().string().substr(3);
         mPlugins[pluginName] = std::make_unique<Plugin>(handle);
      }
   }
#elif defined(FOR_WINDOWS)
   for (const auto& file: std::filesystem::directory_iterator(mPluginRoot))
   {
      const auto& filePath = file.path();
      if(filePath.filename().string().ends_with(librarySuffix))
      {
         const auto handle = LoadLibraryA(filePath.string().c_str());
         if (!handle)
         {
            std::cerr << "Failed to load plugin: " << filePath.string() << std::endl;
            continue;
         }
         else
         {
            std::cout << "Successfully loaded plugin: " << filePath.string() << std::endl;
         }

         const auto pluginName = filePath.stem().string();
         mPlugins[pluginName] = std::make_unique<Plugin>(handle);
      }
   }
#endif
}
}