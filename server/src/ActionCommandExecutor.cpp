// (c) Copyright 2023, Qat’s Authors

#include <qat-server/ActionCommandExecutor.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/ExternalEventsFilter.h>
#include <qat-server/ImageWrapper.h>
#include <qat-server/IObjectPicker.h>
#include <qat-server/IWidget.h>
#include <qat-server/ObjectPickerFilter.h>
#include <qat-server/NativeEventsFilter.h>
#include <qat-server/WidgetLocator.h>
#include <qat-server/WidgetWrapper.h>

#include <QGuiApplication>
#include <QMouseEvent>
#include <QObject>
#include <QPointer>
#include <QWindow>
// Qt private headers are required to force application deactivation
#include <QtGui/qpa/qwindowsysteminterface.h>
#include <QtGui/qpa/qwindowsysteminterface_p.h>

#include <filesystem>
#include <iostream>
#include <string>

namespace
{

/// Pointer to the shared external event filter
static QPointer<Qat::ExternalEventsFilter> externalEventFilter{nullptr};
static Qat::NativeEventsFilter nativeEventFilter;

} // anonymous namespace

namespace Qat
{

using namespace Constants;

ActionCommandExecutor::ActionCommandExecutor(const nlohmann::json& request):
   BaseCommandExecutor(request)
{
   for (const auto& field : { OBJECT_ATTRIBUTE, ARGUMENTS })
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " + field
         );
      }
   }
}

nlohmann::json ActionCommandExecutor::Run() const
{

   const auto actionName = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   const auto arg = mRequest.at(ARGUMENTS).get<std::string>();
   if (actionName == Action::SCREENSHOT)
   {
      std::filesystem::path path(arg);
      if (!path.has_filename())
      {
         path = path / "qat-screenshot.png";
      }
      std::filesystem::create_directories(path.parent_path());
      const auto mainWindows = WidgetWrapper::GetTopWindows();
      bool success = true;
      const auto originalFileName = path.stem();
      int currentIndex = 1;
      for (std::size_t i = 0; i < mainWindows.size(); ++i)
      {
         auto* window = mainWindows[i];
         if (!window)
         {
            continue;
         }
         
         auto image = WidgetWrapper::GrabImage(window);
         if(!image || image->size().isEmpty())
         {
            continue;
         }
         const auto ext = path.extension();
         if (mainWindows.size() > 1)
         {
            path.replace_filename(originalFileName.string() + std::to_string(currentIndex));
         }
         ++currentIndex;
         path.replace_extension(ext);
         std::cout << "Generating window screenshot to: " << path << std::endl;
         std::filesystem::create_directories(path.parent_path());
         success = success && image->save(QString::fromStdString(path.string()));
      }
      nlohmann::json result;
      result["found"] = success;
      return result;
   }
   else if (actionName == Action::GRAB)
   {
      auto* object = FindObject();
      const auto widget = WidgetWrapper::Cast(object);
      nlohmann::json result;
      result["found"] = widget != nullptr;
      if (widget)
      {
         QPointer wrapper = new ImageWrapper();
         result[CACHE_UID] = RegisterObject(wrapper);

         auto callback = [wrapper](const QImage& image)
         {
            if (wrapper)
            {
               wrapper->SetImage(image);
            }
         };
         widget->GrabImage(callback);
      }
      return result;
   }
   else if (actionName == Action::PICKER)
   {
      nlohmann::json pickerDefinition;
      pickerDefinition[OBJECT_NAME] = PICKER_NAME;
      bool pickerFound = false;

      /// Handle CTRL key, allowing to bypass picking
      static auto* pickerFilter = new ObjectPickerFilter(qApp);
      if (arg == "enable")
      {
         qApp->installEventFilter(pickerFilter);
      }
      else if (arg == "disable")
      {
         qApp->removeEventFilter(pickerFilter);
      }

      // There is one picker per main window
      for (auto* mainWindow : WidgetWrapper::GetTopWindows())
      {
         IObjectPicker* picker = nullptr;
         QObject* qobject = nullptr;
         try
         {
            qobject = mainWindow->findChild<QObject*>(
               QString::fromStdString(PICKER_NAME), Qt::FindDirectChildrenOnly);

            // qobject_cast does not work with interfaces
            picker = dynamic_cast<IObjectPicker*>(qobject);
            if (picker)
            {
               pickerFound = true;
               picker->Reset();
            }
            else
            {
               std::cout << "ObjectPicker not found" << std::endl;
            }
         }
         catch (...)
         {
            std::cout << "Cannot reset ObjectPicker (exception)" << std::endl;
         }
         if (arg == "enable")
         {
            if (!picker)
            {
               picker = WidgetWrapper::CreatePicker(mainWindow);
               if (!picker)
               {
                  std::cerr << "Could not create picker for window" << std::endl;
                  continue;
               }
               picker->setObjectName(QString::fromStdString(PICKER_NAME));
               pickerFound = true;
            }
            picker->SetActivated(true);
            if (mainWindow->property("visible").toBool())
            {
               mainWindow->installEventFilter(picker);
            }
            std::cout << "ObjectPicker enabled" << std::endl;
         }
         else if (arg == "disable")
         {
            if (picker)
            {
               picker->SetActivated(false);
               mainWindow->removeEventFilter(picker);
               std::cout << "ObjectPicker disabled" << std::endl;
            }
            else
            {
               std::cerr << "ObjectPicker not found (nullptr)" << std::endl;
            }
         }
         else
         {
            throw Exception(
               "Cannot execute command " + actionName + ": "
               "Argument " + arg + " is not supported"
            );
         }
      }

      if (!pickerFound)
      {
         throw Exception(
            "Cannot execute command " + actionName + " " + arg + ": "
            "Object picker was not found"
         );
      }

      nlohmann::json result;
      result["found"] = true;
      return result;
   }
   else if (actionName == Action::LOCK_UI)
   {
      const bool lock = (arg == "enable");
      
      if (lock)
      {
         if (!externalEventFilter)
         {
            externalEventFilter = new ExternalEventsFilter(qApp);
         }
         std::cout << "Locking application" << std::endl;
         qApp->installEventFilter(externalEventFilter);
         qApp->installNativeEventFilter(&nativeEventFilter);
         // Force the application to become inactive to avoid receiving this event later
         // since it affects some other events such as mouse moves or touch drags
         QWindowSystemInterface::handleApplicationStateChanged<QWindowSystemInterface::SynchronousDelivery>(
            Qt::ApplicationInactive, true);
      }
      else if (arg == "disable")
      {
         std::cout << "Unlocking application" << std::endl;
         qApp->removeEventFilter(externalEventFilter);
         qApp->removeNativeEventFilter(&nativeEventFilter);
      }
      else
      {
         throw Exception("Unsupported arg '" + arg + "' for command '" + actionName + "'");
      }

      #if defined(__linux__)
         // Use Window flags to configure Qt's behaviour
         // Note: this only works on linux and is replaced with nativeEventFilter on Windows
         const auto mainWindows = qApp->topLevelWindows();
         for (auto* window : mainWindows)
         {
            window->setFlag(Qt::WindowDoesNotAcceptFocus, lock);
         }
      #endif

      nlohmann::json result;
      result["found"] = true;
      return result;
   }
   else
   {
      throw Exception(
         "Command " + actionName + " not supported"
      );
   }
}

} // namespace Qat