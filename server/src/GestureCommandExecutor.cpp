// (c) Copyright 2023, Qat’s Authors

#include <qat-server/GestureCommandExecutor.h>
#include <qat-server/Constants.h>
#include <qat-server/Devices.h>
#include <qat-server/Exception.h>
#include <qat-server/IWidget.h>
#include <qat-server/ObjectLocator.h>
#include <qat-server/WidgetLocator.h>
#include <qat-server/WidgetWrapper.h>

#include <QEvent>
#include <QGuiApplication>
#include <QMetaMethod>
#include <QMetaObject>
#include <QNativeGestureEvent>
#include <QObject>
#include <QVariant>
#include <QWindow>

#include <string>

namespace
{

/// \brief Send a gesture event to the given widget
/// \param type Type of event
/// \param widget Target widget
/// \param sceneCoordinates Coordinates of the event in the Scene
/// \param globalCoordinates Coordinates of the event in the Screen
/// \param value Payload of the event (depends on event type)
/// \return True if the event was accepted, False otherwise
bool sendGestureEvent(
   Qt::NativeGestureType type,
   const Qat::IWidget& widget,
   QPoint localCoordinates,
   QPoint sceneCoordinates,
   QPoint globalCoordinates,
   double value)
{
   // Event propagation does not seem to work with Qt5
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      auto* targetWidget = widget.GetQtObject();
   #else
      auto* targetWidget = widget.GetWindow();
      localCoordinates = sceneCoordinates;
   #endif
   QNativeGestureEvent gestureEvent(
      type,
      Qat::Devices::GetTouchDevice(),
      #if QT_VERSION >= QT_VERSION_CHECK(6, 2, 0)
      0,
      #endif
      localCoordinates,
      sceneCoordinates,
      globalCoordinates,
      value,
      #if QT_VERSION < QT_VERSION_CHECK(6, 2, 0)
      0, 0
      #else
      QPointF{0,0}
      #endif
   );
   if (!qApp->sendEvent(targetWidget, &gestureEvent))
   {
      throw Qat::Exception(
         std::string("Cannot send gesture event ") + std::to_string(type)
      );
   }
   return gestureEvent.isAccepted();
}

/// \brief RAII implementation ensuring Begin/End sequence of events
class EventSequence
{
public:
   EventSequence(
      const Qat::IWidget& widget,
      QPoint localCoordinates,
      QPoint sceneCoordinates,
      QPoint globalCoordinates):
      mWidget{widget},
      mLocalCoordinates{std::move(localCoordinates)},
      mSceneCoordinates{std::move(sceneCoordinates)},
      mGlobalCoordinates{std::move(globalCoordinates)}
   {
      mIsStarted = sendGestureEvent(
         Qt::BeginNativeGesture, mWidget, mLocalCoordinates, mSceneCoordinates, mGlobalCoordinates, 0.);
   }

   ~EventSequence()
   {
      sendGestureEvent(
         Qt::EndNativeGesture, mWidget, mLocalCoordinates, mSceneCoordinates, mGlobalCoordinates, 0.);
      mIsStarted = false;
   }

   bool IsStarted() const noexcept
   {
      return mIsStarted;
   }

private:
   const Qat::IWidget& mWidget;
   const QPoint mLocalCoordinates;
   const QPoint mSceneCoordinates;
   const QPoint mGlobalCoordinates;
   bool mIsStarted{false};
};
}

namespace Qat
{

using namespace Constants;

GestureCommandExecutor::GestureCommandExecutor(const nlohmann::json& request):
   BaseCommandExecutor(request)
{
   for (const auto& field : { OBJECT_DEFINITION, OBJECT_ATTRIBUTE, ARGUMENTS })
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " + field
         );
      }
   }
}

nlohmann::json GestureCommandExecutor::Run() const
{
   nlohmann::json result;
   result["status"] = true;
   auto* object = FindObject();
   result[CACHE_UID] = GetObjectCacheUid(object);
   const auto action = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   const auto args = mRequest.at(ARGUMENTS);

   if (action == Gesture::FLICK)
   {
      if (!object->inherits("QQuickFlickable"))
      {
         throw Exception(
            "Cannot flick object: "
            "Object is not a Flickable"
         );
      }
      if (!args.contains(Args::DX) || !args.contains(Args::DY))
      {
         throw Exception(
            "Cannot flick object: "
            "DX and DY arguments are mandatory"
         );
      }
      const auto dx = args.at(Args::DX).get<int>();
      const auto dy = args.at(Args::DY).get<int>();
      auto x = object->property("contentX").toInt();
      auto y = object->property("contentY").toInt();
      x += dx;
      y += dy;
      const auto startIdx = object->metaObject()->indexOfSignal("movementStarted()");
      const auto endIdx = object->metaObject()->indexOfSignal("movementEnded()");
      const auto startMethod = object->metaObject()->method(startIdx);
      const auto endMethod = object->metaObject()->method(endIdx);
      startMethod.invoke(object);
      object->setProperty("contentX", x);
      object->setProperty("contentY", y);
      endMethod.invoke(object);

      return result;
   }

   else if (action != Gesture::PINCH)
   {
      throw Exception(
         "Cannot send unknown event '" + action + "': "
         "Gesture type is not supported"
      );
   }

   auto widget = WidgetWrapper::Cast(object);
   if (!widget)
   {
      throw Exception(
         "Cannot execute gesture: "
         "Object is not a supported Widget"
      );
   }

   // Optional position arguments (X, Y)
   QPoint coordinates, globalCoordinates;
   if (args.contains(Args::X) && args.contains(Args::Y))
   {
      const auto x = args.at(Args::X).get<int>();
      const auto y = args.at(Args::Y).get<int>();
      coordinates = QPoint(x, y);
      if (!widget->Contains(coordinates))
      {
         throw Exception(
            "Cannot execute mouse operation: "
            "Given coordinates are outside widget's boundaries"
         );
      }
      globalCoordinates = widget->MapToGlobal(coordinates).toPoint();
   }
   else
   {
      std::tie(coordinates, globalCoordinates) = WidgetLocator::GetWidgetCenter(object);
   }
   auto sceneCoordinates = widget->MapToScene(coordinates).toPoint();
   
   // Make sure mandatory Begin/End events are sent
   EventSequence sequence{*widget, coordinates, sceneCoordinates, globalCoordinates};
   bool eventAccepted = sequence.IsStarted();

   if (eventAccepted && action == Gesture::PINCH)
   {
      if (args.contains(Args::ANGLE))
      {
         const auto angle = args.at(Args::ANGLE).get<double>();
         eventAccepted = sendGestureEvent(Qt::RotateNativeGesture, *widget, coordinates, sceneCoordinates, globalCoordinates, angle);
      }
      if (args.contains(Args::SCALE))
      {
         const auto scale = args.at(Args::SCALE).get<double>();
         eventAccepted = eventAccepted && 
            sendGestureEvent(Qt::ZoomNativeGesture, *widget, coordinates, sceneCoordinates, globalCoordinates, scale);
      }
   }

   if (!eventAccepted)
   {
      result["warning"] = "No widget accepted this event";
   }

   return result;
}

} // namespace Qat