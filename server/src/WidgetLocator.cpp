// (c) Copyright 2023, Qat’s Authors

#include <qat-server/WidgetLocator.h>
#include <qat-server/Exception.h>
#include <qat-server/IWidget.h>
#include <qat-server/ObjectLocator.h>
#include <qat-server/WidgetWrapper.h>

#include <QObject>
#include <QWindow>


namespace
{
/// Find the child widget at the given position relative to the given parent
/// \param[in] parent The parent widget
/// \param[in] coordinates The position of the child in parent CS
/// \return The child widget at the given position or NULL if not found
std::unique_ptr<Qat::IWidget> ChildAt(const Qat::IWidget& parent, QPoint coordinates)
{
   const auto children = Qat::ObjectLocator::CollectAllChildren(parent.GetQtObject(), "");
   double lastZ = std::numeric_limits<double>::lowest();
   auto lastSize = parent.GetSize();
   std::unique_ptr<Qat::IWidget> candidate = nullptr;
   for (auto* childObject : children)
   {
      auto child = Qat::WidgetWrapper::Cast(childObject);
      if (!child)
      {
         continue;
      }
      if (!child->IsVisible() || child->GetZ() < lastZ)
      {
         continue;
      }
      const auto childCoordinates = parent.MapToWidget(child.get(), coordinates);
      if (!child->Contains(childCoordinates))
      {
         continue;
      }

      // Ignore empty overlays
      if (childObject->inherits("QQuickOverlay") && child->GetChildWidgets().empty())
      {
         continue;
      }
      // Ignore root items
      if (childObject->inherits("QQuickRootItem"))
      {
         continue;
      }

      const auto childSize = child->GetSize();
      const auto isSmaller = 
         childSize.width() < lastSize.width() && childSize.height() < lastSize.height();
      if (!candidate || isSmaller)
      {
         lastZ = child->GetZ();
         lastSize = childSize;
         candidate = std::move(child);
      }
   }
   return candidate;
}
} // Anonymous namespace

namespace Qat::WidgetLocator
{

std::tuple<QPoint, QPoint> GetWidgetCenter(QObject* object)
{
   const auto widget = WidgetWrapper::Cast(object);
   if (!widget)
   {
      throw Exception(
         "Cannot find widget center: "
         "Given object is not a supported widget"
      );
   }
   const auto x = static_cast<int>(widget->GetWidth() / 2);
   const auto y = static_cast<int>(widget->GetHeight() / 2);
   const QPoint coordinates(x, y);
   return { coordinates, widget->MapToGlobal(coordinates).toPoint() };
}

QObject* FindWidget(QObject* parent, QPoint coordinates)
{
   return std::get<QObject*>(FindWidgetAt(parent, coordinates));
}

std::tuple<QObject*, QPoint, QPoint> FindWidgetAt(QObject* parent, QPoint coordinates)
{
   QPoint windowCoordinates;
   // Handle special case when the object is a Window
   if (parent->isWindowType())
   {
      const auto* window = qobject_cast<QWindow*>(parent);
      if (!window)
      {
         throw Exception(
            "Cannot find widget: "
            "Main window is not an instance of QWindow"
         );
      }
      const auto globalCoordinates = window->mapToGlobal(coordinates);
      const auto rootChildren = parent->children();

      for (auto* rootChild : rootChildren)
      {
         const auto widget = WidgetWrapper::Cast(rootChild);
         if (widget)
         {
            const auto mappedCoordinates = widget->MapFromGlobal(globalCoordinates);
            if (widget->Contains(mappedCoordinates))
            {
               // With Qt implementation of windows for QML,
               // the QWindow parent is not the actual visual parent of its "child" widgets
               // (it usually delegate this to its contentItem / root item)
               parent = rootChild;
               break;
            }
         }
      }
   }

   const auto parentWidget = WidgetWrapper::Cast(parent);
   if (!parentWidget)
   {
      throw Exception(
         "Cannot find parent widget: "
         "Object is not a supported widget"
      );
   }

   if (!parentWidget->Contains(coordinates))
   {
      throw Exception(
         "Cannot execute operation: "
         "Given coordinates are outside widget's boundaries"
      );
   }
   windowCoordinates = parentWidget->MapToGlobal(coordinates).toPoint();
   std::unique_ptr<Qat::IWidget> widget;
   auto child = ChildAt(*parentWidget, coordinates);
   while (child)
   {
      widget = std::move(child);
      coordinates = widget->MapFromGlobal(windowCoordinates).toPoint();
      child = ChildAt(*widget, coordinates);
      if (child && child->GetQtObject() == widget->GetQtObject())
      {
         break;
      }
   }

   if (!widget)
   {
      throw Exception(
         "Cannot find widget: "
         "There is no child widget at the given coordinates"
      );
   }

   return std::make_tuple(widget->GetQtObject(), coordinates, windowCoordinates);
}

} // namespace Qat