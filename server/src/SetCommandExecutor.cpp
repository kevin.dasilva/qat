// (c) Copyright 2023, Qat’s Authors

#include <qat-server/SetCommandExecutor.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/QVariantToJson.h>

#include <QBuffer>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMetaProperty>
#include <QObject>

#include <string>

namespace Qat
{

using namespace Constants;

SetCommandExecutor::SetCommandExecutor(const nlohmann::json& request):
   BaseCommandExecutor(request)
{
   for (const auto& field : { OBJECT_DEFINITION, OBJECT_ATTRIBUTE, ARGUMENTS })
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " + field
         );
      }
   }
}

nlohmann::json SetCommandExecutor::Run() const
{
   auto* object = FindObject();

   const auto name = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   const auto args = mRequest.dump();
   const auto jsonDoc = QJsonDocument::fromJson(QByteArray::fromStdString(args));
   const auto jsonValue = jsonDoc.object()[ARGUMENTS.c_str()];
   const auto value = FromJson(jsonValue);   

   auto index = object->metaObject()->indexOfProperty(name.c_str());
   if (index < 0)
   {
      throw Exception(
         "Invalid property: "
         "Property '" + name + "' does not exist"
      );
   }
   if (!object->metaObject()->property(index).isWritable())
   {
      throw Exception(
         "Invalid property: "
         "Property '" + name + "' is read-only"
      );
   }
   if (!object->metaObject()->property(index).write(object, value))
   {
      throw Exception(
         "Invalid property value: "
         "Property '" + name + "' could not be changed to '" + mRequest.at(ARGUMENTS).get<std::string>() + "'"
      );
   }
   const auto newValue = object->property(name.c_str());
   const auto expectedValue = ToJson(value);
   const auto actualValue = ToJson(newValue);
   if (GetVariantType(value) == GetVariantType(newValue) && expectedValue != actualValue)
   {
      throw Exception(
         "Unable to change property value: "
         "Property '" + name + "' could not be changed to " + expectedValue.dump() +
         ". Current value is " + actualValue.dump()
      );
   }
   nlohmann::json result;
   result["found"] = true;
   result[CACHE_UID] = GetObjectCacheUid(object);
   return result;
}

} // namespace Qat