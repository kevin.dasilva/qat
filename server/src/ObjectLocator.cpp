// (c) Copyright 2023, Qat’s Authors

#include <qat-server/ObjectLocator.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/IWidget.h>
#include <qat-server/MethodCaller.h>
#include <qat-server/WidgetWrapper.h>

#include <QJsonArray>
#include <QObject>
#include <QVariant>

#include <nlohmann/json.hpp>

#include <algorithm>
#include <string>

namespace
{
/// Compare a JSON value to an object property
/// \param[in] name The name of the property
/// \param[in] expected The expected value in JSON format
/// \param[in] object The object to compare
/// \return True if both properties are equal, False otherwise
bool CompareProperty(
   const std::string& name,
   const nlohmann::json& expected,
   QObject* object)
{
   // QML id is not a real property
   if (name == Qat::Constants::OBJECT_ID)
   {
      const auto widget = Qat::WidgetWrapper::Cast(object);
      if (widget)
      {
         return widget->GetId() == expected.get<std::string>();
      }
      return false;
   }
   // Get current value
   const auto value = object->property(name.c_str());
   if (!value.isValid())
   {
      // The 'visible' and 'enabled' properties don't always exist (e.g. Qt3D entities, Dialog)
      return name == "visible" || name == "enabled";
   }

   // Cast and compare values
   if (expected.is_string())
   {
      const auto typedValue = expected.get<std::string>();
      return (typedValue == value.toString().toStdString());
   }
   else if (expected.is_boolean())
   {
      const auto typedValue = expected.get<bool>();
      return (typedValue == value.toBool());
   }
   else if (expected.is_number_integer())
   {
      const auto typedValue = expected.get<int>();
      return (typedValue == value.toInt());
   }
   else if (expected.is_number_unsigned())
   {
      const auto typedValue = expected.get<unsigned long>();
      return (typedValue == value.toULongLong());
   }
   else if (expected.is_number_float())
   {
      const auto typedValue = expected.get<double>();
      return (typedValue == value.toDouble());
   }
   else
   {
      throw Qat::Exception(
         "Unable to find property: " + name + "("
         "Unsupported value: '" + expected.get<std::string>() + "')"
      );
   }
}

/// Compare an object type to an expected value
/// \param[in] type The expected type of the object
/// \param[in] object A Qt object
/// \return True if object is of the given type, False otherwise
bool CompareTypes(const std::string& type, const QObject* object)
{
   if (type == Qat::Constants::QOBJECT_TYPE)
   {
      // All Qt objects inherit from QObject
      return true;
   }

   const auto formattedType = Qat::ObjectLocator::FormatType(type);
   const auto* metaObject = object->metaObject();
   std::string className = Qat::ObjectLocator::GetObjectType(object);
   while (className != Qat::Constants::QOBJECT_TYPE)
   {
      if (formattedType == className)
      {
         return true;
      }
      metaObject = metaObject->superClass();
      className = metaObject->className();
      className = Qat::ObjectLocator::FormatType(className);
   }
   return false;
}

/// Find child items of the given container matching the given name.
/// \note This is based on the GUI parenting relationship between Widgets/Items
/// instead of the "normal" QObjects' relationship.
/// \param[in] container The container of the search object
/// \param[in] objectName The name of the search object. If empty, all child items will be returned.
/// \param[in] recursive If True, collect all children recursively. 
///            Otherwise collect direct children only.
/// \return The list of child items matching the given name if provided
QObjectList FindChildItems(
   const QObject* container,
   const std::string& objectName,
   bool recursive)
{
   QObjectList result; 
   const auto item = Qat::WidgetWrapper::Cast(container);
   if (item)
   {
      const auto childItems = item->GetChildWidgets();
      for (auto* childItem : childItems)
      {
         if (objectName.empty() || objectName == childItem->objectName().toStdString())
         {
            result.push_back(childItem);
         }
         if (recursive)
         {
            const auto grandChildItems = FindChildItems(childItem, objectName, true);
            result.append(grandChildItems);
         }
      }
   }
   return result;
}

/// Find child nodes of the given container matching the given name.
/// \note This is based on the Qt3D parenting relationship between Entities
/// instead of the "normal" QObjects' relationship.
/// \param[in] container The container of the search object, must be a Scene3D instance
/// \param[in] objectName The name of the search object. If empty, all child nodes will be returned.
/// \param[in] recursive If True, collect all children recursively. 
///            Otherwise collect direct children only.
/// \return The list of child items matching the given name if provided
QObjectList FindChildNodes(
   const QObject* container,
   const std::string& objectName,
   bool recursive)
{
   QObjectList result;
   const auto isScene3dObject = container->inherits("Qt3DRender::Scene3DItem");
   if (isScene3dObject)
   {
      // Find root entity which is the parent of all other nodes
      const auto entityProperty = container->property("entity");
      if (entityProperty.canConvert<QObject*>())
      {
         const auto findOption = recursive ?
            Qt::FindChildOption::FindChildrenRecursively :
            Qt::FindChildOption::FindDirectChildrenOnly;
         auto* rootEntity = entityProperty.value<QObject*>();
         if (rootEntity->inherits("Qt3DCore::QEntity"))
         {
            if (objectName.empty())
            {
               result = rootEntity->findChildren<QObject*>(QString(), findOption);
            }
            else
            {
               result = rootEntity->findChildren<QObject*>(
                  QString::fromStdString(objectName), findOption);
            }
            // Root entity is also a child of the scene
            result.push_front(rootEntity);
         }
      }
   }
   return result;
}

} // anonymous namespace

namespace Qat::ObjectLocator
{
std::vector<QObject*> FindObjects(
   const QObject* container,
   const nlohmann::json& definition,
   const QObject* parentObject,
   bool allMatches)
{
   using namespace Constants;
   std::vector<QObject*> matches;
   // If objectName is empty, all children will be collected
   std::string objectName = "";
   if (definition.contains(OBJECT_NAME))
   {
      objectName = definition.at(OBJECT_NAME).get<std::string>();
   }

   QList<QObject*> children = CollectAllChildren(container, objectName);

   for (auto* child : children)
   {
      if(ObjectMatches(child, definition, parentObject))
      {
         matches.push_back(child);
         if (!allMatches && matches.size() > 1)
         {
            return matches;
         }
      }
   }

   return matches;
}

bool ObjectMatches(
   QObject* object,
   const nlohmann::json& definition,
   const QObject* parentObject)
{
   using namespace Constants;
   if (!object)
   {
      return false;
   }

   // Filter by object type
   if (definition.contains(OBJECT_TYPE))
   {
      const auto type = definition.at(OBJECT_TYPE).get<std::string>();
      if (!CompareTypes(type, object))
      {
         return false;
      }
   }

   if (parentObject && parentObject != object->parent())
   {
      const auto widget = WidgetWrapper::Cast(object);
      if (!widget || parentObject != widget->GetParent())
      {
         if (!object->inherits("Qt3DCore::QNode"))
         {
            return false;
         }

         MethodCaller caller;
         if (!caller.Call(object, "parentNode", {}))
         {
            return false;
         }
         const auto& returnValue = caller.GetResult();
         if (!returnValue.canConvert<QObject*>())
         {
            return false;
         }
      }
   }

   // Verify other given properties to match
   bool doMatch = true;
   for (const auto& [key, value] : definition.items())
   {
      // Ignore properties that have already been verified
      if (key == OBJECT_TYPE || key == CONTAINER || key == OBJECT_PARENT)
      {
         continue;
      }

      doMatch = doMatch && CompareProperty(key, value, object);
      if (!doMatch)
      {
         return false;
      }
   }

   return doMatch;
}


QObjectList CollectAllChildren(
   const QObject* container,
   const std::string& objectName,
   bool recursive)
{
   QList<QObject*> children, objectChildren;
   const auto findOption = recursive ?
      Qt::FindChildOption::FindChildrenRecursively :
      Qt::FindChildOption::FindDirectChildrenOnly;
   if (objectName.empty())
   {
      objectChildren = container->findChildren<QObject*>(QString(), findOption);
   }
   else
   {
      objectChildren = container->findChildren<QObject*>(objectName.c_str(), findOption);
   }
   // Filter out ScreenInfo attached property
   children.reserve(objectChildren.size());
   std::copy_if(
      objectChildren.cbegin(),
      objectChildren.cend(),
      std::back_inserter(children),
      [](const auto& child)
      {
         const std::string type {child->metaObject()->className()};
         return type != "QQuickScreenInfo";
      }
   );

   const auto childItems = FindChildItems(container, objectName, recursive);
   const auto childNodes = FindChildNodes(container, objectName, recursive);

   // Merge results and remove duplicates
   for (auto* child : childItems)
   {
      if (!children.contains(child))
      {
         children.push_back(child);
      }
   }
   for (auto* child : childNodes)
   {
      if (!children.contains(child))
      {
         children.push_back(child);
      }
   }

   return children;
}

bool HasChildren(const QObject* object)
{
   if (!object) return false;
   if (!object->children().isEmpty()) return true;
   if (!FindChildItems(object, "", false).isEmpty()) return true;
   if (!FindChildNodes(object, "", false).isEmpty()) return true;
   return false;
}

std::string FormatType(const std::string& className)
{
   std::string type = className;
   // Prefix
   if (type.starts_with("QQuick"))
   {
      type = type.substr(strlen("QQuick"));
   }
   // Suffix
   const auto qmlTypeIndex = type.find("_QMLTYPE_");
   if (qmlTypeIndex != std::string::npos)
   {
      type = type.substr(0, qmlTypeIndex);
   }
   const auto qmlIndex = type.find("_QML_");
   if (qmlIndex != std::string::npos)
   {
      type = type.substr(0, qmlIndex);
   }
   return type;
}

std::string GetObjectType(const QObject* object)
{
   const auto typeProperty = object->property(Qat::Constants::OBJECT_TYPE.c_str());
   if (typeProperty.isValid())
   {
      return typeProperty.toString().toStdString();
   }
   else
   {
      return FormatType(object->metaObject()->className());
   }
}

} // namespace Qat::ObjectLocator