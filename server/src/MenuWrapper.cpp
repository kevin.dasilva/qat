// (c) Copyright 2024, Qat’s Authors

#include <qat-server/MenuWrapper.h>
#include <qat-server/Exception.h>
#include <qat-server/IWidget.h>
#include <qat-server/WidgetWrapper.h>

#include <QVariant>

namespace Qat
{

MenuWrapper::MenuWrapper(
   QObject* menu,
   std::string textOrName) :
      QObject(),
      mMenu{menu},
      mTextOrName{std::move(textOrName)}
{
}

QObject* MenuWrapper::GetMenu() const
{
   return mMenu;
}

const std::string& MenuWrapper::GetString() const
{
   return mTextOrName;
}

QObject* MenuWrapper::GetAction() const
{
   const auto widget = WidgetWrapper::Cast(this);
   if (!widget) {
      throw Exception("Could not find widget associated with this menu");
   }
   auto* action =  widget->GetQtObject();
   if (!action) {
      throw Exception("Could not find action associated with this menu");
   }
   return action;
}

QString MenuWrapper::GetText() const
{
   return GetAction()->property("text").toString();
}

bool MenuWrapper::IsVisible() const
{
   return GetMenu()->property("visible").toBool();
}

bool MenuWrapper::IsEnabled() const
{
   return GetAction()->property("enabled").toBool();
}

bool MenuWrapper::IsChecked() const
{
   return GetAction()->property("checked").toBool();
}

} // namespace Qat