// (c) Copyright 2023, Qat’s Authors

#include <qat-server/RequestExecutor.h>

#include <qat-server/RequestHandler.h>
#include <qat-server/ActionCommandExecutor.h>
#include <qat-server/CallCommandExecutor.h>
#include <qat-server/CommCommandExecutor.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/FindCommandExecutor.h>
#include <qat-server/GestureCommandExecutor.h>
#include <qat-server/GetCommandExecutor.h>
#include <qat-server/KeyboardCommandExecutor.h>
#include <qat-server/ListCommandExecutor.h>
#include <qat-server/MouseCommandExecutor.h>
#include <qat-server/SetCommandExecutor.h>
#include <qat-server/TouchCommandExecutor.h>

#include <memory>
#include <string>

namespace Qat
{

using namespace Constants;

RequestExecutor::RequestExecutor(
   const nlohmann::json& request,
   RequestHandler* requestHandler):
   mRequest(request),
   mRequestHandler(requestHandler)
{
   if (!request.contains(COMMAND_TYPE))
   {
      throw Exception(
         "Invalid command: "
         "Missing required field: " + COMMAND_TYPE
      );
   }
}

nlohmann::json RequestExecutor::Run() const
{
   const auto command = mRequest.at(COMMAND_TYPE).get<std::string>();
   std::unique_ptr<ICommandExecutor> commandExecutor;

   if (command == Command::FIND)
   {
      commandExecutor = std::make_unique<FindCommandExecutor>(mRequest);
   }
   else if (command == Command::LIST)
   {
      commandExecutor = std::make_unique<ListCommandExecutor>(mRequest);
   }
   else if (command == Command::GET)
   {
      commandExecutor = std::make_unique<GetCommandExecutor>(mRequest);
   }
   else if (command == Command::SET)
   {
      commandExecutor = std::make_unique<SetCommandExecutor>(mRequest);
   }
   else if (command == Command::CALL)
   {
      commandExecutor = std::make_unique<CallCommandExecutor>(mRequest);
   }
   else if (command == Command::MOUSE)
   {
      commandExecutor = std::make_unique<MouseCommandExecutor>(mRequest);
   }
   else if (command == Command::KEYBOARD)
   {
      commandExecutor = std::make_unique<KeyboardCommandExecutor>(mRequest);
   }
   else if (command == Command::ACTION)
   {
      commandExecutor = std::make_unique<ActionCommandExecutor>(mRequest);
   }
   else if (command == Command::COMMUNICATION)
   {
      commandExecutor = std::make_unique<CommCommandExecutor>(mRequest, mRequestHandler);
   }
   else if (command == Command::GESTURE)
   {
      commandExecutor = std::make_unique<GestureCommandExecutor>(mRequest);
   }
   else if (command == Command::TOUCH)
   {
      commandExecutor = std::make_unique<TouchCommandExecutor>(mRequest);
   }
   else
   {
      throw Exception(
         "Invalid command: "
         "Unsupported command type: " + command
      );
   }
   return commandExecutor->Run();
}

} // namespace Qat