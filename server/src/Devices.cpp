// (c) Copyright 2024, Qat’s Authors

#include <qat-server/Devices.h>

#include <qat-server/Constants.h>

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
#include <QInputDevice>
#endif

// Qt private headers are required to manage simulated mouse device
#include <QtGui/qpa/qwindowsysteminterface.h>
#include <QtGui/qpa/qwindowsysteminterface_p.h>

namespace Qat::Devices
{

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   Qt::MouseEventSource GetMouseDevice()
   {
      return Qt::MouseEventSynthesizedByApplication;
   }
#else
   const PointingDeviceType* GetMouseDevice()
   {
      using namespace Qat::Constants;
      static QPointingDevice* device = nullptr;

      if (!device)
      {
         QInputDevice::Capabilities caps;
         caps.setFlag(QInputDevice::Capability::Position);
         caps.setFlag(QInputDevice::Capability::NormalizedPosition);
         caps.setFlag(QInputDevice::Capability::Scroll);
         caps.setFlag(QInputDevice::Capability::Hover);

         device = new QPointingDevice(
            QString::fromStdString(Mouse::DEVICE_NAME),
            0x100000001,
            QInputDevice::DeviceType::Mouse,
            QPointingDevice::PointerType::Cursor,
            caps,
            3,
            0
         );
         QWindowSystemInterface::registerInputDevice(device);
      }

      return device;
   }
#endif

   PointingDeviceType* GetTouchDevice()
   {
      using namespace Qat::Constants;
      static PointingDeviceType* device = nullptr;

      if (!device)
      {
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         device = new QTouchDevice();
         device->setName(QString::fromStdString(Touch::DEVICE_NAME));
         device->setType(QTouchDevice::TouchScreen);
         QWindowSystemInterface::registerTouchDevice(device);
      #else
         device = new QPointingDevice(
            QString::fromStdString(Touch::DEVICE_NAME),
            0x100000000,
            QInputDevice::DeviceType::TouchScreen,
            QPointingDevice::PointerType::Finger,
            QInputDevice::Capability::Position,
            3,
            0
         );
         QWindowSystemInterface::registerInputDevice(device);
      #endif
      }

      return device;
   }


#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
   const QInputDevice* GetKeyboardDevice()
   {
      using namespace Qat::Constants;
      static QInputDevice* device = nullptr;

      if (!device)
      {
         device = new QInputDevice(
            QString::fromStdString(KeyBoard::DEVICE_NAME),
            0x100000002,
            QInputDevice::DeviceType::Keyboard
         );
         QWindowSystemInterface::registerInputDevice(device);
      }

      return device;
   }
#endif

} // namespace Qat::Devices