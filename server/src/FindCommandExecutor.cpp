// (c) Copyright 2023, Qat’s Authors

#include <qat-server/FindCommandExecutor.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>

#include <QObject>

#include <string>

namespace Qat
{

using namespace Constants;

FindCommandExecutor::FindCommandExecutor(const nlohmann::json& request):
   BaseCommandExecutor(request)
{
   if (!request.contains(OBJECT_DEFINITION))
   {
      throw Exception(
         "Invalid command: "
         "Missing required field: " + OBJECT_DEFINITION
      );
   }
}

nlohmann::json FindCommandExecutor::Run() const
{
   const auto* object = FindObject();

   nlohmann::json result;
   result["found"] = object != nullptr;
   return result;
}

} // namespace Qat