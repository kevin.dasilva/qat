// (c) Copyright 2023, Qat’s Authors

#include <qat-server/Plugin.h>
#include <qat-server/Exception.h>
#include <qat-server/IWidget.h>

#include <iostream>
#include <vector>
#include <string>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #define FOR_WINDOWS
   #include <windows.h>
#elif defined(__linux__)
   #define FOR_LINUX
   #include <dlfcn.h>
   #include <link.h>
#elif defined (__APPLE__)
   #define FOR_MACOS
   #include <dlfcn.h>
#else
#   error "Unsupported platform"
#endif

namespace Qat
{
Plugin::Plugin(LibHandle handle) :
   mHandle(handle)
{
#if defined(FOR_LINUX) || defined(FOR_MACOS)
   *(void**)(&mCastFunction) = dlsym(mHandle, "CastObject");
   *(void**)(&mGetTopWindowsFunction) = dlsym(mHandle, "GetTopWindows");
   *(void**)(&mGrabFunction) = dlsym(mHandle, "GrabImage");
   *(void**)(&mCreatePickerFunction) = dlsym(mHandle, "CreatePicker");
#elif defined (FOR_WINDOWS)
   mCastFunction = (CastFunctionType)GetProcAddress(mHandle, "CastObject");
   mGetTopWindowsFunction = (GetTopWindowsFunctionType)GetProcAddress(mHandle, "GetTopWindows");
   mGrabFunction = (GrabFunctionType)GetProcAddress(mHandle, "GrabImage");
   mCreatePickerFunction = (CreatePickerFunctionType)GetProcAddress(mHandle, "CreatePicker");
#endif
   if(!mCastFunction)
   {
      std::cerr << "Could not find Cast function" << std::endl;
   }
   if(!mGetTopWindowsFunction)
   {
      std::cerr << "Could not find GetTopWindows function" << std::endl;
   }
   if(!mGrabFunction)
   {
      std::cerr << "Could not find GrabImage function" << std::endl;
   }
   if(!mCreatePickerFunction)
   {
      std::cerr << "Could not find CreatePicker function" << std::endl;
   }
}

std::unique_ptr<IWidget> Plugin::CastObject(const QObject* qobject) const
{
   if(!mCastFunction)
   {
      return nullptr;
   }
   std::unique_ptr<IWidget> result;
   result.reset(mCastFunction(qobject));
   return result;
}

bool Plugin::GetTopWindows(QObject** windowList, unsigned int* count) const
{
   if(!mGetTopWindowsFunction)
   {
      return false;
   }
   return mGetTopWindowsFunction(windowList, count);
}

std::unique_ptr<QImage> Plugin::GrabImage(QObject* window) const
{
   if (!mGrabFunction)
   {
      return nullptr;
   }

   std::unique_ptr<QImage> result;
   result.reset(mGrabFunction(window));
   return result;
}

IObjectPicker* Plugin::CreatePicker(QObject* window) const
{
   if (!mCreatePickerFunction)
   {
      return nullptr;
   }
   return mCreatePickerFunction(window);
}

}