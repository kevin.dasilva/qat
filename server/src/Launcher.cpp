// (c) Copyright 2023, Qat’s Authors

#include <qat-server/Launcher.h>
#include <qat-server/Server.h>

#include <QGuiApplication>
#include <QTimer>

#if __APPLE__
#include <TargetConditionals.h>
#endif

#include <atomic>
#include <chrono>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <thread>

#ifdef IS_WINDOWS
std::string GetTempPath()
{
   char* pValue;
   size_t len;
   const auto rc = _dupenv_s(&pValue, &len, "TEMP");
   if (rc || !pValue || !len)
   {
      return "";
   }

   std::string value = pValue;
   free(pValue);
   return value;
}

int GetPid()
{
   return GetCurrentProcessId();
}
#else
std::string GetTempPath()
{
   const auto value = std::getenv("TEMP");

   if (value)
   {
      std::filesystem::path tempPath{value};
      if(!std::filesystem::exists(tempPath))
      {
         return "";
      }
      return value;
   }

   return "";
}

int GetPid()
{
   return getpid();
}
#endif

namespace
{
   std::atomic<bool> gStopServer = {false};
}

/// Instantiate the Qat Server in the target application
void Start() 
{
   // Useful on MacOS since Universal libraries support both architectures
   #if TARGET_CPU_ARM64
      std::cout << "Current platform is ARM64" << std::endl;
   #elif TARGET_CPU_X86_64
      std::cout << "Current platform is X86_64" << std::endl;
   #endif

   std::cout << "Injecting Qat server" << std::endl;

   // Wait until QApplication instance exists
   /// \todo derouauq 28-DEC-2022 Add a timeout
   std::cout << "Waiting for QApplication to start" << std::endl;
   while (QGuiApplication::startingUp()) {
      if (gStopServer.load())
      {
         std::cout << "Aborting server launch" << std::endl;
         return;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(50) );
   }

   std::cout << "Creating server" << std::endl;
   Qat::Server::Create([](const Qat::Server* server)
      {
         QObject::connect(
            QGuiApplication::instance(),
            SIGNAL(aboutToQuit()),
            server,
            SLOT(deleteLater()));   

         const auto pid = GetPid();
         const auto fileName = "qat-" + std::to_string(pid) + ".txt";
         std::filesystem::path appPath = GetTempPath();
         const auto outputPath = appPath.append(fileName);
         std::ofstream outputFile(outputPath);
         if (outputFile.fail())
         {
            std::cerr << "Failed to create file: " << outputPath << std::endl;
         }
         outputFile << server->GetPort() << std::endl;
      }
   );
}

/// Stop or abort the Qat Server in the target application
void Stop() 
{
   std::cout << "Stopping Qat server" << std::endl;
   gStopServer = true;
}

#ifdef IS_WINDOWS
extern "C" int __stdcall DllMain(HINSTANCE, DWORD signal, LPVOID) 
{
   switch (signal) 
   {
   case DLL_PROCESS_ATTACH:
      Start();
      break;
   case DLL_PROCESS_DETACH:
      Stop();
      break;
   default:
      break;
   }

   return TRUE;
}

#endif
