// (c) Copyright 2023, Qat’s Authors

#include <qat-server/CommandHelper.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>

namespace Qat::CommandHelper
{

Qt::MouseButton GetButton(const nlohmann::json& args)
{
   using namespace Qat::Constants;
   Qt::MouseButton button = Qt::MouseButton::LeftButton;
   if (args.contains(Args::BUTTON))
   {
      const auto value = args.at(Args::BUTTON).get<std::string>();
      if (value == Button::LEFT)
      {
         button = Qt::MouseButton::LeftButton;
      }
      else if (value == Button::RIGHT)
      {
         button = Qt::MouseButton::RightButton;
      }
      else if (value == Button::MIDDLE)
      {
         button = Qt::MouseButton::MiddleButton;
      }
      else if (value == Button::NONE)
      {
         button = Qt::MouseButton::NoButton;
      }
      else
      {
         throw Qat::Exception(
            "Cannot run mouse command: "
            "Unknown button: " + value
         );
      }
   }

   return button;
}

Qt::KeyboardModifiers GetModifier(const nlohmann::json& args)
{
   using namespace Qat::Constants;
   Qt::KeyboardModifiers modifier = Qt::KeyboardModifiers::enum_type::NoModifier;
   if (args.contains(Args::MODIFIER))
   {
      const auto modifierArg = args.at(Args::MODIFIER);
      nlohmann::json modifierList;
      if (modifierArg.is_array())
      {
         modifierList = modifierArg;
      }
      else
      {
         modifierList.push_back(modifierArg.get<std::string>());
      }

      for (const auto& value : modifierList)
      {
         if (value == Modifier::ALT)
         {
            modifier.setFlag(Qt::KeyboardModifiers::enum_type::AltModifier);
         }
         else if (value == Modifier::CTL)
         {
            modifier.setFlag(Qt::KeyboardModifiers::enum_type::ControlModifier);
         }
         else if (value == Modifier::SHIFT)
         {
            modifier.setFlag(Qt::KeyboardModifiers::enum_type::ShiftModifier);
         }
         else if (value == Modifier::NONE)
         {
            // Ignore
         }
         else
         {
            throw Qat::Exception(
               "Cannot run mouse command: "
               "Unknown modifier: " + value.get<std::string>()
            );
         }
      }
   }
   return modifier;
}

} // namespace Qat::CommandHelper