// (c) Copyright 2023, Qat’s Authors

#include <qat-server/CallCommandExecutor.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/MethodCaller.h>
#include <qat-server/QVariantToJson.h>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMetaMethod>
#include <QObject>
#include <QVariant>
#include <QVariantList>

#include <string>

namespace Qat
{

using namespace Constants;

CallCommandExecutor::CallCommandExecutor(const nlohmann::json& request):
   BaseCommandExecutor(request)
{
   for (const auto& field : { OBJECT_DEFINITION, OBJECT_ATTRIBUTE, ARGUMENTS })
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " + field
         );
      }
   }
}

nlohmann::json CallCommandExecutor::Run() const
{
   auto* object = FindObject();

   const auto functionName = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   const auto args = mRequest.dump();
   const auto jsonDoc = QJsonDocument::fromJson(QByteArray::fromStdString(args));
   const auto jsonArray = jsonDoc.object()[ARGUMENTS.c_str()].toArray();

   MethodCaller caller;
   const auto success = caller.Call(object, functionName, jsonArray);
   if (success)
   {
      nlohmann::json result;
      result["found"] = true;
      result[CACHE_UID] = GetObjectCacheUid(object);

      const auto& returnValue = caller.GetResult();
      if (returnValue.canConvert<QObject*>())
      {
         nlohmann::json jsonObject;
         auto* returnObject = returnValue.value<QObject*>();
         if (!returnObject)
         {
            jsonObject = nullptr;
         }
         else
         {
            jsonObject[CACHE_UID] = RegisterObject(returnObject);
         }
         nlohmann::json jsonValue;
         jsonValue["returnObject"] = jsonObject;
         result["value"] = jsonValue.dump();
      }
      else
      {
         nlohmann::json jsonValue;
         jsonValue["returnValue"] = ToJson(returnValue);
         result["value"] = jsonValue;
      }

      return result;
   }

   throw Exception(
      "Cannot call function '" + functionName + "': "
      "No overload found for the given arguments"
   );
}

} // namespace Qat