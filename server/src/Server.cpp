// (c) Copyright 2023, Qat’s Authors

#include <qat-server/Server.h>
#include <qat-server/DoubleTapEventsFilter.h>
#include <qat-server/RequestHandler.h>

#include <QCoreApplication>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTimer>

#include <iostream>

namespace
{
   Qat::DoubleTapEventsFilter* doubleTapFilter = nullptr;
}

namespace Qat
{

void Server::Create(std::function<void(const Server*)> onCreated)
{
   auto* server = new Server();
   
   QObject::connect(
      server,
      &Server::isRunning,
      [=]()
      {
         onCreated(server);
      });
   QTimer::singleShot(0, server, SLOT(Start()));
}

Server::Server() : QObject()
{
   // QTimer must run in a Qt thread
   moveToThread(qApp->thread());
}

Server::~Server()
{
   Stop();
}

void Server::Start()
{
   setParent(qApp);
   mServer = new QTcpServer(this);
   connect(
      mServer,
      SIGNAL(newConnection()),
      this,
      SLOT(ServeClient()));

   if (!mServer->listen()) 
   {
      std::cerr << "Unable to initialize server. " << mServer->errorString().toStdString() << std::endl;
      return;
   }

   mPort = mServer->serverPort();
   std::cout << "Server initialized on port " << mPort << std::endl;

   connect(
      mServer,
      &QTcpServer::acceptError,
      [this] {
         std::cerr << "Accept Error from TCP server:" << mServer->errorString().toStdString() << std::endl;
      }
   );
   doubleTapFilter = new DoubleTapEventsFilter(qApp);
   qApp->installEventFilter(doubleTapFilter);

   emit isRunning();
}

void Server::ServeClient()
{
   std::cout << "New client" << std::endl;
   QTcpSocket* socket = mServer->nextPendingConnection();
   const auto* handler = new RequestHandler(this, socket);

   connect(socket, SIGNAL(disconnected()), socket, SLOT(deleteLater()));
   connect(socket, SIGNAL(destroyed()), handler, SLOT(deleteLater()));
}

void Server::Stop()
{
   std::cout << "Closing server..." << std::endl;
   mServer->close();
   mServer->deleteLater();
   if (doubleTapFilter)
   {
      qApp->removeEventFilter(doubleTapFilter);
      doubleTapFilter->deleteLater();
   }
}

int Server::GetPort() const noexcept
{
   return mPort;
}

} // namespace Qat