// (c) Copyright 2023, Qat’s Authors

#include <qat-server/RequestHandler.h>
#include <qat-server/RequestExecutor.h>
#include <qat-server/Exception.h>

#include <QHostAddress>
#include <QIODevice>
#include <QTcpSocket>

#include <nlohmann/json.hpp>

#include <iostream>
#include <map>
#include <sstream>
#include <string>

namespace Qat
{

RequestHandler::RequestHandler(QObject* parent, QTcpSocket* socket) :
   QObject(parent),
   mSocket(socket)
{
   if (mSocket)
   {
      connect(mSocket, SIGNAL(readyRead()), this, SLOT(OnReadyRead()));
   }
   else
   {
      throw Exception(
         "Unable to create request handler: "
         "Received socket is NULL"
      );
   }
}

RequestHandler::~RequestHandler()
{
   if (mClientSocket)
   {
      mClientSocket->close();
      mClientSocket = nullptr;
   }
   std::cout << "Client disconnected" << std::endl;
}

void RequestHandler::ConnectToHost(
   const std::string& hostName,
   int port)
{
   if (mClientSocket)
   {
      mClientSocket->close();
      mClientSocket = nullptr;
   }
   mClientSocket = new QTcpSocket(this);
   QHostAddress address(QString::fromStdString(hostName));
   connect(mClientSocket, &QTcpSocket::connected, this, &RequestHandler::onConnectedToClient);
   mClientSocket->connectToHost(address, port, QIODevice::WriteOnly);
}

void RequestHandler::onConnectedToClient()
{
   // Reserved empty message to synchronize the remote server
   const auto port = mClientSocket->peerPort();
   std::cout << "Sending sync message to port " << port << std::endl;
   mClientSocket->write("0\n");
   mClientSocket->flush();
}

void RequestHandler::DisconnectFromHost()
{
   std::cout << "Disconnecting client" << std::endl;
   if (mClientSocket)
   {
      mClientSocket->close();
      mClientSocket = nullptr;
   }
}

void RequestHandler::SendMessage(const std::string& message) const
{
   if (!mClientSocket)
   {
      throw Exception(
         "Cannot send message to host: no socket available"
      );
   }
   std::stringstream header;
   header << std::to_string(message.length()) << std::endl;
   mClientSocket->write(header.str().c_str());
   mClientSocket->write(message.c_str());
   mClientSocket->flush();
}

void RequestHandler::OnReadyRead()
{
   while (true)
   {
      if (!mSocket)
      {
         std::cerr << "Unable to read message header: socket is null" << std::endl;
         return;
      }
      if (mCurrentMessageLength == 0) {
         // Read header containing message length
         if (!mSocket->canReadLine()) 
         {
            std::cerr << "Unable to read message header: ignoring request" << std::endl;
            return;
         }
         QString header = QString(mSocket->readLine());
         bool ok = false;
         mCurrentMessageLength = header.toLongLong(&ok);
         if (!ok || mCurrentMessageLength == 0) {
            std::cerr << "Error while reading message header: " << header.toStdString() << std::endl;
            return;
         }
      }
      const auto availBytes = mSocket->bytesAvailable();
      if (availBytes < mCurrentMessageLength) {
         continue;
      }
      const auto message = mSocket->read(mCurrentMessageLength).toStdString();
      mCurrentMessageLength = 0;

      // Reply
      std::string result;
      try
      {
         const auto command = nlohmann::json::parse(message);
         RequestExecutor executor(command, this);
         result = executor.Run().dump();
      }
      catch (const Exception& e)
      {
         nlohmann::json error;
         error["error"] = e.what();
         result = error.dump();
      }
      catch (const std::exception& e)
      {
         nlohmann::json error;
         error["error"] = e.what();
         result = error.dump();
      }
      catch (...)
      {
         nlohmann::json error;
         error["error"] = "Unknown error while executing request";
         result = error.dump();
      }
      std::stringstream header;
      header << std::to_string(result.length()) << std::endl;
      
      if (!mSocket)
      {
         std::cerr << "Unable to send response: socket is null" << std::endl;
         return;
      }
      mSocket->write(header.str().c_str());
      mSocket->write(result.c_str());
      mSocket->flush();
      break;
   }
}

} // namespace Qat