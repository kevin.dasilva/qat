// (c) Copyright 2023, Qat’s Authors

#include <qat-server/ModelIndexWrapper.h>
#include <qat-server/Exception.h>

#include <QAbstractItemModel>
#include <QItemSelectionModel>

namespace Qat
{

ModelIndexWrapper::ModelIndexWrapper(
   QAbstractItemModel* model,
   QItemSelectionModel* selectionModel,
   const QModelIndex& index,
   QObject* parent) :
      QObject(),
      mModel(model),
      mSelectionModel(selectionModel),
      mIndex(index),
      mParent(parent)
{
   if (!mModel)
   {
      throw Exception("Cannot create ModelIndexWrapper: model is null");
   }
   if (!mParent)
   {
      throw Exception("Cannot create ModelIndexWrapper: parent is null");
   }
}

QAbstractItemModel* ModelIndexWrapper::GetModel() const
{
   return mModel;
}

QItemSelectionModel* ModelIndexWrapper::GetSelectionModel() const
{
   return mSelectionModel;
}

QModelIndex ModelIndexWrapper::GetIndex() const
{
   return mIndex;
}

QObject* ModelIndexWrapper::GetParentWidget() const
{
   return mParent;
}

int ModelIndexWrapper::GetRow() const
{
   return mIndex.row();
}

int ModelIndexWrapper::GetColumn() const
{
   return mIndex.column();
}

QModelIndex ModelIndexWrapper::GetParent() const
{
   return mIndex.parent();
}

QVariant ModelIndexWrapper::data(int role) const
{
   return mIndex.data(role);
}

bool ModelIndexWrapper::setData(const QVariant& value, int role) const
{
   if(!mIndex.isValid())
   {
      throw Exception("Cannot retrieve data: index is invalid");
   }
   return mModel->setData(mIndex, value, role);
}

QString ModelIndexWrapper::GetText() const
{
   if(!mIndex.isValid())
   {
      throw Exception("Cannot get text: index is invalid");
   }
   return mIndex.data().toString();
}

void ModelIndexWrapper::SetText(const QString& text)
{
   if(!mIndex.isValid())
   {
      throw Exception("Cannot change text: index is invalid");
   }
   mModel->setData(mIndex, text);
}

QColor ModelIndexWrapper::GetColor() const
{
   if(!mIndex.isValid())
   {
      throw Exception("Cannot get color: index is invalid");
   }
   return mIndex.data(Qt::ForegroundRole).value<QColor>();
}

void ModelIndexWrapper::SetColor(const QColor& color)
{
   if(!mIndex.isValid())
   {
      throw Exception("Cannot change color: index is invalid");
   }
   mModel->setData(mIndex, color, Qt::ForegroundRole);
}

void ModelIndexWrapper::ScrollTo() const
{
   if (!mSelectionModel)
   {
      throw Exception("Cannot scroll to item: no selection model available");
   }
   mSelectionModel->setCurrentIndex(mIndex, QItemSelectionModel::NoUpdate);
}

} // namespace Qat