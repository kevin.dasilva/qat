// (c) Copyright 2023, Qat’s Authors

#include <qat-server/SignalListener.h>

#include <qat-server/BaseCommandExecutor.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/QVariantToJson.h>
#include <qat-server/RequestHandler.h>

#include <nlohmann/json.hpp>

#include <sstream>

namespace Qat
{

SignalListener::SignalListener(
   RequestHandler* requestHandler) :
   QObject(requestHandler),
   mRequestHandler(requestHandler)
{
   mId = std::to_string(reinterpret_cast<std::uintptr_t>(this));
}

void SignalListener::AttachTo(QObject* object, std::string property)
{
   mObject = object;
   mPropertyName = std::move(property);
}

void SignalListener::Notify()
{
   nlohmann::json message;

   if (mObject)
   {
      const auto value = mObject->property(mPropertyName.c_str());
      if (value.canConvert<QObject*>())
      {
         nlohmann::json definition;
         auto* childObject = value.value<QObject*>();
         if (!childObject)
         {
            definition["object"] = nullptr;
         }
         nlohmann::json objectJson;
         objectJson[Constants::CACHE_UID] = BaseCommandExecutor::RegisterObject(childObject);
         definition["object"] = objectJson;
         message[Constants::ARGUMENTS] = definition;
      }
      else
      {
         nlohmann::json jsonValue;
         jsonValue["value"] = ToJson(value);
         message[Constants::ARGUMENTS] = jsonValue;
      }
   }

   message[Constants::OBJECT_ID] = mId;
   mRequestHandler->SendMessage(message.dump());
}

std::string SignalListener::GetId() const noexcept
{
   return mId;
}

} // namespace Qat