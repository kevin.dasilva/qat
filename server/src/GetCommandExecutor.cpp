// (c) Copyright 2023, Qat’s Authors

#include <qat-server/GetCommandExecutor.h>
#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/MethodCaller.h>
#include <qat-server/ObjectLocator.h>
#include <qat-server/QVariantToJson.h>
#include <qat-server/IWidget.h>
#include <qat-server/WidgetWrapper.h>

#include <QAbstractItemModel>
#include <QItemSelectionModel>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMetaMethod>
#include <QRect>
#include <QObject>

#include <string>

namespace Qat
{
using namespace Constants;

GetCommandExecutor::GetCommandExecutor(const nlohmann::json& request):
   BaseCommandExecutor(request)
{
   for (const auto& field : { OBJECT_DEFINITION, OBJECT_ATTRIBUTE })
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " + field
         );
      }
   }
}

nlohmann::json GetCommandExecutor::Run() const
{
   nlohmann::json result;
   auto* object = FindObject();
   if (!object)
   {
      throw Exception(
         "Cannot get property: object is null"
      );
   }
   result[CACHE_UID] = GetObjectCacheUid(object);
   const auto name = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   if (name == CHILDREN)
   {
      const auto children = ObjectLocator::CollectAllChildren(object, "", false);
      std::vector<nlohmann::json> jsonChildren;
      for (auto* child : children)
      {
         nlohmann::json jsonObject;
         jsonObject[CACHE_UID] = RegisterObject(child);
         // Add common properties to returned data to help the GUI build its tree faster.
         jsonObject[OBJECT_TYPE] = ObjectLocator::GetObjectType(child);
         const auto object_namename = child->objectName().toStdString();
         if (!object_namename.empty())
         {
            jsonObject[OBJECT_NAME] = object_namename;
         }
         jsonObject[CHILDREN] = ObjectLocator::HasChildren(child);
         jsonChildren.push_back(jsonObject);
      }
      result["children"] = jsonChildren;
      return result;
   }
   // QML id is not a real property
   else if (name == OBJECT_ID)
   {
      const auto widget = WidgetWrapper::Cast(object);
      if (widget)
      {
         result["value"] = widget->GetId();
         return result;
      }
      else
      {
         throw Exception(
            "Invalid ID: "
            "Cannot retrieve id property"
         );
      }
   }
   // objectName is not a real property
   else if (name == OBJECT_NAME)
   {
      const auto objectName = object->objectName().toStdString();
      result["value"] = objectName;
      return result;
   }
   else if (name == OBJECT_TYPE || name == CLASS_NAME)
   {
      result["value"] = ObjectLocator::GetObjectType(object);
      return result;
   }
   // Parent is different for 3D QNode / QEntity
   else if (name == OBJECT_PARENT)
   {
      if (object->inherits("Qt3DCore::QNode"))
      {
         MethodCaller caller;
         if (!caller.Call(object, "parentNode", {}))
         {
            throw Exception(
               "Cannot find parent node: "
               "unable to call parentNode()"
            );
         }
         const auto& returnValue = caller.GetResult();
         if (!returnValue.canConvert<QObject*>())
         {
            throw Exception(
               "Cannot find parent node: "
               "Given object is not a QObject instance"
            );
         }
         nlohmann::json jsonObject;
         auto* qobject = returnValue.value<QObject*>();
         if (qobject)
         {
            jsonObject[CACHE_UID] = RegisterObject(qobject);
         }
         result["object"] = jsonObject;
         return result;
      }
      else
      {
         const auto widget = WidgetWrapper::Cast(object);
         if (widget)
         {
            auto* parentWidget = widget->GetParent();
            nlohmann::json jsonObject;
            if (parentWidget)
            {
               jsonObject[CACHE_UID] = RegisterObject(parentWidget);
            }
            result["object"] = jsonObject;
            return result;
         }
      }
   }
   else if (name == CACHE_UID)
   {
      result["value"] = RegisterObject(object);
      return result;
   }
   else if (name == GLOBAL_BOUNDS)
   {
      auto widget = WidgetWrapper::Cast(object);
      if (widget)
      {
         result["value"] = ToJson(widget->GetBounds());
         return result;
      }
   }
   else if (name == PIXEL_RATIO)
   {
      auto widget = WidgetWrapper::Cast(object);
      if (widget)
      {
         result["value"] = ToJson(widget->GetPixelRatio());
         return result;
      }
   }
   else if (object->inherits("QAbstractItemView"))
   {
      auto widget = WidgetWrapper::Cast(object);
      if (widget)
      {
         nlohmann::json objectJson;
         if (name == MODEL)
         {
            objectJson[CACHE_UID] = RegisterObject(widget->GetModel());
         }
         else if (name == SELECTION_MODEL)
         {
            objectJson[CACHE_UID] = RegisterObject(widget->GetSelectionModel());
         }

         if (objectJson.contains(CACHE_UID))
         {
            result["object"] = objectJson;
            return result;
         }
      }
   }
   const auto value = object->property(name.c_str());
   if (!value.isValid())
   {
      // Maybe a method is requested
      const auto nbMethods = object->metaObject()->methodCount();
      for (int i = 0; i < nbMethods; ++i)
      {
         const auto methodName = object->metaObject()->method(i).name().toStdString();
         if (name == methodName)
         {
            result["found"] = true;
            return result;
         }
      }
      throw Exception(
         "Invalid property: "
         "Property '" + name + "' does not exist"
      );
   }
   else if (value.canConvert<QObject*>())
   {
      auto* childObject = value.value<QObject*>();
      if (!childObject)
      {
         result["object"] = nullptr;
         return result;
      }
      nlohmann::json objectJson;
      objectJson[CACHE_UID] = RegisterObject(childObject);
      result["object"] = objectJson;
      return result;
   }

   result["value"] = ToJson(value);
   
   return result;
}

} // namespace Qat