// (c) Copyright 2023, Qat’s Authors

#include <qat-server/WidgetWrapper.h>
#include <qat-server/GlobalApplication.h>
#include <qat-server/IWidget.h>
#include <qat-server/Plugin.h>
#include <qat-server/PluginManager.h>

#include <QGuiApplication>
#include <QObject>
#include <QVariant>
#include <QWindow>

#include <algorithm>
#include <memory>
#include <set>
#include <vector>

namespace Qat
{

std::unique_ptr<IWidget> WidgetWrapper::Cast(const QObject* qobject)
{
   if (qobject == qGuiApp)
   {
      return std::make_unique<GlobalApplication>();
   }
   const auto& pluginMgr = PluginManager::GetInstance();
   for (const auto& it : pluginMgr.GetPlugins())
   {
      auto result = it.second->CastObject(qobject);
      if (result)
      {
         return result;
      }
   }
   return nullptr;
}

std::vector<QObject*> WidgetWrapper::GetTopWindows()
{
   std::set<QObject*> windows;
   const auto& pluginMgr = PluginManager::GetInstance();
   // All plugins can provide top windows
   for (const auto& it : pluginMgr.GetPlugins())
   {
      unsigned int count = 0;
      std::vector<QObject*> windowList;
      if(!it.second->GetTopWindows(nullptr, &count) || count == 0)
      {
         continue;
      }
      windowList.reserve(count);
      windowList.resize(count);
      if(!it.second->GetTopWindows(windowList.data(), &count))
      {
         continue;
      }

      // Ignore null items that may happen due to the delay between the 2 calls to GetTopWindows()
      // (a window may have been closed between calls)
      const auto last = std::remove_if(windowList.begin(), windowList.end(), [](const auto& value){return !value;});
      windows.insert(windowList.begin(), last);
   }
   // Get native handles of all current Qt windows
   std::set<QString> qtIds;
   for (const auto& object : windows)
   {
      const auto topWidget = WidgetWrapper::Cast(object);
      if (!topWidget) continue;
      const auto w = topWidget->GetWindow();
      if (w)
      {
         const auto handleStr = QString("0x") + QString("%1").arg(
            w->winId(), 
            QT_POINTER_SIZE, 16, QChar('0') ).toUpper();
         qtIds.emplace(handleStr);
      }
   }
   
   // Build result as a vector
   std::vector<QObject*> result(windows.size());
   std::copy(windows.cbegin(), windows.cend(), result.begin());

   // Filter native windows having a Qt counterpart
   const auto lastItem = std::remove_if(
      result.begin(), 
      result.end(),
      [&qtIds](const QObject* candidate)
      {
         const auto handle = candidate->property("handle");
         if (!handle.isValid()) return false; // Not a native window, keep it
         return std::find(qtIds.cbegin(), qtIds.cend(), handle.toString()) != qtIds.cend();
      });
   result.resize(std::distance(result.begin(), lastItem));

   return result;
}

std::unique_ptr<QImage> WidgetWrapper::GrabImage(QObject* window)
{
   const auto& pluginMgr = PluginManager::GetInstance();
   for (const auto& it : pluginMgr.GetPlugins())
   {
      auto result = it.second->GrabImage(window);
      if (result)
      {
         return result;
      }
   }
   return nullptr;
}


IObjectPicker* WidgetWrapper::CreatePicker(QObject* window)
{
   const auto& pluginMgr = PluginManager::GetInstance();
   for (const auto& it : pluginMgr.GetPlugins())
   {
      auto result = it.second->CreatePicker(window);
      if (result)
      {
         return result;
      }
   }
   return nullptr;
}

} // namespace Qat