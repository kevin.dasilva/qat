// (c) Copyright 2023, Qat’s Authors

#include <qat-server/ObjectPickerFilter.h>

#include <qat-server/Constants.h>
#include <qat-server/IObjectPicker.h>
#include <qat-server/WidgetWrapper.h>

#include <QKeyEvent>

#include <iostream>

namespace Qat
{

bool ObjectPickerFilter::eventFilter(QObject*, QEvent* event)
{
   bool pausePicker = false;
   bool isAccepted = true;
   // Allow CTRL key to bypass picker. This is useful to open a combobox 
   // and pick an item for example
   if (event->type() == QEvent::KeyPress ||
       event->type() == QEvent::KeyRelease)
   {
      auto* keyEvent = static_cast<QKeyEvent*>(event);
      const auto isCtrlKey = keyEvent->key() == Qt::Key_Control;
      if (!isCtrlKey)
      {
         return false;
      }
      pausePicker = event->type() == QEvent::KeyPress;
   }
   else if (event->type() == QEvent::HoverMove)
   {
      auto* hoverEvent = static_cast<QHoverEvent*>(event);
      pausePicker = hoverEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier);
      isAccepted = false;
   }
   else
   {
      return false;
   }
   for (auto* mainWindow : WidgetWrapper::GetTopWindows())
   {
      IObjectPicker* picker = nullptr;
      QObject* qobject = nullptr;
      try
      {
         qobject = mainWindow->findChild<QObject*>(
            QString::fromStdString(Constants::PICKER_NAME), Qt::FindDirectChildrenOnly);

         // qobject_cast does not work with interfaces
         picker = dynamic_cast<IObjectPicker*>(qobject);
         if (!picker)
         {
            continue;
         }
         if (pausePicker)
         {
            picker->Pause();
         }
         else
         {
            picker->Restore();
         }
      }
      catch(...)
      {
         std::cerr << "ObjectPicker not found" << std::endl;
      }
   }
   return isAccepted;
}

} // namespace Qat