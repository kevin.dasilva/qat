// (c) Copyright 2023, Qat’s Authors

#include <qat-server/ExternalEventsFilter.h>
#include <qat-server/Constants.h>

#include <QEvent>
#include <QGuiApplication>
#include <QWindow>
#include <QMouseEvent>
#include <QTouchEvent>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QTouchDevice>
#else
#include <QPointingDevice>
#endif

namespace
{
const std::vector<QEvent::Type> ACCEPTED_EVENTS 
{
   QEvent::Close,
   QEvent::Expose,
   QEvent::Move,
   QEvent::Paint,
   QEvent::Polish,
   QEvent::PolishRequest,
   QEvent::Resize,
   QEvent::UpdateLater,
   QEvent::Show,
   QEvent::ShowToParent,
   QEvent::SockAct
};

/// \brief Return whether the given event was synthesized from one of Qat's virtual device
/// \param event Any event
/// \return True if the event was synthesized, False otherwise
bool IsQatEvent(const QEvent* event)
{
   using namespace Qat::Constants;

   switch(event->type())
   {
      case QEvent::MouseButtonPress:
      case QEvent::MouseButtonRelease:
      case QEvent::MouseMove:
      case QEvent::MouseButtonDblClick:
      {
         const auto* mouseEvent = static_cast<const QMouseEvent*>(event);
         #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            return mouseEvent->source() != Qt::MouseEventNotSynthesized;
         #else
            const auto deviceName = mouseEvent->pointingDevice()->name().toStdString();
            return deviceName.starts_with(Device::NAME_PREFIX);
         #endif
      }
      case QEvent::TouchBegin:
      case QEvent::TouchEnd:
      case QEvent::TouchUpdate:
      {
         const auto* touchEvent = static_cast<const QTouchEvent*>(event);
         #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            const auto deviceName = touchEvent->device()->name().toStdString();
         #else
            const auto deviceName = touchEvent->pointingDevice()->name().toStdString();
         #endif
         return deviceName.starts_with(Device::NAME_PREFIX);
      }
      case QEvent::KeyPress:
      case QEvent::KeyRelease:
      case QEvent::ShortcutOverride:
      {
         #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            return true;
         #else
            const auto* keyEvent = static_cast<const QKeyEvent*>(event);
            const auto deviceName = keyEvent->device()->name().toStdString();
            return deviceName.starts_with(Device::NAME_PREFIX);
         #endif
      }
      default:
         return false;
   }
}

} // anonymous namespace

namespace Qat
{

bool ExternalEventsFilter::eventFilter(QObject*, QEvent* event)
{
   // Quickly ignore update events for better performance
   if (event->type() == QEvent::UpdateRequest)
   {
      return false;
   }

   // Ignore most system events
   if (event->spontaneous() && !IsQatEvent(event))
   {
      const auto it = std::find(ACCEPTED_EVENTS.cbegin(), ACCEPTED_EVENTS.cend(), event->type());
      return it == ACCEPTED_EVENTS.cend();
   }

   // Ignore window manager events
   switch(event->type())
   {
      case QEvent::WindowActivate:
      case QEvent::WindowDeactivate:
      case QEvent::WindowStateChange:
      case QEvent::ApplicationActivate:
      case QEvent::ApplicationDeactivate:
      case QEvent::ApplicationStateChange:
         return true;
      default:
         return false;
   }
}

} // namespace Qat