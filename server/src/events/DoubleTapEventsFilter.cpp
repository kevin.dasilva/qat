// (c) Copyright 2024, Qat’s Authors

#include <qat-server/DoubleTapEventsFilter.h>
#include <qat-server/Constants.h>

#include <QEvent>
#include <QGuiApplication>
#include <QMouseEvent>

namespace
{
/// \brief Convert the received double-click event and send a simple click instead
/// \param object The receiver of the event
/// \param event The original event
void ConvertDoubleClickToClick(QObject* object, const QMouseEvent* mouseEvent)
{
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      QMouseEvent pressEvent(
         QEvent::Type::MouseButtonPress,
         mouseEvent->pos(),
         mouseEvent->windowPos(),
         mouseEvent->globalPos(),
         mouseEvent->button(),
         mouseEvent->buttons(),
         mouseEvent->modifiers(),
         mouseEvent->source()
      );
      QMouseEvent releaseEvent(
         QEvent::Type::MouseButtonRelease,
         mouseEvent->pos(),
         mouseEvent->windowPos(),
         mouseEvent->globalPos(),
         mouseEvent->button(),
         Qt::MouseButtons(),
         mouseEvent->modifiers(),
         mouseEvent->source()
      );
   #else
      QMouseEvent pressEvent(
         QEvent::Type::MouseButtonPress,
         mouseEvent->position(),
         mouseEvent->globalPosition(),
         mouseEvent->button(),
         mouseEvent->buttons(),
         mouseEvent->modifiers(),
         mouseEvent->pointingDevice()
      );
      QMouseEvent releaseEvent(
         QEvent::Type::MouseButtonRelease,
         mouseEvent->position(),
         mouseEvent->globalPosition(),
         mouseEvent->button(),
         Qt::MouseButtons(),
         mouseEvent->modifiers(),
         mouseEvent->pointingDevice()
      );
   #endif
   qApp->sendEvent(object, &pressEvent);
   qApp->sendEvent(object, &releaseEvent);
}
} // anonymous namespace

namespace Qat
{

bool DoubleTapEventsFilter::eventFilter(QObject* object, QEvent* event)
{
   // Detect and filter out generated double-clicks.
   // This generally happens when calling touch_tap() multiple times: Qt will
   // synthesize two mouse click events that will then be interpreted as a double click.
   if (event->type() == QEvent::MouseButtonDblClick)
   {
      const auto* mouseEvent = static_cast<const QMouseEvent*>(event);
      #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         const auto dblClickGenerated = mouseEvent->flags().testFlag(Qt::MouseEventCreatedDoubleClick);
      #else
         const auto dblClickGenerated = mouseEvent->device()->name().toStdString() == Constants::Touch::DEVICE_NAME;
      #endif
      if (dblClickGenerated)
      {
         ConvertDoubleClickToClick(object, mouseEvent);
         return true;
      }
   }
   return false;
}

} // namespace Qat