#!/bin/bash

mkdir -p build
cmake -G "Unix Makefiles" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_OSX_ARCHITECTURES="x86_64;arm64" -S . -B build

export QAT_VERSION=0.0.dev1
pip3 install qat -e .

# Create symbolic links for Sphinx doc generation
. sphinx/prebuild.sh

read -p "Do you want to open VSCode? (y/[N])" answer

if [[ "$answer" == "y" ]];
then
  export PYTHONPATH=$PYTHONPATH:client:test
  code .
fi
