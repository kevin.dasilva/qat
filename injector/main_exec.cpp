// (c) Copyright 2023, Qat’s Authors

#include <Windows.h>

#include <iostream>
#include <string>

enum ErrorCode
{
   SUCCESS = 0,
   INVALID_ARGS,
   PROCESS_NOT_FOUND,
   PROCESS_BUSY,
   LIB_NOT_FOUND,
   ALLOC_FAILED,
   THREAD_FAILED,
   TIMEOUT
};

struct WindowSearchData
{
   DWORD processId;
   HWND windowHandle;
};

bool IsMainWindow(HWND windowHandle)
{
   return GetWindow(windowHandle, GW_OWNER) == (HWND)0 && IsWindowVisible(windowHandle);
}

BOOL CALLBACK SeachWindowCallback(HWND windowHandle, LPARAM payload)
{
   if (!payload)
   {
      return TRUE;
   }

   auto* searchData = reinterpret_cast<WindowSearchData*>(payload);
   unsigned long windowPid = 0;
   GetWindowThreadProcessId(windowHandle, &windowPid);
   if (searchData->processId != windowPid || !IsMainWindow(windowHandle))
   {
      // Wrong window
      return TRUE;
   }

   // Found the window
   searchData->windowHandle = windowHandle;
   return FALSE;
}

bool FindMainWindow(DWORD processId)
{
   WindowSearchData payload;
   payload.processId = processId;
   payload.windowHandle = 0;
   if (EnumWindows(SeachWindowCallback, (LPARAM)&payload))
   {
      return false;
   }

   return payload.windowHandle != 0;
}

int main(int argc, char **argv) 
{
   if (argc < 3)
   {
      std::cerr << "Process ID argument is required" << std::endl;
      return INVALID_ARGS;
   }

   // Get the process handle
   const auto pid = std::stoi(argv[1]);
   const std::string dllPath = argv[2];
   HANDLE processHandle =
      OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION
         | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ,
         FALSE, pid);

   if (processHandle)
   {
      // Wait for the target process to have its main window created
      // to avoid freezing it by loading the library
      /// \todo Support console applications too
      while (!FindMainWindow(pid))
      {
         DWORD exitCode;
         auto status = GetExitCodeProcess(processHandle, &exitCode);
         if (!status || exitCode != STILL_ACTIVE)
         {
            std::cerr << "Process did not succeed or was terminated" << std::endl;
            return ErrorCode::PROCESS_NOT_FOUND;
         }
         Sleep(100);
      }

      // Allocate memory into the target process
      HMODULE hernel32Handle = ::GetModuleHandle("Kernel32");
      if (!hernel32Handle)
      {
         return ErrorCode::LIB_NOT_FOUND;
      }

      char szLibPath[_MAX_PATH];
      strncpy_s(szLibPath, dllPath.c_str(), sizeof(szLibPath) / sizeof(szLibPath[0]));
      void* remoteBuffer = ::VirtualAllocEx(processHandle, NULL,
         sizeof(szLibPath), MEM_COMMIT, PAGE_READWRITE);

      if (!remoteBuffer)
      {
         return ErrorCode::ALLOC_FAILED;
      }

      ::WriteProcessMemory(processHandle, remoteBuffer,
         (void*)szLibPath, sizeof(szLibPath), NULL);

      // Create a thread in the target process to load the server library
      HANDLE threadHandle = ::CreateRemoteThread(processHandle, NULL, 0,
         (LPTHREAD_START_ROUTINE)::GetProcAddress(hernel32Handle,
            "LoadLibraryA"),
         remoteBuffer, 0, NULL);

      if (threadHandle)
      {
         DWORD exitCode = 0;
         ::WaitForSingleObject(threadHandle, INFINITE);
         ::GetExitCodeThread(threadHandle, &exitCode);
         ::CloseHandle(threadHandle);
      }
      else
      {
         return ErrorCode::THREAD_FAILED;
      }
   }
   else
   {
      return ErrorCode::PROCESS_NOT_FOUND;
   }

   return ErrorCode::SUCCESS;
}
