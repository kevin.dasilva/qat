// (c) Copyright 2024, Qat’s Authors

#include <cstdlib>
#include <dlfcn.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

namespace
{
/// Thread calling the Start() function of the server
std::thread* startThread{nullptr};

/// Handle to the injected library
void* handle{nullptr};

/// Class implementing RAII for loading/unloading the server in the injected library
struct Loader
{
   /// Constructor - loads and starts the server
   Loader();

   /// Destructor - stops the server
   ~Loader();
};

/// Global instance to automatically start the server when this library is loaded.
static Loader gLoaderInstance;
}
 
void onLoad()
{
   std::cout << "Loading injector" << std::endl;
   // Get a handle to the current executable
   handle = dlopen(nullptr, RTLD_NOLOAD | RTLD_LAZY);

   // Assuming the current executable is linked to Qt, find the handle to the qVersion() function
   const char* (*qtVersionFunc)(void);
   *(void**)(&qtVersionFunc) = dlsym(handle, "qVersion");
   if(!qtVersionFunc)
   {
      std::cerr << "Could not find qVersion function" << std::endl;
      return;
   }

   // Get current Qt version
   const std::string qtVersion = qtVersionFunc();

   // Get server library name
   std::stringstream versionStream(qtVersion);
   std::string token;
   std::vector<std::string> elements;
   while (std::getline(versionStream, token, '.'))
   {
      elements.push_back(token);
   }
   if (elements.size() < 2)
   {
      std::cerr << "Could not get Qt version elements" << std::endl;
      return;
   }
   const auto libraryName = "libQatServer." + elements[0] + "." + elements[1] + ".dylib";

   Dl_info libInfo;
   if (!dladdr(&gLoaderInstance, &libInfo))
   {
      std::cerr << "Could not retrieve library path (dladdr failed)" << std::endl;
      return;
   }

   std::string libPathData = libInfo.dli_fname;
   if (libPathData.empty())
   {
      std::cerr << "Could not retrieve library path" << std::endl;
      return;
   }

   std::filesystem::path libPath = libPathData;
   libPath = libPath.parent_path() / libraryName;
   
   // Load server library
   handle = dlopen(libPath.string().c_str(), RTLD_LAZY);
   if (!handle)
   {
      std::cerr << "Failed to load Qat server: " << libPath.string() << std::endl;
      return;
   }
   else
   {
      std::cout << "Successfully loaded Qat server" << std::endl;
   }

   void (*func_start)(void);
   *(void**)(&func_start) = dlsym(handle, "Start");
   if(!func_start)
   {
      std::cerr << "Could not find Start function" << std::endl;
      return;
   }

   startThread = new std::thread([func_start](){func_start();});
}


void onUnload()
{
   std::cout << "onUnload" << std::endl;
   if(startThread)
   {
      // Stop or abort the injected server
      void (*func_stop)(void);
      *(void**)(&func_stop) = dlsym(handle, "Stop");
      if(!func_stop)
      {
         std::cerr << "Could not find Stop function" << std::endl;
      }
      else
      {
         func_stop();
      }

      startThread->join();
      std::cout << "Deleting startup thread" << std::endl;
      delete(startThread);
      startThread = nullptr;
   }
}

Loader::Loader()
{
   onLoad();
}

Loader::~Loader()
{
   onUnload();
}