// (c) Copyright 2023, Qat’s Authors

#include <Windows.h>

#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace
{
/// Return the value of the given environment variable
/// \param[in] name The env var name
/// \return The value of the given env var
std::string GetEnvVar(const std::string& name)
{
   char* pValue{nullptr};
   size_t len{0};
   const auto rc = _dupenv_s(&pValue, &len, name.c_str());
   if (rc || !pValue || !len)
   {
      return "";
   }

   std::string value = pValue;
   free(pValue);
   return value;
}
}
 
void onLoad()
{
   // Get a handle to the Qt Core library
   HMODULE handle{0};
   for (const auto lib : {"Qt5Core.dll", "Qt6Core.dll"})
   {
      handle = GetModuleHandleA(lib);
      if (handle)
      {
         break;
      }
   }

   // Assuming the current executable is linked to Qt, find the handle to the qVersion() function
   typedef const char* (CALLBACK* LPFNDLLFUNC1)(void); 
   auto qtVersionFunc = (LPFNDLLFUNC1)GetProcAddress(handle, "qVersion");
   if(!qtVersionFunc)
   {
      std::cerr << "Could not find qVersion function" << std::endl;
      std::cerr << "Error code is " << GetLastError() << std::endl;
      return;
   }

   // Get current Qt version
   const std::string qtVersion = qtVersionFunc();

   // Get server library name
   std::stringstream versionStream(qtVersion);
   std::string token;
   std::vector<std::string> elements;
   while (std::getline(versionStream, token, '.'))
   {
      elements.push_back(token);
   }
   if (elements.size() < 2)
   {
      std::cerr << "Could not get Qt version elements" << std::endl;
      return;
   }
   const auto libraryName = "QatServer." + elements[0] + "." + elements[1] + ".dll";

   // Find the path to the library (assuming it is the same as the path to the current library)
   constexpr int MAX_PATH_LEN{4096};
   char libPathData[MAX_PATH_LEN];
   const auto libHandle = GetModuleHandleA("injector.dll");
   const auto rc = GetModuleFileNameA(libHandle, libPathData, MAX_PATH_LEN);
   if (!rc)
   {
      std::cerr << "Could not retrieve library path (error #" << 
         GetLastError() << ")" << std::endl;
      return;
   }

   std::filesystem::path libPath = libPathData;
   libPath = libPath.parent_path() / libraryName;
   
   // Load server library
   handle = LoadLibraryA(libPath.string().c_str());
   if (!handle)
   {
      std::cerr << "Failed to load Qat server: " << libPath.string() << std::endl;
      return;
   }
   else
   {
      std::cout << "Successfully loaded Qat server" << std::endl;
   }
}


extern "C" int __stdcall DllMain(HINSTANCE, DWORD signal, LPVOID) 
{
   switch (signal) 
   {
   case DLL_PROCESS_ATTACH:
      onLoad();
      break;
   default:
      break;
   }

   return TRUE;
}
