<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by "type::bug" label:

- https://gitlab.com/testing-tool/qat/issues?label_name%5B%5D=type::bug

and verify the issue you're about to submit isn't a duplicate.

Write a descriptive proposal title above.
--->

### Description of Issue

<!-- Summarize the issue encountered concisely. -->

### Affected version(s)

<!-- List the affected version(s) that this issue applies to.
 Example: 0.5.1 -->

### System information

<!-- List the system information.
 Example:
 - OS: Linux
 - OS version: Ubuntu 23.10
 - Qt version:  6.5.0
 - Python version: 3.11.8 -->
- OS: <Linux|Windows>
- OS version: 
- Qt version: 
- Python version: 

### Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important. Please use an ordered list. -->

### What is the current *issue* behavior?

<!-- Describe what actually happens. -->

### What is the expected *expected* behavior?

<!-- Describe briefly how it is expected to work along with any images and/or videos. -->

### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise. -->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. 
 Please use code blocks to format console output, logs, and code (```) -->

/label ~"type::bug"
