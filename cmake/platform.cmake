
string(TOLOWER ${CMAKE_SYSTEM_NAME} PLATFORM_NAME)

string(FIND ${PLATFORM_NAME} "linux" index)
if(NOT ${index} EQUAL -1)
   set(PLATFORM_IS_LINUX 1)
endif()

string(FIND ${PLATFORM_NAME} "windows" index)
if(NOT ${index} EQUAL -1)
   set(PLATFORM_IS_WINDOWS 1)
endif()

string(FIND ${PLATFORM_NAME} "darwin" index)
if(NOT ${index} EQUAL -1)
   set(PLATFORM_IS_MACOS 1)
endif()