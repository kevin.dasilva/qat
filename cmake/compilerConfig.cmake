
SET(CMAKE_CXX_STANDARD 20)
SET(CMAKE_CXX_STANDARD_REQUIRED ON)
SET(CMAKE_CXX_EXTENSIONS OFF)

if (PLATFORM_IS_LINUX)
   add_compile_options(-Wall -Werror -fPIC -fstrict-aliasing -pedantic-errors -pedantic -Wno-deprecated-declarations)
elseif(PLATFORM_IS_WINDOWS)
   add_compile_options(/MP /W4 /WX)
elseif (PLATFORM_IS_MACOS)
   add_compile_options(-Wall -Werror -fPIC -fstrict-aliasing -Wno-deprecated-declarations)
endif()