# Tutorials

Here is a list of available tutorials:

## [Identifying objects](./tutorials/IdentifyingObjects.md)
This tutorial explains how to create object definitions to keep test execution fast, reliable and flexible.

## [Synchronization](./tutorials/Synchronization.md)
In this tutorial, we will explore how to leverage Qat synchronization mechanisms to tackle timing issues effectively during test execution.

## [Model / View Framework](./tutorials/ItemViewModel.md)
This tutorial shows how to interact with objects that are part of the Qt's Model/View framework.

## [Menus and actions](./tutorials/Menus.md)
This tutorial shows how to interact with menus and their associated actions.