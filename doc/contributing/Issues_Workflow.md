# Issues workflow

## Creating an issue

**Before you submit an issue, [search the issue tracker](https://gitlab.com/testing-tool/qat/-/issues)**
for similar entries. Someone else might have already had the same bug or feature proposal.
If you find an existing issue, show your support with an emoji reaction and add your notes to the discussion.

### Bugs

To submit a bug:

- Use the [**Bug** issue template](https://gitlab.com/testing-tool/qat/-/issues/new?issuable_template=Bug).
  The text in the comments (`<!-- ... -->`) should help you with which information to include.

In order to help track of bugs, we use the
[`~"type::bug"`](https://gitlab.com/testing-tool/qat/-/issues?label_name=type::bug) label.

### Feature proposals

To create a feature proposal:

- Use the [**Feature Proposal** issue template](https://gitlab.com/testing-tool/qat/-/issues/new?issuable_template=Feature%20Proposal).
  The text in the comments (`<!-- ... -->`) should help you with which information to include.

In order to help track feature proposal, we use the
[`~"type::feature"`](https://gitlab.com/testing-tool/qat/-/issues?label_name=type::feature) label.
