# How to build Qat

## Qt support
In order to build Qat, you need a functional Qt installation for each version you want to support.

However, Qat does not distribute any Qt binary since it uses DLL injection to load its libraries into the application being tested. This means that Qat will dynamically link to the Qt binaries currently used by your application.

So if you use a custom Qt version that is binary compatible with one of the versions supported by Qat (i.e with the same Major.Minor version) you should not need to rebuild Qat.

If you want to support a new Qt version, or a supported one with another toolchain, you will need to follow the instructions below to build your own Qat package. If you do so, please feel free to [contribute](./Contributing.md) your work to this project.

If you do not already have a working Qt SDK for the version you want to support, you can follow [these instructions](./qt/Build%20instructions.md) to build it from sources.


## System Requirements
You will need the following dependencies:
- A Git client
- CMake >= 3.10
- A functional C++ toolchain
- Python >= 3.9
- A functional Qt installation for each version you want to use

To run the tests, you will need:
- The `pytest`, `pytest-mock`, `behave`, `mouse` and `psutil` modules for your Python interpreter

You can install all these requirements by using the _requirements.txt_ file located in the _config_ folder of the repository:
```bash
pip install -r config/requirements.txt
```

## Setup

Clone the Qat repository in any folder:
``` bash
git clone https://gitlab.com/testing-tool/qat.git
```

For standard setup, run the ___init_repo.sh___ (for linux and MacOS) or ___init_repo.bat___ (for Windows) script from the cloned repository. This script will configure the CMake project and the Python Pip package.

Alternatively, you can call CMake with the configuration and generator of your choice and configure a local Pip package.

``` bash
cmake -G <generator> -DCMAKE_BUILD_TYPE=<Release|Debug> -B <build_folder>

# linux and MacOS:
export QAT_VERSION="0.0.0"
# Windows:
set QAT_VERSION="0.0.0"

pip install qat -e .
```

At this stage, you can perform any modification you want to the Qat source code. You may have to run the previous CMake command again if you add or remove files.

## Build

Setup the CMAKE_PREFIX_PATH CMake variable corresponding to the Qt version you want to use. If you want to support multiple Qt versions, simply repeat this build process for each version.

``` bash
# linux and MacOS:
export CMAKE_PREFIX_PATH=<path to your Qt installation folder>
# Windows:
set CMAKE_PREFIX_PATH=<path to your Qt installation folder>
```

Go to the build folder (default is _./build_):
``` bash
cd <build_folder>
```

Then build Qat with CMake:
``` bash
cmake --build .
cmake --install .
```

## Test
Once Qat has been successfully built, you can run the API tests by opening a shell in the repository root and call PyTest:

``` bash
# linux and MacOS:
export PYTHONPATH=$PYTHONPATH:client
# Windows:
set PYTHONPATH=$PYTHONPATH:client

pytest test/pytest --app_name=QmlApp,WidgetApp --qt_version=X.Y.Z
```
where X.Y.Z is the current Qt version located at CMAKE_PREFIX_PATH.

You can also run tests from your IDE, for example with the _Test Explorer UI_ extension in _VSCode_

## Package
To create a local package, you need to copy all binaries to the python module then use the _build_ module:

```bash
mkdir -p client/qat/bin/plugins/
mkdir -p client/qat/templates/
cp build/libinjector.so client/qat/bin
cp build/libinjector.dylib client/qat/bin
cp build/injector.dll client/qat/bin
cp build/injector.exe client/qat/bin
cp build/libQatServer*.so client/qat/bin
cp build/QatServer*.dll client/qat/bin
cp build/libQatServer*.dylib client/qat/bin
cp -R build/plugins/* client/qat/bin/plugins
cp -R templates/* client/qat/templates
rm -rf ./pypackage
python -m build . -o pypackage
```

The package will be generated in the _pypackage_ folder and can then be managed with _Twine_.

> Note: once copied, these binaries will take precedence over your local build when using Qat. To use the local build again, simply delete the client/qat/bin folder.
