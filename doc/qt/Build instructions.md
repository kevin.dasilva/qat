# How to build Qt

The official documentation for building Qt from sources is available at https://doc.qt.io/qt-6/build-sources.html.

> _Note_: These instructions are for building the open-source version of Qt. If you have a commercial license, please use the corresponding binaries using the [official Qt Installer](https://www.qt.io/download).

## System Requirements
- Git >= 1.6.x
- CMake >= 3.18.4
- Ninja (optional, required on MacOS)
- A C++ compiler supporting C++17
- Perl >= 5.14 (e.g. Strawberry Perl)
- Python >= 2.6
- Windows: A C++ development environment such as MS Visual Studio >= 2019 or MinGW >= 11.2
- Linux: OpenGL development libraries (libgl-dev, libegl-dev, libinput-dev, libfontconfig1-dev)
- MacOS: Xcode with the clang toolchain (Xcode 13 for Qt < 6.5, Xcode 15 for Qt >= 6.5)


## Getting the Sources
Official source code is available at code.qt.io:
```bash
cd <source_folder>
git clone https://code.qt.io/qt/qt5.git
```
> _Note_: Although the URL contains __Qt5__, the sources also contain the Qt6 code.

> Alternatively, the exact source files used to build the current Qat version can be provided: please [open an issue](https://gitlab.com/testing-tool/qat/-/issues) on the Qat's Gitlab project and an archive will be sent to you.

Checkout the version you want to use:
```bash
# Switch to latest version of branch X.Y
git switch X.Y
# or use specific version (tag)
git checkout vX.Y.Z
```

Run the _init-repository_ script to get the submodules:
### _Linux and MacOS:_
```bash
./init-repository --module-subset=default,-qtwebengine
```

### _Windows:_
```bash
perl .\init-repository --module-subset=default,-qtwebengine
```

Please refer to [Building_Qt_5_from_Git](https://wiki.qt.io/Building_Qt_5_from_Git) or [Building_Qt_6_from_Git](https://wiki.qt.io/Building_Qt_6_from_Git) for more details.

## Setup

### _Windows only:_

> Make sure your your compiler and Python executables are found in your PATH environment variable.

> Make sure your compiler is before Perl in the PATH variable since Perl comes with version of g++ (8.3) that is not compatible with Qt 5.

> Also make sure that Perl is before Git in the PATH variable since Git comes with version of Perl that is not compatible with Qt.

> You will also need to add <source_folder>/qtbase/bin, <source_folder>/gnuwin32/bin to the PATH variable.

### Configure
Create an empty build folder and call the ___configure___ script. It is recommended to create the build folder as a sibling of the source folder.

### _Windows:_
> Make sure to use a Visual Studio command prompt "x64 Native Tools Command Prompt for VS 20XX"

```bash
cd <source_folder>
cd ..
mkdir <build_folder>
cd <build_folder>
../<source_folder>/configure -opensource -confirm-license -nomake examples -nomake tests -release -prefix <install_folder>
```

Where <install_folder> if the folder where the Qt binaries will be generated. This is the folder the CMAKE_PREFIX_PATH CMake variable should point to when building Qat or any other Qt application.

To select the C++ toolchain, you can pass the additional ___-platform___ argument to the ___configure___ script.
Supported toolchain values are:

### _Linux:_
- linux-clang
- linux-g++
- linux-g++-32

### _Windows:_
- win32-g++
- win32-msvc

### _MacOS:_
On MacOS, you can select the target architecture(s) with a CMake variable and create universal libraries:
```bash
./configure <other arguments> -- -DCMAKE_OSX_ARCHITECTURES="x86_64;arm64"
```

> Note: arm64 and universal builds are not supported on Qt5.

> Note: Intel architecture ("x86_64") must be specified first, otherwise Qt may not enable all the x86 functionality.

> Note: When building Qt < 6.5 with a recent MacOS version (e.g. Sonoma), the configure script may generate errors related to OpenGL. In such cases, a workaround is to remove or comment the following lines from *<qt_sources>/qtbase/cmake/FindWrapOpenGL.cmake*:

```cmake
   if (NOT __opengl_fw_path)
      # Just a safety measure...
      set(__opengl_fw_path "-framework OpenGL")
   endif()
```

## Build
The build process is managed by CMake: 
```bash
cd <build_folder>
cmake --build . --parallel
cmake --install .
```

When building Qt 5.X on Windows, the build process is managed by make or mingw32-make: 
```bash
cd <build_folder>
mingw32-make
mingw32-make install
```

At this point, Qt should be available in your <install_folder> so you can use it in other applications.
