// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <memory>
#include <string>

class QQmlApplicationEngine;

class ApplicationLauncher
{
public:

   /// Constructor
   ApplicationLauncher() = default;

   /// Destructor
   virtual ~ApplicationLauncher() = default;

   /// Instantiate the application
   /// \return Pointer to the QML application engine.
   QQmlApplicationEngine* Build();

private:
   /// Instantiate the application user interface.
   /// \return Pointer to the application's QML engine.
   QQmlApplicationEngine* BuildUI();

};
