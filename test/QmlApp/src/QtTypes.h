// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QObject>
#include <QBrush>
#include <QByteArray>
#include <QColor>
#include <QFont>
#include <QLine>
#include <QQuaternion>
#include <QPoint>
#include <QRect>
#include <QSize>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>


/// \brief A container for testing Qt types serialization.
class QtTypes : public QObject
{
   Q_OBJECT

   Q_PROPERTY(
      QBrush brush
      MEMBER mBrush
   )
   Q_PROPERTY(
      QColor color
      MEMBER mColor
   )
   Q_PROPERTY(
      QFont font
      MEMBER mFont
   )
   Q_PROPERTY(
      QPoint point
      MEMBER mPoint
   )
   Q_PROPERTY(
      QPointF pointF
      MEMBER mPointF
   )
   Q_PROPERTY(
      QLine line
      MEMBER mLine
   )
   Q_PROPERTY(
      QLineF lineF
      MEMBER mLineF
   )
   Q_PROPERTY(
      QSize size
      MEMBER mSize
   )
   Q_PROPERTY(
      QSizeF sizeF
      MEMBER mSizeF
   )
   Q_PROPERTY(
      QRect rect
      MEMBER mRect
   )
   Q_PROPERTY(
      QRectF rectF
      MEMBER mRectF
   )
   Q_PROPERTY(
      QQuaternion quaternion
      MEMBER mQuaternion
   )
   Q_PROPERTY(
      QVector2D vector2d
      MEMBER mVector2d
   )
   Q_PROPERTY(
      QVector3D vector3d
      MEMBER mVector3d
   )
   Q_PROPERTY(
      QVector4D vector4d
      MEMBER mVector4d
   )
   Q_PROPERTY(
      QByteArray byteArray
      MEMBER mByteArray
   )

public:
   /// Constructor
   /// \param[in] parent The Qt parent
   explicit QtTypes(QObject* parent) : QObject(parent) {};

   /// Default destructor.
   ~QtTypes() override = default;

private:
   /// One instance of each supported type.
   /// @{
   QBrush mBrush;
   QColor mColor;
   QFont mFont;
   QPoint mPoint;
   QPointF mPointF;
   QLine mLine;
   QLineF mLineF;
   QSize mSize;
   QSizeF mSizeF;
   QRect mRect;
   QRectF mRectF;
   QQuaternion mQuaternion;
   QVector2D mVector2d;
   QVector3D mVector3d;
   QVector4D mVector4d;
   QByteArray mByteArray;
   /// @}

};
Q_DECLARE_METATYPE(QtTypes*)
