// (c) Copyright 2023, Qat’s Authors

#include <ApplicationLauncher.h>
#include <MainContextProxy.h>

#include <QQmlContext>
#include <QQmlApplicationEngine>

#include <iostream>

QQmlApplicationEngine* ApplicationLauncher::Build()
{   
   const auto engine(BuildUI());

   // Add the context proxy to the QML root context
   engine->rootContext()->setContextProperty(
      QString::fromStdString("_mainContextProxy"),
      new MainContextProxy(engine));

   // Load QML interface
   engine->addImportPath("qrc:/");
   engine->load(QUrl(QStringLiteral("qrc:///Main.qml")));

   return engine;
}

QQmlApplicationEngine* ApplicationLauncher::BuildUI()
{
   std::cout << "Building application UI" << std::endl;
   QQmlApplicationEngine* engine = new QQmlApplicationEngine();      
   QObject::connect(engine, &QQmlApplicationEngine::warnings,
      [=](const QList<QQmlError> &warnings)
   {
      for (auto& error : warnings)
      {
         qDebug() << "*** warning: " << error.description();
      }
   });
   return engine;
}