// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QtTypes.h>
#include <QObject>
#include <QColor>
#include <QPointer>
#include <QStringList>
#include <QVariant>

#include <memory>

class SubContextProxy;

/// \brief Main context proxy for QML interface
class MainContextProxy : public QObject
{
   Q_OBJECT

   Q_PROPERTY(
      QString qtVersion
      READ GetQtVersion
      CONSTANT
   )

   Q_PROPERTY(
      double doubleValue
      MEMBER mDoubleValue
      NOTIFY doubleValueChanged
   )

   Q_PROPERTY(
      QtTypes* qtTypes
      MEMBER mQtTypes
      CONSTANT
   )

   Q_PROPERTY(
      QObject* subProxy
      READ GetSubProxy
      NOTIFY subProxyChanged
   )

   Q_PROPERTY(
      QStringList stringList
      MEMBER mStringList
      WRITE SetStringList
      NOTIFY stringListChanged
   )

public:
   /// Constructor
   /// \param[in] parent The Qt parent
   explicit MainContextProxy(QObject* parent);

   /// Default destructor.
   ~MainContextProxy() override = default;

   /// Return the current Qt version
   /// \return the current Qt version
   QString GetQtVersion() const;

   /// Add the two given numbers
   Q_INVOKABLE double Add(double a, double b) const;

   /// Add the three given numbers
   Q_INVOKABLE double Add(double a, double b, double c) const;

   /// Return the current color
   /// \return the current color
   Q_INVOKABLE QVariant GetColor() const;

   /// Change the current color
   /// \param[in] color A color
   Q_INVOKABLE void SetColor(const QColor& color);

   /// Replace the current sub-proxy with a new one
   Q_INVOKABLE void ChangeSubProxy();

   /// Set the string list
   /// \param[in] list The new string list
   void SetStringList(const QStringList& list);

signals:
   /// Emitted when properties change
   /// @{
   void doubleValueChanged();
   void subProxyChanged();
   void stringListChanged();
   /// @}

private:
   /// Qt accessor to the sub-proxy
   Q_INVOKABLE QObject* GetSubProxy();

   /// Some double number
   double mDoubleValue{ 0. };

   /// Some color
   QColor mColor;

   /// Testing object for Qt types
   QPointer<QtTypes> mQtTypes;

   /// Pointer to a sub-proxy
   QPointer<SubContextProxy> mSubProxy;

   /// A list of QStrings
   QStringList mStringList;

};
Q_DECLARE_METATYPE(MainContextProxy*)
