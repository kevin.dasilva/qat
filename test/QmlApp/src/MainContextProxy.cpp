// (c) Copyright 2023, Qat’s Authors

#include <MainContextProxy.h>
#include <QtTypes.h>
#include <SubContextProxy.h>

#include <QVariant>

MainContextProxy::MainContextProxy(QObject* parent) :
   QObject(parent)
{
   mSubProxy = new SubContextProxy(this, 0);
   mQtTypes = new QtTypes(this);
   mQtTypes->setObjectName("qtTypesContainer");
}

QString MainContextProxy::GetQtVersion() const
{
   return QT_VERSION_STR;
}

double MainContextProxy::Add(double a, double b) const
{
   return a + b;
}

double MainContextProxy::Add(double a, double b, double c) const
{
   return a + b + c;
}

QVariant MainContextProxy::GetColor() const
{
   return mColor;
}

void MainContextProxy::SetColor(const QColor& color)
{
   mColor = color;
}

void MainContextProxy::ChangeSubProxy()
{
   if (mSubProxy)
   {
      mSubProxy->deleteLater();
   }
   mSubProxy = new SubContextProxy(this, 0);
   emit subProxyChanged();
}

QObject* MainContextProxy::GetSubProxy()
{
   return mSubProxy;
}


void MainContextProxy::SetStringList(const QStringList& list)
{
   mStringList = list;
   emit stringListChanged();
}
