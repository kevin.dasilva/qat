// (c) Copyright 2023, Qat’s Authors

#include <ApplicationLauncher.h>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QOpenGLContext>
#include <Qt3DRender/qt3drender-config.h>

#include <future>
#include <iostream>


void setSurfaceFormat()
{
    QSurfaceFormat format;
#if QT_CONFIG(opengles2)
    format.setRenderableType(QSurfaceFormat::OpenGLES);
#else
    if (QOpenGLContext::openGLModuleType() == QOpenGLContext::LibGL) {
        format.setVersion(4, 3);
        format.setProfile(QSurfaceFormat::CoreProfile);
    }
#endif
    format.setDepthBufferSize(24);
    format.setSamples(4);
    format.setStencilBufferSize(8);
    QSurfaceFormat::setDefaultFormat(format);

#if qt3d_rhi_renderer == 0L || !QT_CONFIG(qt3d_rhi_renderer)
    qputenv("QSG_RHI_BACKEND", "opengl");
    qputenv("QT3D_RENDERER", "opengl");
#endif
}

int main(int argc, char *argv[])
{
   if (argc > 2)
   {
      std::cerr << "Too many arguments" << std::endl;
      return 2;
   }
   if (argc > 1)
   {
      std::string argument{argv[1]};
      if (argument == "-h" || argument == "--help")
      {
         std::cout << "QML-based testing application" << std::endl;
         return 0;
      }
      else
      {
         std::cerr << "Unrecognized argument" << std::endl;
         return 2;
      }
   }

   QGuiApplication app(argc, argv);
   setSurfaceFormat();
   ApplicationLauncher launcher;
   QScopedPointer<QQmlApplicationEngine> appEngine{launcher.Build()};
   if (appEngine->rootObjects().isEmpty() || !appEngine->rootObjects().first())
   {
      std::cerr << "QML engine initialization error caught :"
                   "QML engine rootObjects() is empty."
                << std::endl;
      return 1;
   }

   return app.exec();
}
