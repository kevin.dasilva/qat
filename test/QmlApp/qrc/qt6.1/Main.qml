// (c) Copyright 2023, Qat’s Authors


import Qt.labs.platform 1.1
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window

ApplicationWindow
{
   id: mainWindow
   objectName: "mainWindow"
   visible: true
   width: 1400
   height: 900
   property var mainProxy: _mainContextProxy
   property var counter : 0

   MessagePopup
   {
      id: messagePopup
      objectName: "messagePopup"
      message: "Some message"
      title: "Some title"
   }

   FileDialog 
   {
      id: fileDialog
      objectName: "fileDialog"
      folder: StandardPaths.standardLocations(StandardPaths.HomeLocation)[0]
      onAccepted:
      {
         openFileButton.text = fileDialog.file
      }
   }

   ApplicationWindow
   {
      id: secondWindow
      objectName: "secondWindow"
      visible: false
      width: 800
      height: 600

      MenuBar
      {
         Menu
         {
            title: "Widgets"
            objectName: "widgetsMenu"
            MenuItem
            {
               text: "Add"
               enabled: false
            }
            Menu
            {
               title: "Close button"
               objectName: "closeButtonMainMenu"
               MenuItem
               {
                  text: "Enabled"
                  checkable: true
                  checked: true
                  onTriggered: closeWindowButton.enabled = !closeWindowButton.enabled
               }
               MenuItem
               {
                  text: "Visible"
                  objectName: "visibleAction"
                  checkable: true
                  checked: true
                  onTriggered: closeWindowButton.visible = !closeWindowButton.visible
               }
            }
         }
      }

      MouseArea
      {
         anchors.fill: parent
         acceptedButtons: Qt.RightButton
         onClicked:
         {
            contextMenu.open(secondWindow)
         }

         Menu
         {
            id: contextMenu
            objectName: "contextMenu"

            MenuItem
            {
               text: "Placeholder"
            }
            MenuSeparator{}

            Menu
            {
               title: "Close button"
               objectName: "closeButtonMenu"
               MenuItem
               {
                  text: "Enabled"
                  checkable: true
                  checked: closeWindowButton.enabled
                  onTriggered: {closeWindowButton.enabled = !closeWindowButton.enabled}
               }
               MenuItem
               {
                  text: "Visible"
                  objectName: "visibleAction"
                  checkable: true
                  checked: closeWindowButton.visible
                  onTriggered: {closeWindowButton.visible = !closeWindowButton.visible}
               }
            }
         }
      }

      Button
      {
         id: closeWindowButton
         objectName: "closeWindowButton"
         visible: true
         checkable: false
         text: "Close"
         onClicked:
         {
            secondWindow.close()
         }
      }
   }

   RowLayout
   {
      anchors.fill:parent
      objectName: "mainRowLayout"
      ColumnLayout
      {
         objectName: "leftColumn"
         Layout.fillWidth: false
         Layout.fillHeight: true

         Button
         {
            objectName: "checkableButton"
            Layout.fillWidth: true
            visible: true
            checkable: true
            text: checked ? "checked" : "unchecked"
         }

         Button
         {
            objectName: "counterButton"
            id: "counterButtonId"
            Layout.fillWidth: true
            visible: true
            text: counter
            onClicked:
            {
               counter = counter + 1
            }
            Shortcut 
            {
               sequences: ["Ctrl+Z", "Shift+-"]
               onActivated:
               {
                  counter = counter - 1
               }
            }
         }

         Button
         {
            objectName: "modifierButton"
            id: modifierButton
            Layout.fillWidth: true
            visible: true
            text: ""
            MouseArea
            {
               anchors.fill: parent
               onClicked: function(mouse)
               {
                  modifierButton.text =  ""
                  if (mouse.modifiers & Qt.ControlModifier)
                  {
                     modifierButton.text += "CTRL "
                  }
                  if (mouse.modifiers & Qt.ShiftModifier)
                  {
                     modifierButton.text += "SHIFT "
                  }
                  if (mouse.modifiers & Qt.AltModifier)
                  {
                     modifierButton.text += "ALT "
                  }
               }
            }
         }

         Button
         {
            objectName: "openButton"
            Layout.fillWidth: true
            visible: true
            text: "Open popup"
            onClicked:
            {
               messagePopup.open()
            }
            Shortcut 
            {
               sequence: "Ctrl+O"
               onActivated:
               {
                  messagePopup.open()
               }
            }
         }

         Button
         {
            objectName: "openWindow"
            Layout.fillWidth: true
            text: "Open window"
            onClicked:
            {
               secondWindow.show()
            }
         }

         Button
         {
            id: openFileButton
            objectName: "openFile"
            Layout.fillWidth: true
            text: "Open File"
            onClicked:
            {
               fileDialog.open()
            }
         }
         
         Button
         {
            objectName: "quitButton"
            Layout.fillWidth: true
            text: "Quit"
            onClicked:
            {
               mainWindow.close()
            }
         }

         Text
         {
            id: textWithNoName
            text: "unnamed"
         }

         Text
         {
            text: "no_name_or_id"
         }

         CustomTextEditor
         {
            objectName: "customTextEditor"
            width: textWithNoName.width
            height: textWithNoName.height
         }

         Slider
         {
            objectName: "slider"
            from: 0
            to: 10
            stepSize: 1
            value: 5
         }

         GridLayout
         {
            objectName: "buttonGrid"
            columns: 5
            rows: 5
            Repeater
            {
               model: 25
               delegate: ColorButton
               {
                  objectName: "colorButton" + modelData
                  width: 50
                  height: width
               }
            }
         }

         ListView
         {
            id: listView
            objectName: "listView"
            Layout.fillWidth: true
            boundsBehavior: Flickable.StopAtBounds
            height: 50
            model: 50
            delegate: Rectangle
            {
               width: element.width
               height: element.height
               Text
               {
                  id: element
                  text: "item " + modelData
                  color: listView.currentIndex == index ? "red" : "black"
               }
               MouseArea 
               {
                  anchors.fill: parent
                  onClicked: listView.currentIndex = index
                  preventStealing: true
               }
            }

            ScrollBar.vertical: ScrollBar
            {
               objectName: "listScrollBar"
            }
         }

         TextEdit
         {
            objectName: "editableText"
            text: ""
         }
         TextEdit
         {
            objectName: "editableText2"
            text: ""
         }

         ComboBox
         {
            id: customCombo
            objectName: "customCombo"
            Layout.fillWidth: true
            height: 54
            property bool firstClick: true

            MouseArea
            {
               anchors.fill: parent
               z: 50
               onPressed: function(mouse)
               {
                  if (customCombo.firstClick)
                  {
                     customCombo.focus = false
                     customCombo.down = undefined
                  }
                  else
                  {
                     customCombo.down = customCombo.pressed ? false : undefined
                     customCombo.focus = !customCombo.focus
                     comboBoxPopup.visible = !comboBoxPopup.visible
                  }
                  customCombo.firstClick = !customCombo.firstClick
               }
            }

            popup: Popup
            {
               id: comboBoxPopup
               width: customCombo.width
               height: 100
               contentItem: ListView
               {
                  id: comboListView
                  objectName: "comboListView"
                  boundsBehavior: Flickable.StopAtBounds
                  interactive: true
                  height: 100
                  width: customCombo.width
                  model: 10
                  delegate: Rectangle
                  {
                     width: element.width
                     height: element.height
                     Text
                     {
                        id: element
                        text: "item " + modelData
                        color: comboListView.currentIndex == index ? "red" : "black"
                     }
                     MouseArea 
                     {
                        anchors.fill: parent
                        onClicked: comboListView.currentIndex = index
                        preventStealing: true
                     }
                  }
               }
            }
         }
      }
      ColumnLayout
      {
         objectName: "middleColumn"
         Layout.fillWidth: true
         Layout.fillHeight: true
         Item
         {
            width: 300
            height: 50

            Label
            {
               id: hoverLabel
               objectName: "hoverLabel"
               anchors.fill: parent
               horizontalAlignment: Text.AlignHCenter
               verticalAlignment: Text.AlignVCenter
            }

            MouseArea
            {
               anchors.fill: parent
               hoverEnabled: true
               onEntered: hoverLabel.text = mouseX + " X " + mouseY
               onPositionChanged: hoverLabel.text = mouseX + " X " + mouseY
               onExited: hoverLabel.text = "-1 X -1"
            }
         }
         
         Item
         {
            id: gestureItem
            width: 300
            height:300
            Rectangle
            {
               id: borderRectangle
               objectName: "gestureItem"
               color: "green"
               width: 100
               height: 100

               Rectangle
               {
                  color: "white"
                  width: 60
                  height: 60
                  anchors.centerIn: parent
               }
            }

            PinchArea
            {
               objectName: "pinchArea"
               anchors.fill: gestureItem
               pinch.target: borderRectangle
               pinch.minimumRotation: -180
               pinch.maximumRotation: 180
               pinch.minimumScale: 0.1
               pinch.maximumScale: 10
               pinch.minimumX: 0
               pinch.maximumX: gestureItem.width
               pinch.minimumY: 0
               pinch.maximumY: gestureItem.height
               pinch.dragAxis: Pinch.XAndYAxis
               onPinchStarted: borderRectangle.color = "orange"
               onPinchFinished: borderRectangle.color = "green"
            }
         }
      }
      ColumnLayout
      {
         id: spacer // For testing purposes
         objectName: "rightColumn"
         Layout.fillWidth: true
         Layout.fillHeight: true
         spacing: 20

         // 3D scene
         Item
         {
            Layout.fillWidth: false
            Layout.fillHeight: false
            width: 500
            height:300
            MainScene
            {
               objectName: "main3dScene"
               anchors.fill: parent
            }
         }

         // Multi-touch area
         Item
         {
            objectName: "multiTouchArea"
            Layout.fillWidth: false
            Layout.fillHeight: false
            width: 500
            height:300
            MultiPointTouchArea
            {
               anchors.fill: parent
               minimumTouchPoints: 1
               maximumTouchPoints: 3
               touchPoints: [
                  TouchPoint 
                  {
                     id: point1
                     objectName: "point1"
                  },
                  TouchPoint 
                  {
                     id: point2
                  },
                  TouchPoint 
                  {
                     id: point3
                  }
               ]
            }

            Rectangle
            {
               objectName: "touchRectangle1"
               color: point1.pressed ? "blue" : "green"
               width: 30
               height: 30
               x: point1.x - width / 2
               y: point1.y - height / 2
            }

            Rectangle
            {
               objectName: "touchRectangle2"
               color: point2.pressed ? "yellow" : "red"
               width: 30
               height: 30
               x: point2.x - width / 2
               y: point2.y - height / 2
            }

            Rectangle
            {
               objectName: "touchRectangle3"
               color: point3.pressed ? "black" : "white"
               width: 30
               height: 30
               x: point3.x - width / 2
               y: point3.y - height / 2
            }
         }
      }
      ColumnLayout
      {

      }
   }
}