# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to method calls
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_list_methods():
    """
    Verify that the list of existing methods can be accessed for any object
    """
    # Widget
    button = qat.wait_for_object('counterButton')
    for method_type, prototype, return_type in button.list_methods():
        assert method_type in ['', 'signal', 'slot']
        assert str(prototype).endswith(')')
        assert len(return_type) > 0

    main_window_def = {
        'objectName': 'mainWindow',
        'type': 'ApplicationWindow'
    }

    # Custom C++ object
    main_window = qat.wait_for_object(main_window_def)
    proxy = main_window.mainProxy

    methods = proxy.list_methods()
    for method_type, prototype, return_type in methods:
        assert method_type in ['', 'signal', 'slot']
        assert str(prototype).endswith(')')
        assert len(return_type) > 0

    assert methods.count(('', 'Add(double a, double b)', 'double')) == 1
    assert methods.count(('slot', 'deleteLater()', 'void')) == 1
    assert methods.count(('signal', 'destroyed()', 'void')) == 1


def test_call_method():
    """
    Verify that C++ methods can be called from the API
    """

    main_window_def = {
        'objectName': 'mainWindow',
        'type': 'ApplicationWindow'
    }

    main_window = qat.wait_for_object(main_window_def)
    proxy = main_window.mainProxy

    # Call first overload
    assert 3 == proxy.Add(1.5, 1.5)

    # Call second overload
    assert 4 == proxy.Add(1.0, 1.5, 1.5)

    # Conversions
    assert -7 == proxy.Add(-2, -5)
    assert 16.9 == proxy.Add(2.0, 5, 9.9)
    assert 9 == proxy.Add("4", "5")

    # Invalid arguments
    with pytest.raises(Exception):
        proxy.Add()
    with pytest.raises(Exception):
        proxy.Add(2)
    with pytest.raises(Exception):
        proxy.Add(2, 5, 9, 45, 5, 0)
    with pytest.raises(Exception):
        proxy.Add(2, [3, 5, 4])

    # Call function with complex types
    color_dict = {"QVariantTypeName": 'QColor', 'name': 'red'}
    color_object = qat.QtCustomObject(color_dict)
    proxy.SetColor(color_object)
    color = proxy.GetColor()
    assert color.name == '#ffff0000'
    assert color.red == 255
    assert color.green == 0
    assert color.blue == 0

    # Dict should be automatically converted to QtCustomObject
    proxy.SetColor({"QVariantTypeName": 'QColor', 'name': 'blue'})
    color = proxy.GetColor()
    assert color.name == '#ff0000ff'
    assert color.red == 0
    assert color.green == 0
    assert color.blue == 255

    # Call Getter
    sub_proxy = proxy.GetSubProxy()
    assert sub_proxy is not None
    assert not sub_proxy.is_null()
    assert sub_proxy.value == 0


def test_qstringlist():
    """
    Verify that remote methods can handle QStringList arguments
    """

    main_window_def = {
        'objectName': 'mainWindow',
        'type': 'ApplicationWindow'
    }

    main_window = qat.wait_for_object(main_window_def)

    proxy = main_window.mainProxy

    string_list = proxy.stringList

    assert len(string_list) == 0

    # Set a new list
    proxy.stringList = ['a', 'b', 'c']

    # Verify that the list was updated
    string_list = proxy.stringList
    assert len(string_list) == 3
    assert string_list == ['a', 'b', 'c']

    # Append value to the list
    proxy.stringList += 'd'
    string_list = proxy.stringList
    assert string_list == ['a', 'b', 'c', 'd']

    # Error management (wrong type)
    with pytest.raises(Exception):
        proxy.stringList = True
