# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to bindings
"""

import threading
import time
import pytest

import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_connection(app_name):
    """
    Verify that we can connect to a remote property
    """

    button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    counter_button = qat.wait_for_object(button_def)
    assert counter_button.text == "0"

    # Bind a remote property to a local variable
    current_value = ''
    sync = threading.Condition()

    def update(new_value):
        nonlocal current_value
        current_value = new_value
        nonlocal sync
        with sync:
            sync.notify_all()

    property_name = 'customText' if app_name == 'WidgetApp' else 'text'
    conn_id = qat.connect(counter_button, property_name, callback=update)

    qat.mouse_click(counter_button)
    assert counter_button.text == "1"
    with sync:
        sync.wait(3.0)
    assert current_value == "1"

    # Once disconnected, the callback is not updated
    assert qat.disconnect(conn_id)
    qat.mouse_click(counter_button)
    assert counter_button.text == "2"
    # Make sure the value is not updated in the next few seconds
    time.sleep(3)
    assert current_value == "1"

    # Signal already disconnected (not an error, but API will return False)
    assert not qat.disconnect(conn_id)


def test_multiple_connection():
    """
    Verify that we can connect to multiple remote properties
    """

    button_def = {
        'objectName': 'colorButton1',
        'type': 'ColorButton'
    }
    button1 = qat.wait_for_object(button_def)

    button_def = {
        'objectName': 'colorButton2',
        'type': 'ColorButton'
    }
    button2 = qat.wait_for_object(button_def)

    color1 = None
    color2 = None
    call_count1 = 0
    call_count2 = 0

    def update1(new_value):
        nonlocal call_count1
        call_count1 += 1
        nonlocal color1
        color1 = new_value

    def update2(new_value):
        nonlocal call_count2
        call_count2 += 1
        nonlocal color2
        color2 = new_value

    qat.connect(button1, 'color', callback=update1)

    # Double-connection should not change the result
    qat.connect(button2, 'color', callback=update2)
    qat.connect(button2, 'color', callback=update2)

    qat.mouse_click(button1)
    time.sleep(1)
    assert color1.name == '#ff008000'  # green
    assert color2 is None

    qat.mouse_click(button2, button=qat.Button.RIGHT)
    time.sleep(1)
    assert color1.name == '#ff008000'  # green
    assert color2.name == '#ffff0000'  # red

    assert call_count1 == 1
    assert call_count2 == 2


def test_binding(app_name):
    """
    Verify that we can bind a remote property to a local object
    """
    button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }
    counter_button = qat.wait_for_object(button_def)

    class Receiver():
        """
        A basic receiver object holding a single value
        """
        def __init__(self):
            self.value = ''

    # Create a binding between the text of the button and the receiver's value
    receiver = Receiver()
    property_name = 'customText' if app_name == 'WidgetApp' else 'text'
    binding = qat.bind(counter_button, property_name, receiver, 'value')
    # Make sure the current value is initialized properly
    assert receiver.value == '0'

    # Update local value manually
    binding('8')
    assert receiver.value == '8'

    # Invalid call should be ignored
    binding('3', '5')
    assert receiver.value == '8'

    # Click on the button to update its text
    qat.mouse_click(counter_button)
    assert counter_button.text == '1'

    # Make sure the receiver is updated accordingly
    qat.wait_for(lambda: receiver.value == '1')
    assert receiver.value == '1'

    # Break the binding
    binding.disconnect()
    qat.mouse_click(counter_button)
    assert counter_button.text == '2'

    # Make sure the receiver is not updated anymore
    time.sleep(3)
    assert receiver.value == '1'

    # Disconnect has no effect when called multiple times
    assert binding.disconnect()

    # Re-establish the connection
    binding.connect()
    # Make sure the current value is re-initialized properly
    assert receiver.value == '2'

    # Click on the button to update its text
    qat.mouse_click(counter_button)
    assert counter_button.text == '3'

    # Make sure the receiver is updated accordingly
    qat.wait_for(lambda: receiver.value == '3')
    assert receiver.value == '3'

    # Destroy binding
    binding.__del__()

    # Click on the button to update its text
    qat.mouse_click(counter_button)
    assert counter_button.text == '4'

    # Make sure the receiver is not updated anymore
    time.sleep(3)
    assert receiver.value == '3'


def test_object_connection():
    """
    Verify that we can connect to remote property of QObject type
    """
    main_window_def = {
        'type': 'ApplicationWindow'
    }

    main_window = qat.wait_for_object(main_window_def)
    main_proxy = main_window.mainProxy
    sub_proxy = main_proxy.subProxy
    assert sub_proxy is not None
    assert sub_proxy.value == 0

    # Bind the remote value to a local variable
    current_proxy = None
    sync = threading.Condition()

    def update(new_proxy):
        nonlocal current_proxy
        current_proxy = new_proxy
        nonlocal sync
        with sync:
            sync.notify_all()

    conn_id = qat.connect(main_proxy, 'subProxy', callback=update)
    main_proxy.ChangeSubProxy()
    main_proxy.subProxy.Increment()
    main_proxy.subProxy.Increment()

    with sync:
        sync.wait(1.0)
    assert current_proxy is not None
    assert current_proxy.value == 2

    qat.disconnect(conn_id)
