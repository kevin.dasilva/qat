# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to screenshots
"""

from pathlib import Path
import os
import shutil
import time

import pytest
from helpers import round_float_to_int
from qat import report
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_screenshot(app_name):
    """
    Verify that we can take screenshot of the whole application
    """
    object_def = {}
    object_def['objectName'] = 'counterButton'
    object_def['type'] = 'Button'

    screenshot_dir = os.getcwd() + f"/screenshots/{app_name}/test_screenshot"
    if os.path.exists(screenshot_dir):
        shutil.rmtree(screenshot_dir)

    # Make sure the main window is fully initialized and displayed
    time.sleep(2)

    qat.take_screenshot(screenshot_dir)

    assert os.path.exists(screenshot_dir)
    files = os.listdir(screenshot_dir)
    assert len(files) == 1


def test_grab_object(app_name):
    """
    Verify that we can take screenshot of a single widget
    """
    color_button_def = {
        'objectName': 'colorButton8',
        'type': 'ColorButton'
    }

    screenshot_dir = os.getcwd() + f"/screenshots/{app_name}/test_grab_object/"
    if os.path.exists(screenshot_dir):
        shutil.rmtree(screenshot_dir)

    os.makedirs(screenshot_dir)

    file_path = Path(screenshot_dir) / 'test_grab.png'
    if os.path.exists(file_path):
        os.remove(file_path)

    button = qat.wait_for_object(color_button_def)
    # Set color to RGB(80, 149, 241)
    button.color = {'name': '#FF5095F1', 'QVariantTypeName': 'QColor'}

    image = qat.grab_screenshot(button)
    image.save(str(file_path))

    # Screenshot should have the same size as the widget
    assert image.width == round_float_to_int(button.width * button.pixelRatio)
    assert image.height == round_float_to_int(button.height * button.pixelRatio)

    pixel = image.getPixelRGBA(10, 10)
    assert pixel.red == 80
    assert pixel.green == 149
    assert pixel.blue == 241
    assert pixel.alpha == 255

    assert pixel[0] == 80
    assert pixel[1] == 149
    assert pixel[2] == 241
    assert pixel[3] == 255

    pixel = image.getPixel(10, 10)
    assert pixel == 0xFF5095F1

    # Close the app and re-open it
    qat.close_application(qat.current_application_context())
    qat.start_application(app_name, "")
    # Button should have its initial color
    assert button.color.name == '#ffffffff'

    # But image should still be available (auto-reload)
    pixel = image.getPixel(10, 10)
    assert pixel == 0xFF5095F1

    # Save screenshot to file
    file_path = Path(screenshot_dir) / 'test_image.png'
    if os.path.exists(file_path):
        os.remove(file_path)

    image.save(str(file_path))
    assert os.path.exists(file_path)

    # Test maximum image cache size
    button_grid_def = {
        'objectName': 'buttonGrid'
    }

    object_def = {}
    object_def['container'] = button_grid_def
    object_def['objectName'] = 'colorButton'
    object_def['type'] = 'ColorButton'

    for i in range(4):
        object_def['objectName'] = f'colorButton{i}'
        color_button = qat.wait_for_object(object_def)
        button_image = qat.grab_screenshot(color_button)
        assert button_image.getPixel(5, 5) == 0xffffffff  # white

        # Default (=Left) click
        qat.mouse_click(color_button)
        button_image = qat.grab_screenshot(color_button)
        assert button_image.getPixel(5, 5) == 0xff008000  # green

        # Right click
        qat.mouse_click(color_button, button=qat.Button.RIGHT)
        button_image = qat.grab_screenshot(color_button)
        assert button_image.getPixel(5, 5) == 0xffff0000  # red

    # At this point the first image should have been destroyed
    # then automatically reloaded
    original_uid = image.get_definition()["cache_uid"]
    pixel = image.getPixel(10, 10)
    image = qat.wait_for_object_exists(image)
    assert image.get_definition()["cache_uid"] != original_uid
    assert pixel == 0xFF5095F1

    # Change the color of the button again
    button.color = qat.QtCustomObject({'name': '#FF5095F1', 'QVariantTypeName': 'QColor'})
    # Take a second screenshot and compare results
    image2 = qat.grab_screenshot(color_button_def, delay=500)
    file_path = Path(screenshot_dir) / 'test_image2.png'
    if os.path.exists(file_path):
        os.remove(file_path)
    image2.save(str(file_path))
    assert os.path.exists(file_path)
    assert image.equals(image2)

    # Copy image to the report
    report_filename = Path(os.getcwd()) / 'report' / app_name / 'test_grab_object_results.xml'
    report.start_report("test", report_filename)
    try:
        copy_path = report.attach_image(image2, "test_image", "Attaching image to report")
    finally:
        report.stop_report()
    assert os.path.exists(copy_path)
    
