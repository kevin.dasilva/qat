# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to native Windows widgets
"""

from pathlib import Path
import os

try:
    import mouse as native_mouse
except OSError:
    print('Could not import mouse module. Native test will be disabled.')
import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    if not qat.app_launcher.is_windows():
        pytest.skip(reason='Native Windows widgets are supported on Windows only')

    test_file = Path.home() / '.AAA' / 'test.txt'
    os.makedirs(test_file.parent, exist_ok=True)
    with open(test_file, 'w', encoding='utf-8') as file:
        file.write('test')

    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()
    if os.path.exists(test_file):
        os.remove(test_file)
        os.rmdir(test_file.parent)


@pytest.mark.serial
def test_find_widgets(app_name):
    """
    Verify that native widgets can be accessed
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'Dialog',
        'text': 'Open'
    }

    # Open the File dialog
    num_windows_before = len(qat.list_top_windows())
    qat.mouse_click(open_file_btn)
    dialog = qat.wait_for_object_exists(open_file_dlg)
    assert dialog.visible
    if app_name == 'QmlApp':
        assert len(qat.list_top_windows()) == num_windows_before + 1
    elif app_name == 'WidgetApp':
        # QFileDialog + native Dialog
        assert len(qat.list_top_windows()) == num_windows_before + 2

    # Look for the Cancel button
    cancel_btn = {
        'container': open_file_dlg,
        'text': 'Cancel',
        'type': 'Button'
    }
    assert qat.wait_for_object(cancel_btn) is not None

    # Force close the dialog
    assert dialog.close()
    qat.wait_for_object_missing(open_file_dlg, timeout=500)


@pytest.mark.serial
def test_click():
    """
    Verify that we can click on native widgets
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'Dialog',
        'text': 'Open'
    }

    # Open the File dialog
    qat.mouse_click(open_file_btn)
    dialog = qat.wait_for_object(open_file_dlg)

    # Look for the Cancel button
    cancel_btn = {
        'container': dialog,
        'text': 'Cancel',
        'type': 'Button'
    }

    button = qat.wait_for_object(cancel_btn)

    # Click outside button's boundaries should fail
    with pytest.raises(RuntimeError):
        qat.mouse_click(cancel_btn, x=-15, y=-15, check=True)
    with pytest.raises(RuntimeError):
        qat.mouse_click(cancel_btn, x=800, y=800, check=True)
    with pytest.raises(Exception):
        qat.mouse_click(button, x=button.width + 2, y=button.height + 2)

    # Right-click should have no effect
    qat.mouse_click(cancel_btn, button=qat.Button.RIGHT, check=True)
    assert dialog.visible

    # Click on Cancel to close the dialog
    qat.mouse_click(cancel_btn, check=True)
    # Dialog should have been destroyed
    qat.wait_for_object_missing(open_file_dlg)

    # Re-open the File dialog
    qat.mouse_click(open_file_btn)
    dialog = qat.wait_for_object(open_file_dlg)

    # Click on specific coordinates
    cancel_btn = qat.wait_for_object(cancel_btn)
    qat.mouse_click(cancel_btn, x=cancel_btn.width - 2, y=cancel_btn.height - 2, check=True)
    qat.wait_for_object_missing(open_file_dlg)


@pytest.mark.serial
def test_double_click():
    """
    Verify that we can double-click on native widgets
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'Dialog',
        'text': 'Open'
    }

    # Open the File dialog
    qat.mouse_click(open_file_btn)
    dialog = qat.wait_for_object(open_file_dlg)

    # Look for the File list
    file_list_def = {
        'container': dialog,
        'type': 'SHELLDLL_DefView'
    }
    file_list = qat.wait_for_object(file_list_def)
    handle = file_list.handle
    # Double-click on the test folder (first in list) to open it
    qat.double_click(file_list, x=50, y=50, check=True)

    # Opening a folder changes the underlying shell object
    qat.wait_for_property_change(file_list_def, 'handle', handle, check=True)

    # Double-click on the test file (first in list) to select it
    qat.double_click(file_list, x=50, y=50, check=True)

    # Selected file should be displayed on the button
    qat.wait_for_property_value(
        open_file_btn,
        'text',
        '/.AAA/test.txt',
        comparator=lambda x,y: x.endswith(y))

    # Dialog should be closed
    qat.wait_for_object_missing(open_file_dlg, timeout=500)


@pytest.mark.serial
def test_keyboard(app_name):
    """
    Verify that we can type in native widgets
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'Dialog',
        'text': 'Open'
    }

    # Open the File dialog
    qat.mouse_click(open_file_btn)
    dialog = qat.wait_for_object(open_file_dlg)
    # Both dialogs have the same basic definition
    # Use native handles to identify them
    open_file_dlg['handle'] = dialog.handle

    # Look for the file name input
    file_combobox = {
        'container': open_file_dlg,
        'text': '',
        'type': 'ComboBox'
    }

    file_input = {
        'container': file_combobox,
        'type': 'Edit'
    }

    text_box = qat.wait_for_object(file_input)
    assert text_box.text == ''

    # Edit file name
    qat.type_in(text_box, "Invalid_File.ext")
    assert text_box.text == 'Invalid_File.ext'

    # Try opening this file
    open_btn = {
        'container': open_file_dlg,
        'text': 'Open',
        'type': 'Button'
    }
    qat.mouse_click(open_btn, check=False)

    # An error dialog should be displayed
    error_dialog_def = {
        'type': 'Dialog',
        'text': 'Open',
        'enabled': True # Used to distinguish between the 2 dialogs
    }

    # Make sure a new dialog is open (with a different handle)
    qat.wait_for_property_change(error_dialog_def, 'handle', open_file_dlg['handle'], check=True)
    error_dialog = qat.wait_for_object(error_dialog_def)
    assert error_dialog.visible
    error_dialog_def['handle'] = error_dialog.handle

    # Close error dialog by clicking on OK
    ok_btn = {
        'container': error_dialog_def,
        'text': 'OK',
        'type': 'Button'
    }
    qat.mouse_click(ok_btn)
    assert qat.wait_for_property_value(open_file_dlg, 'enabled', True)
    with pytest.raises(LookupError):
        qat.wait_for_object(error_dialog_def, timeout=500)

    # todo: Investigate why this only works with QtQuick
    if app_name == 'QmlApp':
        # Close the dialog by hitting ESC
        qat.type_in(dialog, '<Escape>')
        with pytest.raises(LookupError):
            qat.wait_for_object(open_file_dlg, timeout=500)


@pytest.mark.serial
def test_shortcut():
    """
    Verify that we can use shortcuts in native widgets
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'Dialog',
        'text': 'Open'
    }

    # Open the File dialog
    qat.mouse_click(open_file_btn)
    assert qat.wait_for_property_value(open_file_dlg, 'visible', True)
    qat.shortcut(open_file_dlg, "Alt+F4")
    # Dialog should have been closed
    qat.wait_for_object_missing(open_file_dlg, timeout=500)


@pytest.mark.serial
def test_screenshot():
    """
    Verify that we can take screenshots of native widgets
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'Dialog',
        'text': 'Open'
    }

    # Open the File dialog
    qat.mouse_click(open_file_btn)
    dialog = qat.wait_for_object(open_file_dlg)

    # Look for the Cancel button
    cancel_btn = {
        'container': dialog,
        'text': 'Cancel',
        'type': 'Button'
    }

    # Full dialog
    screenshot = qat.grab_screenshot(dialog, delay = 200)
    screenshot.save('screenshots/dlg.png')
    assert screenshot.width == dialog.width
    assert screenshot.height == dialog.height

    # Single button
    screenshot = qat.grab_screenshot(cancel_btn)
    button = qat.wait_for_object(cancel_btn)
    assert screenshot.width == button.width
    assert screenshot.height == button.height
    screenshot.save('screenshots/btn.png')


@pytest.mark.serial
def test_picker():
    """
    Verify that we can pick native widgets
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'Dialog',
        'text': 'Open'
    }
    picker_def = {
        'parent': open_file_dlg,
        'objectName': 'QatObjectPicker'
    }

    # File name input
    file_combobox = {
        'container': open_file_dlg,
        'text': '',
        'type': 'ComboBox'
    }

    file_input = {
        'container': file_combobox,
        'type': 'Edit'
    }

    # Since this test uses native events, application must be unlocked
    qat.unlock_application()

    # Open the File dialog
    qat.mouse_click(open_file_btn)
    dialog = qat.wait_for_object(open_file_dlg)
    # Both dialogs have the same basic definition
    # Use native handles to identify them
    open_file_dlg['handle'] = dialog.handle

    # Get buttons
    cancel_btn = {
        'container': dialog,
        'text': 'Cancel',
        'type': 'Button'
    }
    cancel_btn = qat.wait_for_object(cancel_btn)

    open_btn = {
        'container': dialog,
        'text': 'Open',
        'type': 'Button'
    }
    open_btn = qat.wait_for_object(open_btn)

    qat.activate_picker()
    picker = qat.wait_for_object(picker_def)

    # Use the (native) mouse module to simulate a native click
    button_bounds = cancel_btn.globalBounds
    ratio = cancel_btn.pixelRatio
    x = (button_bounds.x / ratio) + (button_bounds.width / 2) / ratio
    y = (button_bounds.y / ratio) + (button_bounds.height / 2) / ratio
    native_mouse.move(x, y, absolute=True, duration=0)
    native_mouse.click('left')

    # Verify that the Cancel button was picked
    assert qat.wait_for_property_value(picker, 'hasNewObject', True)
    picked_object = picker.pickedObject
    assert not picked_object.is_null()
    assert picked_object.type == 'Button'
    assert picked_object.text == 'Cancel'
    assert dialog.visible

    # Deactivate the picker
    qat.deactivate_picker()

    # Edit file name
    text_box = qat.wait_for_object(file_input)
    assert text_box.text == ''
    qat.type_in(text_box, "Invalid_File.ext")
    qat.mouse_click(open_btn)
    # An error dialog should be displayed
    error_dialog_def = {
        'type': 'Dialog',
        'text': 'Open',
        'enabled': True # Used to distinguish between the 2 dialogs
    }

    # Make sure a new dialog is open (with a different handle)
    qat.wait_for_property_change(error_dialog_def, 'handle', open_file_dlg['handle'], check=True)
    error_dialog = qat.wait_for_object(error_dialog_def)
    assert error_dialog.visible
    error_dialog_def['handle'] = error_dialog.handle

    # OK button on the error dialog
    ok_btn = {
        'container': error_dialog_def,
        'text': 'OK',
        'type': 'Button'
    }
    ok_btn = qat.wait_for_object(ok_btn)

    # Re-activate the pickers
    qat.activate_picker()

    # Use the (native) mouse module to simulate a native click
    button_bounds = ok_btn.globalBounds
    ratio = ok_btn.pixelRatio
    x = (button_bounds.x / ratio) + (button_bounds.width / 2) / ratio
    y = (button_bounds.y / ratio) + (button_bounds.height / 2) / ratio
    native_mouse.move(x, y, absolute=True, duration=0)
    native_mouse.click('left')

    # Each dialog has its own picker
    picker_def['parent'] = error_dialog
    picker = qat.wait_for_object(picker_def)
    # Verify that the OK button was picked
    assert qat.wait_for_property_value(picker, 'hasNewObject', True)
    picked_object = picker.pickedObject
    assert not picked_object.is_null()
    assert picked_object.type == 'Button'
    assert picked_object.text == 'OK'

    # Deactivate the pickers
    qat.deactivate_picker()

    # Close the error dialog
    native_mouse.click('left')
    qat.wait_for_object_missing(error_dialog_def)
