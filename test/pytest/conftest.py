# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Pytest configuration
"""

from pytest import fixture

def pytest_addoption(parser):
    """
    Handle custom arguments
    """
    parser.addoption(
        '--app_name',
        action='store',
        default='QmlApp,WidgetApp'
    )
    parser.addoption(
        '--qt_version',
        action='store',
        default=''
    )


@fixture()
def app_name(request):
    """
    Return the 'app_name' argument
    """
    return request.config.getoption('--app_name')


@fixture()
def qt_version(request):
    """
    Return the 'qt_version' argument
    """
    return request.config.getoption('--qt_version')


def pytest_configure(config):
    """
    Register additional markers
    """
    config.addinivalue_line(
        "markers", "serial: mark test that cannot be run in parallel"
    )


def pytest_generate_tests(metafunc):
    """
    Generate all tests
    """
    app_name_arg = metafunc.config.option.app_name
    if 'app_name' in metafunc.fixturenames and app_name_arg is not None:
        metafunc.parametrize('app_name', str(app_name_arg).split(','))
