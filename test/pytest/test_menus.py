# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to menus
"""

import time
import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_menu_bar(app_name):
    """
    Verify that we can click on menu toolbar
    """
    if app_name == 'QmlApp':
        qt_version = str(qat.current_application_context().qt_version)
        if str(qt_version).startswith('6.1.'):
            pytest.skip(reason=f"QML menus are not fully supported on Qt {qt_version}")
        if str(qt_version).startswith('5.') and qat.app_launcher.is_macos():
            pytest.skip(reason=f"QML menus are not fully supported on MacOS and Qt {qt_version}")

    if app_name == 'WidgetApp' and qat.app_launcher.is_macos():
        pytest.skip(reason='Menu bars are not supported on MacOS since the OS menu is used instead')

    open_window_btn = {
        'objectName': 'openWindow',
        'type': 'Button'
    }
    qat.mouse_click(open_window_btn)

    window_def = {
        'objectName': 'secondWindow'
    }
    qat.wait_for_object(window_def)

    close_btn_def = {
        'objectName': 'closeWindowButton',
        'type': 'Button'
    }

    close_btn = qat.wait_for_object(close_btn_def)

    menu_type = 'Menu' if app_name == 'QmlApp' else 'QMenu'
    menubar_type = 'MenuBar' if app_name == 'QmlApp' else 'QMenuBar'

    menu_bar_def = {
        'container': window_def,
        'type': menubar_type
    }

    widget_menu_item = {
        'container': menu_bar_def,
        'text': 'Widgets',
        'type': 'MenuBarItem' # Will be ignored in Widget apps
    }
    qat.mouse_click(widget_menu_item)

    # Open sub-menu
    widget_menu_def = {
        'container' : menu_bar_def,
        'type': menu_type,
        'objectName': 'widgetsMenu'
    }
    button_menu_def = {
        'container': widget_menu_def,
        'text': 'Close button',
        'type': 'MenuItem' # Will be ignored in Widget apps
    }
    button_menu = qat.wait_for_object(button_menu_def)
    qat.mouse_click(button_menu, check=True)

    # Menu items by text
    sub_menu = {
        'type': menu_type,
        'objectName': 'closeButtonMainMenu'
    }
    enabled_menu_def = {
        'container': sub_menu,
        'text': 'Enabled',
        'type': 'MenuItem' # Will be ignored in Widget apps
    }
    enabled_menu = qat.wait_for_object_exists(enabled_menu_def)

    visible_menu_def = {
        'container': sub_menu,
        'text': 'Visible',
        'type': 'MenuItem' # Will be ignored in Widget apps
    }

    assert enabled_menu.checked
    qat.mouse_click(enabled_menu_def, check=True)
    close_btn = qat.wait_for_object_exists(close_btn_def)
    assert close_btn.enabled is False

    # Menu item by objectName
    qat.mouse_click(widget_menu_item, check=True)
    qat.mouse_click(button_menu, check=True)
    assert not enabled_menu.checked
    visible_menu_def = {
        'container': sub_menu,
        'objectName': 'visibleAction'
    }
    qat.mouse_click(visible_menu_def, check=True)
    close_btn = qat.wait_for_object_exists(close_btn_def)
    assert close_btn.visible is False


@pytest.mark.serial
def test_context_menu(app_name):
    """
    Verify that we can click on context menus
    """
    if app_name == 'QmlApp':
        qt_version = str(qat.current_application_context().qt_version)
        if str(qt_version).startswith('6.1.'):
            pytest.skip(reason=f"QML menus are not fully supported on Qt {qt_version}")

    open_window_btn = {
        'objectName': 'openWindow',
        'type': 'Button'
    }
    qat.mouse_click(open_window_btn)

    window_def = {
        'objectName': 'secondWindow'
    }
    qat.wait_for_object(window_def)

    close_btn_def = {
        'objectName': 'closeWindowButton',
        'type': 'Button'
    }

    close_btn = qat.wait_for_object(close_btn_def)

    menu_type = 'Menu' if app_name == 'QmlApp' else 'QMenu'

    context_menu_def = {
        'objectName': 'contextMenu'
    }

    # Open the context menu by right-clicking on the window
    # Note: There is a delay between the window being opened
    # and the window being ready to accept right-click events
    for _ in range(10):
        try:
            qat.mouse_click(window_def, button=qat.Button.RIGHT)
            qat.wait_for_object(context_menu_def, timeout=500)
            break
        except LookupError:
            time.sleep(0.5)
            continue

    context_menu = qat.wait_for_object_exists(context_menu_def)
    assert context_menu.visible

    # Open sub-menu
    button_menu_def = {
        'container': context_menu_def,
        'text': 'Close button',
        'type': 'MenuItem' # Will be ignored in Widget apps
    }
    button_menu = qat.wait_for_object(button_menu_def)
    qat.mouse_click(button_menu, check=True)

    # Menu items by text
    sub_menu = {
        'type': menu_type,
        'objectName': 'closeButtonMenu'
    }
    enabled_menu_def = {
        'container': sub_menu,
        'text': 'Enabled',
        'type': 'MenuItem' # Will be ignored in Widget apps
    }
    enabled_menu = qat.wait_for_object_exists(enabled_menu_def)

    visible_menu_def = {
        'container': sub_menu,
        'text': 'Visible',
        'type': 'MenuItem' # Will be ignored in Widget apps
    }

    assert enabled_menu.checked
    qat.mouse_click(enabled_menu_def, check=True)
    close_btn = qat.wait_for_object_exists(close_btn_def)
    assert close_btn.enabled is False

    # Menu item by objectName
    qat.mouse_click(window_def, button=qat.Button.RIGHT)
    qat.mouse_click(button_menu, check=True)
    assert not enabled_menu.checked
    visible_menu_def = {
        'container': sub_menu,
        'objectName': 'visibleAction'
    }
    qat.mouse_click(visible_menu_def, check=True)
    close_btn = qat.wait_for_object_exists(close_btn_def)
    assert close_btn.visible is False
