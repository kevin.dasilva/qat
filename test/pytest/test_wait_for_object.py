# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to object identification
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_find_object_by_name():
    """
    Verify that we can identify objects by name
    """
    # Standard definition
    object_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    assert qat.wait_for_object_exists(object_def) is not None
    assert qat.wait_for_object(object_def) is not None
    assert str(qat.wait_for_object(object_def)) == 'counterButton'
    assert qat.wait_for_object(object_def) == object_def

    # Short notation (name only)
    object_name = 'counterButton'
    assert qat.wait_for_object_exists(object_name) is not None
    assert qat.wait_for_object(object_name) is not None
    assert str(qat.wait_for_object(object_name)) == 'counterButton'
    assert qat.wait_for_object(object_name) == object_def

    # Negative test: wrong definition
    object_def = {
        'objectName': 'none',
        'type': 'Button'
    }

    with pytest.raises(LookupError):
        qat.wait_for_object_exists(object_def, timeout=1000)


def test_find_object_by_properties(app_name):
    """
    Verify that we can identify objects by properties other than name
    """
    # Multiple matches
    object_def = {}
    object_def['type'] = 'Button'

    with pytest.raises(Exception):
        qat.wait_for_object_exists(object_def)

    # Invalid property value
    object_def['objectName'] = 'counterButton'
    object_def['type'] = 'Button'
    object_def['visible'] = False

    with pytest.raises(Exception):
        qat.wait_for_object_exists(object_def)

    if app_name == 'QmlApp':
        # Match by ID only
        object_def = {}
        object_def['id'] = 'textWithNoName'

        assert None != qat.wait_for_object_exists(object_def)
        assert str(qat.wait_for_object_exists(object_def)) == 'textWithNoName'

        # Match by ID and type
        object_def['type'] = 'Text'

        assert qat.wait_for_object_exists(object_def) is not None


def test_find_nested_definitions():
    """
    Verify that we can identify objects in an object hierarchy
    """
    window_def = {
        'objectName': 'mainWindow',
        'type': 'ApplicationWindow'
    }
    assert qat.wait_for_object_exists(window_def) is not None

    row_def = {
        'container': window_def,
        'objectName': "mainRowLayout"
    }
    assert qat.wait_for_object_exists(row_def) is not None

    left_column_def = {
        'container': row_def,
        'objectName': "leftColumn"
    }
    assert qat.wait_for_object_exists(left_column_def) is not None

    quit_button_def = {
        'container': left_column_def,
        'objectName': "quitButton"
    }
    assert qat.wait_for_object_exists(quit_button_def) is not None

    unnamed_text_def = {
        'container': left_column_def,
        'text': "no_name_or_id"
    }
    assert qat.wait_for_object_exists(unnamed_text_def) is not None


def test_find_nested_objects():
    """
    Verify that QtObjects can be used instead of definition dictionaries as containers
    """
    window_def = {
        'objectName': 'mainWindow',
        'type': 'ApplicationWindow'
    }
    window = qat.wait_for_object_exists(window_def)

    row_def = {
        'container': window,
        'objectName': "mainRowLayout"
    }
    row = qat.wait_for_object_exists(row_def)

    left_column_def = {
        'container': row,
        'objectName': "leftColumn"
    }
    left_column = qat.wait_for_object_exists(left_column_def)

    quit_button_def = {
        'container': left_column,
        'objectName': "quitButton"
    }
    assert qat.wait_for_object_exists(quit_button_def) is not None


def test_find_object_by_object():
    """
    Verify that returned QtObject can be used with wait_for_object* functions
    """
    object_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    button = qat.wait_for_object_exists(object_def)
    assert button.text == '0'

    assert qat.wait_for_object_exists(button) is not None
    assert qat.wait_for_object(button) is not None
    qat.mouse_click(qat.wait_for_object(button))
    assert button.text == '1'


def test_find_object_in_popup():
    """
    Verify that we can identify objects in a Popup window
    """
    button_def = {
        'objectName': 'openButton',
        'type': 'Button'
    }
    popup_def = {
        'objectName': 'messagePopup',
        'type': 'MessagePopup'
    }
    title_def = {
        'container': popup_def,
        'objectName': 'title'
    }
    message_def = {
        'container': popup_def,
        'objectName': 'message'
    }

    qat.mouse_click(button_def)
    popup = qat.wait_for_object(popup_def)
    assert popup.visible

    title = qat.wait_for_object(title_def)
    assert title.text == "Some title"

    message = qat.wait_for_object(message_def)
    assert message.text == "Some message"

    # Close popup
    try:
        qat.type_in(popup_def, "<Escape>")
        popup = qat.wait_for_object(popup_def, timeout=500)
    except: # pylint: disable=bare-except
        popup = None
    assert popup is None


def test_reconnect_object():
    """
    Verify that we can identify objects by name
    """
    object_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    # Get a local reference to the button
    button = qat.wait_for_object_exists(object_def)

    # Kill the application
    app_name = qat.current_application_context().get_name()
    qat.current_application_context().kill()

    # The button is not accessible anymore
    with pytest.raises(ConnectionAbortedError):
        qat.wait_for_object_exists(object_def)
    with pytest.raises(AttributeError):
        assert button.text == '0'

    # Restart the application
    qat.start_application(app_name)

    # The button should automatically be reconnected to the new instance
    assert button.text == '0'
    qat.mouse_click(button)
    assert button.text == '1'


def test_missing_object():
    """
    Verify that we can wait for objects to be deleted
    """
    object_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    with pytest.raises(TimeoutError):
        qat.wait_for_object_missing(object_def, 200)

    button = qat.wait_for_object_exists(object_def)
    button.deleteLater()

    qat.wait_for_object_missing(button)
    qat.wait_for_object_missing(object_def)

    with pytest.raises(LookupError):
        qat.wait_for_object_exists(object_def, 200)
