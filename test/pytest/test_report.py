# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to XML report
"""

from pathlib import Path

import xmlschema

from behave.model import Status

from qat import bdd_hooks as hooks
from qat import report
import qat

class ContextMock:
    """
    Mock for Behave context
    """
    class Config:
        """
        Empty class for Behave compatibility only
        """


    def __init__(self) -> None:
        self.config = self.Config()
        self.config.userdata = {}

class FeatureMock:
    """
    Mock for Behave Feature
    """
    def __init__(self, filename, name, description) -> None:
        self.filename = filename
        self.name = name
        self.description = [description]

class ScenarioMock:
    """
    Mock for Behave Scenario
    """
    def __init__(self, name, description) -> None:
        self.name = name
        self.description = [description]
        self._row = None


class ExampleMock:
    """
    Mock for Behave Example
    """
    def __init__(self, headings, values) -> None:
        self.headings = headings
        self.cells = values

class StepMock:
    """
    Mock for Behave Step
    """
    def __init__(self, name, text = None) -> None:
        self.name = name
        self.status = Status.passed
        self.exception = None
        self.exc_traceback = None
        if text is not None:
            self.text = text


def test_valid_xml_report(app_name):
    """
    Valid XML report format
    """
    file_name = Path(f'./report/{app_name}/test_xml_report.xml')

    xml_report = qat.XmlReport("suite_name", file_name)
    xml_report.start_test_case("case1")
    # Feature 1
    xml_report.start_feature("feature1", "First feature")
    xml_report.start_scenario("scenario1", "First scenario")
    xml_report.start_step("step1")
    xml_report.log("A simple log message")
    xml_report.end_step()
    xml_report.start_step("step2")
    xml_report.log("A custom message", "CUSTOM")
    xml_report.end_step()
    xml_report.end_scenario()

    xml_report.start_scenario("scenario2", "Second scenario")
    xml_report.start_step("step1")
    xml_report.passed("Step 1", "Verification1 = pass")
    xml_report.end_step()
    xml_report.start_step("step2")
    xml_report.passed("Step 2", "Verification1 = pass")
    xml_report.failed("Step 2", "Verification2 = fail")
    xml_report.end_step()
    xml_report.start_step("step3")
    xml_report.passed("Step 3", "Verification1 = fail")
    xml_report.failed("Step 3", "Verification2 = pass")
    xml_report.end_step()
    xml_report.end_scenario()
    xml_report.end_feature()

    # Feature 2 with scenario outline
    xml_report.start_feature("feature2", "Second feature")
    xml_report.start_scenario("scenario1", "First scenario")
    header = ['name1', 'name2']
    values = ['value 1.1', 'value 1.2']
    xml_report.start_example(header, values)
    xml_report.start_step("step1")
    xml_report.end_step()
    xml_report.start_step("step2")
    xml_report.end_step()
    xml_report.end_scenario()

    xml_report.start_scenario("scenario1", "First scenario")
    values = ['value 2.1', 'value 2.2']
    xml_report.start_example(header, values)
    xml_report.start_step("step1")
    xml_report.end_step()
    xml_report.start_step("step2")
    xml_report.end_step()
    xml_report.end_scenario()

    xml_report.end_feature()

    xml_report.end_test_case()
    del xml_report

    schema = xmlschema.XMLSchema('test/pytest/report.xsd')
    assert schema.is_valid(file_name)


def test_invalid_xml_report(app_name):
    """
    Invalid XML report format
    """

    file_name = Path(f'./report/{app_name}/test_invalid_report.xml')

    # No structure (steps only)
    xml_report = qat.XmlReport("suite_name", file_name)
    xml_report.start_test_case("case1")
    xml_report.start_step("step1")
    xml_report.end_step()
    xml_report.start_step("step2")
    xml_report.end_step()
    xml_report.start_step("step3")
    xml_report.end_step()
    xml_report.end_test_case()
    del xml_report

    schema = xmlschema.XMLSchema('test/pytest/report.xsd')
    assert not schema.is_valid(file_name)

    # Unclosed element
    xml_report = qat.XmlReport("suite_name", file_name)
    xml_report.start_test_case("case1")
    xml_report.start_feature("feature1", "First feature")
    xml_report.start_scenario("scenario1", "First scenario")
    xml_report.start_step("step1")
    xml_report.end_step()

    xml_report.start_scenario("scenario2", "Invalid nested scenario")
    xml_report.start_step("step1")
    xml_report.end_step()
    xml_report.end_step()
    xml_report.end_scenario()
    xml_report.end_feature()
    xml_report.end_test_case()
    del xml_report

    schema = xmlschema.XMLSchema('test/pytest/report.xsd')
    assert not schema.is_valid(file_name)


def test_bdd_hooks(app_name):
    """
    Verify that hooks used by Behave generate a valid report
    """
    file_name = Path(f'./report/{app_name}/test_bdd_hooks.xml')
    context = ContextMock()
    context.config.userdata['report_file'] = file_name
    hooks.before_all(context, "suite")

    # Feature 1
    hooks.before_feature(context, FeatureMock(name='feature1', description='first feature', filename='features/some_test.feature'))
    # Scenario 1
    hooks.before_scenario(context, ScenarioMock('scenario1', 'first scenario'))
    step = StepMock('step1')
    hooks.before_step(context, step)
    hooks.after_step(context, step)
    step = StepMock('step2', 'This is a step')
    hooks.before_step(context, step)
    hooks.after_step(context, step)
    step = StepMock('step3', 'This is another step')
    hooks.before_step(context, step)
    hooks.after_step(context, step)
    hooks.after_scenario(context)

    # Scenario Outline
    # pylint: disable = protected-access
    scenario = ScenarioMock('scenario2', 'scenario outline')
    scenario._row = ExampleMock(['header1', 'header2'], ['item1.1', 'item1.2'])
    hooks.before_scenario(context, scenario)
    step = StepMock('step1')
    hooks.before_step(context, step)
    hooks.after_step(context, step)
    hooks.after_scenario(context)

    scenario._row.cells = ['item2.1', 'item2.2']
    hooks.before_scenario(context, scenario)
    step = StepMock('step1')
    hooks.before_step(context, step)
    hooks.after_step(context, step)
    hooks.after_scenario(context)

    hooks.after_feature(context)

    # Feature 2
    hooks.before_feature(context, FeatureMock(name='feature2', description='second feature', filename='features/other_test.feature'))
    # Scenario 1
    hooks.before_scenario(context, ScenarioMock('scenario1', 'first scenario'))
    step = StepMock('step1')
    hooks.before_step(context, step)
    hooks.after_step(context, step)
    multiline_text = """
        Step line 1
        Step line 2
        Step line 3
    """
    step = StepMock('step2', multiline_text)
    hooks.before_step(context, step)
    hooks.after_step(context, step)
    hooks.after_scenario(context)

    # Scenario 2
    hooks.before_scenario(context, ScenarioMock('scenario2', 'second scenario'))
    step = StepMock('step1')
    hooks.before_step(context, step)
    report.passed("step1.1 passed")
    report.passed("step1.2 passed", "All verifications passed")
    hooks.after_step(context, step)

    # Explicit failure
    step = StepMock('step2')
    hooks.before_step(context, step)
    try:
        report.failed("step failed", "Some verification failed")
    except report.PropagatedException as error:
        step.status = Status.failed
        step.exception = error
    assert step.status is Status.failed
    hooks.after_step(context, step)
    # Script failure
    step = StepMock('step2')
    hooks.before_step(context, step)
    step.status = Status.failed
    try:
        raise ValueError("Invalid value")
    except ValueError as error:
        step.exception = error
        step.exc_traceback = error.__traceback__
    assert step.status is Status.failed
    hooks.after_step(context, step)

    hooks.after_scenario(context)

    hooks.after_feature(context)
    hooks.after_all(context)

    schema = xmlschema.XMLSchema('test/pytest/report.xsd')
    assert schema.is_valid(file_name)


def test_bdd_default_report():
    """
    Verify that hooks used by Behave generate a valid default report
    """
    default_report_path = Path('./report/results.xml')
    context = ContextMock()
    hooks.before_all(context, "suite")

    # Feature 1
    hooks.before_feature(context, FeatureMock(name='feature1', description='first feature', filename='features/some_test.feature'))
    # Scenario 1
    hooks.before_scenario(context, ScenarioMock('scenario1', 'first scenario'))
    step = StepMock('step1')
    hooks.before_step(context, step)
    hooks.after_step(context, step)
    step = StepMock('step2', 'This is a step')
    hooks.before_step(context, step)
    # Successful verification
    report.verify(
        True,
        "Should pass"
    )

    hooks.after_step(context, step)
    step = StepMock('step3', 'This is another step')
    hooks.before_step(context, step)
    # Failed verification
    try:
        report.verify(
            False,
            "Should fail"
        )
    except report.PropagatedException as error:
        step.status = Status.failed
        step.exception = error
    assert step.status is Status.failed
    hooks.after_step(context, step)
    hooks.after_scenario(context)
    hooks.after_feature(context)
    hooks.after_all(context)

    schema = xmlschema.XMLSchema('test/pytest/report.xsd')
    assert schema.is_valid(default_report_path)
