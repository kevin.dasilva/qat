# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to native Cocoa widgets
"""

from pathlib import Path
import os
import pytest
import qat


@pytest.fixture(autouse=True)
def teardown():
    """
    Clean up on exit
    """
    if not qat.app_launcher.is_macos():
        pytest.skip(reason='Native Cocoa widgets are supported on MacOS only')

    test_file = Path.home() / '.AAA' / 'test.txt'
    os.makedirs(test_file.parent, exist_ok=True)
    with open(test_file, 'w', encoding='utf-8') as file:
        file.write('test')

    context = qat.start_application('WidgetApp', "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()
    if os.path.exists(test_file):
        os.remove(test_file)
        os.rmdir(test_file.parent)


@pytest.mark.serial
def test_find_widgets():
    """
    Verify that native widgets can be accessed
    """

    # Open Cocoa Window
    num_windows_before = len(qat.list_top_windows())
    main_window = qat.wait_for_object('mainWindow')
    proxy = main_window.mainProxy
    proxy.OpenCocoaWindow()
    # +2 because the native window is embedded in a Qt window
    assert len(qat.list_top_windows()) == num_windows_before + 2

    # Find window by name
    window_def = {
        'text': 'Cocoa window',
        'type': 'QNSWindow'
    }
    window = qat.wait_for_object(window_def)
    assert not window.is_null()

    # Window has 2 widgets
    assert len(window.children) == 2

    # Find other widgets
    button_def = {
        'container': window,
        'text': '0',
        'type': 'CounterButton'
    }
    button = qat.wait_for_object(button_def)
    assert not button.is_null()

    textfield_def = {
        'container': window,
        'type': 'NSTextField'
    }
    textfield = qat.wait_for_object(textfield_def)
    assert not textfield.is_null()

    # Force close the window
    assert window.close()
    qat.wait_for_object_missing(window, timeout=500)


@pytest.mark.serial
def test_click():
    """
    Verify that we can click on native widgets
    """
    # Open native window
    main_window = qat.wait_for_object('mainWindow')
    proxy = main_window.mainProxy
    proxy.OpenCocoaWindow()

    window_def = {
        'text': 'Cocoa window',
        'type': 'QNSWindow'
    }
    window = qat.wait_for_object(window_def)

    # Click on counter button
    button_def = {
        'container': window,
        'type': 'CounterButton'
    }
    button = qat.wait_for_object(button_def)
    assert button.text == '0'
    qat.mouse_click(button, check=True)
    assert button.text == '1'

    # Click outside button's boundaries should fail
    with pytest.raises(RuntimeError):
        qat.mouse_click(button, x=-15, y=-15, check=True)
    with pytest.raises(RuntimeError):
        qat.mouse_click(button, x=800, y=800, check=True)
    with pytest.raises(Exception):
        qat.mouse_click(button, x=button.width + 2, y=button.height + 2)

    # Right-click should have no effect
    qat.mouse_click(button, button=qat.Button.RIGHT, check=True)
    assert button.text == '1'

    # Click at specific coordinates
    qat.mouse_click(button, x=button.width - 10, y=button.height / 2, check=True)
    assert button.text == '2'

    # Double-click should reset the counter
    qat.double_click(button, check=True)
    assert button.text == '0'


@pytest.mark.serial
def test_keyboard():
    """
    Verify that we can type in native widgets
    """

    # Open Cocoa Window
    main_window = qat.wait_for_object('mainWindow')
    proxy = main_window.mainProxy
    proxy.OpenCocoaWindow()

    window_def = {
        'text': 'Cocoa window',
        'type': 'QNSWindow'
    }
    textfield_def = {
        'container': window_def,
        'type': 'NSTextField'
    }
    textfield = qat.wait_for_object(textfield_def)
    assert not textfield.is_null()

    qat.type_in(textfield, "Hello<Enter>")
    qat.wait_for_property_value(textfield, 'text', "Hello", check=True)

    qat.type_in(textfield, "Bye")
    qat.wait_for_property_value(textfield, 'text', "HelloBye", check=True)

    qat.type_in(textfield, "<Backspace><Backspace><Backspace>")
    qat.wait_for_property_value(textfield, 'text', "Hello", check=True)


@pytest.mark.serial
def test_dialog():
    """
    Verify that we can use native dialogs (panels)
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'NSOpenPanel',
        'text': 'Open'
    }

    # Open the Open File dialog
    qat.mouse_click(open_file_btn)
    dialog = qat.wait_for_object(open_file_dlg)
    assert not dialog.is_null()


@pytest.mark.serial
def test_screenshot():
    """
    Verify that we can take screenshots of native widgets
    """
    # Open native window
    main_window = qat.wait_for_object('mainWindow')
    proxy = main_window.mainProxy
    proxy.OpenCocoaWindow()

    window_def = {
        'text': 'Cocoa window',
        'type': 'QNSWindow'
    }
    window = qat.wait_for_object(window_def)

    # Click on counter button
    button_def = {
        'container': window,
        'type': 'CounterButton'
    }

    # Full window
    screenshot = qat.grab_screenshot(window, delay = 200)
    screenshot.save('screenshots/window.png')
    assert screenshot.width == window.width
    assert screenshot.height == window.height

    # Single button
    screenshot = qat.grab_screenshot(button_def)
    button = qat.wait_for_object(button_def)
    assert screenshot.width == button.width
    assert screenshot.height == button.height
    screenshot.save('screenshots/btn.png')


@pytest.mark.serial
def test_picker():
    """
    Verify that we can pick a Cocoa widget.
    Note: this test is not complete due to the lack of tool to simulate a native event on MacOS.
    """
    # Open native window
    main_window = qat.wait_for_object('mainWindow')
    proxy = main_window.mainProxy
    proxy.OpenCocoaWindow()

    native_window_def = {
        'text': 'Cocoa window',
        'type': 'QNSWindow'
    }
    native_window = qat.wait_for_object(native_window_def)

    # Click on counter button
    button_def = {
        'container': native_window,
        'type': 'CounterButton'
    }
    picker_def = {
        'parent': native_window,
        'objectName': 'QatObjectPicker'
    }

    qat.activate_picker()
    picker = qat.wait_for_object_exists(picker_def)

    button = qat.wait_for_object(button_def)
    
    button_bounds = button.globalBounds
    ratio = button.pixelRatio
    x = (button_bounds.x / ratio) + (button_bounds.width / 2) / ratio
    # Click near the bottom border to select the button itself and not one of its children
    y = (button_bounds.y / ratio) + (button_bounds.height - 3) / ratio
    assert picker.ClickAt(x, y)

    # Click should have been ignored by the button
    assert button.text == "0"

    # Picked object should be the button
    picker = qat.wait_for_object_exists(picker_def)
    picked_object = picker.pickedObject
    assert picked_object is not None
    assert not picked_object.is_null()
    assert picked_object.id == button.id

    # Verify that objects cannot be picked when Picker is de-activated
    qat.deactivate_picker()
    assert not picker.ClickAt(x, y)
    qat.mouse_click(button)
    picked_object = picker.pickedObject
    assert picked_object.is_null()

    # The click should be handled by the button
    assert button.text == "1"