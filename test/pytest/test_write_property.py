# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to property writing
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_set_object_property():
    """
    Verify that we can write remote properties
    """

    object_def = {}
    object_def['objectName'] = 'counterButton'
    object_def['type'] = 'Button'

    # Boolean
    button = qat.wait_for_object_exists(object_def)
    assert button.enabled

    button.enabled = False
    assert not button.enabled

    # Int
    button.autoRepeatDelay = 10
    assert button.autoRepeatDelay == 10

    # Write invalid property
    with pytest.raises(Exception):
        button.not_a_property = False

    # Set invalid value
    with pytest.raises(Exception):
        button.autoRepeatDelay = "avoid conversion"
    assert button.autoRepeatDelay == 10


def test_set_proxy_property():
    """
    Verify that we can read remote C++ properties
    """
    main_window_def = {
        'objectName': 'mainWindow',
        'type': 'ApplicationWindow'
    }

    main_window = qat.wait_for_object(main_window_def)
    proxy = main_window.mainProxy

    assert proxy.doubleValue == 0.0

    proxy.doubleValue = 9.9
    assert proxy.doubleValue == 9.9


def test_set_color():
    button_def = {
        'objectName' : 'colorButton4',
        'type' : 'ColorButton'
    }
    button = qat.wait_for_object(button_def)

    # Set by object definition with name
    color_def = {'name': '#FF5095F1', 'QVariantTypeName': 'QColor'}
    button.color = qat.QtCustomObject(color_def)
    assert str(button.color.name).upper() == color_def['name']

    # Set by object definition with channels
    color_def = {'red': 10, 'green': 20, 'blue':50, 'QVariantTypeName': 'QColor'}
    button.color = qat.QtCustomObject(color_def)
    assert button.color.red == color_def['red']
    assert button.color.green == color_def['green']
    assert button.color.blue == color_def['blue']

    # Set by color name
    button.color = "white"
    assert str(button.color.name).upper() == "#FFFFFFFF"
    assert button.color.red == 255
    assert button.color.green == 255
    assert button.color.blue == 255

