# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors
"""
Tests related to bindings
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_simple_text():
    """
    Verify that we can type text in fields
    """
    text_field_def = {
        'objectName': 'editableText',
        'type': 'TextEdit'
    }
    text_field = qat.wait_for_object(text_field_def)
    assert text_field.text == ""

    qat.type_in(text_field, "Hello")
    assert text_field.text == "Hello"

    text_field_def = {
        'objectName': 'editableText2',
        'type': 'TextEdit'
    }
    text_field = qat.wait_for_object(text_field_def)
    assert text_field.text == ""

    qat.type_in(text_field, "Bye")
    assert text_field.text == "Bye"

    open_window_button = qat.wait_for_object('openWindow')
    with pytest.raises(RuntimeWarning):
        qat.type_in(open_window_button, "test")


def test_special_keys():
    """
    Verify that we can use special key such as Del, Backspace, ...
    """
    text_field_def = {
        'objectName': 'editableText',
        'type': 'TextEdit'
    }
    text_field = qat.wait_for_object(text_field_def)
    assert text_field.text == ""

    qat.type_in(text_field, "Hello")
    assert text_field.text == "Hello"

    # Backspace
    qat.type_in(text_field, "<Backspace><Backspace><Backspace>")
    assert text_field.text == "He"
    qat.type_in(text_field, "<Backspace><Backspace><Backspace>")
    assert text_field.text == ""

    # Return/Enter
    qat.type_in(text_field, "Line1")
    assert text_field.text == "Line1"
    qat.type_in(text_field, "<Return>")
    qat.type_in(text_field, "<Enter>")
    qat.type_in(text_field, "Line2<Enter>")
    assert text_field.text == "Line1\n\nLine2\n"
    qat.type_in(text_field, "Li<Backspace>ine3")
    assert text_field.text == "Line1\n\nLine2\nLine3"


@pytest.mark.serial
def test_shortcuts():
    """
    Verify that shortcuts can be activated
    """
    text_field_def = {
        'objectName': 'editableText',
        'type': 'TextEdit'
    }
    text_field = qat.wait_for_object(text_field_def)
    assert text_field.text == ""

    qat.type_in(text_field, "Hello")
    assert text_field.text == "Hello"

    # Standard shortcuts
    qat.shortcut(text_field, 'Ctrl+Z')
    assert text_field.text == ""

    qat.shortcut(text_field, 'Ctrl+Shift+Z')
    assert text_field.text == "Hello"

    text_field.selectAll()
    qat.shortcut(text_field, 'Ctrl+C')
    qat.shortcut(text_field, 'Ctrl+Z')
    assert text_field.text == ""
    qat.shortcut(text_field, 'Ctrl+V')
    assert text_field.text == "Hello"

    # Alt+F4
    open_window_button = {
        'objectName': 'openWindow',
        'type': 'Button'
    }
    secondary_window = {
        'objectName': 'secondWindow'
    }
    qat.mouse_click(open_window_button)
    window = qat.wait_for_object_exists(secondary_window)
    assert window.visible

    qat.shortcut(window, 'Alt+F4')
    assert qat.wait_for_property_value(window, 'visible', False)

    # Custom shortcuts
    button_def = {
        'objectName': 'openButton',
        'type': 'Button'
    }

    popup_def = {
        'objectName': 'messagePopup',
        'type': 'MessagePopup'
    }

    qat.shortcut(button_def, 'Ctrl+O')

    popup = qat.wait_for_object(popup_def)
    assert popup.visible


@pytest.mark.serial
def test_multi_shortcuts():
    """
    Verify that ambiguous shortcuts can be activated when their owner is provided
    """
    button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    text_field_def = {
        'objectName': 'editableText',
        'type': 'TextEdit'
    }
    text_field = qat.wait_for_object(text_field_def)

    qat.type_in(text_field, "Hello")
    assert text_field.text == "Hello"

    button = qat.wait_for_object(button_def)
    qat.mouse_click(button_def)
    assert button.text == '1'

    # Counter button has 2 shortcuts defined
    qat.shortcut(button_def, "Shift+-")
    assert button.text == '0'

    qat.shortcut(button_def, 'Ctrl+Z')
    assert button.text == '-1'

    # Ctrl+Z is defined for both the button and the text field
    qat.shortcut(text_field, 'Ctrl+Z')
    assert button.text == '-1'
    assert text_field.text == ""


@pytest.mark.serial
def test_custom_editor(app_name):
    """
    Verify that ambiguous shortcuts can be activated when their owner is provided
    """
    if app_name != 'QmlApp':
        pytest.skip(reason='Custom editor is a QML-specific feature')

    custom_editor = {
        'objectName': 'customTextEditor',
        'type': 'CustomTextEditor'
    }

    text_field = {
        'container': custom_editor,
        'type': 'TextField'
    }

    editor = qat.wait_for_object(custom_editor)
    assert editor.text == ''
    text_field = qat.wait_for_object_exists(text_field)

    # Activate editor
    qat.mouse_click(editor)

    qat.type_in(text_field, "Hello")
    # Cancel change
    qat.type_in(text_field, "<Escape>")
    assert editor.text == ''

    # Activate editor
    qat.mouse_click(editor)
    qat.type_in(text_field, "Hello")
    # Accept change
    qat.type_in(text_field, "<Enter>")
    assert editor.text == 'Hello'
