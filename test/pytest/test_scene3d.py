# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to Qt3D scenes
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    if app_name != 'QmlApp':
        pytest.skip(reason='3D scenes not supported with QWidget app')
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_entities():
    """
    Verify that we can access QEntities
    """
    scene_def = {
        'objectName': 'main3dScene'
    }
    scene = qat.wait_for_object(scene_def)
    assert scene is not None

    # Root entity is a property of the scene
    root = scene.entity
    assert root.enabled

    # Find root entity by name
    root_def = {
        'container': scene_def,
        'objectName': 'root'
    }
    root = qat.wait_for_object(root_def)
    assert root.enabled

    # Find child of scene
    sphere_def = {
        'container': scene_def,
        'objectName': 'sphere'
    }
    sphere = qat.wait_for_object(sphere_def)
    assert sphere is not None
    assert sphere.enabled

    # Find child of root entity
    torus_def = {
        'container': root_def,
        'objectName': 'torus'
    }
    torus = qat.wait_for_object_exists(torus_def)
    assert torus is not None
    assert torus.enabled

    torus.enabled = False
    assert not torus.enabled

    # Find child of given parent
    torus_def = {
        'container': root_def,
        'parent': root_def,
        'objectName': 'torus'
    }
    torus = qat.wait_for_object_exists(torus_def)
    assert torus is not None
    assert not torus.enabled


def test_camera(app_name):
    """
    Verify that we can access the camera entity
    """

    scene_def = {
        'objectName': 'main3dScene'
    }
    scene = qat.wait_for_object(scene_def)
    assert scene is not None

    # Find camera by name
    camera_def = {
        'container': scene_def,
        'objectName': 'camera'
    }
    camera = qat.wait_for_object_exists(camera_def)

    upVector = camera.upVector
    assert upVector is not None
    assert upVector.x == 0
    assert upVector.y == 1
    assert upVector.z == 0
