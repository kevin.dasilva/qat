# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors
"""
Tests related to object picker (Spy)
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_picker():
    """
    Verify that we can pick a widget
    """
    button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }
    main_window = {
        'objectName': 'mainWindow'
    }
    picker_def = {
        'parent': main_window,
        'objectName': 'QatObjectPicker'
    }

    qat.activate_picker()

    button = qat.wait_for_object(button_def)
    qat.mouse_click(button)

    # Click should have been ignored by the button
    assert button.text == "0"

    # Picked object should be the button
    picker = qat.wait_for_object_exists(picker_def)
    picked_object = picker.pickedObject
    assert picked_object is not None
    assert not picked_object.is_null()

    # Picking can be temporarily disabled by holding the CTRL key
    qat.press_key(main_window, "<Control>")
    qat.mouse_click(button_def, modifier=qat.Modifier.CTL)
    qat.release_key(main_window, "<Control>")
    assert button.text == "1"

    qat.mouse_click(button)
    assert button.text == "1"

    # Verify that objects cannot be picked when Picker is de-activated
    qat.deactivate_picker()
    qat.mouse_click(button)
    picked_object = picker.pickedObject
    assert picked_object.is_null()

    # The click should be handled by the button
    assert button.text == "2"


def test_multiple_pickers():
    """
    Verify that we can use multiple pickers (one per window)
    """
    main_window = {
        'objectName': 'mainWindow'
    }
    counter_button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }
    open_button = {
        'objectName': 'openWindow',
        'type': 'Button'
    }
    close_button = {
        'objectName': 'closeWindowButton',
        'type': 'Button'
    }
    secondary_window = {
        'objectName': 'secondWindow'
    }
    secondary_picker_def = {
        'parent': secondary_window,
        'objectName': 'QatObjectPicker'
    }
    picker_overlay_def = {
        'container': secondary_window,
        'objectName': 'QatObjectPickerOverlay'
    }

    # Activate picker from the main window
    qat.activate_picker()

    # Open a second window
    qat.press_key(main_window, "<Control>")
    qat.mouse_click(open_button, modifier=qat.Modifier.CTL)
    qat.release_key(main_window, "<Control>")
    qat.wait_for_object(secondary_window)
    second_window = qat.wait_for_object_exists(secondary_window)
    assert second_window.visible

    # Verify that picker is enabled for the second window
    picker = qat.wait_for_object_exists(secondary_picker_def)
    qat.mouse_move(close_button, x = 5, y = 5, button=qat.Button.NONE)
    qat.mouse_move(close_button, button=qat.Button.NONE)
    qat.wait_for_object(picker_overlay_def)
    qat.mouse_click(close_button)
    picked_object = picker.pickedObject
    assert not picked_object.is_null()
    assert second_window.visible

    # Deactivate picker from the second window
    qat.deactivate_picker()

    # Close the window
    qat.mouse_click(close_button)
    assert not second_window.visible

    # Verify that the picker is not activated in the main window
    qat.mouse_click(counter_button_def)
    button = qat.wait_for_object(counter_button_def)
    assert button.text == "1"
