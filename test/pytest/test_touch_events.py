# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors
"""
Tests related to touch screen events.

Note:
Although touch events are synchronously executed, associated
bindings are not. Hence the use of synchronization functions
(such as wait_for_property_value) throughout these tests.
"""

from dataclasses import dataclass

import pytest
import qat


@dataclass
class ColorNames:
    """
    Hold constants for color name in hexadecimal form
    """
    BLACK = '#ff000000'
    WHITE = '#ffffffff'
    BLUE = '#ff0000ff'
    RED = '#ffff0000'
    GREEN = '#ff008000'
    YELLOW = '#ffffff00'


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def position_comparator(value, expected):
    return round(value) == expected

def test_touch_tap():
    """
    Verify that we can simulate tap on touch screen
    """
    touch_area = {
        'objectName': 'multiTouchArea'
    }
    rectangle1_def = {
        'container': touch_area,
        'objectName': 'touchRectangle1'
    }
    rectangle1 = qat.wait_for_object(rectangle1_def)
    assert round(rectangle1.x) == 0 - rectangle1.width / 2
    assert round(rectangle1.y) == 0 - rectangle1.height / 2

    qat.touch_tap(touch_area, x = 100, y = 100)

    assert round(rectangle1.x) == 100 - rectangle1.width / 2
    assert round(rectangle1.y) == 100 - rectangle1.height / 2


def test_touch_press_release():
    """
    Verify that we can press and release on touch screen
    """
    touch_area_def = {
        'objectName': 'multiTouchArea'
    }
    rectangle1_def = {
        'container': touch_area_def,
        'objectName': 'touchRectangle1'
    }
    touch_area = qat.wait_for_object(touch_area_def)
    rectangle1 = qat.wait_for_object(rectangle1_def)
    assert round(rectangle1.x) == 0 - rectangle1.width / 2
    assert round(rectangle1.y) == 0 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.GREEN

    qat.touch_press(touch_area, x = 100, y = 100)
    assert qat.wait_for_property_value(rectangle1, 'x', 100 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', 100 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.BLUE)

    # Without giving (x,y) the event occurs at the center of the area
    qat.touch_release(touch_area)
    assert qat.wait_for_property_value(rectangle1, 'x', touch_area.width / 2 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', touch_area.height / 2 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.GREEN)


def test_touch_move():
    """
    Verify that we can simulate finger moves on touch screen
    """
    touch_area = {
        'objectName': 'multiTouchArea'
    }
    rectangle1_def = {
        'container': touch_area,
        'objectName': 'touchRectangle1'
    }
    point1_def = {
        'container': touch_area,
        'objectName': 'point1',
        'type': 'TouchPoint'
    }

    rectangle1 = qat.wait_for_object(rectangle1_def)
    assert round(rectangle1.x) == 0 - rectangle1.width / 2
    assert round(rectangle1.y) == 0 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.GREEN

    point1 = qat.wait_for_object_exists(point1_def)
    qat.touch_press(touch_area, x = 100, y = 100)
    assert qat.wait_for(lambda: point1.pressed)

    qat.touch_move(touch_area, x = 50, y = 20)

    assert qat.wait_for_property_value(rectangle1, 'x', 50 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', 20 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.BLUE)

    qat.touch_release(touch_area, x=150, y=120)

    assert qat.wait_for_property_value(rectangle1, 'x', 150 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', 120 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.GREEN)

    # Try to move without pressing first
    qat.touch_move(touch_area, x = 30, y = 50)
    assert not qat.wait_for_property_change(rectangle1, 'x', 150 - rectangle1.width / 2, comparator=position_comparator, timeout=500)
    assert round(rectangle1.x) == 150 - rectangle1.width / 2
    assert round(rectangle1.y) == 120 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.GREEN


def test_touch_drag():
    """
    Verify that we can simulate finger drags on touch screen
    """
    touch_area = {
        'objectName': 'multiTouchArea'
    }
    rectangle1_def = {
        'container': touch_area,
        'objectName': 'touchRectangle1'
    }
    rectangle1 = qat.wait_for_object(rectangle1_def)
    assert round(rectangle1.x) == 0 - rectangle1.width / 2
    assert round(rectangle1.y) == 0 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.GREEN

    qat.touch_drag(touch_area, x = 30, y = 30, dx = 100, dy = 50)

    assert qat.wait_for_property_value(rectangle1, 'x', 130 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', 80 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.GREEN)


def test_touch_as_click():
    """
    Verify that tapping on a button behaves like a mouse click
    """
    qt_version = str(qat.current_application_context().qt_version)
    if str(qt_version).startswith('6.1.'):
        pytest.skip(reason=f"Synthesized touch events not supported on Qt {qt_version}")
    # Simple tap
    button_def = {
        'objectName': 'counterButton'
    }
    button = qat.wait_for_object(button_def)
    assert button.text == '0'

    qat.touch_tap(button)
    assert qat.wait_for_property_value(button, 'text', '1')

    # Test modifiers
    button_def = {
        'objectName': 'modifierButton'
    }
    button = qat.wait_for_object(button_def)
    assert button.text == ''

    def text_comparator(value: str, expected: str):
        """
        Strip() the value and compare it with the expected string
        """
        return value.strip() == expected

    qat.touch_tap(button, modifier=qat.Modifier.ALT)
    assert qat.wait_for_property_value(button, 'text', 'ALT', comparator=text_comparator)

    qat.touch_tap(button, modifier=qat.Modifier.CTL)
    assert qat.wait_for_property_value(button, 'text', 'CTRL', comparator=text_comparator)

    qat.touch_tap(button, modifier=qat.Modifier.SHIFT)
    assert qat.wait_for_property_value(button, 'text', 'SHIFT', comparator=text_comparator)

    qat.touch_tap(button, modifier=[qat.Modifier.CTL, qat.Modifier.ALT])
    assert qat.wait_for_property_value(button, 'text', 'CTRL ALT', comparator=text_comparator)

    qat.touch_tap(button, modifier=[qat.Modifier.ALT, qat.Modifier.SHIFT])
    assert qat.wait_for_property_value(button, 'text', 'SHIFT ALT', comparator=text_comparator)


def test_multi_touch_tap():
    """
    Verify that we can simulate tap with multiple fingers on touch screen
    """
    touch_area = {
        'objectName': 'multiTouchArea'
    }
    rectangle1_def = {
        'container': touch_area,
        'objectName': 'touchRectangle1'
    }
    rectangle2_def = {
        'container': touch_area,
        'objectName': 'touchRectangle2'
    }
    rectangle3_def = {
        'container': touch_area,
        'objectName': 'touchRectangle3'
    }
    rectangle1 = qat.wait_for_object(rectangle1_def)
    rectangle2 = qat.wait_for_object(rectangle2_def)
    rectangle3 = qat.wait_for_object(rectangle3_def)

    # Verify initial positions
    assert round(rectangle1.x) == 0 - rectangle1.width / 2
    assert round(rectangle1.y) == 0 - rectangle1.height / 2

    assert round(rectangle2.x) == 0 - rectangle2.width / 2
    assert round(rectangle2.y) == 0 - rectangle2.height / 2

    assert round(rectangle3.x) == 0 - rectangle3.width / 2
    assert round(rectangle3.y) == 0 - rectangle3.height / 2

    qat.touch_tap(touch_area, x = [100, 200, 300], y = [100, 150, 200])

    assert round(rectangle1.x) == 100 - rectangle1.width / 2
    assert round(rectangle1.y) == 100 - rectangle1.height / 2

    assert round(rectangle2.x) == 200 - rectangle2.width / 2
    assert round(rectangle2.y) == 150 - rectangle2.height / 2

    assert round(rectangle3.x) == 300 - rectangle3.width / 2
    assert round(rectangle3.y) == 200 - rectangle3.height / 2

    # Colors should not change
    assert rectangle1.color.name == ColorNames.GREEN
    assert rectangle2.color.name == ColorNames.RED
    assert rectangle3.color.name == ColorNames.WHITE

    # Negative test: too many touch points
    qat.touch_tap(touch_area, x = [50, 60, 70, 80], y = [50, 60, 70, 80])

    # Verify that command was ignored (no change)
    assert round(rectangle1.x) == 100 - rectangle1.width / 2
    assert round(rectangle1.y) == 100 - rectangle1.height / 2

    assert round(rectangle2.x) == 200 - rectangle2.width / 2
    assert round(rectangle2.y) == 150 - rectangle2.height / 2

    assert round(rectangle3.x) == 300 - rectangle3.width / 2
    assert round(rectangle3.y) == 200 - rectangle3.height / 2

    assert rectangle1.color.name == ColorNames.GREEN
    assert rectangle2.color.name == ColorNames.RED
    assert rectangle3.color.name == ColorNames.WHITE


def test_multi_touch_drag():
    """
    Verify that we can simulate multiple finger drags on touch screen
    """
    touch_area = {
        'objectName': 'multiTouchArea'
    }
    rectangle1_def = {
        'container': touch_area,
        'objectName': 'touchRectangle1'
    }
    rectangle2_def = {
        'container': touch_area,
        'objectName': 'touchRectangle2'
    }
    rectangle3_def = {
        'container': touch_area,
        'objectName': 'touchRectangle3'
    }
    rectangle1 = qat.wait_for_object(rectangle1_def)
    rectangle2 = qat.wait_for_object(rectangle2_def)
    rectangle3 = qat.wait_for_object(rectangle3_def)

    # Initial positions and colors
    assert round(rectangle1.x) == 0 - rectangle1.width / 2
    assert round(rectangle1.y) == 0 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.GREEN
    assert round(rectangle2.x) == 0 - rectangle2.width / 2
    assert round(rectangle2.y) == 0 - rectangle2.height / 2
    assert rectangle2.color.name == ColorNames.RED
    assert round(rectangle3.x) == 0 - rectangle3.width / 2
    assert round(rectangle3.y) == 0 - rectangle3.height / 2
    assert rectangle3.color.name == ColorNames.WHITE

    qat.touch_drag(
        touch_area,
        x = [30, 40, 50],
        y = [30, 35, 40],
        dx = [100, 80, 60],
        dy = [50, 40, 30])
    assert qat.wait_for_property_value(rectangle1, 'x', 130 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', 80 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.GREEN)

    assert qat.wait_for_property_value(rectangle2, 'x', 120 - rectangle2.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'y', 75 - rectangle2.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'color.name', ColorNames.RED)

    assert qat.wait_for_property_value(rectangle3, 'x', 110 - rectangle3.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle3, 'y', 70 - rectangle3.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle3, 'color.name', ColorNames.WHITE)


def test_multi_touch_move():
    """
    Verify that we can simulate multiple finger moves on touch screen
    """
    touch_area = {
        'objectName': 'multiTouchArea'
    }
    rectangle1_def = {
        'container': touch_area,
        'objectName': 'touchRectangle1'
    }
    rectangle2_def = {
        'container': touch_area,
        'objectName': 'touchRectangle2'
    }

    rectangle1 = qat.wait_for_object(rectangle1_def)
    rectangle2 = qat.wait_for_object(rectangle2_def)
    assert round(rectangle1.x) == 0 - rectangle1.width / 2
    assert round(rectangle1.y) == 0 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.GREEN
    assert round(rectangle2.x) == 0 - rectangle2.width / 2
    assert round(rectangle2.y) == 0 - rectangle2.height / 2
    assert rectangle2.color.name == ColorNames.RED

    qat.touch_press(touch_area, x = [100, 200], y = [100, 100])

    assert qat.wait_for_property_value(rectangle1, 'x', 100 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', 100 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.BLUE)
    assert qat.wait_for_property_value(rectangle2, 'x', 200 - rectangle2.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'y', 100 - rectangle2.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'color.name', ColorNames.YELLOW)

    # Move first point
    qat.touch_move(touch_area, x = 50, y = 20)

    assert qat.wait_for_property_value(rectangle1, 'x', 50 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', 20 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.BLUE)

    # Second rectangle should not move
    assert round(rectangle2.x) == 200 - rectangle2.width / 2
    assert round(rectangle2.y) == 100 - rectangle2.height / 2
    assert rectangle2.color.name == ColorNames.YELLOW

    # Move second point
    qat.touch_move(touch_area, x = [50, 150], y = [20, 30])

    assert qat.wait_for_property_value(rectangle2, 'x', 150 - rectangle2.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'y', 30 - rectangle2.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'color.name', ColorNames.YELLOW)

    # First rectangle should not move
    assert round(rectangle1.x) == 50 - rectangle1.width / 2
    assert round(rectangle1.y) == 20 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.BLUE

    qat.touch_release(touch_area, x=[150, 10], y=[120, 50])

    assert qat.wait_for_property_value(rectangle1, 'x', 150 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', 120 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.GREEN)
    assert qat.wait_for_property_value(rectangle2, 'x', 10 - rectangle2.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'y', 50 - rectangle2.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'color.name', ColorNames.RED)

    # Try to move without pressing first
    qat.touch_move(touch_area, x = [30, 200], y = [50, 80])

    assert not qat.wait_for_property_change(rectangle1, 'x', 150 - rectangle1.width / 2, comparator=position_comparator, timeout=500)
    assert round(rectangle1.x) == 150 - rectangle1.width / 2
    assert round(rectangle1.y) == 120 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.GREEN
    assert round(rectangle2.x) == 10 - rectangle2.width / 2
    assert round(rectangle2.y) == 50 - rectangle2.height / 2
    assert rectangle2.color.name == ColorNames.RED


def test_multi_touch_press_release():
    """
    Verify that we can press and release fingers on touch screen
    """
    touch_area_def = {
        'objectName': 'multiTouchArea'
    }
    rectangle1_def = {
        'container': touch_area_def,
        'objectName': 'touchRectangle1'
    }
    rectangle2_def = {
        'container': touch_area_def,
        'objectName': 'touchRectangle2'
    }
    touch_area = qat.wait_for_object(touch_area_def)
    rectangle1 = qat.wait_for_object(rectangle1_def)
    rectangle2 = qat.wait_for_object(rectangle2_def)
    assert round(rectangle1.x) == 0 - rectangle1.width / 2
    assert round(rectangle1.y) == 0 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.GREEN

    qat.touch_press(touch_area, x = [100, 50], y = [100, 20])

    assert qat.wait_for_property_value(rectangle1, 'x', 100 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', 100 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.BLUE)
    assert qat.wait_for_property_value(rectangle2, 'x', 50 - rectangle2.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'y', 20 - rectangle2.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle2, 'color.name', ColorNames.YELLOW)

    # Without giving (x,y) the event occurs at the center of the area
    qat.touch_release(touch_area)

    assert qat.wait_for_property_value(rectangle1, 'x', touch_area.width / 2 - rectangle1.width / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'y', touch_area.height / 2 - rectangle1.height / 2, comparator=position_comparator)
    assert qat.wait_for_property_value(rectangle1, 'color.name', ColorNames.GREEN)

    # Second point should not have moved
    assert round(rectangle2.x) == 50 - rectangle2.width / 2
    assert round(rectangle2.y) == 20 - rectangle2.height / 2
    assert rectangle2.color.name == ColorNames.RED

    # Releasing again should have no effect
    qat.touch_release(touch_area, x=[200, 100], y=[100, 50])

    assert not qat.wait_for_property_value(rectangle1, 'x', 200 - rectangle1.width / 2, comparator=position_comparator, timeout=500)

    assert round(rectangle1.x) == touch_area.width / 2 - rectangle1.width / 2
    assert round(rectangle1.y) == touch_area.height / 2 - rectangle1.height / 2
    assert rectangle1.color.name == ColorNames.GREEN

    assert round(rectangle2.x) == 50 - rectangle2.width / 2
    assert round(rectangle2.y) == 20 - rectangle2.height / 2
    assert rectangle2.color.name == ColorNames.RED
