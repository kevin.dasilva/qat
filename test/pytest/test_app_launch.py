# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to application launcher
"""

from pathlib import Path
import os
import subprocess
import pytest

import qat
from qat.internal import app_launcher

APP_PROCESS = None

# pylint: disable=protected-access

@pytest.fixture(autouse=True)
def teardown():
    """
    Clean up on exit
    """
    yield

    if APP_PROCESS is not None:
        try:
            APP_PROCESS.kill()
        except: # pylint: disable=bare-except
            pass
    if qat.current_application_context():
        qat.close_application()


def test_app_launch(app_name, qt_version):
    """
    Verify that a registered application can be launched
    """
    assert len(qat.get_context_list()) == 0
    # Start an existing application
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0
    assert context._internal.process is not None
    assert context.is_running()

    assert qat.current_application_context() == context
    assert len(qat.get_context_list()) == 1
    assert qat.get_context_list()[0] == context
    assert context.qt_version == qt_version

    assert qat.close_application(context) != 0
    assert not context.is_running()
    assert len(qat.get_context_list()) == 0

    # Start a non-existing application
    with pytest.raises(Exception):
        qat.start_application("NoApp", "")
    assert qat.current_application_context() is None
    with pytest.raises(Exception):
        qat.close_application(qat.current_application_context())


def test_two_apps(app_name):
    """
    Verify that 2 registered applications can be launched and tested
    """
    button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    # Start the first instance
    context1 = qat.start_application(app_name, "")
    assert context1 is not None
    assert context1.pid != 0
    assert context1._internal.process is not None

    # Start the second instance
    context2 = qat.start_application(app_name, "")
    assert context2 is not None
    assert context2.pid != 0
    assert context2._internal.process is not None

    assert len(qat.get_context_list()) == 2
    assert context1 in qat.get_context_list()
    assert context2 in qat.get_context_list()

    # Click on a button in the first instance
    qat.set_current_application_context(context1)
    assert qat.current_application_context() == context1

    button1 = qat.wait_for_object(button_def)
    assert button1.text == '0'

    qat.mouse_click(button1)
    assert button1.text == '1'

    # Verify that the second instance was not affected
    qat.set_current_application_context(context2)
    assert qat.current_application_context() == context2

    button2 = qat.wait_for_object(button_def)
    assert button2.text == '0'

    # Click on a button in the second instance
    qat.mouse_click(button2)
    assert button2.text == '1'
    qat.mouse_click(button2)
    assert button2.text == '2'

    # Verify that the first instance was not affected
    assert button1.text == '1'

    # Close both instances
    qat.close_application(context1)
    assert len(qat.get_context_list()) == 1
    assert context1 not in qat.get_context_list()
    assert context2 in qat.get_context_list()
    qat.close_application(context2)
    assert len(qat.get_context_list()) == 0


def test_attach_to_app():
    """
    Verify that Qat can attach to a running application
    """
    button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    # Use a dedicated application to avoid conflicts when running in parallel
    app_name = 'WidgetApp2'

    # Start the application to retrieve its path
    context1 = qat.start_application(app_name, "")
    assert context1 is not None
    assert context1.pid != 0
    assert context1._internal.process is not None

    app_path = Path(context1.get_app_path())

    qat.close_application(context1)

    # Decrease timeout to make this test faster. Test applications are small and start quickly.
    qat.Settings.wait_for_app_start_timeout = 3000

    # Cannot attach when the application is not running
    with pytest.raises(Exception):
        qat.attach_to_application(app_name)

    # Start the application without Qat
    global APP_PROCESS
    if app_launcher.is_windows():
        APP_PROCESS = subprocess.Popen([app_path])
    elif app_launcher.is_linux():
        # Library must be pre-loaded, Qat does not provide real injection on linux
        local_env = dict(os.environ)
        root_path = Path(__file__).parent.parent.parent / 'build'
        local_env['LD_PRELOAD'] = str(root_path / 'libinjector.so')
        APP_PROCESS = subprocess.Popen([app_path], env=local_env)
    elif app_launcher.is_macos():
        # Library must be pre-loaded, Qat does not provide real injection on MacOS
        local_env = dict(os.environ)
        root_path = Path(__file__).parent.parent.parent / 'build'
        local_env['DYLD_INSERT_LIBRARIES'] = str(root_path / 'libinjector.dylib')
        APP_PROCESS = subprocess.Popen([app_path], env=local_env)

    # Invalid arguments
    with pytest.raises(Exception):
        qat.attach_to_application(True)
    with pytest.raises(Exception):
        qat.attach_to_application('')
    with pytest.raises(Exception):
        qat.attach_to_application(app_name + '_suffix')

    # Attach by name
    context2 = qat.attach_to_application(app_name)
    assert context2 is not None
    assert context2.pid != 0
    assert context2._internal.process is None  # process object is not available when attaching
    assert len(qat.get_context_list()) == 1
    assert qat.get_context_list()[0] == context2

    # After attaching successfully, the process will be closed automatically by Qat
    pid = APP_PROCESS.pid
    APP_PROCESS = None

    # Verify we can interact with the application
    button1 = qat.wait_for_object(button_def)
    assert button1.text == '0'

    qat.mouse_click(button1)
    assert button1.text == '1'

    # Attach by process ID
    context3 = qat.attach_to_application(pid)
    assert context3 is not None
    assert context3.pid == pid
    assert context3._internal.process is None  # process object is not available when attaching

    # Verify we can interact with the same application
    button2 = qat.wait_for_object(button_def)
    assert button2.text == '1'

    qat.mouse_click(button2)
    assert button2.text == '2'
    assert button1.text == '2'

    # Support .exe extension
    if app_launcher.is_windows():
        context4 = qat.attach_to_application(app_name + '.exe')
        assert context4 is not None
        assert context4.pid == pid

    # Invalid process ID
    with pytest.raises(Exception):
        qat.attach_to_application(0)


def test_ui_lock(app_name):
    """
    Verify that UI can be manipulated while it is locked for user events
    """
    button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }
    qat.start_application(app_name)

    button = qat.wait_for_object(button_def)
    assert button.text == '0'

    qat.unlock_application()
    qat.mouse_click(button)
    assert button.text == '1'

    qat.lock_application()
    qat.mouse_click(button)
    assert button.text == '2'


def test_app_args(app_name):
    """
    Verify that arguments can be passed to the application
    """
    with pytest.raises(Exception):
        qat.start_application(app_name, "--invalid_arg")
    context = qat.start_application(app_name, "--invalid_arg", detached=True)
    assert context.get_exit_code() == 2
    assert context.pid > 0
    assert context.is_finished()

    with pytest.raises(Exception):
        qat.start_application(app_name, "-h --too-many")
    context = qat.start_application(app_name, "-h --too-many", detached=True)
    assert context.get_exit_code() == 2
    assert context.is_finished()
    assert context.pid > 0

    with pytest.raises(Exception):
        qat.start_application(app_name, "-h")
    context = qat.start_application(app_name, "-h", detached=True)
    assert context.get_exit_code() == 0
    assert context.is_finished()
    assert context.pid > 0


def test_start_application_unsupported_system(app_name, mocker):
    """
    Verify that the application throws an exception when the system is unsupported
    """

    # Throws an exception when the current system is not supported
    is_windows_mock = mocker.patch("qat.app_launcher.is_windows", return_value=False)
    is_linux_mock = mocker.patch("qat.app_launcher.is_linux", return_value=False)
    is_macos_mock = mocker.patch("qat.app_launcher.is_macos", return_value=False)

    with pytest.raises(NotImplementedError):
        app_launcher.start_application(app_name, args="")

    is_windows_mock.assert_called_once()
    is_linux_mock.assert_called_once()
    is_macos_mock.assert_called_once()


def test_cleanup_temp_files_exceptions(mocker):
    """
    Verify that the clean qat temporary files function does not throw an exception when :
        - Unable to remove a temporary file
        - Unable to check the output of a subprocess call on linux
        - Unable to check the run subprocess command on windows
    """

    if not app_launcher.is_linux():
        pytest.skip(reason='This test does not depend on the system and should only run once')

    mocker.patch("pathlib.Path.glob", return_value=[Path("qat-1.txt")])
    mocker.patch("subprocess.Popen")

    class MockProcessOutput:
        """
        Simulating an empty process stdout
        """
        stdout = ""

    # Ensure the function does not throw when unable to remove a file
    is_window_mock = mocker.patch("qat.app_launcher.is_windows", return_value=False)
    is_linux_mock = mocker.patch("qat.app_launcher.is_linux", return_value=True)
    run_mock = mocker.patch("subprocess.run", return_value=MockProcessOutput())
    remove_mock = mocker.patch("os.remove", side_effect=OSError)
    qat.app_launcher.cleanup_temp_files()

    # Ensure the remove file function has been called once
    remove_mock.assert_called_once()
    run_mock.assert_called_once()

    remove_mock.stop()
    run_mock.reset_mock()

    # Ensure the function does not throw when unable to call check the output on linux
    is_window_mock.return_value=False
    is_linux_mock.return_value=True
    run_mock.side_effect = subprocess.CalledProcessError(-1,"")
    qat.app_launcher.cleanup_temp_files()

    run_mock.assert_called_once()

    run_mock.stop()

    # Ensure the function does not throw when unable to check the output on windows
    is_window_mock.return_value = True
    is_linux_mock.return_value = False
    subprocess_run_mock = mocker.patch("subprocess.run", side_effect=subprocess.CalledProcessError(-1,""))
    qat.app_launcher.cleanup_temp_files()

    subprocess_run_mock.assert_called_once()
