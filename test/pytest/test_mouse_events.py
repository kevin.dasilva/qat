# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors
"""
Tests related to mouse
"""

import math
import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_mouse_click(app_name):
    """
    Verify that we can click on widgets
    """
    # Simple button
    object_def = {}
    object_def['objectName'] = 'counterButton'
    object_def['type'] = 'Button'
    counter_button = qat.wait_for_object(object_def)
    qat.mouse_click(counter_button)
    assert counter_button.text == '1'

    # Right and middle buttons should have no effect
    qat.mouse_click(counter_button, button=qat.Button.RIGHT)
    assert counter_button.text == '1'
    qat.mouse_click(counter_button, button=qat.Button.MIDDLE)
    assert counter_button.text == '1'

    # Disabled button should not accept clicks
    counter_button.enabled = False
    counter_button = qat.wait_for_object_exists(object_def)
    with pytest.raises(Exception):
        qat.mouse_click(counter_button)
    assert counter_button.text == '1'

    # Checkable button
    button_def = {
        'objectName': 'checkableButton'
    }
    button = qat.wait_for_object_exists(button_def)
    assert button.checkable
    assert not button.checked
    qat.mouse_click(button)
    assert button.checked
    qat.mouse_click(button)
    assert not button.checked

    # Button in popup
    open_popup_button = {
        'objectName': 'openButton'
    }
    popup_def = {
        'objectName': 'messagePopup'
    }
    close_button = {
        'container': popup_def,
        'objectName': 'messagePopupCloseButton'
    }
    qat.mouse_click(open_popup_button)
    assert qat.wait_for_object(popup_def) is not None

    # Non-regression test for QML list
    if app_name == 'QmlApp':
        list_view_def = {
            'objectName': 'popupListView',
            'type': 'ListView'
        }

        list_element = {
            'container': list_view_def,
            'text': 'item 2'
        }
        element = qat.wait_for_object(list_element)
        assert element.color.name == '#ff000000'  # black

        qat.mouse_click(element)
        assert element.color.name == '#ffff0000'  # red

        qat.mouse_click(close_button)

        # Click inside ListView
        list_view_def = {
            'objectName': 'listView',
            'type': 'ListView'
        }

        list_element_3 = {
            'container': list_view_def,
            'text': 'item 3'
        }

        element3 = qat.wait_for_object(list_element_3)
        assert element3.color.name == '#ff000000'  # black

        qat.mouse_click(element3)
        assert element3.color.name == '#ffff0000'  # red

        # Custom combobox with ListView
        combo_def = {
            'objectName': 'customCombo'
        }

        combo_list_def = {
            'container': combo_def,
            'objectName': 'comboListView'
        }

        combo_element_def = {
            'container': combo_list_def,
            'text': 'item 3'
        }

        combobox = qat.wait_for_object(combo_def)
        # First click is ignored
        qat.mouse_click(combobox)
        assert not combobox.down

        # Second click opens the popup
        qat.mouse_click(combobox)
        assert combobox.down

        combo_list = qat.wait_for_object_exists(combo_list_def)
        assert combo_list.visible

        combo_element = qat.wait_for_object_exists(combo_element_def)
        assert combo_element.color.name == '#ff000000'  # black

        qat.mouse_click(combo_element)
        assert combo_element.color.name == '#ffff0000'  # red
    else:
        qat.mouse_click(close_button)
        list_view_def = {
            'objectName': 'listView',
            'type': 'ListView'
        }

        list_element = {
            'container': list_view_def,
            'text': 'item 2'
        }
        element = qat.wait_for_object(list_element)
        assert element.color.name == '#ff000000'  # black

        qat.mouse_click(element)
        assert element.color.name == '#ffff0000'  # red

        # Cannot click on invisible (out-of-bounds) elements
        list_element['text'] = 'item 15'
        with pytest.raises(Exception):
            qat.mouse_click(list_element)

    # Grid of custom buttons
    button_grid_def = {
        'objectName': 'buttonGrid'
    }

    object_def = {}
    object_def['container'] = button_grid_def
    object_def['objectName'] = 'colorButton'
    object_def['type'] = 'ColorButton'

    button_grid = qat.wait_for_object_exists(button_grid_def)
    assert button_grid is not None
    for i in range(5):
        object_def['objectName'] = f'colorButton{i}'
        color_button = qat.wait_for_object(object_def)
        assert color_button.color.name == '#ffffffff'  # white

        # Default (=Left) click
        qat.mouse_click(color_button)
        assert color_button.color.name == '#ff008000'  # green

        # Right click
        qat.mouse_click(color_button, button=qat.Button.RIGHT)
        assert color_button.color.name == '#ffff0000'  # red

        # Middle click
        qat.mouse_click(color_button, button=qat.Button.MIDDLE)
        assert color_button.color.name == '#ff000000'  # black

        # Left click
        qat.mouse_click(color_button, button=qat.Button.LEFT)
        assert color_button.color.name == '#ff008000'  # green

        # Invalid click
        with pytest.raises(Exception):
            qat.mouse_click(color_button, button=qat.Button.NONE)
        assert color_button.color.name == '#ff008000'  # green

        # Double click
        qat.double_click(color_button)
        assert color_button.color.name == '#ff0000ff'  # blue

        # Right double click
        qat.double_click(color_button, button=qat.Button.RIGHT)
        assert color_button.color.name == '#ffffff00'  # yellow

        # Middle double click
        qat.double_click(color_button, button=qat.Button.MIDDLE)
        assert color_button.color.name == '#ff808080'  # grey


def test_mouse_modifiers():
    """
    Verify that we can use keyboard modifiers when clicking
    """
    button_def = {
        'objectName': 'modifierButton'
    }
    button = qat.wait_for_object(button_def)
    assert button.text == ''

    qat.mouse_click(button, modifier=qat.Modifier.ALT)
    assert button.text.strip() == 'ALT'

    qat.mouse_click(button, modifier=qat.Modifier.CTL)
    assert button.text.strip() == 'CTRL'

    qat.mouse_click(button, modifier=qat.Modifier.SHIFT)
    assert button.text.strip() == 'SHIFT'

    qat.mouse_click(button, modifier=[qat.Modifier.CTL, qat.Modifier.ALT])
    assert button.text.strip() == 'CTRL ALT'

    qat.mouse_click(button, modifier=[qat.Modifier.ALT, qat.Modifier.SHIFT])
    assert button.text.strip() == 'SHIFT ALT'


def test_mouse_click_position():
    """
    Verify that we can click at a specific position
    """
    # Simple button
    object_def = {}
    object_def['objectName'] = 'counterButton'
    object_def['type'] = 'Button'
    counter_button = qat.wait_for_object(object_def)
    qat.mouse_click(counter_button)
    assert counter_button.text == '1'

    with pytest.raises(Exception):
        qat.mouse_click(counter_button, x=1000, y=1000)
    with pytest.raises(Exception):
        qat.mouse_click(counter_button, x=-10, y=-65)
    assert counter_button.text == '1'

    # Test bounds
    with pytest.raises(Exception):
        qat.mouse_click(counter_button, x=counter_button.width + 2, y=counter_button.height + 2)
    qat.mouse_click(counter_button, x=counter_button.width - 2, y=counter_button.height - 2)
    assert counter_button.text == '2'


def test_mouse_press_release():
    """
    Verify that we can press and release mouse buttons
    """
    # Simple button
    object_def = {}
    object_def['objectName'] = 'counterButton'
    object_def['type'] = 'Button'
    counter_button = qat.wait_for_object(object_def)
    assert counter_button.text == '0'
    qat.mouse_press(counter_button)
    assert counter_button.text == '0'
    qat.mouse_release(counter_button)
    assert counter_button.text == '1'


def test_mouse_move():
    """
    Verify that we can move the mouse
    """
    slider_def = {
        'objectName': 'slider',
        'type': 'Slider'
    }

    slider = qat.wait_for_object(slider_def)
    h = slider.height
    w = slider.width
    # Initial value is 5
    assert slider.value == 5

    # Pick the handle
    qat.mouse_press(slider_def)

    # Move handle to the left
    qat.mouse_move(slider_def, x=5, y=h/2)
    assert slider.value < 5

    # Move handle to the right
    qat.mouse_move(slider_def, x=w-5, y=h/2)
    assert slider.value > 5

    # Release mouse at the center: value should go back to 5
    qat.mouse_move(slider_def)
    qat.mouse_release(slider_def)
    assert slider.value == 5


def test_mouse_drag():
    """
    Verify that we can drag the mouse
    """
    slider_def = {
        'objectName': 'slider',
        'type': 'Slider'
    }

    slider = qat.wait_for_object(slider_def)
    w = slider.width
    # Initial value is 5
    assert slider.value == 5

    # Drag handle to the right
    qat.mouse_drag(slider_def, dx=w/2, dy=0)
    assert slider.value == 10

    # Drag handle to the left
    slider.value = 5
    qat.mouse_drag(slider_def, dx=-w/2, dy=0)
    assert slider.value == 0

    # Invalid drag with right button
    with pytest.raises(Exception):
        qat.mouse_drag(slider_def, dx=w/2, dy=0,
                      button=qat.Button.RIGHT, check=True)
    assert slider.value == 0


def test_mouse_wheel_3d_scene(app_name):
    """
    Verify that we can scroll on 3D scenes
    """
    if app_name != 'QmlApp':
        pytest.skip(reason='3D scenes are supported for QML applications only')

    scene_def = {
        'objectName': 'main3dScene'
    }

    camera_def = {
        'container': scene_def,
        'objectName': 'camera'
    }
    # Make sure the scene is initialized
    qat.wait_for_object(scene_def)

    camera = qat.wait_for_object_exists(camera_def)
    original_z = camera.transform.translation.z
    last_z = original_z

    for _ in range(20):
        qat.mouse_wheel(scene_def, y_degrees=15, check=True)
        # Zoom is performed asynchronously in 3D scenes
        assert qat.wait_for_property_change(camera, 'transform.translation.z', last_z)
        last_z = camera.transform.translation.z
    assert original_z < camera.transform.translation.z

    original_z = camera.transform.translation.z
    last_z = original_z
    for _ in range(20):
        qat.mouse_wheel(scene_def, y_degrees=-15, check=True)
        # Zoom is performed asynchronously in 3D scenes
        assert qat.wait_for_property_change(camera, 'transform.translation.z', last_z)
        last_z = camera.transform.translation.z
    assert original_z > camera.transform.translation.z


def test_mouse_wheel(app_name):
    """
    Verify that we can scroll on Flickables
    """

    list_view = qat.wait_for_object('listView')

    if app_name == 'QmlApp':
        scrollbar_def = {
            'objectName': 'listScrollBar',
            'type': 'ScrollBar'
        }
        scrollbar = qat.wait_for_object_exists(scrollbar_def)
        max_value = 1.0 - scrollbar.size
        pos_property = 'position'
    elif app_name == 'WidgetApp':
        scrollbar_def = {
            'container': list_view,
            'type': 'QScrollBar',
            'visible': True # Only the vertical bar is visible
        }
        scrollbar = qat.wait_for_object_exists(scrollbar_def)
        max_value = scrollbar.maximum
        pos_property = 'value'
    else:
        pytest.fail(f"Unsupported application: {app_name}")

    tolerance = 1e-4
    def comparator(actual, expected):
        return math.isclose(actual, expected, abs_tol=tolerance)

    scrollbar = qat.wait_for_object_exists(scrollbar_def)
    min_value = 0.0
    # Default position is at 0
    assert getattr(scrollbar, pos_property) == min_value

    # Scroll by one increment (15 degrees)
    qat.mouse_wheel(list_view, y_degrees=-15)
    # There is a delay/animation between the scroll event and the position change
    qat.wait_for_property_change(scrollbar, pos_property, min_value, check=True)
    assert getattr(scrollbar, pos_property) > min_value

    # list view seems to require a click to enable scrolling
    qat.mouse_click(list_view)

    # Scroll to the bottom
    last_pos = getattr(scrollbar, pos_property)
    for _ in range(15):
        qat.mouse_wheel(list_view, y_degrees=-30)
        qat.wait_for_property_change(scrollbar, pos_property, last_pos, timeout=100)
        last_pos = getattr(scrollbar, pos_property)
    qat.wait_for_property_value(scrollbar, pos_property, max_value, comparator=comparator, check=True)

    # Scroll up
    qat.mouse_wheel(list_view, y_degrees=15)
    qat.wait_for_property_change(scrollbar, pos_property, max_value, check=True)
    assert getattr(scrollbar, pos_property) < max_value

    # Scroll to the top
    last_pos = getattr(scrollbar, pos_property)
    for _ in range(15):
        qat.mouse_wheel(list_view, y_degrees=30)
        qat.wait_for_property_change(scrollbar, pos_property, last_pos, timeout=100)
        last_pos = getattr(scrollbar, pos_property)
    qat.wait_for_property_value(scrollbar, pos_property, min_value, comparator=comparator, check=True)


def test_mouse_hover():
    """
    Verify that we can hover the mouse cursor over a widget
    """
    hover_label_def = {
        "objectName": "hoverLabel"
    }
    hover_label = qat.wait_for_object(hover_label_def)

    qat.mouse_move(hover_label, x=20, y=10, button=qat.Button.NONE)
    assert hover_label.text == "20 X 10"

    qat.mouse_move(hover_label, x=50, y=20, button=qat.Button.NONE)
    assert hover_label.text == "50 X 20"

    qat.mouse_move(hover_label, x=60, y=10, button=qat.Button.NONE)
    assert hover_label.text == "60 X 10"

    qat.mouse_move(hover_label, x=-10, y=-10, button=qat.Button.NONE)
    assert hover_label.text == "-1 X -1"
