# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to the wait_for_property functions
"""

import threading
import time

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_property_value():
    """
    Verify that we can wait for a property to reach a value
    """
    button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    text_field_def = {
        'objectName': 'editableText',
        'type': 'TextEdit'
    }

    def click_on_button():
        time.sleep(0.5)
        qat.mouse_click(button_def)

    def type_in_text():
        time.sleep(0.5)
        qat.type_in(text_field_def, "1234657890")

    click_thread = threading.Thread(target=click_on_button)
    click_thread.start()

    result = qat.wait_for_property_value(button_def, 'text', '1')
    click_thread.join()
    assert result

    # Negative test
    assert not qat.wait_for_property_value(button_def, 'text', '2', timeout=500)
    with pytest.raises(ValueError):
        qat.wait_for_property_value(button_def, 'text', '2', timeout=500, check=True)
    with pytest.raises(TimeoutError):
        qat.wait_for_property_value({'objectName': 'invalid'}, 'text', '2', timeout=0, check=True)

    # Custom comparator
    type_thread = threading.Thread(target=type_in_text)
    type_thread.start()

    result = qat.wait_for_property_value(
        text_field_def,
        'text',
        10,
        comparator=lambda current, expected: len(current) == expected,
        check=True)

    type_thread.join()
    assert result


def test_property_change():
    """
    Verify that we can wait for a property to change its value
    """
    button_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    text_field_def = {
        'objectName': 'editableText',
        'type': 'TextEdit'
    }

    def click_on_button():
        time.sleep(0.5)
        qat.mouse_click(button_def)

    def type_in_text():
        time.sleep(0.5)
        qat.type_in(text_field_def, "1234657890")

    click_thread = threading.Thread(target=click_on_button)
    click_thread.start()

    result = qat.wait_for_property_change(button_def, 'text', '0')
    click_thread.join()
    assert result

    # Negative test
    assert not qat.wait_for_property_change(button_def, 'text', '1', timeout=500)

    # Custom comparator
    type_thread = threading.Thread(target=type_in_text)
    type_thread.start()

    result = qat.wait_for_property_change(
        text_field_def,
        'text',
        0,
        comparator=lambda current, expected: len(current) == expected,
        check=True)

    type_thread.join()
    assert result
