# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Definitions of all objects used by the tests
"""

main_window = {
    'objectName': 'mainWindow',
    'type': 'ApplicationWindow'
}

button_grid = {
    'container': main_window,
    'objectName': "buttonGrid",
    'type': "GridLayout"
}

quit_button = {
    'container': main_window,
    'objectName': "quitButton",
    'type': "Button"
}

counter_button = {
    'container': main_window,
    'objectName': "counterButton",
    'type': "Button"
}

color_button = {
    'container': button_grid,
    'objectName': "colorButton",
    'type': "ColorButton"
}

# BDD dictionaries
windows = {
    'Main': main_window
}

buttons = {
   'Colored': color_button,
   'Counter': counter_button,
   'Quit': quit_button
}

# Main dict (to be moved to DynamicNames)
objects = {}
objects['window'] = windows
objects['button'] = buttons
