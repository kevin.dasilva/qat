# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
When steps implementation
"""

# pylint: disable = no-name-in-module
# pylint: disable = missing-function-docstring
# pylint: disable = function-redefined
# pylint: disable = redefined-builtin
# pylint: disable = wildcard-import

from copy import deepcopy

from behave import When

from features.scripts import object_dictionary
from features.scripts.global_functions import *

import qat


@When("Clicking on the {name} {type:w}")
def step(context, name: str, type):
    qat.get_state().current_report.log(context.userData['current_step'])
    name = strip_object_name(name)
    object = qat.wait_for_object(object_dictionary.objects[type.lower()][name])
    qat.mouse_click(object)


@When("Clicking on each {name} {type:w} with the {button:w} button")
def step(context, name: str, type, button):
    qat.get_state().current_report.log(context.userData['current_step'])
    name = strip_object_name(name)
    generic_def = object_dictionary.objects[type.lower()][name]
    button = button.lower()
    if button == "left":
        button = qat.Button.LEFT
    elif button == "right":
        button = qat.Button.RIGHT
    elif button == "middle":
        button = qat.Button.MIDDLE
    # Iterate over the 25 buttons
    for i in range(25):
        # time.sleep(0.01)
        button_def = deepcopy(generic_def)
        button_def['objectName'] += str(i)
        object = qat.wait_for_object(button_def)
        qat.mouse_click(object, button=button)
