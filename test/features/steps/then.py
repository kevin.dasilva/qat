# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Then steps implementation
"""

# pylint: disable = no-name-in-module
# pylint: disable = missing-function-docstring
# pylint: disable = function-redefined
# pylint: disable = redefined-builtin

from copy import deepcopy

from features.scripts import object_dictionary
from features.scripts.global_functions import strip_object_name
from behave import Then

import qat
from qat import report


def get_color(color: str) -> str:
    if color == 'white':
        color = '#ffffffff'
    elif color == 'black':
        color = '#ff000000'
    elif color == 'green':
        color = '#ff008000'
    elif color == 'red':
        color = '#ffff0000'
    elif color == 'blue':
        color = '#ff0000ff'
    return color


@Then("the {property} of {name} {type:w} is {value}")
def step(context, property, name, type, value):
    name = strip_object_name(name)
    object_def = object_dictionary.objects[type.lower()][name]
    object = qat.wait_for_object_exists(object_def)
    current_value = getattr(object, property)
    report.verify(
        current_value == value,
        context.userData['current_step'],
        f"Property {property} is {current_value}"
    )
    assert current_value == value


@Then("the application exits with code {code:d}")
def step(context, code):
    exit_code = context.userData['appCtxt'].get_exit_code()
    report.verify(
        exit_code == code,
        context.userData['current_step'],
        f"Application returned exit code {exit_code}"
    )
    assert exit_code == code


@Then("the {name} {type:w} is colored in {color:w}")
def step(context, name, type, color):
    name = strip_object_name(name)
    object_def = object_dictionary.objects[type.lower()][name]
    object = qat.wait_for_object_exists(object_def)

    color = get_color(color)
    object_color = object.color.name
    report.verify(
        object_color == color,
        context.userData['current_step'],
        f"{name} {type} has color: {object_color}"
    )
    assert object_color == color


@Then("each {name} {type:w} is colored in {color:w}")
def step(context, name, type, color):
    generic_def = object_dictionary.objects[type.lower()][name]
    # Iterate over the 25 buttons
    for i in range(25):
        button_def = deepcopy(generic_def)
        button_def['objectName'] += str(i)

        button = qat.wait_for_object_exists(button_def)
        button_color = button.color.name
        color = get_color(color)
        report.verify(
            button_color == color,
            context.userData['current_step'],
            f"{name} {type} has color: {button_color}"
        )
        assert button_color == color
