@fixture.app.autoclose
Feature: Example feature

   Just a simple example

   Background: 
      Given current test name is Simple example 1

   Scenario: Button scenario
      Given the QmlApp application is running
         When Clicking on the Counter button
            Then the text of the Counter button is 1
         When Clicking on the Quit button
            Then the application exits with code 0

   Scenario Outline: Colored buttons
      Given the QmlApp application is running
         When Clicking on each Colored button with the <button> button
            Then each Colored button is colored in <color>
      Examples:
         | button | color |
         |  left  | green |
         |  right |  red  |
         | middle | black |
