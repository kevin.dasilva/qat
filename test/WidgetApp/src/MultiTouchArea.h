// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <ColorRectangle.h>

#include <QColor>
#include <QFrame>
#include <QTouchEvent>
#include <QWidget>

#include <array>
#include <map>

/// A mock class to make MultiTouch testable in the same way as QML implementation
class TouchPoint : public QObject
{
   Q_OBJECT
   Q_PROPERTY(
      bool pressed
      MEMBER isPressed
   )
public:
   /// Constructor 
   /// \param parent Qt parent object
   explicit TouchPoint(QWidget* parent) : QObject(parent){};

   /// Flag indicating whether this touch point is pressed or not
   bool isPressed{false};
};

namespace MultiTouch
{
/// Holds the colors for each touch point
inline const std::map<std::pair<int, bool>, QColor> COLOR_MAP =
{
   {{1, false}, QColor("#008000")},
   {{1, true}, Qt::blue},
   {{2, false}, Qt::red},
   {{2, true}, Qt::yellow},
   {{3, false}, Qt::white},
   {{3, true}, Qt::black}
};
}

/// \brief This class implements a touch area similar to the one in the QML test app
class MultiTouchArea : public QFrame
{
   Q_OBJECT
public:
   /// Constructor
   /// \param[in] parent The parent QWidget
   explicit MultiTouchArea(QWidget* parent) : QFrame(parent) 
   {
      setFrameShape(QFrame::Box);
      setAttribute(Qt::WA_AcceptTouchEvents);
      // Configure touch points
      mPoint1 = new TouchPoint(this);
      mPoint1->setObjectName("point1");

      // Add movable rectangles
      mRectangles[0] = new ColorRectangle(this);
      mRectangles[0]->setObjectName("touchRectangle1");
      mRectangles[0]->setFixedSize(30, 30);
      mRectangles[0]->move(-15, -15);
      mRectangles[0]->SetColor(QColor("#008000"));

      mRectangles[1] = new ColorRectangle(this);
      mRectangles[1]->setObjectName("touchRectangle2");
      mRectangles[1]->setFixedSize(30, 30);
      mRectangles[1]->move(-15, -15);
      mRectangles[1]->SetColor(QColor("#ff0000"));

      mRectangles[2] = new ColorRectangle(this);
      mRectangles[2]->setObjectName("touchRectangle3");
      mRectangles[2]->setFixedSize(30, 30);
      mRectangles[2]->move(-15, -15);
      mRectangles[2]->SetColor(QColor("#ffffff"));
   }

   /// \copydoc QWidget::event
   bool event(QEvent* e) override
   {
      switch(e->type())
      {
         case QEvent::TouchBegin:
         case QEvent::TouchUpdate:
         case QEvent::TouchEnd:
         {
            const auto* touchEvent = static_cast<QTouchEvent*>(e);
            #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
               const auto points = touchEvent->touchPoints();
            #else
               const auto points = touchEvent->points();
            #endif
            if (points.isEmpty() || points.size() > static_cast<int>(mRectangles.size()))
            {
               return false;
            }
            bool isReleaseEvent = false;
            for (auto i = 0; i < points.size(); ++i)
            {
               const auto& point = points[i];
               #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
                  const auto position = point.pos().toPoint();
                  const bool isPressed = point.state() != Qt::TouchPointReleased;
               #else
                  const auto position = point.position().toPoint();
                  const bool isPressed = point.state() != QEventPoint::State::Released;
               #endif
               isReleaseEvent = isReleaseEvent || !isPressed;

               mRectangles[i]->move(position - QPoint(15,15));
               mPoint1->isPressed = e->type() != QEvent::TouchEnd;
               mRectangles[i]->SetColor(MultiTouch::COLOR_MAP.at(std::make_pair(i + 1, isPressed)));
            }
            if (isReleaseEvent && points.size() < static_cast<int>(mRectangles.size()))
            {
               for (auto i = points.size(); i < static_cast<int>(mRectangles.size()); ++i)
               {
                  mRectangles[i]->SetColor(MultiTouch::COLOR_MAP.at(std::make_pair(i + 1, false)));
               }
            }
            return true;
         }
         case QEvent::TouchCancel:
         {
            for (auto i = 0u; i < mRectangles.size(); ++i)
            {
               mRectangles[i]->SetColor(MultiTouch::COLOR_MAP.at(std::make_pair(i + 1, false)));
            }
            return true;
         }
         default:
            return QWidget::event(e);
      }
   }

private:

   /// First touch point
   TouchPoint* mPoint1;

   /// Child rectangles manipulated with the touch points
   std::array<ColorRectangle*, 3> mRectangles;
};