// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <Button.h>

#include <QString>

class ModifierButton : public Button
{
public:
   /// Constructor
   /// \param parent Parent widget
   explicit ModifierButton(QWidget* parent) : Button(parent) {}

   /// Overloading mouse press event to handle modifiers
   /// \param e Mouse event to handle
   void mousePressEvent(QMouseEvent* e) override
   {
      QString text = "";
      if (e->modifiers() & Qt::ControlModifier)
      {
         text.append("CTRL ");
      }
      if (e->modifiers() & Qt::ShiftModifier)
      {
         text.append("SHIFT ");
      }
      if (e->modifiers() & Qt::AltModifier)
      {
         text.append("ALT ");
      }

      this->setText(text);
   }
};