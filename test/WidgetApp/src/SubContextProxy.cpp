// (c) Copyright 2023, Qat’s Authors

#include <SubContextProxy.h>

SubContextProxy::SubContextProxy(QObject* parent, int value) :
   QObject(parent),
   mValue{std::move(value)}
{
}

void SubContextProxy::Increment()
{
   mValue++;
   emit valueChanged();
}

void SubContextProxy::Decrement()
{
   mValue--;
   emit valueChanged();
}
