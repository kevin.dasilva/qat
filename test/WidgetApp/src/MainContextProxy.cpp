// (c) Copyright 2023, Qat’s Authors

#include <MainContextProxy.h>
#include <SubContextProxy.h>

#include <QVariant>
#include <QWidget>

#if __APPLE__
#include <TargetConditionals.h>

extern "C" void populateCocoaWindow(unsigned long handle);
#endif

MainContextProxy::MainContextProxy(QObject* parent) :
   QObject(parent)
{
   mSubProxy = new SubContextProxy(this, 0);
   qRegisterMetaType<MainContextProxy*>("MainContextProxy*");
   qRegisterMetaType<SubContextProxy*>("SubContextProxy*");
}

double MainContextProxy::Add(double a, double b) const
{
   return a + b;
}

double MainContextProxy::Add(double a, double b, double c) const
{
   return a + b + c;
}

QVariant MainContextProxy::GetColor() const
{
   return mColor;
}

void MainContextProxy::SetColor(const QColor& color)
{
   mColor = color;
}

void MainContextProxy::ChangeSubProxy()
{
   if (mSubProxy)
   {
      mSubProxy->deleteLater();
   }
   mSubProxy = new SubContextProxy(this, 0);
   emit subProxyChanged();
}

QObject* MainContextProxy::GetSubProxy()
{
   return mSubProxy;
}


void MainContextProxy::SetStringList(const QStringList& list)
{
   mStringList = list;
   emit stringListChanged();
}

#if TARGET_OS_MAC
   void MainContextProxy::OpenCocoaWindow() const
   {
      auto window = new QWidget(nullptr);
      window->setWindowTitle("Cocoa window");
      window->show();
      const auto handle = window->winId();
      populateCocoaWindow(handle);
   }
#endif
