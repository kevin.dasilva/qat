// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QLineEdit>
#include <QListWidget>
#include <QSlider>
#include <QTextEdit>
#include <QTreeView>
#include <QWidget>

/// @brief This class is used to provide QML-like name to QLineEdit
class Text : public QLineEdit
{
   Q_OBJECT
public:
   /// Constructor
   /// \param[in] parent The parent QWidget
   explicit Text(QWidget* parent) : QLineEdit(parent){}
};

/// @brief This class is used to provide QML-like name to QTextEdit
class TextEdit : public QTextEdit
{
   Q_OBJECT
   Q_PROPERTY(
      QString text
      READ GetText
      WRITE SetText
   )
public:
   /// Constructor
   /// \param[in] parent The parent QWidget
   explicit TextEdit(QWidget* parent) : QTextEdit(parent){}

   /// Return the current text
   /// \return the current text
   QString GetText() const
   {
      return toPlainText();
   }

   /// Set the current text
   /// \param[in] text the text to display
   void SetText(const QString& text)
   {
      setPlainText(text);
   }
};

/// @brief This class is used to provide QML-like name to QSlider
class Slider : public QSlider
{
   Q_OBJECT
public:
   /// Constructor
   /// \param[in] parent The QWidget parent
   explicit Slider(QWidget* parent) : QSlider(parent){}
};

/// @brief This class is used to provide QML-like name to QListView
class ListView : public QListView
{
   Q_OBJECT
public:
   /// Constructor
   /// \param[in] parent The QWidget parent
   explicit ListView(QWidget* parent) : QListView(parent){}
};

/// @brief This class is used to provide QML-like name to QTreeView
class TreeView : public QTreeView
{
   Q_OBJECT
public:
   /// Constructor
   /// \param[in] parent The QWidget parent
   explicit TreeView(QWidget* parent) : QTreeView(parent){}
};

/// @brief This class is used to provide QML-like name to Window
class Window : public QWidget
{
public:
   /// Default constructor
   Window() : QWidget()
   {
      setWindowFlags(Qt::Window);
   }
};