// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <ColorRectangle.h>

#include <QMouseEvent>

namespace
{
/// Return the local position of the given mouse event
/// \param[in] mouseEvent The mouse event
/// \return The local position
QPoint GetLocalPosition(QMouseEvent* mouseEvent)
{   
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      return mouseEvent->localPos().toPoint();
   #else
      return mouseEvent->position().toPoint();
   #endif
}

/// Return the global position of the given mouse event
/// \param[in] mouseEvent The mouse event
/// \return The global position
QPoint GetGlobalPosition(QMouseEvent* mouseEvent)
{   
   #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
      return mouseEvent->globalPos();
   #else
      return mouseEvent->globalPosition().toPoint();
   #endif
}
} // anonymous namespace

class ColorButton : public ColorRectangle
{
   Q_OBJECT

public:
   /// Constructor
   /// \param[in] parent Parent QWidget
   explicit ColorButton(QWidget* parent) : ColorRectangle(parent) 
   {
      SetColor(Qt::white);
   }

   /// Override mouse press event
   /// \param[in] e Mouse event
   void mousePressEvent(QMouseEvent* e) override
   {
      switch(e->button())
      {
         case Qt::LeftButton:
            break;
         case Qt::RightButton:
            break;
         case Qt::MiddleButton:
            break;
         default:
            e->ignore();
            return;
      }
      QMouseEvent modifiedEvent(
         e->type(),
         GetLocalPosition(e),
         GetGlobalPosition(e),
         Qt::LeftButton,
         Qt::MouseButtons().setFlag(Qt::LeftButton),
         e->modifiers()
      );
      Button::mousePressEvent(&modifiedEvent);
   }

   /// Override mouse release event
   /// \param[in] e Mouse event
   void mouseReleaseEvent(QMouseEvent* e) override
   {
      QMouseEvent modifiedEvent(
         e->type(),
         GetLocalPosition(e),
         GetGlobalPosition(e),
         Qt::LeftButton,
         Qt::MouseButtons().setFlag(Qt::LeftButton),
         e->modifiers()
      );
      Button::mouseReleaseEvent(&modifiedEvent);
      if (!modifiedEvent.isAccepted())
      {
         return;
      }

      if (mIsDoubleClicking)
      {
         mIsDoubleClicking = false;
         switch(e->button())
         {
            case Qt::LeftButton:
               SetColor(Qt::blue);
               break;
            case Qt::RightButton:
               SetColor(Qt::yellow);
               break;
            case Qt::MiddleButton:
               SetColor("#808080");
               break;
            default:
               e->ignore();
               return;
         }
      }
      else
      {
         switch(e->button())
         {
            case Qt::LeftButton:
               SetColor("#008000");
               break;
            case Qt::RightButton:
               SetColor(Qt::red);
               break;
            case Qt::MiddleButton:
               SetColor(Qt::black);
               break;
            default:
               e->ignore();
               return;
         }
      }
   }

   /// Override double click event
   /// \param[in] e Mouse event
   void mouseDoubleClickEvent(QMouseEvent* e) override
   {
      mIsDoubleClicking = true;
      QMouseEvent modifiedEvent(
         e->type(),
         GetLocalPosition(e),
         GetGlobalPosition(e),
         Qt::LeftButton,
         Qt::MouseButtons().setFlag(Qt::LeftButton),
         e->modifiers()
      );
      Button::mouseDoubleClickEvent(&modifiedEvent);
      if (!modifiedEvent.isAccepted())
      {
         return;
      }
   }

private:
   /// Flag indicating that a double-click is in progress
   bool mIsDoubleClicking{false};
};