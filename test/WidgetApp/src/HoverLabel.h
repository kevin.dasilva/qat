// (c) Copyright 2024, Qats Authors
#pragma once

#include <QEvent>
#include <QHoverEvent>
#include <QLabel>
#include <QString>

/// \brief This class is used to test hover events
class HoverLabel : public QLabel
{
   Q_OBJECT
public:
   /// Constructor
   /// \param[in] parent The parent QWidget
   explicit HoverLabel(QWidget* parent) : QLabel(parent) 
   {
      this->setAttribute(Qt::WA_Hover);
   }

   /// \copydoc QObject::event
   bool event(QEvent* e) override
   {
      if (e->type() == QEvent::HoverEnter ||
          e->type() == QEvent::HoverMove ||
          e->type() == QEvent::HoverLeave)
      {
         const auto* hoverEvent = static_cast<QHoverEvent*>(e);
         #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            const auto pos = hoverEvent->pos();
         #else
            const auto pos = hoverEvent->position().toPoint();
         #endif
         this->setText(QString("%1 X %2").arg(pos.x()).arg(pos.y()));
         return true;
      }
      return QWidget::event(e);
   }
};