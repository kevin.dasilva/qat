// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QWidget>

#include <memory>
#include <string>

class MainContextProxy;
class QContextMenuEvent;
class QMenu;

class ApplicationWindow final : public QWidget
{
   Q_OBJECT
   Q_PROPERTY(
      MainContextProxy* mainProxy
      MEMBER mMainProxy
   )
public:

   /// Constructor
   ApplicationWindow();

   /// Destructor
   ~ApplicationWindow() = default;

private:

   /// Pointer to the main context proxy
   MainContextProxy* mMainProxy;
};
