// (c) Copyright 2023, Qat’s Authors

#include <MainWindow.h>
#include <SecondaryWindow.h>

#include <Button.h>
#include <ColorButton.h>
#include <CustomWidgets.h>
#include <HoverLabel.h>
#include <MainContextProxy.h>
#include <MessagePopup.h>
#include <ModifierButton.h>
#include <MultiTouchArea.h>
#include <PinchArea.h>

#include <QAction>
#include <QApplication>
#include <QFileDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QIdentityProxyModel>
#include <QItemSelectionModel>
#include <QMenu>
#include <QObject>
#include <QMouseEvent>
#include <QPushButton>
#include <QStandardItemModel>
#include <QStandardPaths>
#include <QString>
#include <QTimer>
#include <QToolButton>
#include <QVBoxLayout>

#include <iostream>

namespace
{
class ListColorProxyModel : public QIdentityProxyModel
{
public:
   void SetSelection(const QModelIndex &index)
   {
      mSelectedIndex = index;
   }

   QVariant data(const QModelIndex &index, int role) const override
   {
      if (role == Qt::ForegroundRole && index == mSelectedIndex)
      {
         return QBrush(QColor("red"));
      }

      return QIdentityProxyModel::data(index, role);
   }
private:
   QModelIndex mSelectedIndex;
};
}

ApplicationWindow::ApplicationWindow() : QWidget()
{
   setWindowFlags(Qt::Window);
   setFixedSize(1400, 800);
   setObjectName("mainWindow");

   mMainProxy = new MainContextProxy(this);

   // Columns
   auto leftColumn = new QVBoxLayout();
   leftColumn->setObjectName("leftColumn");
   auto middleColumn = new QVBoxLayout();
   middleColumn->setObjectName("middleColumn");
   auto rightColumn = new QVBoxLayout();
   rightColumn->setObjectName("rightColumn");

   // Buttons

   // Checkable button
   auto checkButton = new QToolButton(this);
   checkButton->setObjectName("checkableButton");
   checkButton->setCheckable(true);
   checkButton->setText("unchecked");
   QObject::connect(checkButton, &QToolButton::toggled, 
      [checkButton](bool checked){
         checkButton->setText(checked ? "checked" : "unchecked");         
      });
   leftColumn->addWidget(checkButton);

   // Counter button
   static int counter = 0;
   auto counterButton = new Button(this);
   counterButton->setObjectName("counterButton");
   counterButton->SetCustomText("0");
   counterButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
   QObject::connect(counterButton, &Button::clicked, 
      [counterButton](){
         const auto text = std::to_string(++counter);
         counterButton->SetCustomText(QString::fromStdString(text));
      });
   auto shortcutAction = new QAction(this);
   shortcutAction->setShortcuts({
      QKeySequence("Shift+-"),
      QKeySequence("Ctrl+Z")
   });
   counterButton->addAction(shortcutAction);
   counterButton->setStyleSheet("QToolButton::menu-indicator {image: none; }");
   QObject::connect(
      shortcutAction, 
      &QAction::triggered,
      [counterButton]()
      {
         const auto text = std::to_string(--counter);
         counterButton->SetCustomText(QString::fromStdString(text));
      });
   leftColumn->addWidget(counterButton);

   // Modifier button
   auto modifierButton = new ModifierButton(this);
   modifierButton->setObjectName("modifierButton");
   modifierButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
   leftColumn->addWidget(modifierButton);

   // Popup button
   auto openButton = new Button(this);
   openButton->setObjectName("openButton");
   openButton->setText("Open popup");
   openButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
   openButton->setShortcut(QKeySequence("Ctrl+O"));
   auto popup = new MessagePopup(this);
   popup->setObjectName("messagePopup");
   QObject::connect(openButton, &Button::clicked, 
      [popup]()
      {
         popup->show();
      });
   leftColumn->addWidget(openButton);

   // Window button
   auto openWindow = new Button(this);
   openWindow->setObjectName("openWindow");
   openWindow->setText("Open window");
   openWindow->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

   auto secondWindow = new SecondaryWindow();
   secondWindow->setFixedSize(800, 600);
   secondWindow->setObjectName("secondWindow");

   QObject::connect(openWindow, &Button::clicked, 
      [secondWindow]()
      {
         secondWindow->show();
      });
   leftColumn->addWidget(openWindow);

   // Open File button
   auto openFile = new Button(this);
   openFile->setObjectName("openFile");
   openFile->setText("Open File");
   openFile->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
   QObject::connect(openFile, &Button::clicked, 
      [this, openFile]()
      {
         QTimer::singleShot(0, [this, openFile]()
         {
            const auto dir = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).front();
            const auto fileName = QFileDialog::getOpenFileName(this, "Open", dir);
            openFile->setText(fileName);
         });
      });
   leftColumn->addWidget(openFile);

   // Quit button
   auto quitButton = new Button(this);
   quitButton->setObjectName("quitButton");
   quitButton->setText("Quit");
   quitButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
   QObject::connect(quitButton, &QToolButton::clicked, 
      [this]()
      {
         this->close();
      });
   leftColumn->addWidget(quitButton);

   // Text boxes
   auto textNoName = new Text(this);
   textNoName->setText("unnamed");
   leftColumn->addWidget(textNoName);

   auto textNoIdNoName = new Text(this);
   textNoIdNoName->setText("no_name_or_id");
   leftColumn->addWidget(textNoIdNoName);

   // Slider
   auto slider = new Slider(this);
   slider->setObjectName("slider");
   slider->setOrientation(Qt::Horizontal);
   slider->setMinimum(0);
   slider->setMaximum(10);
   slider->setSingleStep(1);
   slider->setPageStep(1);
   slider->setValue(5);
   leftColumn->addWidget(slider);

   // Button grid
   auto buttonGrid = new QWidget(this);
   auto buttonGridLayout = new QGridLayout();
   buttonGridLayout->setObjectName("buttonGrid");
   buttonGridLayout->setHorizontalSpacing(0);
   buttonGridLayout->setVerticalSpacing(0);
   buttonGrid->setLayout(buttonGridLayout);
   
   for (int r = 0; r < 5; ++r)
   {
      for (int c = 0; c < 5; ++c)
      {
         auto button = new ColorButton(this);
         const auto name = "colorButton" + std::to_string(5 * r + c);
         button->setObjectName(QString::fromStdString(name));
         button->setFixedSize(50, 50);
         buttonGridLayout->addWidget(button, r, c);
      }
   }
   leftColumn->addWidget(buttonGrid);

   // List
   QStringList listContent;
   for (int i = 0; i < 50; ++i)
   {
      QString item("item ");
      item += QString::number(i);
      listContent << item;
   }
   auto listModel = new QStringListModel(this);
   listModel->setStringList(listContent);

   auto proxy = new ListColorProxyModel();
   proxy->setSourceModel(listModel);

   auto list = new ListView(this);
   list->setObjectName("listView");
   list->setModel(proxy);
   list->setSelectionRectVisible(false);
   list->setSelectionMode(QAbstractItemView::SelectionMode::NoSelection);

   QObject::connect(list->selectionModel(), &QItemSelectionModel::currentChanged, 
      [proxy](const QModelIndex &current, const QModelIndex &){
         proxy->SetSelection(current);
      });

   leftColumn->addWidget(list);

   // Text inputs
   auto editableText = new TextEdit(this);
   editableText->setObjectName("editableText");
   leftColumn->addWidget(editableText);

   auto editableText2 = new TextEdit(this);
   editableText2->setObjectName("editableText2");
   leftColumn->addWidget(editableText2);

   // TODO: Combobox

   // Middle column
   auto hoverLabel = new HoverLabel(this);
   hoverLabel->setObjectName("hoverLabel");
   hoverLabel->setFixedSize(300, 50);
   hoverLabel->setAlignment(Qt::AlignCenter);
   middleColumn->addWidget(hoverLabel);
   
   auto pinchArea = new PinchArea(this);
   pinchArea->setObjectName("pinchArea");
   pinchArea->setFixedSize(300, 300);

   middleColumn->addWidget(pinchArea);

   // Right column
   auto treeView = new TreeView(this);
   treeView->setObjectName("treeView");
   auto treeModel = new QStandardItemModel(this);
   treeModel->setColumnCount(3);
   auto* treeRoot = treeModel->invisibleRootItem();
   for (int i = 0; i < 5; ++i)
   {
      auto rootItem = new QStandardItem(QString("Root item %0").arg(i));
      treeRoot->appendRow(rootItem);
      rootItem->setCheckable(true);
      rootItem->setForeground(QBrush(QColor("darkBlue")));
      for (int c = 0; c < 4; ++c)
      {
         const auto index = 4 * i + c;
         QList<QStandardItem*> row;
         row << new QStandardItem(QString("Child item %0").arg(index));
         row << new QStandardItem(QString("Name %0").arg(index));
         row << new QStandardItem(QString("Value %0").arg(index));

         rootItem->appendRow(row);
      }
   }
   treeView->setModel(treeModel);

   // Expand all
   const auto indexes = treeModel->match(treeModel->index(0,0), Qt::DisplayRole, "*", -1, Qt::MatchWildcard|Qt::MatchRecursive);
   for(auto index : indexes)
   {
      treeView->expand(index);
   }
   rightColumn->addWidget(treeView);

   // Touch area
   auto* touchArea = new MultiTouchArea(this);
   touchArea->setObjectName("multiTouchArea");
   touchArea->setFixedSize(500, 300);
   rightColumn->addWidget(touchArea);

   leftColumn->addStretch();
   
   // Main window layout
   auto mainLayout = new QHBoxLayout(this);
   mainLayout->setObjectName("mainRowLayout");
   mainLayout->addLayout(leftColumn);
   mainLayout->addLayout(middleColumn);
   mainLayout->addLayout(rightColumn);
   mainLayout->addStretch();
   this->setLayout(mainLayout);
}

