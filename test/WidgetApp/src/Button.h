// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QToolButton>

/// @brief This class is used to provide QML-like name to QPushButton
class Button : public QToolButton
{
   Q_OBJECT
   Q_PROPERTY(
      QString customText
      MEMBER mCustomText
      NOTIFY customTextChanged
   )
public:
   /// Constructor
   /// \param[in] parent The parent QWidget
   explicit Button(QWidget* parent) : QToolButton(parent) {}

   /// Change button text and update custom property
   /// \note Used for testing purposes: tests need a signal to use connections and bindings
   /// \param[in] text the new text to display
   void SetCustomText(const QString& text)
   {
      mCustomText = text;
      QToolButton::setText(text);
      emit customTextChanged();
   }

signals:
   /// Signals for Qt properties
   /// @{
   void customTextChanged();
   /// @}

private:
   QString mCustomText;
};