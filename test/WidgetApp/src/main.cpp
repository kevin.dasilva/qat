// (c) Copyright 2023, Qat’s Authors

#include <MainWindow.h>

#include <QApplication>

#include <iostream>

int main(int argc, char **argv) 
{
   if (argc > 2)
   {
      std::cerr << "Too many arguments" << std::endl;
      return 2;
   }
   if (argc > 1)
   {
      std::string argument {argv[1]};
      if (argument == "-h" || argument == "--help")
      {
         std::cout << "QML-based testing application" << std::endl;
         return 0;
      }
      else
      {
         std::cerr << "Unrecognized argument" << std::endl;
         return 2;
      }
   }
   QApplication app(argc, argv);
   ApplicationWindow window;

   window.show();

   return QCoreApplication::exec();
}
