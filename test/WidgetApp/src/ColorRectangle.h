// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <Button.h>


class ColorRectangle : public Button
{
   Q_OBJECT
   Q_PROPERTY(
      QColor color
      MEMBER mColor
      WRITE SetColor
      NOTIFY colorChanged)

public:
   /// Constructor
   /// \param[in] parent Parent QWidget
   explicit ColorRectangle(QWidget* parent) : Button(parent) 
   {
      SetColor(Qt::white);
   }

   /// Change the background color
   /// \param[in] color The color to apply
   void SetColor(const QColor& color)
   {
      if(color.isValid()) {
         QString qss = QString("background-color: %1; border:none").arg(color.name());
         this->setStyleSheet(qss);
         mColor = color;
         emit colorChanged();
      }
   }

signals:
   /// Emitted when color changes
   void colorChanged();

protected:
   /// Color of the button
   QColor mColor;
};