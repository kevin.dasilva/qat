// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <ColorRectangle.h>

#include <QColor>
#include <QGestureEvent>
#include <QGraphicsItem>
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsWidget>
#include <QPinchGesture>
#include <QWidget>


/// \brief This class implements a pinch area similar to the one in the QML test app
class PinchArea : public QWidget
{
   Q_OBJECT
public:
   /// Constructor
   /// \param[in] parent The parent QWidget
   explicit PinchArea(QWidget* parent) : QWidget(parent) 
   {
      setMinimumSize(300, 300);
      grabGesture(Qt::GestureType::PinchGesture);

      // Use a QGraphics scene too allow rotating widgets
      auto scene = new QGraphicsScene(this);
      scene->setSceneRect(this->rect());
      auto view = new QGraphicsView(this);
      view->setScene(scene);
      QBrush greenBrush(Qt::green);
      QPen outlinePen(Qt::black);
      outlinePen.setWidth(2);

      // Underlying rectangle to be manipulated
      auto gestureRectangle = new ColorRectangle(nullptr);
      gestureRectangle->setFixedSize(100, 100);
      gestureRectangle->SetColor(Qt::green);

      // Create a proxy item managing the transforms
      mGestureItem = scene->addWidget(gestureRectangle);
      mGestureItem->setObjectName("gestureItem");
      mGestureItem->setPreferredSize(100, 100);
      // Parent is required so item can be found by Qat
      mGestureItem->setParent(this);
   }

   /// \copydoc QObject::event()
   bool event(QEvent* event) override
   {
      if (event->type() == QEvent::Gesture)
      {
         const auto* gestureEvent = static_cast<QGestureEvent*>(event);
         const auto* gesture = gestureEvent->gesture(Qt::PinchGesture);
         if (!gesture) return false;

         // Handle pinch gestures
         const auto* pinchGesture = static_cast<const QPinchGesture*>(gesture);
         const auto flags = pinchGesture->changeFlags();
         if (gesture->state() == Qt::GestureFinished) return true;
         if (flags & QPinchGesture::CenterPointChanged)
         {
            const auto move = pinchGesture->centerPoint() - pinchGesture->lastCenterPoint();
            mGestureItem->setPos(mGestureItem->pos() + move);
         }
         if (flags & QPinchGesture::RotationAngleChanged)
         {
            mGestureItem->setTransformOriginPoint(mGestureItem->boundingRect().center());
            const auto angle = pinchGesture->rotationAngle() - pinchGesture->lastRotationAngle();
            mGestureItem->setRotation(mGestureItem->rotation() + angle);
         }
         if (flags & QPinchGesture::ScaleFactorChanged)
         {
            mGestureItem->setTransformOriginPoint(mGestureItem->boundingRect().center());
            const auto scale = pinchGesture->scaleFactor();
            mGestureItem->setScale(mGestureItem->scale() * scale);
         }
         return true;
      }
      return QWidget::event(event);
   }

private:

   /// Child rectangle manipulated with the gestures
   QGraphicsProxyWidget* mGestureItem;
};