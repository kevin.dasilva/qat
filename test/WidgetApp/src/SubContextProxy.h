// (c) Copyright 2023, Qat’s Authors

#pragma once

#include <QObject>

#include <memory>

class SubContextProxy : public QObject
{
   Q_OBJECT
   Q_PROPERTY(
      int value
      MEMBER mValue
      NOTIFY valueChanged
   )

public:
   /// Constructor
   /// \param[in] parent The Qt parent
   /// \param[in] value The initial value
   SubContextProxy(QObject* parent, int value);

   /// Default destructor.
   ~SubContextProxy() override = default;

   /// Increment the underlying value
   Q_INVOKABLE void Increment();

   /// Decrement the underlying value
   Q_INVOKABLE void Decrement();

signals:
   /// Emitted when properties change
   /// @{
   void valueChanged();
   /// @}

private:
   /// Some integer
   int mValue{ 0 };

};
Q_DECLARE_METATYPE(SubContextProxy*)
