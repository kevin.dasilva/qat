// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <Button.h>
#include <CustomWidgets.h>

#include <QDialog>
#include <QLabel>
#include <QListView>
#include <QObject>
#include <QStringList>
#include <QStringListModel>
#include <QVBoxLayout>

class MessagePopup : public QDialog
{
   Q_OBJECT
public:
   /// Constructor
   /// \param[in] parent The QWidget parent
   explicit MessagePopup(QWidget* parent) : QDialog(parent) 
   {
      setFixedSize(700, 400);
      setModal(true);

      auto mainLayout = new QVBoxLayout(this);
      this->setLayout(mainLayout);

      auto title = new QLabel(this);
      title->setObjectName("title");
      title->setText("Some title");
      mainLayout->addWidget(title);

      auto message = new QLabel(this);
      message->setObjectName("message");
      message->setText("Some message");
      mainLayout->addWidget(message);

      auto list = new ListView(this);
      list->setObjectName("popupListView");

      auto listModel = new QStringListModel(list);
      QStringList content;
      for (int i = 0; i < 50; ++i)
      {
         content << "item " + QString::fromStdString(std::to_string(i));
      }
      listModel->setStringList(content);

      list->setModel(listModel);
      mainLayout->addWidget(list);

      auto closeButton = new Button(this);
      closeButton->setObjectName(this->objectName() + "CloseButton");
      closeButton->setText("Close");
      QObject::connect(closeButton, &Button::clicked, 
         [this](){
            this->close();
         });
      QObject::connect(this, &MessagePopup::objectNameChanged, 
         [this, closeButton](){
            closeButton->setObjectName(this->objectName() + "CloseButton");
         });
      mainLayout->addWidget(closeButton);
   }
};

