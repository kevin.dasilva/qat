// (c) Copyright 2024, Qat’s Authors

#include <SecondaryWindow.h>

#include <Button.h>

#include <QAction>
#include <QContextMenuEvent>
#include <QMenu>
#include <QMenuBar>
#include <QWidget>

#include <iostream>

SecondaryWindow::SecondaryWindow() : QMainWindow() 
{
   // Contents
   QWidget* mainWidget = new QWidget(this);
   mainWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

   setCentralWidget(mainWidget);

   auto closeButton = new Button(mainWidget);
   closeButton->setObjectName("closeWindowButton");
   closeButton->setText("Close");

   QObject::connect(closeButton, &Button::clicked, 
      [this]()
      {
         this->close();
      });

   // Actions
   mEnableAction = new QAction("&Enabled", this);
   mEnableAction->setCheckable(true);
   mEnableAction->setChecked(true);

   mVisibleAction = new QAction("&Visible", this);
   mVisibleAction->setObjectName("visibleAction");
   mVisibleAction->setCheckable(true);
   mVisibleAction->setChecked(true);

   // Main menu
   auto* menuToolBar = menuBar();
   auto* widgetMenu = menuToolBar->addMenu("Widgets");
   widgetMenu->setObjectName("widgetsMenu");
   widgetMenu->addAction("Add")->setEnabled(false);
   auto* buttonMenu = widgetMenu->addMenu("Close button");
   buttonMenu->setObjectName("closeButtonMainMenu");
   buttonMenu->addAction(mEnableAction);
   buttonMenu->addAction(mVisibleAction);

   // Context menu
   mContextMenu = new QMenu(this);
   mContextMenu->setObjectName("contextMenu");

   mContextMenu->addAction("Placeholder");
   mContextMenu->addSeparator();

   auto* subMenu = new QMenu(mContextMenu);
   subMenu->setTitle("Close button");
   subMenu->setObjectName("closeButtonMenu");
   mContextMenu->addMenu(subMenu);
   subMenu->addAction(mEnableAction);
   subMenu->addAction(mVisibleAction);

   connect(mEnableAction, &QAction::triggered, [closeButton]()
   {
      closeButton->setEnabled(!closeButton->isEnabled());
   });

   connect(mVisibleAction, &QAction::triggered, [closeButton]()
   {
      closeButton->setVisible(!closeButton->isVisible());
   });
}

void SecondaryWindow::contextMenuEvent(QContextMenuEvent* event)
{
   const auto pos = mapToGlobal(event->pos());
   mContextMenu->popup(pos);
}

