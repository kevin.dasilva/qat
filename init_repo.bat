mkdir build
cmake -G "Visual Studio 17 2022" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Release -S . -B build

set QAT_VERSION=0.0.dev1
python -m pip install qat -e .

@echo off

set PYTHONPATH=client
set QSG_RHI_BACKEND=opengl
set QT3D_RENDERER=opengl

set /p answer=Do you want to open VSCode? (y/N) 
if /I "%answer%" == "y" start code .

exit
